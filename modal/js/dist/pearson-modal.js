var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function (w, doc) {
  'use strict';
  // Setup our web component template

  var template = doc.createElement('template');

  // The innerHTML is our markup and CSS taken from pattern lab
  template.innerHTML = ' \n<style>\n\n@import url("https://fonts.googleapis.com/css?family=Open+Sans:400,600");pearson-modal{display:none}:host{\n  /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */}@-webkit-keyframes fadeIn{0%{opacity:0}to{opacity:.6}}@keyframes fadeIn{0%{opacity:0}to{opacity:.6}}@-webkit-keyframes fadeOut{0%{opacity:.6}to{opacity:0}}@keyframes fadeOut{0%{opacity:.6}to{opacity:0}}@-webkit-keyframes slideInDown{0%{-webkit-transform:translate3d(-50%,-100%,0);transform:translate3d(-50%,-100%,0);visibility:visible}to{-webkit-transform:translate3d(-50%,0,0);transform:translate3d(-50%,0,0)}}@keyframes slideInDown{0%{-webkit-transform:translate3d(-50%,-100%,0);transform:translate3d(-50%,-100%,0);visibility:visible}to{-webkit-transform:translate3d(-50%,0,0);transform:translate3d(-50%,0,0)}}@-webkit-keyframes slideOutDown{0%{opacity:1;-webkit-transform:translate3d(-50%,0,0);transform:translate3d(-50%,0,0)}to{opacity:0;visibility:hidden;-webkit-transform:translate3d(-50%,300%,0);transform:translate3d(-50%,300%,0)}}@keyframes slideOutDown{0%{opacity:1;-webkit-transform:translate3d(-50%,0,0);transform:translate3d(-50%,0,0)}to{opacity:0;visibility:hidden;-webkit-transform:translate3d(-50%,300%,0);transform:translate3d(-50%,300%,0)}}:host .animated{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-fill-mode:backwards;animation-fill-mode:backwards}:host .fadeIn{-webkit-animation-name:fadeIn;animation-name:fadeIn}:host .fadeOut{-webkit-animation-name:fadeOut;animation-name:fadeOut}:host .slideInDown{-webkit-animation-name:slideInDown;animation-name:slideInDown;-webkit-animation-duration:.7s;animation-duration:.7s;-webkit-transition-timing-function:ease-in;transition-timing-function:ease-in}:host .slideOutDown{-webkit-animation-name:slideOutDown;animation-name:slideOutDown;-webkit-animation-duration:.5s;animation-duration:.5s;-webkit-transition-timing-function:ease-out;transition-timing-function:ease-out}@media (-ms-high-contrast:none){:host .fadeIn,:host .fadeOut{-webkit-animation-name:fade;animation-name:fade;-webkit-animation-duration:.5s;animation-duration:.5s}}:host html{line-height:1.15;-webkit-text-size-adjust:100%}:host body{margin:0}:host main{display:block}:host h1{font-size:2em;margin:.67em 0}:host hr{-webkit-box-sizing:content-box;box-sizing:content-box;height:0;overflow:visible}:host pre{font-family:monospace,monospace;font-size:1em}:host a{background-color:transparent}:host abbr[title]{border-bottom:none;text-decoration:underline;-webkit-text-decoration:underline dotted;text-decoration:underline dotted}:host b,:host strong{font-weight:bolder}:host code,:host kbd,:host samp{font-family:monospace,monospace;font-size:1em}:host small{font-size:80%}:host sub,:host sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}:host sub{bottom:-.25em}:host sup{top:-.5em}:host img{border-style:none}:host button,:host input,:host optgroup,:host select,:host textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}:host button,:host input{overflow:visible}:host button,:host select{text-transform:none}:host [type=button],:host [type=reset],:host [type=submit],:host button{-webkit-appearance:button}:host [type=button]::-moz-focus-inner,:host [type=reset]::-moz-focus-inner,:host [type=submit]::-moz-focus-inner,:host button::-moz-focus-inner{border-style:none;padding:0}:host [type=button]:-moz-focusring,:host [type=reset]:-moz-focusring,:host [type=submit]:-moz-focusring,:host button:-moz-focusring{outline:1px dotted ButtonText}:host fieldset{padding:.35em .75em .625em}:host legend{-webkit-box-sizing:border-box;box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}:host progress{vertical-align:baseline}:host textarea{overflow:auto}:host [type=checkbox],:host [type=radio]{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0}:host [type=number]::-webkit-inner-spin-button,:host [type=number]::-webkit-outer-spin-button{height:auto}:host [type=search]{-webkit-appearance:textfield;outline-offset:-2px}:host [type=search]::-webkit-search-decoration{-webkit-appearance:none}:host ::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}:host details{display:block}:host summary{display:list-item}:host template{display:none}:host [hidden]{display:none}:host html{font-size:14px}:host *,:host html{-webkit-box-sizing:border-box;box-sizing:border-box}:host body{font-family:Open Sans,Arial,Helvetica,sans-serif}:host body,:host p{font-size:14px;line-height:1.5;font-weight:400}:host strong{font-weight:600}:host a{font-size:14px;color:#047a9c}:host a:hover{color:#03536a;text-decoration:none}:host a:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host button{cursor:pointer}:host li,:host ul{font-size:14px}:host svg{fill:#252525}:host svg[focusable=false]:focus{outline:none}:host input,:host textarea{font:inherit;letter-spacing:inherit;word-spacing:inherit}:host .no-border{border:0}:host .icon-18{width:18px;height:18px}:host .icon-24{width:24px;height:24px}:host .hidden{display:none!important}:host .gr-h1{font-size:24px;line-height:28px}:host .gr-h1,:host .gr-h2{font-weight:400;margin-top:0}:host .gr-h2{font-size:20px;line-height:26px}:host .gr-h3{font-size:18px;line-height:24px;font-weight:400;margin-top:0}:host .gr-label{font-size:12px;line-height:16px;color:#6a7070;display:block;margin-bottom:4px}:host .gr-meta{font-size:12px;line-height:12px;color:#6a7070}:host .gr-semi-bold{font-weight:600}:host .gr-font-large{font-size:16px;line-height:24px}:host .gr-font-normal{font-size:14px;line-height:20px}:host .gr-btn{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;border:1px solid #6a7070;background:transparent;color:#6a7070;min-width:152px;padding:8px 24px;font-size:16px;cursor:pointer;border-radius:22px;position:relative;margin:12px;line-height:1.15}:host .gr-btn:hover{color:#252525;border:1px solid #252525}:host .gr-btn:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host a.gr-btn{text-decoration:none}:host .gr-btn.primary{background-color:#047a9c;border:1px solid #047a9c;color:#fff}:host .gr-btn.primary:hover{color:#fff;background-color:#035f79}:host .gr-btn.attention{background-color:#ffb81c;border:1px solid #ffb81c;color:#252525}:host .gr-btn.attention:hover{background-color:#f7aa00}:host .gr-btn.small{min-width:128px;padding:7px 20px;font-size:14px}:host .gr-btn.small:focus:after{padding:18px 21px}:host .gr-btn.extra-large{min-width:152px;padding:10px 24px;font-size:16px}:host .gr-btn.extra-large:focus:after{padding:22px 21px;border-radius:24px}:host .gr-btn:disabled{background-color:#e9e9e9!important;color:#6a7070!important;border:0!important;cursor:default!important}:host .gr-btn.icon-btn-24{min-width:auto;padding:5px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-24:focus{border-radius:4px}:host .gr-btn.icon-btn-24:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-24:active svg{fill:#005a70}:host .gr-btn.icon-btn-24 svg{fill:#6a7070}:host .gr-btn.icon-btn-18{min-width:auto;padding:8px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-18:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-18:active svg{fill:#005a70}:host .gr-btn.icon-btn-18:focus{border-radius:4px}:host .gr-btn.icon-btn-18 svg{fill:#6a7070}:host .gr-btn.no-border{border:0}:host .gr-btn.no-border:hover{border:0}:host .gr-btn.no-border:focus:after{border-radius:5px;min-width:auto;left:auto;top:auto;padding:16px}:host .gr-modal-container .hide-overflow{overflow:hidden}:host .gr-modal-container #modalOverlay{width:100%;height:100%;z-index:2;background-color:#252525;opacity:.6;position:fixed;top:0;left:0;margin:0;padding:0}:host .gr-modal-container .modal{border:thin solid #6a7070;background-color:#fff;z-index:3;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);transform:translate(-50%,-50%);width:600px;max-width:100%;outline:none;border-radius:2px}:host .gr-modal-container .close{position:absolute;height:44px;width:44px;background:none;border:0;top:0;right:0;cursor:pointer}:host .gr-modal-container .close:focus{outline:1px solid #1977d4;outline-offset:2px}:host .gr-modal-container .modal h2.gr-h2{display:inline-block;margin:0;font-size:24px;padding-left:40px;padding-top:40px;padding-right:40px}:host .gr-modal-container .modal h2.gr-h2:focus{outline:1px solid #1977d4;outline-offset:2px}:host .gr-modal-container .modal-body{margin-bottom:28px;padding:0 40px}:host .gr-modal-container .actions{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:end;-webkit-justify-content:flex-end;-ms-flex-pack:end;justify-content:flex-end;padding-left:40px;padding-right:40px;padding-bottom:40px}:host .gr-modal-container .actions button{margin:0}:host .gr-modal-container .actions button:last-child{-webkit-box-ordinal-group:0;-webkit-order:-1;-ms-flex-order:-1;order:-1;margin-right:1em}@media (max-width:767px){:host .gr-modal-container .modal{width:440px}:host .gr-modal-container .modal .actions button{width:100%}:host .gr-modal-container .modal .actions{-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between}}@media (max-width:480px){:host .gr-modal-container .modal .actions button:last-child{margin:.5em 0}:host .gr-modal-container .modal .actions{-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column}:host .gr-modal-container .modal h2.gr-h2{font-size:20px}}:host .gr-modal-container #modalOverlay{background-color:rgba(37,37,37,.6);z-index:1200}:host #modal{z-index:1201}:host .title-flex{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-align-content:center;-ms-flex-line-pack:center;align-content:center;padding-right:40px}:host .title-flex button{margin-top:16px!important}:host #modal.scroll{overflow:hidden;height:-webkit-calc(100% - 100px);height:calc(100% - 100px)}@media (max-width:360px){:host #modal.scroll{overflow:scroll}}:host #modal.scroll .modal-body{margin-bottom:28px;padding:0 40px;overflow:auto;height:-webkit-calc(100% - 200px);height:calc(100% - 200px)}@media (max-width:360px){:host #modal.scroll .modal-body{height:auto}}:host #modal.fullwidth{top:0!important;width:100%!important;height:100%!important}\n</style>\n<div id="modalContainer" class="gr-modal-container">\n   <div id="modalOverlay" class="animated hidden" data-event="cancel"></div>\n   <div id="modal" class="modal animated hidden" role="dialog" aria-labelledby="dialog-heading" aria-describedby="dialog-description" aria-modal="true">\n      <div class="title-flex">\n         <h2 id="dialog-heading" class="gr-h2">Modal Dialog Title</h2>\n         <button id="closeButton" class="gr-btn icon-btn-24" aria-label="close dialog">\n            <svg focusable="false" class="icon-24" aria-hidden="true">\n               <path d="M12,10.5857864 L17.2928932,5.29289322 C17.6834175,4.90236893 18.3165825,4.90236893 18.7071068,5.29289322 C19.0976311,5.68341751 19.0976311,6.31658249 18.7071068,6.70710678 L13.4142136,12 L18.7071068,17.2928932 C19.0976311,17.6834175 19.0976311,18.3165825 18.7071068,18.7071068 C18.3165825,19.0976311 17.6834175,19.0976311 17.2928932,18.7071068 L12,13.4142136 L6.70710678,18.7071068 C6.31658249,19.0976311 5.68341751,19.0976311 5.29289322,18.7071068 C4.90236893,18.3165825 4.90236893,17.6834175 5.29289322,17.2928932 L10.5857864,12 L5.29289322,6.70710678 C4.90236893,6.31658249 4.90236893,5.68341751 5.29289322,5.29289322 C5.68341751,4.90236893 6.31658249,4.90236893 6.70710678,5.29289322 L12,10.5857864 Z"></path>\n            </svg>\n         </button>\n      </div>\n      <div class="modal-body" id="dialog-description">\n         <slot></slot>\n      </div>\n      <div id="actions" class="actions hidden">\n         <button id="lastButton" class="gr-btn primary" data-event="success">Success Button</button>\n         <button id="firstButton" class="gr-btn" data-event="cancel">Cancel Button</button>\n      </div>\n   </div>\n</div>\n';

  // Prepare the template to use ShadyCSS a polyfill for older browsers
  // Do this for each template that is needed
  if (w.ShadyCSS) {
    w.ShadyCSS.prepareTemplate(template, 'pearson-modal');
  }

  // Create our Web Component Class extending the HTMLElement

  var Modal = function (_HTMLElement) {
    _inherits(Modal, _HTMLElement);

    _createClass(Modal, [{
      key: 'triggerId',

      // Add get and set methods so we can easily access the attributes using 'this'
      // Example: to get the trigger-id attribute from the component
      // just type 'this.triggerId'
      get: function get() {
        return this.getAttribute('triggerid');
      }
    }, {
      key: 'showOnLoad',
      get: function get() {
        return this.hasAttribute('showonload');
      }
    }, {
      key: 'hideCancel',
      get: function get() {
        return this.hasAttribute('hidecancel');
      },
      set: function set(bool) {
        if (bool === true) {
          this.setAttribute('hidecancel', true);
        } else {
          this.removeAttribute('hidecancel');
        }
      }
    }, {
      key: 'hideSuccess',
      get: function get() {
        return this.hasAttribute('hidesuccess');
      },
      set: function set(bool) {
        if (bool === true) {
          this.setAttribute('hidesuccess', true);
        } else {
          this.removeAttribute('hidesuccess');
        }
      }
    }, {
      key: 'titleText',
      get: function get() {
        return this.getAttribute('titletext');
      }
    }, {
      key: 'successButtonText',
      get: function get() {
        return this.getAttribute('successbtntext');
      }
    }, {
      key: 'cancelButtonText',
      get: function get() {
        return this.getAttribute('cancelbtntext');
      }
    }, {
      key: 'footer',
      get: function get() {
        return this.hasAttribute('footer');
      },
      set: function set(bool) {
        if (bool === true) {
          this.setAttribute('footer', true);
        } else {
          this.removeAttribute('footer');
        }
      }

      // Prepares Component

    }, {
      key: 'open',
      get: function get() {
        return this.hasAttribute('open');
      },
      set: function set(bool) {
        this.setAttribute('open', bool);
      }
    }], [{
      key: 'observedAttributes',

      // Return the attributes added to the component
      get: function get() {
        return ['open', 'footer', 'triggerid', 'titletext', 'successbtntext', 'cancelbtntext', 'hidecancel', 'hidesuccess', 'showonload', 'fullwidth'];
      }
    }]);

    function Modal() {
      _classCallCheck(this, Modal);

      // Clone the template we created above and assign it to a variable.
      var _this = _possibleConstructorReturn(this, (Modal.__proto__ || Object.getPrototypeOf(Modal)).call(this));

      var modalClone = template.content.cloneNode(true);
      // Use the above variable to reference key nodes and bind them to this.
      _this.modal = modalClone.querySelector('#modal');
      _this.overlay = modalClone.querySelector('#modalOverlay');
      _this.actions = modalClone.querySelector('#actions');
      _this.closeButton = modalClone.querySelector('#closeButton');
      _this.cancelButton = modalClone.querySelector('#firstButton');
      _this.successButton = modalClone.querySelector('#lastButton');
      _this.heading = modalClone.querySelector('#dialog-heading');

      // use the references to apply any CSS or Events
      _this.modal.style.top = window.pageYOffset + 50 + 'px';
      _this.modal.style.transform = 'translate(-50%)';
      _this.modal.style.marginBottom = '50px';
      _this.overlay.addEventListener('click', function (event) {
        _this.close('cancel');
      });
      _this.modal.addEventListener('animationend', function (event) {
        if (event.animationName === 'slideOutDown') {
          _this.modal.classList.add('hidden');
          _this.overlay.classList.add('hidden');
          doc.body.removeEventListener('keydown', _this.closeOnEscape);
        }
      });
      _this.closeButton.addEventListener('click', function (event) {
        _this.close('cancel');
      });
      _this.cancelButton.addEventListener('click', function (event) {
        _this.close('cancel');
      });
      _this.successButton.addEventListener('click', function (event) {
        _this.close('success');
      });
      // Create the shadow Dom
      _this.attachShadow({ mode: 'open' });
      // Append the template we created to shadow Dom
      _this.shadowRoot.appendChild(modalClone);

      _this.closeOnEscape = _this.closeOnEscape.bind(_this);
      return _this;
    }

    // Lifecycle method 1
    // After component is prepared, this function immediately runs once.


    _createClass(Modal, [{
      key: 'connectedCallback',
      value: function connectedCallback() {
        // you now have access to the getters created above
        // get the attribute provided to the component and assign it to the template


        this.successButton.innerHTML = this.successButtonText;
        this.cancelButton.innerHTML = this.cancelButtonText;
        this.heading.innerHTML = this.titleText;

        if (this.footer) {
          this.actions.classList.remove('hidden');
        }
      }
      // Lifecycle method 2
      // This method runs every time an attribute on the component is changed.

    }, {
      key: 'attributeChangedCallback',
      value: function attributeChangedCallback(name, oldValue, newValue) {
        var _this2 = this;

        var trigger = doc.querySelector('#' + this.triggerId);
        // check to see if the name matches the name of the attribute changed, if it does, run this code
        if (name === 'open') {
          if (!this.showOnLoad) {
            trigger.disabled = true;
          }
          keyboardTrap(this, doc);
          // if open is present on the component, open the modal

          if (this.open === true) {
            // removes display none for FOUC
            this.classList.remove('hidden');
            doc.body.style.overflow = 'hidden';
            doc.querySelector('main').setAttribute('aria-hidden', true);
            this.setPosition();
            this.overlay.classList.remove('hidden');
            this.overlay.classList.add('fadeIn');
            this.modal.classList.remove('hidden');
            this.modal.classList.add('slideInDown');
            if (this.modal.classList.contains('slideOutDown')) {
              this.modal.classList.remove('slideOutDown');
              this.overlay.classList.remove('fadeOut');
            }
            this.modal.addEventListener('animationend', function (event) {
              if (event.animationName === 'slideInDown') {
                if (!_this2.hideSuccess && _this2.footer) {
                  console.log('one');
                  _this2.successButton.focus();
                } else if (!_this2.hideCancel && _this2.footer) {
                  console.log('two');
                  _this2.cancelButton.focus();
                } else {
                  console.log('three');
                  _this2.closeButton.focus();
                }
              }
            });

            doc.body.addEventListener('keydown', this.closeOnEscape);

            // else, close the modal
          } else {
            doc.body.style.overflow = 'auto';
            doc.querySelector('main').setAttribute('aria-hidden', false);
            this.overlay.classList.remove('fadeIn');
            this.modal.classList.remove('slideInDown');
            this.overlay.classList.add('fadeOut');
            this.modal.classList.add('slideOutDown');

            if (!this.showOnLoad) {
              trigger.disabled = false;
              trigger.focus();
            }
          }
        }
        if (name === 'footer') {
          if (this.footer === false) {
            this.actions.classList.add('hidden');
          } else {
            this.actions.classList.remove('hidden');
          }
        }
        if (name === 'hidecancel') {
          if (this.hideCancel === true) {
            this.cancelButton.classList.add('hidden');
          } else {
            this.cancelButton.classList.remove('hidden');
          }
        }
        if (name === 'hidesuccess') {
          if (this.hideSuccess === true) {
            this.successButton.classList.add('hidden');
          } else {
            this.successButton.classList.remove('hidden');
          }
        }
        if (name === 'fullwidth') {
          if (newValue !== null) {
            this.modal.classList.add('fullwidth');
          }
        }
      }
    }, {
      key: 'closeOnEscape',
      value: function closeOnEscape(event) {
        if (event.key === 'Escape') {
          this.close('cancel');
        }
      }
    }, {
      key: 'setPosition',
      value: function setPosition() {
        var _this3 = this;

        this.modal.style.top = window.pageYOffset + 50 + 'px';
        this.modal.style.marginBottom = '50px';
        this.modal.style.transform = 'translate(-50%)';
        var modalBody = this.modal.querySelector('.modal-body');

        this.modal.addEventListener('animationstart', function (event) {
          var modalPosition = _this3.modal.getBoundingClientRect();
          if (modalPosition.height > window.innerHeight) {
            modalBody.parentNode.classList.add('scroll');
          }
        });
      }

      // add a close method that runs everytime a template button is clicked
      // this method will close the modal and fire of an event of either success or cancel

    }, {
      key: 'close',
      value: function close(type) {
        this.removeAttribute('open');
        this.dispatchEvent(new Event(type, { bubbles: true, composed: true }));
      }
    }]);

    return Modal;
  }(HTMLElement);

  customElements.define('pearson-modal', Modal);
})(window, document);

// ACCESSIBILITY HELPERS LOCATED OUTSIDE OF THE COMPONENT
var focusableElements = '\n    a[href]:not([tabindex^="-"]):not([inert]),\n    area[href]:not([tabindex^="-"]):not([inert]),\n    input:not([disabled]):not([inert]),\n    select:not([disabled]):not([inert]),\n    textarea:not([disabled]):not([inert]),\n    button:not([disabled]):not([inert]),\n    iframe:not([tabindex^="-"]):not([inert]),\n    audio:not([tabindex^="-"]):not([inert]),\n    video:not([tabindex^="-"]):not([inert]),\n    [contenteditable]:not([tabindex^="-"]):not([inert]),\n    [tabindex]:not([tabindex^="-"]):not([inert])';

function getFocusableElements(modalNode) {
  var nodes = [].concat(_toConsumableArray(modalNode.querySelectorAll(focusableElements)), _toConsumableArray(modalNode.shadowRoot.querySelectorAll(focusableElements)));
  var arr = [];
  nodes.forEach(function (node) {
    if (modalNode.hideCancel === true) {
      if (node.id !== 'firstButton') {
        arr.push(node);
      }
    }
    if (modalNode.hideSuccess === true) {
      if (node.id !== 'lastButton') {
        arr.push(node);
      }
    }
    if (modalNode.footer === false) {
      if (node.id !== 'lastButton' && node.id !== 'firstButton') {
        arr.push(node);
      }
    }
  });

  if (arr.length === 0) {
    return nodes;
  } else {
    return arr;
  }
};
function keyboardTrap(focusNodes, doc) {
  getFocusableElements(focusNodes).forEach(function (node, index) {
    node.setAttribute('data-index', index + 1);
    node.addEventListener('keydown', function (event) {
      event.stopPropagation();
      var nextButton = parseInt(event.target.getAttribute('data-index')) + 1,
          prevButton = parseInt(event.target.getAttribute('data-index')) - 1,
          firstTrigger = getFocusableElements(focusNodes)[0],
          lastTrigger = getFocusableElements(focusNodes)[getFocusableElements(focusNodes).length - 1];

      var activeFocus = doc.activeElement;
      while (activeFocus && activeFocus.shadowRoot && activeFocus.shadowRoot.activeElement) {
        activeFocus = activeFocus.shadowRoot.activeElement;
      }

      if (event.key === 'Escape') {
        event.stopImmediatePropagation();
        event.preventDefault();
        focusNodes.removeAttribute('open');
      }

      if (event.key === 'Tab' && event.shiftKey === false) {
        event.preventDefault();
        if (activeFocus === lastTrigger) {
          firstTrigger.focus();
        } else {
          getFocusableElements(focusNodes)[nextButton - 1].focus();
        }
      } else if (event.key === 'Tab' && event.shiftKey === true) {
        event.preventDefault();
        if (activeFocus === firstTrigger) {
          lastTrigger.focus();
        } else {
          getFocusableElements(focusNodes)[prevButton - 1].focus();
        }
      }
    });
  });
}