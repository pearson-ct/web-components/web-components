function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var focusableElements = '\n    a[href]:not([tabindex^="-"]):not([inert]),\n    area[href]:not([tabindex^="-"]):not([inert]),\n    input:not([disabled]):not([inert]),\n    select:not([disabled]):not([inert]),\n    textarea:not([disabled]):not([inert]),\n    button:not([disabled]):not([inert]),\n    iframe:not([tabindex^="-"]):not([inert]),\n    audio:not([tabindex^="-"]):not([inert]),\n    video:not([tabindex^="-"]):not([inert]),\n    [contenteditable]:not([tabindex^="-"]):not([inert]),\n    [tabindex]:not([tabindex^="-"]):not([inert])';

export function getFocusableElements(node) {

  var nodes = [].concat(_toConsumableArray(node.querySelectorAll(focusableElements)), _toConsumableArray(node.shadowRoot.querySelectorAll(focusableElements)));

  var arr = [];

  nodes.forEach(function (node) {
    if (node.hideCancel === true) {
      if (node.id !== 'firstButton') {
        arr.push(node);
      }
    }
    if (node.hideSuccess === true) {
      if (node.id !== 'lastButton') {
        arr.push(node);
      }
    }
    if (node.footer === false) {
      if (node.id !== 'lastButton' && node.id !== 'firstButton') {
        arr.push(node);
      }
    }
  });

  if (arr.length === 0) {
    return nodes;
  } else {
    return arr;
  }
};

export function keyboardTrap(focusNodes, doc) {
  var _this = this;

  console.log(focusNodes, event);
  getFocusableElements(focusNodes).forEach(function (node, index) {
    node.setAttribute('data-index', index + 1);
    node.addEventListener('keydown', function (event) {
      event.stopPropagation();
      var nextButton = parseInt(event.target.getAttribute('data-index')) + 1,
          prevButton = parseInt(event.target.getAttribute('data-index')) - 1,
          firstTrigger = getFocusableElements(focusNodes)[0],
          lastTrigger = getFocusableElements(focusNodes)[getFocusableElements(focusNodes).length - 1];

      var activeFocus = doc.activeElement;
      while (activeFocus && activeFocus.shadowRoot && activeFocus.shadowRoot.activeElement) {
        activeFocus = activeFocus.shadowRoot.activeElement;
      }

      if (event.key === 'Escape') {
        event.preventDefault();
        _this.removeAttribute('open');
      }

      if (event.key === 'Tab' && event.shiftKey === false) {
        event.preventDefault();
        if (activeFocus === lastTrigger) {
          firstTrigger.focus();
        } else {
          getFocusableElements(focusNodes)[nextButton - 1].focus();
        }
      } else if (event.key === 'Tab' && event.shiftKey === true) {
        event.preventDefault();
        if (activeFocus === firstTrigger) {
          lastTrigger.focus();
        } else {
          getFocusableElements(focusNodes)[prevButton - 1].focus();
        }
      }
    });
  });
}