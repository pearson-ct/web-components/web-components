const focusableElements = `
    a[href]:not([tabindex^="-"]):not([inert]),
    area[href]:not([tabindex^="-"]):not([inert]),
    input:not([disabled]):not([inert]),
    select:not([disabled]):not([inert]),
    textarea:not([disabled]):not([inert]),
    button:not([disabled]):not([inert]),
    iframe:not([tabindex^="-"]):not([inert]),
    audio:not([tabindex^="-"]):not([inert]),
    video:not([tabindex^="-"]):not([inert]),
    [contenteditable]:not([tabindex^="-"]):not([inert]),
    [tabindex]:not([tabindex^="-"]):not([inert])`;

export function getFocusableElements (node) {

  const nodes = [
    ...node.querySelectorAll(focusableElements),
    ...node.shadowRoot.querySelectorAll(focusableElements)
  ];

  const arr = [];

  nodes.forEach(node => {
    if (node.hideCancel === true) {
      if (node.id !== 'firstButton') {
        arr.push(node);
      }
    }
    if (node.hideSuccess === true) {
      if (node.id !== 'lastButton') {
        arr.push(node)
      }
    }
    if (node.footer === false) {
      if (node.id !== 'lastButton' && node.id !== 'firstButton') {
        arr.push(node)
      }
    }
  })

  if (arr.length === 0) {
    return nodes;
  } else {
    return arr;
  }
};

export function keyboardTrap (focusNodes, doc) {
  console.log(focusNodes, event)
    getFocusableElements(focusNodes).forEach((node,index) => {
    node.setAttribute('data-index', index + 1);
    node.addEventListener('keydown', event => {
      event.stopPropagation();
      const nextButton = parseInt(event.target.getAttribute('data-index')) + 1,
        prevButton = parseInt(event.target.getAttribute('data-index')) - 1,
        firstTrigger = getFocusableElements(focusNodes)[0],
        lastTrigger = getFocusableElements(focusNodes)[getFocusableElements(focusNodes).length - 1];

      let activeFocus = doc.activeElement;
      while (activeFocus && activeFocus.shadowRoot && activeFocus.shadowRoot.activeElement) {
        activeFocus = activeFocus.shadowRoot.activeElement;
      }

      if (event.key === 'Escape') {
        event.preventDefault();
        this.removeAttribute('open');
      }

      if (event.key === 'Tab' && event.shiftKey === false) {
        event.preventDefault();
        if (activeFocus === lastTrigger) {
          firstTrigger.focus();
        } else {
          getFocusableElements(focusNodes)[nextButton - 1].focus()
        }
      } else if (event.key === 'Tab'  && event.shiftKey === true) {
        event.preventDefault();
        if (activeFocus === firstTrigger) {
          lastTrigger.focus();
        } else {
          getFocusableElements(focusNodes)[prevButton - 1].focus()
        }
      }
    })
  })
}
