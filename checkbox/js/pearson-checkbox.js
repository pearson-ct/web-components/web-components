(function(w, doc) {
  'use strict';

  const template = doc.createElement('template');

  template.innerHTML = `
    <style>
    
    *{
    box-sizing: border-box;
    }
    
    [type="checkbox"]{
        padding: 0;
    }
    
    .gr-meta {
        font-size: 12px;
        line-height: 12px;
        color: #6A7070;
     }
    
    .icon-18 {
        width: 18px;
        height: 18px;
     }
     
     .icon-24 {
        width: 24px;
        height: 24px;
     }
    
    .gr-checkbox {
      margin-bottom: 14px;
      min-height: 16px;
      position: relative;
      display: flex;
      align-items: center;
    }
    
    .gr-checkbox input[type=checkbox] {
      opacity: 0;
      height: 22px;
      width:22px;
      cursor: pointer;
      z-index: 1;
    }
    
    .gr-checkbox input[type=checkbox]~label {
      display: inline-block;
      line-height: 1.5;
      min-height: 24px;
      padding-left: 16px;
      position: relative;
      z-index: 2;
      cursor: pointer;
      font-size: 14px;
    }
    
    .gr-checkbox input[type=checkbox]~span {
      height: 22px;
      line-height: 1.5;
      text-align: center;
      width: 22px;
      border: 2px solid #c7c7c7;
      border-radius: 2px;
      left: 0;
      position: absolute;
      top: 0;
      z-index: 0;
      background: #fff;
    }
     
    .gr-checkbox input[type=checkbox]~span svg {
        height: 24px;
        opacity: 0;
        width: 24px;
        top: -3px;
        position: relative;
        left: -3px;
        fill: #047a9c;
     }
      
     .gr-checkbox input[type=checkbox]:checked~span svg{
        opacity: 1;
     }
     
    
    .gr-checkbox input[type=checkbox]:disabled~span{
        height: 22px;
        line-height: 1.5;
        text-align: center;
        width: 22px;
        border: 2px solid #c7c7c7;
        border-radius: 2px;
        left: 0;
        position: absolute;
        top: 0;
        z-index: 0;
        background-color: #e9e9e9;
    }
    
    .gr-checkbox input[type=checkbox]:focus~span {
      outline: 0;
      box-shadow: 0 0 0 2px #fff, 0 0 0 4px #1977d4;
    }
    
    .gr-checkbox + .error-state{
        display: none;
    }
    
    .gr-checkbox.error + .error-state{
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }
    
    .gr-checkbox.error + .error-state span{
        margin-left: 8px;
        color: #DB0020;
    }
     
     .gr-checkbox.error + .error-state svg{
         fill: #DB0020;
    }
    
    </style>
      <div class="gr-form-element">
        <div class="gr-checkbox">
            <input type="checkbox" id="checkboxId"/>
            <label  class="gr-label" for="checkboxId">Basic checkbox label</label>
            <span>
              <svg focusable="false" class="icon-24" aria-hidden="true">
                  <path d="M4.5,1 L19.5,1 C21.4329966,1 23,2.56700338 23,4.5 L23,19.5 C23,21.4329966 21.4329966,23 19.5,23 L4.5,23 C2.56700338,23 1,21.4329966 1,19.5 L1,4.5 C1,2.56700338 2.56700338,1 4.5,1 Z M9.48055302,17.9017747 C9.913256,18.1093819 10.4463555,17.981452 10.7309467,17.5789431 L17.8197099,7.55301635 C18.135437,7.10647067 18.0257453,6.49107611 17.5747066,6.17849414 C17.1236679,5.86591216 16.5020814,5.9745112 16.1863543,6.42105688 L9.80033032,15.4530709 L7.72281473,13.2676998 C7.3454783,12.8707737 6.71457695,12.851846 6.31365712,13.2254236 C5.91273728,13.5990012 5.89361915,14.2236179 6.27095558,14.620544 L9.17399136,17.6742927 C9.18395884,17.6852092 9.19420598,17.6959379 9.20473089,17.7064685 C9.32298853,17.8285251 9.40711664,17.8887858 9.48055302,17.9017747 Z"></path>
              </svg>
            </span>
        </div>
        <div class="error-state">
            <svg focusable="false" class="icon-18" aria-hidden="true">
                <path d="M10.3203543,1.76322881 L17.7947154,14.7065602 C18.2165963,15.4371302 17.9673988,16.3719805 17.2381172,16.7946067 C17.0059424,16.9291544 16.742459,17 16.4742343,17 L1.52551217,17 C0.682995061,17 0,16.3157983 0,15.4717927 C0,15.2030941 0.0707206291,14.9391453 0.205031086,14.7065602 L7.67939217,1.76322881 C8.10127304,1.03265881 9.03447459,0.78302105 9.76375615,1.20564727 C9.99478499,1.33953088 10.1867068,1.5317918 10.3203543,1.76322881 Z M8.5,13 C8.22385763,13 8,13.2238576 8,13.5 L8,14.5 C8,14.7761424 8.22385763,15 8.5,15 L9.5,15 C9.77614237,15 10,14.7761424 10,14.5 L10,13.5 C10,13.2238576 9.77614237,13 9.5,13 L8.5,13 Z M8.5,7 C8.22385763,7 8,7.22385763 8,7.5 L8,11.5 C8,11.7761424 8.22385763,12 8.5,12 L9.5,12 C9.77614237,12 10,11.7761424 10,11.5 L10,7.5 C10,7.22385763 9.77614237,7 9.5,7 L8.5,7 Z"></path>
            </svg>     
            <span class="warning-text gr-meta">Warning Text</span>
        </div>
      </div>
`;

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-checkbox');


  class Checkbox extends HTMLElement {
    static get observedAttributes() {
      return ['label', 'disabled', 'error'];
    }

    get error() {
      return this.hasAttribute('error')
    }

    get errorText() {
      return this.getAttribute('error')
    }

    get disabled() {
      return this.hasAttribute('disabled');
    }

    get labelText() {
      return this.getAttribute('label');
    }

    constructor() {
      super();
      this.attachShadow({mode: 'open'});
      const clone = template.content.cloneNode(true);
      this.label = clone.querySelector('label');
      this.input = clone.querySelector('input');
      this.inputGroup = clone.querySelector('.gr-checkbox');
      this.warningText = clone.querySelector('.warning-text');
      this.shadowRoot.appendChild(clone);

      this.input.addEventListener('input', event => {
        this.value = event.target.value;
        this.checked = event.target.checked;
      }, true)
    }

    attributeChangedCallback(name, oldValue, newValue) {
      if (name === 'label') {
        if (oldValue !== newValue) {
          this.label.innerHTML = this.labelText;
          this.input.setAttribute('value',this.labelText);
        }
      }

      if (name === 'disabled') {
        if (oldValue !== newValue) {
          if (this.disabled === true) {
            this.input.disabled = true
          } else {
            this.input.disabled = false
          }
        }
      }

      if (name === 'error') {
        if (oldValue !== newValue) {
          if (this.error === true) {
            this.inputGroup.classList.add('error');
            this.warningText.innerHTML = this.errorText
          } else {
            this.inputGroup.classList.remove('error')
          }
        }
      }

    }
  }

  customElements.define('pearson-checkbox', Checkbox);
})(window, document);
