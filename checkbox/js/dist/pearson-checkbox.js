var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function (w, doc) {
  'use strict';

  var template = doc.createElement('template');

  template.innerHTML = '\n    <style>\n    \n    *{\n    box-sizing: border-box;\n    }\n    \n    [type="checkbox"]{\n        padding: 0;\n    }\n    \n    .gr-meta {\n        font-size: 12px;\n        line-height: 12px;\n        color: #6A7070;\n     }\n    \n    .icon-18 {\n        width: 18px;\n        height: 18px;\n     }\n     \n     .icon-24 {\n        width: 24px;\n        height: 24px;\n     }\n    \n    .gr-checkbox {\n      margin-bottom: 14px;\n      min-height: 16px;\n      position: relative;\n      display: flex;\n      align-items: center;\n    }\n    \n    .gr-checkbox input[type=checkbox] {\n      opacity: 0;\n      height: 22px;\n      width:22px;\n      cursor: pointer;\n      z-index: 1;\n    }\n    \n    .gr-checkbox input[type=checkbox]~label {\n      display: inline-block;\n      line-height: 1.5;\n      min-height: 24px;\n      padding-left: 16px;\n      position: relative;\n      z-index: 2;\n      cursor: pointer;\n      font-size: 14px;\n    }\n    \n    .gr-checkbox input[type=checkbox]~span {\n      height: 22px;\n      line-height: 1.5;\n      text-align: center;\n      width: 22px;\n      border: 2px solid #c7c7c7;\n      border-radius: 2px;\n      left: 0;\n      position: absolute;\n      top: 0;\n      z-index: 0;\n      background: #fff;\n    }\n     \n    .gr-checkbox input[type=checkbox]~span svg {\n        height: 24px;\n        opacity: 0;\n        width: 24px;\n        top: -3px;\n        position: relative;\n        left: -3px;\n        fill: #047a9c;\n     }\n      \n     .gr-checkbox input[type=checkbox]:checked~span svg{\n        opacity: 1;\n     }\n     \n    \n    .gr-checkbox input[type=checkbox]:disabled~span{\n        height: 22px;\n        line-height: 1.5;\n        text-align: center;\n        width: 22px;\n        border: 2px solid #c7c7c7;\n        border-radius: 2px;\n        left: 0;\n        position: absolute;\n        top: 0;\n        z-index: 0;\n        background-color: #e9e9e9;\n    }\n    \n    .gr-checkbox input[type=checkbox]:focus~span {\n      outline: 0;\n      box-shadow: 0 0 0 2px #fff, 0 0 0 4px #1977d4;\n    }\n    \n    .gr-checkbox + .error-state{\n        display: none;\n    }\n    \n    .gr-checkbox.error + .error-state{\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n        -ms-flex-align: center;\n        align-items: center;\n    }\n    \n    .gr-checkbox.error + .error-state span{\n        margin-left: 8px;\n        color: #DB0020;\n    }\n     \n     .gr-checkbox.error + .error-state svg{\n         fill: #DB0020;\n    }\n    \n    </style>\n      <div class="gr-form-element">\n        <div class="gr-checkbox">\n            <input type="checkbox" id="checkboxId"/>\n            <label  class="gr-label" for="checkboxId">Basic checkbox label</label>\n            <span>\n              <svg focusable="false" class="icon-24" aria-hidden="true">\n                  <path d="M4.5,1 L19.5,1 C21.4329966,1 23,2.56700338 23,4.5 L23,19.5 C23,21.4329966 21.4329966,23 19.5,23 L4.5,23 C2.56700338,23 1,21.4329966 1,19.5 L1,4.5 C1,2.56700338 2.56700338,1 4.5,1 Z M9.48055302,17.9017747 C9.913256,18.1093819 10.4463555,17.981452 10.7309467,17.5789431 L17.8197099,7.55301635 C18.135437,7.10647067 18.0257453,6.49107611 17.5747066,6.17849414 C17.1236679,5.86591216 16.5020814,5.9745112 16.1863543,6.42105688 L9.80033032,15.4530709 L7.72281473,13.2676998 C7.3454783,12.8707737 6.71457695,12.851846 6.31365712,13.2254236 C5.91273728,13.5990012 5.89361915,14.2236179 6.27095558,14.620544 L9.17399136,17.6742927 C9.18395884,17.6852092 9.19420598,17.6959379 9.20473089,17.7064685 C9.32298853,17.8285251 9.40711664,17.8887858 9.48055302,17.9017747 Z"></path>\n              </svg>\n            </span>\n        </div>\n        <div class="error-state">\n            <svg focusable="false" class="icon-18" aria-hidden="true">\n                <path d="M10.3203543,1.76322881 L17.7947154,14.7065602 C18.2165963,15.4371302 17.9673988,16.3719805 17.2381172,16.7946067 C17.0059424,16.9291544 16.742459,17 16.4742343,17 L1.52551217,17 C0.682995061,17 0,16.3157983 0,15.4717927 C0,15.2030941 0.0707206291,14.9391453 0.205031086,14.7065602 L7.67939217,1.76322881 C8.10127304,1.03265881 9.03447459,0.78302105 9.76375615,1.20564727 C9.99478499,1.33953088 10.1867068,1.5317918 10.3203543,1.76322881 Z M8.5,13 C8.22385763,13 8,13.2238576 8,13.5 L8,14.5 C8,14.7761424 8.22385763,15 8.5,15 L9.5,15 C9.77614237,15 10,14.7761424 10,14.5 L10,13.5 C10,13.2238576 9.77614237,13 9.5,13 L8.5,13 Z M8.5,7 C8.22385763,7 8,7.22385763 8,7.5 L8,11.5 C8,11.7761424 8.22385763,12 8.5,12 L9.5,12 C9.77614237,12 10,11.7761424 10,11.5 L10,7.5 C10,7.22385763 9.77614237,7 9.5,7 L8.5,7 Z"></path>\n            </svg>     \n            <span class="warning-text gr-meta">Warning Text</span>\n        </div>\n      </div>\n';

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-checkbox');

  var Checkbox = function (_HTMLElement) {
    _inherits(Checkbox, _HTMLElement);

    _createClass(Checkbox, [{
      key: 'error',
      get: function get() {
        return this.hasAttribute('error');
      }
    }, {
      key: 'errorText',
      get: function get() {
        return this.getAttribute('error');
      }
    }, {
      key: 'disabled',
      get: function get() {
        return this.hasAttribute('disabled');
      }
    }, {
      key: 'labelText',
      get: function get() {
        return this.getAttribute('label');
      }
    }], [{
      key: 'observedAttributes',
      get: function get() {
        return ['label', 'disabled', 'error'];
      }
    }]);

    function Checkbox() {
      _classCallCheck(this, Checkbox);

      var _this = _possibleConstructorReturn(this, (Checkbox.__proto__ || Object.getPrototypeOf(Checkbox)).call(this));

      _this.attachShadow({ mode: 'open' });
      var clone = template.content.cloneNode(true);
      _this.label = clone.querySelector('label');
      _this.input = clone.querySelector('input');
      _this.inputGroup = clone.querySelector('.gr-checkbox');
      _this.warningText = clone.querySelector('.warning-text');
      _this.shadowRoot.appendChild(clone);

      _this.input.addEventListener('input', function (event) {
        _this.value = event.target.value;
        _this.checked = event.target.checked;
      }, true);
      return _this;
    }

    _createClass(Checkbox, [{
      key: 'attributeChangedCallback',
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'label') {
          if (oldValue !== newValue) {
            this.label.innerHTML = this.labelText;
            this.input.setAttribute('value', this.labelText);
          }
        }

        if (name === 'disabled') {
          if (oldValue !== newValue) {
            if (this.disabled === true) {
              this.input.disabled = true;
            } else {
              this.input.disabled = false;
            }
          }
        }

        if (name === 'error') {
          if (oldValue !== newValue) {
            if (this.error === true) {
              this.inputGroup.classList.add('error');
              this.warningText.innerHTML = this.errorText;
            } else {
              this.inputGroup.classList.remove('error');
            }
          }
        }
      }
    }]);

    return Checkbox;
  }(HTMLElement);

  customElements.define('pearson-checkbox', Checkbox);
})(window, document);