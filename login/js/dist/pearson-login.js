var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function (w, doc) {
  'use strict';

  // Create a template element

  var loginContainer = doc.createElement('template'),
      loginForm = doc.createElement('template'),
      loginUserTest = doc.createElement('template'),
      forgotForm = doc.createElement('template'),
      resetsForm = doc.createElement('template'),
      createForm = doc.createElement('template'),
      validationForm = doc.createElement('template'),
      resendForm = doc.createElement('template');

  loginUserTest.innerHTML = '\n  <h1 class="gr-h1 animated animateIn" part="heading">Sign in</h1>\n    <form class="animated animateIn" part="form">\n         <div class="gr-form-element" part="input-container">\n            <label class="gr-label" for="email" part="label">Username</label>\n            <input class="gr-input" id="email" type="text" part="input">\n         </div>\n         <div class="gr-form-element" part="input-container">\n            <label class="gr-label" for="password" part="label">Password</label>\n            <input class="gr-input" id="password" type="password" part="input">\n         </div>\n         <div class="error-state" id="error" ></div>\n         <button type="button" id="login" class="gr-btn attention" style="width:auto; margin:0" part="button-cta">\n            <span part="button-text">Sign in</span> \n                <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>\n                    <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>\n                </svg>\n         </button>\n      </form>\n  ';

  resendForm.innerHTML = '\n  <h1 class="gr-h1 animated animateIn" part="heading">Verify Your Account</h1>\n  <p class="animated animateIn" part="text">Enter your email address and instructions will be sent on how you can verify your account.</p>\n    <form class="animated animateIn" part="form">\n     <div class="gr-form-element" part="input-container">\n        <label class="gr-label" for="email" part="label">Email Address</label>\n        <input class="gr-input" id="email" type="email" part="input">\n     </div> \n     <div class="error-state" id="error"></div>\n     <button type="button" id="resend" class="gr-btn attention" style="width:auto; margin:0" part="button-cta">            <span part="button-text">Send a new token</span> \n        <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>\n          <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>\n      </svg>\n     </button>\n  </form>\n  ';
  validationForm.innerHTML = '\n  <h1 class="gr-h1 animated animateIn"  part="heading">Verify Your Account</h1>\n  <p class="animated animateIn" part="text">To verify your account, enter your email address and the token you recieved in an email.</p>\n    <form class="animated animateIn" part="form">\n     <div class="gr-form-element" part="input-container">\n        <label class="gr-label" for="email" part="label">Email Address</label>\n        <input class="gr-input" id="email" type="email" part="input">\n     </div>\n     <div class="gr-form-element" part="input-container">\n        <label class="gr-label" for="token" part="label">Verification Token</label>\n        <input class="gr-input" id="token" type="text" part="input">\n     \n     </div>\n     <div class="error-state" id="error"></div>\n     <button type="button" id="validate" class="gr-btn attention" style="width:auto; margin:0" part="button-cta"><span part="button-text">Verify your account</span> \n        <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>\n          <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>\n      </svg>\n     </button>\n  </form>\n  ';

  createForm.innerHTML = '\n    <h1 class="gr-h1 animated animateIn"  part="heading">Create an account</h1>\n    <form class="animated animateIn" part="form">\n   <div class="gr-form-element" part="input-container">\n      <label class="gr-label" for="email" part="label">Email address</label>\n      <input class="gr-input" id="email" type="email" part="input">\n   </div>\n   <div class="gr-form-element" part="input-container">\n      <label class="gr-label" for="password" part="label">Password</label>\n      <input class="gr-input" id="password" type="password" part="input">\n   </div>\n   <div class="gr-form-element" part="input-container">\n      <label class="gr-label" for="confirm" part="label">Confirm Password</label>\n      <input class="gr-input" id="confirm" type="password" part="input">\n   </div>\n   <div class="gr-form-element" part="input-container">\n      <label class="gr-label" for="first" part="label">First name</label>\n      <input class="gr-input" id="first" type="text" part="input">\n   </div>\n   <div class="gr-form-element" part="input-container">\n      <label class="gr-label" for="last" part="label">Last name</label>\n      <input class="gr-input" id="last" type="text" part="input">\n   </div>\n   <div part="toggle">\n      <label style="margin-right:16px" part="label">I agree to the Terms of Use and acknowledge the Privacy Policy.</label>\n      <pearson-toggle tabindex="0" role="switch" aria-checked="false"> </pearson-toggle>\n   </div>\n   <div class="error-state" id="error"></div>\n   <button type="button" id="create" class="gr-btn attention" style="width:auto; margin:0" part="button-cta">            <span part="button-text">Create</span> \n                <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>\n                    <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>\n                </svg>\n   </button>\n      <button type="button" id="back" class="gr-btn" style="width:auto; margin:0" part="button">   Return to login </button>\n</form>\n  ';

  resetsForm.innerHTML = '\n  <h1 class="gr-h1 animated animateIn" part="heading">Change Password?</h1>\n  <p class="animated animateIn" part="text">Enter a new password for email@email.com</p>\n    <form class="animated animateIn" part="form">\n     <div class="gr-form-element" part="input-container">\n        <label class="gr-label" for="newPassword" part="label">New Password</label>\n        <input class="gr-input" id="newPassword" type="password" part="input">  \n     </div>\n     <div class="gr-form-element" part="input-container">\n        <label class="gr-label" for="confirmPassword" part="label">Confirm New Password</label>\n        <input class="gr-input" id="confirmPassword" type="password" part="input">\n     </div>\n     <div class="error-state" id="error"></div>\n     <button type="button" id="reset" class="gr-btn attention" style="width:auto; margin:0" part="button-cta">            <span part="button-text">Reset</span> \n                  <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>\n                    <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>\n                </svg>\n     </button>\n  </form>\n  ';

  forgotForm.innerHTML = '\n<h1 class="gr-h1 animated animateIn"  part="heading">Forgot Password?</h1>\n<p class="animated animateIn" part="text">Enter your email address and if you have an account, we will send you an email with instructions on how to reset your password.</p>\n  <form class="animated animateIn" part="form">\n   <div class="gr-form-element" part="input-container">\n      <label class="gr-label" for="email" part="label">Email</label>\n      <input class="gr-input" id="email" type="email" part="input">\n   </div>\n   <div class="error-state" id="error"></div>\n   <button type="button" id="send" class="gr-btn attention" style="width:auto; margin:0" part="button-cta">            <span part="button-text">Send</span> \n      <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>\n          <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>\n      </svg>\n   </button>\n   <button type="button" id="back" class="gr-btn" style="width:auto; margin:0" part="button">            <span part="button-text">            Return to login </span> </button>\n</form>\n  ';

  loginForm.innerHTML = '\n  <h1 class="gr-h1 animated animateIn" part="heading">Sign in</h1>\n    <form class="animated animateIn" part="form">\n         <div class="gr-form-element" part="input-container">\n            <label class="gr-label" for="email" part="label">Email Address</label>\n            <input class="gr-input" id="email" type="text" part="input">\n         </div>\n         <div class="gr-form-element" part="input-container">\n            <label class="gr-label" for="password" part="label">Password</label>\n            <input class="gr-input" id="password" type="password" part="input">\n         </div>\n         <div class="error-state" id="error" ></div>\n         <button type="button" id="login" class="gr-btn attention" style="width:auto; margin:0" part="button-cta">\n            <span part="button-text">Sign in</span> \n                <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>\n                    <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>\n                </svg>\n         </button>\n         <div class="additional-actions" part="actions">\n           <a id="forgot" href="!#" part="link">Forgot username or password?</a>\n           <button id="create" type="button" id="btn-login" class="gr-btn" part="button">            Create account </button>\n         </div>\n      </form>\n  ';

  loginContainer.innerHTML = '   \n   <style>\n   html {\n  box-sizing: border-box;\n  font-size:14px;\n}\n\n* {\n  box-sizing: border-box;\n}\n@keyframes spin {\n  0% { transform: rotate(0deg); }\n  100% { transform: rotate(360deg); }\n}\n\n  :host #spin {\n    animation: spin 2s linear infinite;\n    margin-left:8px;\n    animation-duration: 1s;\n    visibility:hidden;\n  }\n@keyframes fadeIn {\n  from {\n    opacity: 0;\n  }\n\n  to {\n    opacity: 1;\n  }\n}\n.animateIn {\n    animation-name: fadeIn;\n    animation-duration: .5s;\n}\n@media (prefers-reduced-motion) {\n    .animated {\n        animation: unset !important;\n        transition: none !important;\n    }\n}\n\nhtml[data-prefers-reduced-motion] .animated {\n    animation: unset !important;\n    transition: none !important;\n}\n#email {\ntext-transform: lowercase;\n}\n</style>\n     <div class="login-form" part="container">\n      \n      \n     </div>\n';

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(loginContainer, 'pearson-login');

  var Login = function (_HTMLElement) {
    _inherits(Login, _HTMLElement);

    _createClass(Login, [{
      key: 'token',
      get: function get() {
        return this.getAttribute('token');
      },
      set: function set(value) {
        this.setAttribute('token', value);
      }
    }, {
      key: 'loading',
      get: function get() {
        return this.getAttribute('loading');
      },
      set: function set(value) {
        this.setAttribute('loading', value);
      }
    }, {
      key: 'error',
      get: function get() {
        return this.getAttribute('error');
      },
      set: function set(value) {
        this.setAttribute('error', value);
      }
    }, {
      key: 'status',
      get: function get() {
        return this.getAttribute('status');
      },
      set: function set(value) {
        this.setAttribute('status', value);
      }
    }], [{
      key: 'observedAttributes',
      get: function get() {
        return ['status', 'error', 'loading', 'token'];
      }
    }]);

    function Login() {
      _classCallCheck(this, Login);

      var _this = _possibleConstructorReturn(this, (Login.__proto__ || Object.getPrototypeOf(Login)).call(this));

      _this.attachShadow({ mode: 'open' });
      var clone = loginContainer.content.cloneNode(true);
      _this.container = clone.querySelector('.login-form');
      _this.shadowRoot.appendChild(clone);

      return _this;
    }

    _createClass(Login, [{
      key: 'validateEmail',
      value: function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
      }
    }, {
      key: 'attributeChangedCallback',
      value: function attributeChangedCallback(name, oldValue, newValue) {
        var _this2 = this;

        if (name === 'loading') {
          var spinner = this.shadowRoot.querySelector('#spin');
          if (newValue === 'true') {
            console.log(spinner);
            spinner.style.visibility = 'visible';
          } else {
            spinner.style.visibility = 'hidden';
          }
        }
        if (name === 'error') {
          var errorNode = this.shadowRoot.querySelector('#error');
          if (newValue.length !== 0) {
            errorNode.innerHTML = this.error;
            errorNode.setAttribute('part', 'error');
          } else {
            errorNode.innerHTML = '';
            errorNode.classList.remove('error');
            errorNode.removeAttribute('part', 'error');
          }
        }
        if (name === 'token') {
          var tokenNode = this.shadowRoot.querySelector('#token');
          tokenNode.value = this.token;
        }
        if (name === 'status') {
          this.container.innerHTML = '';
          if (newValue === 'login') {
            var loginFormClone = loginForm.content.cloneNode(true),
                loginBtn = loginFormClone.querySelector('#login'),
                userName = loginFormClone.querySelector('#email'),
                passWord = loginFormClone.querySelector('#password'),
                forgotBtn = loginFormClone.querySelector('#forgot'),
                createBtn = loginFormClone.querySelector('#create'),
                _spinner = loginFormClone.querySelector('#spin'),
                error = loginFormClone.querySelector('#error'),
                data = {};

            passWord.addEventListener('keyup', function (event) {
              if (event.keyCode === 13) {
                event.preventDefault();
                loginBtn.click();
              }
            });
            loginBtn.addEventListener('click', function (event) {
              event.stopPropagation();
              if (userName.value.length === 0) {
                _this2.error = "Please enter your email address";
                userName.focus();
                return;
              }
              if (passWord.value.length === 0) {
                _this2.error = "Please enter your password";
                passWord.focus();
                return;
              }
              if (_this2.validateEmail(userName.value)) {
                data.username = userName.value;
                data.password = passWord.value;
                _this2.dispatchEvent(new CustomEvent('login', { bubbles: true, detail: data }));
                _this2.loading = true;
              } else {
                _this2.error = "Please check your email address and try again";
              }
            });

            forgotBtn.addEventListener('click', function (event) {
              event.preventDefault();
              _this2.status = 'forgot';
            });

            createBtn.addEventListener('click', function (event) {
              event.preventDefault();
              _this2.status = 'create';
            });

            this.container.appendChild(loginFormClone);
          }

          if (newValue === 'usertest') {
            var _loginFormClone = loginUserTest.content.cloneNode(true),
                _loginBtn = _loginFormClone.querySelector('#login'),
                _userName = _loginFormClone.querySelector('#email'),
                _passWord = _loginFormClone.querySelector('#password'),
                _forgotBtn = _loginFormClone.querySelector('#forgot'),
                _createBtn = _loginFormClone.querySelector('#create'),
                _spinner2 = _loginFormClone.querySelector('#spin'),
                _error = _loginFormClone.querySelector('#error'),
                _data = {};

            _passWord.addEventListener('keyup', function (event) {
              if (event.keyCode === 13) {
                event.preventDefault();
                _loginBtn.click();
              }
            });
            _loginBtn.addEventListener('click', function (event) {
              event.stopPropagation();
              if (_userName.value.length === 0) {
                _this2.error = "Please enter the username provided";
                _userName.focus();
                return;
              }
              if (_passWord.value.length === 0) {
                _this2.error = "Please enter the password provided";
                _passWord.focus();
                return;
              }
              _data.username = _userName.value;
              _data.password = _passWord.value;
              _this2.dispatchEvent(new CustomEvent('login', { bubbles: true, detail: _data }));
              _this2.loading = true;
            });
            this.container.appendChild(_loginFormClone);
          }

          if (newValue === 'resend') {
            var resendClone = resendForm.content.cloneNode(true),
                resendBtn = resendClone.querySelector('#resend'),
                email = resendClone.querySelector('#email'),
                _data2 = {};

            resendBtn.addEventListener('click', function (event) {
              _data2.email = email.value;
              _this2.loading = true;
              _this2.dispatchEvent(new CustomEvent('resend', { bubbles: true, detail: _data2 }));
            });
            this.container.appendChild(resendClone);
          }
          if (newValue === 'forgot') {
            var forgotClone = forgotForm.content.cloneNode(true),
                sendBtn = forgotClone.querySelector('#send'),
                returnBtn = forgotClone.querySelector('#back'),
                _email = forgotClone.querySelector('#email'),
                _spinner3 = forgotClone.querySelector('#spin'),
                _data3 = {};

            sendBtn.addEventListener('click', function (event) {
              event.stopPropagation();
              _data3.email = _email.value;
              _this2.dispatchEvent(new CustomEvent('forgot', { bubbles: true, detail: _data3 }));
              _this2.loading = true;
            });
            returnBtn.addEventListener('click', function (event) {
              event.stopPropagation();
              _this2.status = 'login';
            });

            this.container.appendChild(forgotClone);
          }

          if (newValue === 'reset') {
            var resetClone = resetsForm.content.cloneNode(true),
                resetBtn = resetClone.querySelector('#reset'),
                password = resetClone.querySelector('#newPassword'),
                confirm = resetClone.querySelector('#confirmPassword'),
                _error2 = resetClone.querySelector('#error'),
                _spinner4 = resetClone.querySelector('#spin'),
                _data4 = {};

            resetBtn.addEventListener('click', function (event) {
              event.stopPropagation();
              _data4.password = password.value;
              _data4.confirmation = confirm.value;
              if (password.value === confirm.value) {
                if (_error2.classList.contains('error')) {
                  _error2.classList.remove('error');
                  _error2.innerHTML = ' ';
                }
                _this2.dispatchEvent(new CustomEvent('reset', { bubbles: true, detail: _data4 }));
                _this2.loading = true;
              } else {
                _error2.classList.add('error');
                _error2.innerHTML = 'passwords do not match';
              }
            });

            this.container.appendChild(resetClone);
          }
          if (newValue === 'validate') {
            var validateClone = validationForm.content.cloneNode(true),
                validateBtn = validateClone.querySelector('#validate'),
                _resendBtn = validateClone.querySelector('#resendToken'),
                _email2 = validateClone.querySelector('#email'),
                token = validateClone.querySelector('#token'),
                spin = validateClone.querySelector('#spin'),
                _spinner5 = validateClone.querySelector('#resendSpin'),
                _data5 = {};

            validateBtn.addEventListener('click', function (event) {
              if (_email2.value.length === 0) {
                _this2.error = "Please enter an email address";
                _email2.focus();
                return;
              }

              if (!_this2.validateEmail(_email2.value)) {
                _this2.error = "Please check your email address and try again";
                _email2.focus();
                return;
              }
              _data5.email = _email2.value;
              _data5.token = token.value;
              _this2.dispatchEvent(new CustomEvent('validate', { bubbles: true, detail: _data5 }));
              _this2.loading = true;
            });
            this.container.appendChild(validateClone);
          }
          if (newValue === 'create') {
            var createClone = createForm.content.cloneNode(true),
                _createBtn2 = createClone.querySelector('#create'),
                _returnBtn = createClone.querySelector('#back'),
                _email3 = createClone.querySelector('#email'),
                _password = createClone.querySelector('#password'),
                _confirm = createClone.querySelector('#confirm'),
                firstName = createClone.querySelector('#first'),
                lastName = createClone.querySelector('#last'),
                _error3 = createClone.querySelector('#error'),
                _spinner6 = createClone.querySelector('#spin'),
                toggle = createClone.querySelector('pearson-toggle'),
                _data6 = {};

            _createBtn2.addEventListener('click', function (event) {
              console.log(toggle.on);
              if (_email3.value.length === 0) {
                _this2.error = "Please enter an email address";
                _email3.focus();
                return;
              }
              if (!_this2.validateEmail(_email3.value)) {
                _this2.error = "Please check your email address and try again";
                _email3.focus();
                return;
              }
              if (_password.value.length === 0) {
                _this2.error = "Please enter a password";
                _password.focus();
                return;
              }
              if (_password.value !== _confirm.value) {
                _this2.error = "Passwords don't match";
                _password.focus();
                return;
              }
              if (firstName.value.length === 0) {
                _this2.error = "Please enter your first name";
                firstName.focus();
                return;
              }
              if (lastName.value.length === 0) {
                _this2.error = "Please enter your last name";
                lastName.focus();
                return;
              }
              if (toggle.on === 0) {
                _this2.error = "You must agree to the terms of our policies";
                toggle.focus();
                return;
              }

              event.stopPropagation();
              _data6.email = _email3.value;
              _data6.password = _password.value;
              _data6.confirm = _confirm.value;
              _data6.firstName = firstName.value;
              _data6.lastName = lastName.value;
              _this2.dispatchEvent(new CustomEvent('create', { bubbles: true, detail: _data6 }));
              _this2.loading = true;
            });

            _returnBtn.addEventListener('click', function (event) {
              event.stopPropagation();
              _this2.status = 'login';
            });

            this.container.appendChild(createClone);
          }
        }
      }
    }]);

    return Login;
  }(HTMLElement);

  customElements.define('pearson-login', Login);
})(window, document);