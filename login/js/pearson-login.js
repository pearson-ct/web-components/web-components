(function(w, doc) {
  'use strict';

  // Create a template element
  const loginContainer = doc.createElement('template'),
    loginForm = doc.createElement('template'),
      loginUserTest = doc.createElement('template'),
    forgotForm = doc.createElement('template'),
    resetsForm = doc.createElement('template'),
    createForm = doc.createElement('template'),
    validationForm = doc.createElement('template'),
    resendForm = doc.createElement('template')

  loginUserTest.innerHTML = `
  <h1 class="gr-h1 animated animateIn" part="heading">Sign in</h1>
    <form class="animated animateIn" part="form">
         <div class="gr-form-element" part="input-container">
            <label class="gr-label" for="email" part="label">Username</label>
            <input class="gr-input" id="email" type="text" part="input">
         </div>
         <div class="gr-form-element" part="input-container">
            <label class="gr-label" for="password" part="label">Password</label>
            <input class="gr-input" id="password" type="password" part="input">
         </div>
         <div class="error-state" id="error" ></div>
         <button type="button" id="login" class="gr-btn attention" style="width:auto; margin:0" part="button-cta">
            <span part="button-text">Sign in</span> 
                <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>
                    <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>
                </svg>
         </button>
      </form>
  `

  resendForm.innerHTML = `
  <h1 class="gr-h1 animated animateIn" part="heading">Verify Your Account</h1>
  <p class="animated animateIn" part="text">Enter your email address and instructions will be sent on how you can verify your account.</p>
    <form class="animated animateIn" part="form">
     <div class="gr-form-element" part="input-container">
        <label class="gr-label" for="email" part="label">Email Address</label>
        <input class="gr-input" id="email" type="email" part="input">
     </div> 
     <div class="error-state" id="error"></div>
     <button type="button" id="resend" class="gr-btn attention" style="width:auto; margin:0" part="button-cta">            <span part="button-text">Send a new token</span> 
        <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>
          <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>
      </svg>
     </button>
  </form>
  `
  validationForm.innerHTML = `
  <h1 class="gr-h1 animated animateIn"  part="heading">Verify Your Account</h1>
  <p class="animated animateIn" part="text">To verify your account, enter your email address and the token you recieved in an email.</p>
    <form class="animated animateIn" part="form">
     <div class="gr-form-element" part="input-container">
        <label class="gr-label" for="email" part="label">Email Address</label>
        <input class="gr-input" id="email" type="email" part="input">
     </div>
     <div class="gr-form-element" part="input-container">
        <label class="gr-label" for="token" part="label">Verification Token</label>
        <input class="gr-input" id="token" type="text" part="input">
     
     </div>
     <div class="error-state" id="error"></div>
     <button type="button" id="validate" class="gr-btn attention" style="width:auto; margin:0" part="button-cta"><span part="button-text">Verify your account</span> 
        <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>
          <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>
      </svg>
     </button>
  </form>
  `

  createForm.innerHTML = `
    <h1 class="gr-h1 animated animateIn"  part="heading">Create an account</h1>
    <form class="animated animateIn" part="form">
   <div class="gr-form-element" part="input-container">
      <label class="gr-label" for="email" part="label">Email address</label>
      <input class="gr-input" id="email" type="email" part="input">
   </div>
   <div class="gr-form-element" part="input-container">
      <label class="gr-label" for="password" part="label">Password</label>
      <input class="gr-input" id="password" type="password" part="input">
   </div>
   <div class="gr-form-element" part="input-container">
      <label class="gr-label" for="confirm" part="label">Confirm Password</label>
      <input class="gr-input" id="confirm" type="password" part="input">
   </div>
   <div class="gr-form-element" part="input-container">
      <label class="gr-label" for="first" part="label">First name</label>
      <input class="gr-input" id="first" type="text" part="input">
   </div>
   <div class="gr-form-element" part="input-container">
      <label class="gr-label" for="last" part="label">Last name</label>
      <input class="gr-input" id="last" type="text" part="input">
   </div>
   <div part="toggle">
      <label style="margin-right:16px" part="label">I agree to the Terms of Use and acknowledge the Privacy Policy.</label>
      <pearson-toggle tabindex="0" role="switch" aria-checked="false"> </pearson-toggle>
   </div>
   <div class="error-state" id="error"></div>
   <button type="button" id="create" class="gr-btn attention" style="width:auto; margin:0" part="button-cta">            <span part="button-text">Create</span> 
                <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>
                    <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>
                </svg>
   </button>
      <button type="button" id="back" class="gr-btn" style="width:auto; margin:0" part="button">   Return to login </button>
</form>
  `


  resetsForm.innerHTML = `
  <h1 class="gr-h1 animated animateIn" part="heading">Change Password?</h1>
  <p class="animated animateIn" part="text">Enter a new password for email@email.com</p>
    <form class="animated animateIn" part="form">
     <div class="gr-form-element" part="input-container">
        <label class="gr-label" for="newPassword" part="label">New Password</label>
        <input class="gr-input" id="newPassword" type="password" part="input">  
     </div>
     <div class="gr-form-element" part="input-container">
        <label class="gr-label" for="confirmPassword" part="label">Confirm New Password</label>
        <input class="gr-input" id="confirmPassword" type="password" part="input">
     </div>
     <div class="error-state" id="error"></div>
     <button type="button" id="reset" class="gr-btn attention" style="width:auto; margin:0" part="button-cta">            <span part="button-text">Reset</span> 
                  <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>
                    <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>
                </svg>
     </button>
  </form>
  `

  forgotForm.innerHTML = `
<h1 class="gr-h1 animated animateIn"  part="heading">Forgot Password?</h1>
<p class="animated animateIn" part="text">Enter your email address and if you have an account, we will send you an email with instructions on how to reset your password.</p>
  <form class="animated animateIn" part="form">
   <div class="gr-form-element" part="input-container">
      <label class="gr-label" for="email" part="label">Email</label>
      <input class="gr-input" id="email" type="email" part="input">
   </div>
   <div class="error-state" id="error"></div>
   <button type="button" id="send" class="gr-btn attention" style="width:auto; margin:0" part="button-cta">            <span part="button-text">Send</span> 
      <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>
          <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>
      </svg>
   </button>
   <button type="button" id="back" class="gr-btn" style="width:auto; margin:0" part="button">            <span part="button-text">            Return to login </span> </button>
</form>
  `

  loginForm.innerHTML = `
  <h1 class="gr-h1 animated animateIn" part="heading">Sign in</h1>
    <form class="animated animateIn" part="form">
         <div class="gr-form-element" part="input-container">
            <label class="gr-label" for="email" part="label">Email Address</label>
            <input class="gr-input" id="email" type="text" part="input">
         </div>
         <div class="gr-form-element" part="input-container">
            <label class="gr-label" for="password" part="label">Password</label>
            <input class="gr-input" id="password" type="password" part="input">
         </div>
         <div class="error-state" id="error" ></div>
         <button type="button" id="login" class="gr-btn attention" style="width:auto; margin:0" part="button-cta">
            <span part="button-text">Sign in</span> 
                <svg id="spin" part="load-indicator" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M0 0h24v24H0V0z" fill="none"/>
                    <path d="M12 4V1L8 5l4 4V6c3.31 0 6 2.69 6 6 0 1.01-.25 1.97-.7 2.8l1.46 1.46C19.54 15.03 20 13.57 20 12c0-4.42-3.58-8-8-8zm0 14c-3.31 0-6-2.69-6-6 0-1.01.25-1.97.7-2.8L5.24 7.74C4.46 8.97 4 10.43 4 12c0 4.42 3.58 8 8 8v3l4-4-4-4v3z"/>
                </svg>
         </button>
         <div class="additional-actions" part="actions">
           <a id="forgot" href="!#" part="link">Forgot username or password?</a>
           <button id="create" type="button" id="btn-login" class="gr-btn" part="button">            Create account </button>
         </div>
      </form>
  `



  loginContainer.innerHTML = `   
   <style>
   html {
  box-sizing: border-box;
  font-size:14px;
}

* {
  box-sizing: border-box;
}
@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

  :host #spin {
    animation: spin 2s linear infinite;
    margin-left:8px;
    animation-duration: 1s;
    visibility:hidden;
  }
@keyframes fadeIn {
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
}
.animateIn {
    animation-name: fadeIn;
    animation-duration: .5s;
}
@media (prefers-reduced-motion) {
    .animated {
        animation: unset !important;
        transition: none !important;
    }
}

html[data-prefers-reduced-motion] .animated {
    animation: unset !important;
    transition: none !important;
}
#email {
text-transform: lowercase;
}
</style>
     <div class="login-form" part="container">
      
      
     </div>
`;

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(loginContainer, 'pearson-login');

  class Login extends HTMLElement {
    static get observedAttributes() {
      return ['status', 'error', 'loading', 'token'];
    }
    get token () {
      return this.getAttribute('token');
    }
    get loading () {
      return this.getAttribute('loading')
    }
    get error () {
      return this.getAttribute('error')
    }

    get status () {
      return this.getAttribute('status');
    }
    set token(value) {
      this.setAttribute('token', value)
    }
    set loading(value) {
      this.setAttribute('loading', value)
    }

    set error(value) {
      this.setAttribute('error', value)
    }

    set status(value) {
      this.setAttribute('status', value)
    }

    constructor() {
      super();
      this.attachShadow({ mode: 'open' });
      const clone = loginContainer.content.cloneNode(true);
      this.container = clone.querySelector('.login-form');
      this.shadowRoot.appendChild(clone);

    }

    validateEmail(email) {
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }

    attributeChangedCallback(name, oldValue, newValue) {
      if (name === 'loading') {
        const spinner = this.shadowRoot.querySelector('#spin');
        if (newValue === 'true') {
          console.log(spinner)
          spinner.style.visibility = 'visible'
        } else {
          spinner.style.visibility = 'hidden'
        }
      }
      if (name === 'error') {
        const errorNode = this.shadowRoot.querySelector('#error');
        if (newValue.length !== 0) {
          errorNode.innerHTML = this.error
          errorNode.setAttribute('part', 'error')
        } else {
          errorNode.innerHTML = ''
          errorNode.classList.remove('error')
          errorNode.removeAttribute('part', 'error')
        }
      }
      if (name === 'token') {
        const tokenNode = this.shadowRoot.querySelector('#token');
        tokenNode.value = this.token
      }
      if (name === 'status') {
        this.container.innerHTML = '';
        if (newValue === 'login') {
          const loginFormClone = loginForm.content.cloneNode(true),
            loginBtn = loginFormClone.querySelector('#login'),
            userName = loginFormClone.querySelector('#email'),
            passWord = loginFormClone.querySelector('#password'),
            forgotBtn = loginFormClone.querySelector('#forgot'),
            createBtn = loginFormClone.querySelector('#create'),
            spinner = loginFormClone.querySelector('#spin'),
            error = loginFormClone.querySelector('#error'),
            data = {};

          passWord.addEventListener('keyup', event => {
            if (event.keyCode === 13) {
              event.preventDefault()
              loginBtn.click()
            }
          })
          loginBtn.addEventListener('click', event => {
            event.stopPropagation();
            if (userName.value.length === 0) {
               this.error = "Please enter your email address";
               userName.focus();
               return
            }
            if (passWord.value.length === 0) {
              this.error = "Please enter your password";
              passWord.focus();
              return
            }
            if (this.validateEmail(userName.value)) {
              data.username = userName.value
              data.password = passWord.value
              this.dispatchEvent(new CustomEvent('login', { bubbles: true, detail: data }))
              this.loading = true
            } else {
              this.error = "Please check your email address and try again"
            }

          })

          forgotBtn.addEventListener('click', event=> {
            event.preventDefault();
            this.status = 'forgot'
          })

          createBtn.addEventListener('click', event => {
            event.preventDefault();
            this.status = 'create'
          })

          this.container.appendChild(loginFormClone)

        }

        if (newValue === 'usertest') {
          const loginFormClone = loginUserTest.content.cloneNode(true),
              loginBtn = loginFormClone.querySelector('#login'),
              userName = loginFormClone.querySelector('#email'),
              passWord = loginFormClone.querySelector('#password'),
              forgotBtn = loginFormClone.querySelector('#forgot'),
              createBtn = loginFormClone.querySelector('#create'),
              spinner = loginFormClone.querySelector('#spin'),
              error = loginFormClone.querySelector('#error'),
              data = {};

          passWord.addEventListener('keyup', event => {
            if (event.keyCode === 13) {
              event.preventDefault()
              loginBtn.click()
            }
          })
          loginBtn.addEventListener('click', event => {
            event.stopPropagation();
            if (userName.value.length === 0) {
              this.error = "Please enter the username provided";
              userName.focus();
              return
            }
            if (passWord.value.length === 0) {
              this.error = "Please enter the password provided";
              passWord.focus();
              return
            }
            data.username = userName.value
            data.password = passWord.value
            this.dispatchEvent(new CustomEvent('login', { bubbles: true, detail: data }))
            this.loading = true
          })
          this.container.appendChild(loginFormClone)
        }

        if (newValue === 'resend') {
          const resendClone = resendForm.content.cloneNode(true),
            resendBtn = resendClone.querySelector('#resend'),
            email = resendClone.querySelector('#email'),
            data = {};

            resendBtn.addEventListener('click', event => {
            data.email = email.value
            this.loading = true
            this.dispatchEvent(new CustomEvent('resend', { bubbles: true, detail: data }))
          })
          this.container.appendChild(resendClone)
        }
        if (newValue === 'forgot') {
          const forgotClone = forgotForm.content.cloneNode(true),
            sendBtn = forgotClone.querySelector('#send'),
            returnBtn = forgotClone.querySelector('#back'),
            email = forgotClone.querySelector('#email'),
            spinner = forgotClone.querySelector('#spin'),
            data = {};

          sendBtn.addEventListener('click', event => {
            event.stopPropagation();
            data.email = email.value
            this.dispatchEvent(new CustomEvent('forgot', { bubbles: true, detail: data }))
            this.loading = true
          })
          returnBtn.addEventListener('click', event => {
            event.stopPropagation();
            this.status = 'login'
          })


          this.container.appendChild(forgotClone)
        }

        if (newValue === 'reset') {
          const resetClone = resetsForm.content.cloneNode(true),
            resetBtn = resetClone.querySelector('#reset'),
            password = resetClone.querySelector('#newPassword'),
            confirm = resetClone.querySelector('#confirmPassword'),
            error = resetClone.querySelector('#error'),
            spinner = resetClone.querySelector('#spin'),
            data = {};

          resetBtn.addEventListener('click', event => {
            event.stopPropagation();
            data.password = password.value
            data.confirmation = confirm.value
            if (password.value === confirm.value) {
              if (error.classList.contains('error')) {
                error.classList.remove('error')
                error.innerHTML = ' '
              }
              this.dispatchEvent(new CustomEvent('reset', { bubbles: true, detail: data }))
              this.loading = true
            } else {
              error.classList.add('error')
              error.innerHTML = 'passwords do not match'
            }


          })

          this.container.appendChild(resetClone)
        }
        if (newValue === 'validate') {
          const validateClone = validationForm.content.cloneNode(true),
            validateBtn = validateClone.querySelector('#validate'),
            resendBtn = validateClone.querySelector('#resendToken'),
            email = validateClone.querySelector('#email'),
            token = validateClone.querySelector('#token'),
            spin = validateClone.querySelector('#spin'),
            spinner = validateClone.querySelector('#resendSpin'),
            data = {}


          validateBtn.addEventListener('click', event => {
            if (email.value.length === 0) {
              this.error = "Please enter an email address"
              email.focus();
              return
            }

            if (!this.validateEmail(email.value)) {
              this.error = "Please check your email address and try again"
              email.focus();
              return
            }
            data.email = email.value
            data.token = token.value
            this.dispatchEvent(new CustomEvent('validate', { bubbles: true, detail: data }))
            this.loading = true
          })
          this.container.appendChild(validateClone)
        }
        if (newValue === 'create') {
          const createClone = createForm.content.cloneNode(true),
            createBtn = createClone.querySelector('#create'),
            returnBtn = createClone.querySelector('#back'),
            email = createClone.querySelector('#email'),
            password = createClone.querySelector('#password'),
            confirm = createClone.querySelector('#confirm'),
            firstName = createClone.querySelector('#first'),
            lastName = createClone.querySelector('#last'),
            error = createClone.querySelector('#error'),
            spinner = createClone.querySelector('#spin'),
            toggle = createClone.querySelector('pearson-toggle'),
            data = {};

          createBtn.addEventListener('click', event => {
            console.log(toggle.on)
            if (email.value.length === 0) {
              this.error = "Please enter an email address"
              email.focus();
              return
            }
            if (!this.validateEmail(email.value)) {
              this.error = "Please check your email address and try again"
              email.focus();
              return
            }
            if (password.value.length === 0) {
              this.error = "Please enter a password"
              password.focus();
              return
            }
            if (password.value !== confirm.value) {
              this.error = "Passwords don't match"
              password.focus();
              return
            }
            if (firstName.value.length === 0) {
              this.error = "Please enter your first name"
              firstName.focus();
              return
            }
            if (lastName.value.length === 0) {
              this.error = "Please enter your last name"
              lastName.focus();
              return
            }
            if (toggle.on === 0) {
              this.error = "You must agree to the terms of our policies"
              toggle.focus();
              return
            }

            event.stopPropagation();
            data.email = email.value
            data.password = password.value
            data.confirm = confirm.value
            data.firstName = firstName.value
            data.lastName = lastName.value
            this.dispatchEvent(new CustomEvent('create', { bubbles: true, detail: data }))
            this.loading = true

          })

          returnBtn.addEventListener('click', event => {
            event.stopPropagation();
            this.status = 'login'
          })

          this.container.appendChild(createClone)
        }
      }
    }
  }
  customElements.define('pearson-login', Login);
})(window, document);
