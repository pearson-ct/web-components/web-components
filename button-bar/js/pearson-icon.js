var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function (w, doc) {
  'use strict';

  doc.addEventListener("DOMContentLoaded", function () {
    if (!document.getElementById('pe-icons-sprite')) {
      var pe_ajax = new XMLHttpRequest();
      pe_ajax.open("GET", "https://pearsonux.sfo2.cdn.digitaloceanspaces.com/css/p-icons-sprite-1.1.svg", true);
      pe_ajax.responseType = "document";
      pe_ajax.onload = function (e) {
        document.body.insertBefore(pe_ajax.responseXML.documentElement, document.body.childNodes[0]);
        var events = new Event('iconsLoaded');
        document.dispatchEvent(events);
      };
      pe_ajax.send();
    }
  });
  // Create a template element
  var template = doc.createElement('template');

  template.innerHTML = ' \n    <style>\n:host .icon-18{width:18px;height:18px}:host .icon-24{width:24px;height:24px}\n</style>\n    <svg focusable="false" aria-hidden="true">\n\n    </svg>\n';

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-icon');

  var Icon = function (_HTMLElement) {
    _inherits(Icon, _HTMLElement);

    _createClass(Icon, [{
      key: 'icon',
      get: function get() {
        return this.getAttribute('icon');
      },
      set: function set(value) {
        this.setAttribute('icon', value);
      }
    }, {
      key: 'size',
      get: function get() {
        return this.getAttribute('size');
      },
      set: function set(number) {
        this.setAttribute('size', number);
      }
    }], [{
      key: 'observedAttributes',
      get: function get() {
        return ['icon', 'size'];
      }
    }]);

    function Icon() {
      _classCallCheck(this, Icon);

      var _this = _possibleConstructorReturn(this, (Icon.__proto__ || Object.getPrototypeOf(Icon)).call(this));

      _this.attachShadow({ mode: 'open' });
      var clone = template.content.cloneNode(true);
      _this.svg = clone.querySelector('svg');
      _this.use = clone.querySelector('use');
      _this.shadowRoot.appendChild(clone);
      return _this;
    }

    _createClass(Icon, [{
      key: 'ieBuildIcon',
      value: function ieBuildIcon() {
        var _this2 = this;

        var svgSprite = document.getElementsByTagName('svg')[0].childNodes,
            svgNode = this.shadowRoot.querySelector('svg');

        svgSprite.forEach(function (icon) {
          if (icon.nodeName !== '#text') {
            if (icon.id === _this2.iconName) {
              icon.childNodes.forEach(function (node) {
                if (node.nodeName !== '#text') {
                  _this2.svg.appendChild(node.cloneNode(true));
                }
              });
            }
          }
        });
      }
    }, {
      key: 'attributeChangedCallback',
      value: function attributeChangedCallback(name, prevValue, newValue) {
        var _this3 = this;

        if (name === 'icon' || name === 'size') {
          if (prevValue !== newValue) {
            var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
            this.iconName = this.icon + '-' + this.size;
            if (!document.getElementById('pe-icons-sprite')) {
              document.addEventListener('iconsLoaded', function (event) {
                if (isIE11) {
                  _this3.ieBuildIcon();
                } else {
                  var svgNode = document.querySelector('#' + _this3.iconName);
                  _this3.svg.innerHTML = svgNode.innerHTML;
                  _this3.svg.classList.add('icon-' + _this3.size);
                }
              });
            } else {
              if (isIE11) {
                var svgSprite = document.getElementsByTagName('svg')[0].childNodes;
                if (svgSprite) {
                  while (this.svg.firstChild) {
                    this.svg.removeChild(this.svg.firstChild);
                  }
                  this.ieBuildIcon();
                }
              } else {
                var svgNode = document.querySelector('#' + this.iconName);
                if (svgNode) {
                  this.svg.innerHTML = svgNode.innerHTML;
                  this.svg.classList.add('icon-' + this.size);
                }
              }
            }
          }
        }
      }
    }]);

    return Icon;
  }(HTMLElement);

  customElements.define('pearson-icon', Icon);
})(window, document);