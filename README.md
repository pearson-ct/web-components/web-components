# web-components
Pearson's shared component library.  Please read the documentation for installation and usage.

[https://ux.pearson.com/web-components/](https://ux.pearson.com/web-components/)

