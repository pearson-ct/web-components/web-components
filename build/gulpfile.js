const autoprefixer = require('autoprefixer');
const babel = require('gulp-babel');
const server = require('browser-sync').create();
const concat = require('gulp-concat');
const cssnano = require('cssnano');
const gulp = require('gulp');
const postcss = require('gulp-postcss');
const sass = require('sass'); // Use sass directly
const awspublish = require("gulp-awspublish");
const path = require('path');
const through = require('through2'); // Import through2

const rename = require('gulp-rename');
require('dotenv').config();

const NEW_S3_DIRECTORY = 'components';

// Make a collection of paths used by the various build steps
const paths = {
  html: './*.html',
  scripts: ['./js/**/*.js', '!**/dist/*.js'],
  styles: './scss/**/*.scss',
  dist: './js/dist',
  ignore: './js/dist',
};

function compileSass() {
  return through.obj(function (file, enc, cb) {
    if (file.isBuffer()) {
      const result = sass.renderSync({ file: file.path });
      file.contents = result.css;
      cb(null, file);
    } else {
      cb(null, file);
    }
  });
}

function styles() {
  return gulp
      .src(paths.styles)
      .pipe(compileSass()) // Compile Sass directly
      .pipe(postcss([autoprefixer({ cascade: false }), cssnano()]))
      .pipe(concat('style.css'))
      .pipe(gulp.dest('./css'))
      .pipe(server.stream());
}

function scripts() {
  return gulp
      .src(paths.scripts)
      .pipe(
          babel({
            presets: [['env', { modules: false }]]
          })
      )
      .pipe(gulp.dest(paths.dist));
}

function reload(done) {
  server.reload();
  done();
}

function serve(done) {
  server.init({
    server: {
      baseDir: './'
    },
    notify: false
  });
  done();
}

function watch() {
  const opts = { ignored: paths.ignore };

  gulp.watch(paths.styles, styles);
  gulp.watch(paths.scripts, opts, gulp.series(scripts, reload));
  gulp.watch(paths.html, reload);
}

const build = gulp.series(styles, scripts);

exports.build = build;
exports.serve = serve;
exports.watch = watch;

exports.default = gulp.series(build, serve, watch);

gulp.task("publish", function() {
  var publisher = awspublish.create({
    region: "sfo2",
    endpoint: "sfo2.digitaloceanspaces.com",
    params: {
      Bucket: "pearsonux"
    },
    accessKeyId: process.env.S3_KEY,
    secretAccessKey: process.env.S3_SECRET
  });

  const headers = {
    "Cache-Control": "max-age=315360000, no-transform, public"
  };

  return gulp
      .src("./pearson-web-components-min.js")
      .pipe(rename(function(filePath) {
        filePath.dirname = path.join(NEW_S3_DIRECTORY, filePath.dirname);
      }))
      .pipe(awspublish.gzip({ ext: ".gz" }))
      .pipe(publisher.publish(headers))
      .pipe(publisher.cache())
      .pipe(awspublish.reporter());
});
