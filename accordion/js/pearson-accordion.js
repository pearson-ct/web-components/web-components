
(function(w, doc) {
  'use strict';

  // Create a template element
  const template = doc.createElement('template'),
    buttonTemplate = doc.createElement('template'),
    panelTemplate = doc.createElement('template');

  //Styles must be copied from the css file
  // and pasted between the style tags below

  // Other markup should be written after the closing tag.

  template.innerHTML = ` 
 <style>
:host .icon-18 {width:18px; height:18px;}; :host .accordion{border:1px solid #c7c7c7;grid-column:span 12;border-radius:4px}:host .accordion h3{border-bottom:1px solid #c7c7c7;margin:0}:host .accordion h3 button{background-color:#fff;width:100%;border:0;text-align:left;padding:16px; cursor:pointer}:host .accordion h3 button[aria-expanded=false] .collapse{display:none}:host .accordion h3 button[aria-expanded=false] .expand,:host .accordion h3 button[aria-expanded=true] .collapse{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}:host .accordion h3 button[aria-expanded=true] .collapse{margin-left:16px}:host .accordion h3 button[aria-expanded=true] .expand{display:none;margin-left:16px}:host .accordion h3 button:focus{background-color:#efefef;z-index:10000;position:relative;outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .accordion h3 .accordion-title{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between}:host .accordion>div{padding:16px}:host .accordion .accordion-panel{display:none;border-bottom:1px solid #c7c7c7;background:#fff}:host .accordion .accordion-panel:last-child{border-bottom:0}:host .accordion button[aria-expanded=false]+.accordion{display:none}:host .accordion{border-radius:0}:host .accordion button:active{color:#24292e}:host .content .panel{word-break:break-all}</style>
    <div id="accordionGroup" class="accordion" part="container">  
    </div>
`,

    buttonTemplate.innerHTML = `
      <h3 part="h3">
        <button aria-expanded="false" class="accordion-trigger" aria-controls="sect3" id="accordion1id3" data-index="2" part="button">
          <span class="accordion-title" part="title">
            <span class="button-label" part="label">
               <slot name="buttons"></slot>
            </span>
            <svg focusable="false" class="icon-18 expand" aria-hidden="true" part="expand">
                <path d="M9.62193909,12.7616134 C9.25409223,13.0918069 8.69027111,13.0789828 8.33764681,12.7231411 L3.27435567,7.61365203 C2.90854811,7.24450681 2.90854811,6.64600414 3.27435567,6.27685892 C3.64016324,5.90771369 4.23325448,5.90771369 4.59906205,6.27685892 L9,10.7179514 L13.400938,6.27685892 C13.7667455,5.90771369 14.3598368,5.90771369 14.7256443,6.27685892 C15.0914519,6.64600414 15.0914519,7.24450681 14.7256443,7.61365203 L9.66235319,12.7231411 C9.64896608,12.7366503 9.63548354,12.7494543 9.62191255,12.7615685 L9.62193909,12.7616134 Z" fill-rule="nonzero"></path>
            </svg>
            <svg focusable="false" class="icon-18 collapse" aria-hidden="true" part="collapse">
                <path d="M9.62193909,5.2383866 L9.62191255,5.23843148 C9.63548354,5.25054567 9.64896608,5.26334967 9.66235319,5.27685892 L14.7256443,10.386348 C15.0914519,10.7554932 15.0914519,11.3539959 14.7256443,11.7231411 C14.3598368,12.0922863 13.7667455,12.0922863 13.400938,11.7231411 L9,7.28204859 L4.59906205,11.7231411 C4.23325448,12.0922863 3.64016324,12.0922863 3.27435567,11.7231411 C2.90854811,11.3539959 2.90854811,10.7554932 3.27435567,10.386348 L8.33764681,5.27685892 C8.69027111,4.92101724 9.25409223,4.90819314 9.62193909,5.2383866 Z"></path>
            </svg>
          </span>
        </button>
      </h3>
    `,

    panelTemplate.innerHTML = `
			<div id="sect3" role="region" aria-labelledby="accordion1id3" class="accordion-panel animateIn" part="panel" style="display:none">
				<div class="content" part="content">
					  <slot name="panels"></slot>
				</div>
			</div>
    `;


  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-accordion');

  /** Any helper functions that do not need to be part of the class
   * can be declared here, before the class is defined.
   */

  class Accordion extends HTMLElement {
    static get observedAttributes() {
      return ['multiple'];
    }

    get multiple() {
      return this.hasAttribute('multiple');
    }

    constructor() {
      super();
      this.attachShadow({ mode: 'open' });
      const clone = template.content.cloneNode(true),
        buttonClone = buttonTemplate.content.cloneNode(true),
        panelClone = panelTemplate.content.cloneNode(true);

      this.target = clone.querySelector('#accordionGroup');
      this.buttonSlot = buttonClone.querySelector('slot[name="buttons"]');
      this.panelSlot = panelClone.querySelector('slot[name="panels"]');
      this.panelSlotToRemove = clone.querySelector('.accordion > div')

      if (this.buttonSlot !== null) {
        this.buttonSlot.addEventListener('slotchange', event => {
          const panelSlotToRemove = this.shadowRoot.querySelector('.accordion > div'),
            panelContainer = this.panelSlot.assignedNodes()[0],
            buttonSlotToRemove = this.shadowRoot.querySelector('.accordion h3'),
            triggers = this.shadowRoot.querySelectorAll('.accordion-trigger');

          if (panelContainer) {
            const panels = panelContainer.querySelectorAll('.panel'),
              ul = this.buttonSlot.assignedNodes()[0],
              buttons = ul.querySelectorAll('li');

            buttons.forEach((button,index) => {
              this.target.appendChild(this.renderButtons(button.innerHTML, index, buttons.length));
              this.target.appendChild(this.renderPanels(panels, index, button));
            });
            panelSlotToRemove.remove();
            panelContainer.remove();
            ul.remove();
            buttonSlotToRemove.remove();
          }

          triggers.forEach(trigger => {
            trigger.addEventListener('keydown', event => {
              const nextButton = parseInt(event.target.getAttribute('data-index')) + 1,
                prevButton = parseInt(event.target.getAttribute('data-index')) - 1,
                firstTrigger = triggers[0],
                lastTrigger = triggers[triggers.length - 1];

              if (event.key === 'ArrowUp') {
                event.preventDefault();
                if (this.shadowRoot.activeElement === firstTrigger) {
                  lastTrigger.focus();
                } else {
                  triggers[prevButton].focus();
                }
              }

              if (event.key === 'ArrowDown') {
                event.preventDefault();
                if (this.shadowRoot.activeElement === lastTrigger) {
                  firstTrigger.focus();
                } else {
                  triggers[nextButton].focus();
                }
              }

              if (event.key === 'Home') {
                firstTrigger.focus();
              }

              if (event.key === 'End') {
                lastTrigger.focus();
              }

            })
          })
        })
      }


      this.shadowRoot.appendChild(clone);
      buttonClone.appendChild(panelClone);
      this.target.appendChild(buttonClone);

      this.renderButtons = this.renderButtons.bind(this);
      this.renderPanels = this.renderPanels.bind(this);
    }

    renderButtons (text, index, number) {
      const buttonClone = buttonTemplate.content.cloneNode(true),
        label = buttonClone.querySelector('.button-label'),
        button = label.parentNode.parentNode,
        length = number -1;

      button.setAttribute('data-index', index);
      button.setAttribute('aria-controls', 'panel'+index);
      button.setAttribute('id', 'accordionId' + index);

      label.innerHTML = text;
      if (index === length) {
       button.parentNode.style.border = 0;
      }

      button.addEventListener('click', event => {
        const button = event.currentTarget,
          isExpanded = button.getAttribute('aria-expanded'),
          currentPanel = event.currentTarget.parentNode.nextElementSibling;

        if (this.multiple) {
          if (isExpanded === 'false') {
            button.setAttribute('aria-expanded', true);
            currentPanel.style.display = 'flex';
            if (index === length) {
              button.parentNode.style.borderBottom = '1px solid #c7c7c7';
            }
          } else {
            button.setAttribute('aria-expanded', false);
            currentPanel.style.display = 'none';
            if (index === length) {
              button.parentNode.style.border = 0;
            }
          }
        } else {
          const allButtons = this.shadowRoot.querySelectorAll('.accordion-trigger'),
            allPanels = this.shadowRoot.querySelectorAll('.accordion-panel');

          if (isExpanded === 'false') {
            allButtons.forEach(button => {
              button.setAttribute('aria-expanded', false);
            });
            allPanels.forEach(panel => {
              panel.style.display = 'none';
            });

            button.setAttribute('aria-expanded', true);
            currentPanel.style.display = 'flex';
            if (index === length) {
              button.parentNode.style.borderBottom = '1px solid #c7c7c7';
            }
          } else {
            button.setAttribute('aria-expanded', false);
            currentPanel.style.display = 'none';
            if (index === length) {
              button.parentNode.style.border = 0;
            }
          }
        }
      });

      return buttonClone
    }

    renderPanels (panels, index, button) {
      const panelClone = panelTemplate.content.cloneNode(true),
        panel = panelClone.querySelector('.accordion-panel'),
        target = panelClone.querySelector('.content');

      panel.setAttribute('id', 'panel' + index);
      panel.setAttribute('aria-labelledby', 'accordianId' + index);
      target.appendChild(panels[index]);
      return panelClone
    }

  }
  customElements.define('pearson-accordion', Accordion);
})(window, document);
// from:https://github.com/jserz/js_piece/blob/master/DOM/ChildNode/remove()/remove().md
(function (arr) {
  arr.forEach(function (item) {
    if (item.hasOwnProperty('remove')) {
      return;
    }
    Object.defineProperty(item, 'remove', {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function remove() {
        if (this.parentNode === null) {
          return;
        }
        this.parentNode.removeChild(this);
      }
    });
  });
})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);
