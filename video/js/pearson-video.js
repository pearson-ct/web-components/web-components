(function(win, doc) {
  'use strict';

  const template = doc.createElement('template'),
    videoTemplate = doc.createElement('div'),
    videoControlsTemplate = doc.createElement('template'),
    videoScrubberTemplate = doc.createElement('template'),
    menuTemplate = doc.createElement('template'),
    submenuTemplate = doc.createElement('template');


  submenuTemplate.innerHTML = `
  <ul id="submenu" role="menu" class="menu animated animateIn">
   <li role="none">
      <button id="previous" class="playbackoptionsF">
            <pearson-icon icon="previous" size="18" style="color:black"></pearson-icon>
         <span id="speedText" class="playback-speed-label">Playback speed</span>
      </button>
   </li>
   <li role="none">
      <button id="slower" class="playbackoptionsF" data-speed="0.5">
      <span class="all-speed">0.5x - Slower</span>
      </button>
   </li>
   <li role="none">
      <button id="slow" class="playbackoptionsF" data-speed="0.75">
      <span class="all-speed">0.75x - Slow</span>
      </button>
   </li>
   <li role="none">
      <button id="normal" class="playbackoptionsF activeSO" data-speed="1.0">
      <span class="all-speed">1x - Normal</span>
      </button>
   </li>
   <li role="none">
      <button id="fast" class="playbackoptionsF" data-speed="1.5">
      <span class="all-speed">1.5x - Fast</span>
      </button>
   </li>
   <li role="none">
      <button id="faster" class="playbackoptionsF" data-speed="2.0">
      <span class="all-speed">2x - Faster</span>
      </button>
   </li>
</ul>

  `
  menuTemplate.innerHTML = `
    <ul id="menu" role="menu" class="animated animateIn">
     <li role="none">
        <button id="trigger-playback" class="menuitemoptionsF playback-speed-normal" role="menuitemradio" aria-checked="false">
           <span class="playback-lft">Playback speed</span>
           <span class="playback-rt">
              <span class="speed-next-option"></span>
              <pearson-icon icon="next" size="18" style="color:black"></pearson-icon>
           </span>
        </button>
     </li>
     <li role="none" class="captions-optionF caption-btn-option">
        <div class="toggle-container captions-toggle-refactor">
           <label for="toggle" id="captions-toggle-label" class="gr-label captions-toggle-refactor">Subtitles/CC</label>
           <pearson-toggle id="toggle"> </pearson-toggle>
        </div>
     </li>
  </ul>

  `
  videoScrubberTemplate.innerHTML = `

  <div class="row02controls">
     <canvas id="played" class="played" height="16"></canvas>
     <canvas id="buffer" height="5"></canvas>
     <input aria-label="Current Time" type="range" id="seekbar" value="0" aria-valuetext="00:00" step="0.1" max="100">
  </div>
  `

  videoControlsTemplate.innerHTML = `
  <div class="row01controls">
   <div class="row01-left">
   
        <button id="pause" aria-label="Pause Video" type="button" class="gr-btn icon-btn-24">
           <pearson-icon icon="pause" size="24"></pearson-icon>
        </button>
 
      <div class="time">
         <span id="curtime">00:00</span> <span> / </span> <span id="durtime">00:42</span>   
      </div>
   </div>
   <div class="row01-right">
      <div id="volumeContainer" class="volume-container">
         <input id="volume" class="volume-slider animated" type="range" min="0" max="100" value="100" step="10" style="display: none">
         <button aria-expanded="false" id="mute" aria-label="Mute" type="button" class="gr-btn icon-btn-24">
            <pearson-icon icon="audio-high" size="24"></pearson-icon>
         </button>
      </div>
      <button aria-label="Fullscreen" id="fullscreen" type="button" class="gr-btn icon-btn-24">
         <pearson-icon icon="fullscreen-on" size="24"></pearson-icon>
      </button>
      <details>
         <summary>
            <button id="settings" aria-label="Settings" aria-expanded="false" aria-labelledby="videosettingslist" class="gr-btn icon-btn-24">
               <pearson-icon icon="settings" size="24"></pearson-icon>
            </button>
         </summary>
         <div id="settingslistF" class="settings-list-container">
         
         </div>
      </details>
   </div>
</div>

  `

  videoTemplate.innerHTML = `
 <video id="video" preload="metadata" playsinline="" controlsList="nodownload">
    <source id="src" type="video/mp4">
    <track label="English Captions" srclang="en" kind="captions"  default="">
    Your browser does not support the HTML5 &lt;video&gt; tag.
 </video>
  `
  videoTemplate.style.width = '100%'
  videoTemplate.style.display = 'flex'
  videoTemplate.style.alignItems = 'flex-start'
  template.innerHTML = ` 
    <style>

@import url("https://fonts.googleapis.com/css?family=Open+Sans:400,600");@import url("https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;600&display=swap");:host{
  /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */}:host html{line-height:1.15;-webkit-text-size-adjust:100%}:host body{margin:0}:host main{display:block}:host h1{font-size:2em;margin:.67em 0}:host hr{-webkit-box-sizing:content-box;box-sizing:content-box;height:0;overflow:visible}:host pre{font-family:monospace,monospace;font-size:1em}:host a{background-color:transparent}:host abbr[title]{border-bottom:none;text-decoration:underline;-webkit-text-decoration:underline dotted;text-decoration:underline dotted}:host b,:host strong{font-weight:bolder}:host code,:host kbd,:host samp{font-family:monospace,monospace;font-size:1em}:host small{font-size:80%}:host sub,:host sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}:host sub{bottom:-.25em}:host sup{top:-.5em}:host img{border-style:none}:host button,:host input,:host optgroup,:host select,:host textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}:host button,:host input{overflow:visible}:host button,:host select{text-transform:none}:host [type=button],:host [type=reset],:host [type=submit],:host button{-webkit-appearance:button}:host [type=button]::-moz-focus-inner,:host [type=reset]::-moz-focus-inner,:host [type=submit]::-moz-focus-inner,:host button::-moz-focus-inner{border-style:none;padding:0}:host [type=button]:-moz-focusring,:host [type=reset]:-moz-focusring,:host [type=submit]:-moz-focusring,:host button:-moz-focusring{outline:1px dotted ButtonText}:host fieldset{padding:.35em .75em .625em}:host legend{-webkit-box-sizing:border-box;box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}:host progress{vertical-align:baseline}:host textarea{overflow:auto}:host [type=checkbox],:host [type=radio]{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0}:host [type=number]::-webkit-inner-spin-button,:host [type=number]::-webkit-outer-spin-button{height:auto}:host [type=search]{-webkit-appearance:textfield;outline-offset:-2px}:host [type=search]::-webkit-search-decoration{-webkit-appearance:none}:host ::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}:host details{display:block}:host summary{display:list-item}:host [hidden],:host template{display:none}:host html{font-size:14px}:host *,:host html{-webkit-box-sizing:border-box;box-sizing:border-box}:host body{font-family:Open Sans,Arial,Helvetica,sans-serif}:host body,:host p{font-size:14px;line-height:1.5;font-weight:400}:host strong{font-weight:600}:host a{font-size:14px;color:#047a9c}:host a:hover{color:#03536a;text-decoration:none}:host a:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host button{cursor:pointer}:host li,:host ul{font-size:14px}:host svg{fill:#252525}:host svg[focusable=false]:focus{outline:none}:host select{cursor:pointer}:host input,:host textarea{font:inherit;letter-spacing:inherit;word-spacing:inherit}:host svg{pointer-events:none}@-webkit-keyframes shift{to{background-position:9px 9px}}@keyframes shift{to{background-position:9px 9px}}@-webkit-keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@-webkit-keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@-webkit-keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@-webkit-keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@-webkit-keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@-webkit-keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@-webkit-keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}@keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}:host .fadeIn,:host .slideInDown{opacity:1!important;visibility:visible!important}:host .fadeOut,:host .slideOutDown{opacity:0;visibility:hidden}:host .slideInDown{-webkit-animation:slideInDown .3s ease-in-out 0s;animation:slideInDown .3s ease-in-out 0s}:host .slideOutDown{-webkit-animation:slideOutDown .2s ease-in 0s;animation:slideOutDown .2s ease-in 0s}:host .fadeIn{-webkit-animation:fadeIn .3s linear 0s;animation:fadeIn .3s linear 0s}:host .fadeOut{-webkit-animation:fadeOut .2s linear 0s;animation:fadeOut .2s linear 0s}:host .animateIn{-webkit-animation-name:fadeIn;animation-name:fadeIn;-webkit-animation-duration:.5s;animation-duration:.5s}:host .animateOut{-webkit-animation-name:fadeOut;animation-name:fadeOut;-webkit-animation-duration:.2s;animation-duration:.2s}@media (prefers-reduced-motion){:host .animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}}:host html[data-prefers-reduced-motion] .animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}:host .no-border{border:0}:host .icon-18{width:18px;height:18px}:host .icon-24{width:24px;height:24px}:host .hidden{display:none!important}:host pearson-alert{font-size:16px;max-width:580px}:host pearson-alert .alert-title{font-size:14px;margin:0;display:inline;top:0}:host pearson-uploader .alert-title{top:0!important}:host pearson-alert .alert-text{margin:0;display:inline}:host pearson-footer{left:50%;margin-left:-50%;right:50%;margin-right:-50%}:host pearson-header{grid-column:span 12}:host pearson-tabs{font-size:14px}:host pearson-progress-bar{grid-column:1/5}@media (min-width:591px){:host pearson-progress-bar{grid-column:1/9}}@media (min-width:887px){:host pearson-progress-bar{grid-column:1/13}}:host pearson-tabs{grid-column:1/5}@media (min-width:591px){:host pearson-tabs{grid-column:1/9}}@media (min-width:887px){:host pearson-tabs{grid-column:1/13}}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host pearson-card{-ms-grid-column-span:3}:host pearson-card,:host pearson-card[stacked]{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;width:100%}:host pearson-card[stacked]{-ms-grid-column-span:12}}:host pearson-accordion{grid-column:1/5}@media (min-width:591px){:host pearson-accordion{grid-column:1/9}}@media (min-width:887px){:host pearson-accordion{grid-column:1/13}}:host #main{max-width:1280px;margin:0 auto}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host iframe .gr-grid-container{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}}:host .gr-grid-container{display:grid;display:-ms-grid;grid-template-columns:repeat(4,1fr);grid-column-gap:16px;grid-row-gap:16px;margin:0 39.5px}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host .gr-grid-container.ie-flex{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}}@media (min-width:351px){:host .gr-grid-container{margin:0 31.5px}}@media (min-width:399px){:host .gr-grid-container{margin:0 39.5px}}@media (min-width:447px){:host .gr-grid-container{margin:0 79.5px}}@media (min-width:591px){:host .gr-grid-container{grid-template-columns:repeat(8,1fr);margin:0 83.5px}}@media (min-width:727px){:host .gr-grid-container{margin:0 103.5px;grid-column-gap:24px;grid-row-gap:24px}}@media (min-width:887px){:host .gr-grid-container{grid-template-columns:repeat(12,1fr);margin:0 71.5px}}@media (min-width:887px) and (-ms-high-contrast:active),(min-width:887px) and (-ms-high-contrast:none){:host .gr-grid-container>*{margin-right:12px;margin-left:12px;margin-bottom:24px}}@media (min-width:983px){:host .gr-grid-container{margin:0 71.5px}}@media (min-width:1079px){:host .gr-grid-container{margin:0 71.5px}}@media (min-width:1175px){:host .gr-grid-container{margin:0 76px}}:host .gr-grid-container .gr-col-two{grid-column-start:2}:host .gr-grid-container .gr-col-three{grid-column-start:3}:host .gr-grid-container .gr-col-four{grid-column-start:4}:host .gr-grid-container .gr-col-five{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-five{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:5}}:host .gr-grid-container .gr-col-six{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-six{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:6}}:host .gr-grid-container .gr-col-seven{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-seven{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:7}}:host .gr-grid-container .gr-col-eight{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-eight{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:8}}:host .gr-grid-container .gr-col-nine{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-nine{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:9}}:host .gr-grid-container .gr-col-ten{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-ten{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:10}}:host .gr-grid-container .gr-col-eleven{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-eleven{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:11}}:host .gr-grid-container .gr-col-twelve{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-twelve{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:12}}:host .gr-col-span-one{grid-column-end:span 1}:host .gr-col-span-two{grid-column-end:span 2}:host .gr-col-span-three{grid-column-end:span 3}:host .gr-col-span-four{grid-column-end:span 4}@media (min-width:591px){:host .gr-col-span-five{grid-column-end:span 5}}@media (min-width:591px){:host .gr-col-span-six{grid-column-end:span 6}}@media (min-width:591px){:host .gr-col-span-seven{grid-column-end:span 7}}@media (min-width:591px){:host .gr-col-span-eight{grid-column-end:span 8}}@media (min-width:887px){:host .gr-col-span-nine{grid-column-end:span 9}}@media (min-width:887px){:host .gr-col-span-ten{grid-column-end:span 10}}@media (min-width:887px){:host .gr-col-span-eleven{grid-column-end:span 11}}:host .gr-col-span-twelve{grid-column-end:span 4}@media (min-width:591px){:host .gr-col-span-twelve{grid-column-end:span 8}}@media (min-width:887px){:host .gr-col-span-twelve{grid-column-end:span 12}}:host .gr-row-one{grid-row-start:1}:host .gr-row-two{grid-row-start:2}:host .gr-row-three{grid-row-start:3}:host .gr-row-four{grid-row-start:4}:host .gr-row-five{grid-row-start:5}:host .gr-row-six{grid-row-start:6}:host .gr-row-seven{grid-row-start:7}:host .gr-row-eight{grid-row-start:8}:host .gr-row-nine{grid-row-start:9}:host .gr-row-ten{grid-row-start:10}:host .gr-row-eleven{grid-row-start:11}:host .gr-row-twelve{grid-row-start:12}:host .gr-row-thirteen{grid-row-start:13}:host .gr-row-fourteen{grid-row-start:14}:host .gr-row-fifteen{grid-row-start:15}:host .gr-row-span-two{grid-row-end:span 2}:host .gr-row-span-three{grid-row-end:span 3}:host .gr-row-span-four{grid-row-end:span 4}:host .gr-row-span-five{grid-row-end:span 5}:host .gr-row-span-six{grid-row-end:span 6}:host .gr-row-span-seven{grid-row-end:span 7}:host .gr-row-span-eight{grid-row-end:span 8}:host .gr-row-span-nine{grid-row-end:span 9}:host .gr-row-span-ten{grid-row-end:span 10}:host .gr-row-span-eleven{grid-row-end:span 11}:host .gr-row-span-twelve{grid-row-end:span 12}:host .gr-primary{color:#047a9c;fill:#047a9c}:host .gr-secondary{color:#ffb81c;fill:#ffb81c}:host .gr-white{color:#fff;fill:#fff}:host .gr-neutral-high-one{color:#252525;fill:#252525}:host .gr-neutral-high-two{color:#6a7070;fill:#6a7070}:host .gr-neutral-med-one{color:#a9a9a9;fill:#a9a9a9}:host .gr-neutral-med-two{color:#c7c7c7;fill:#c7c7c7}:host .gr-neutral-med-three{color:#d9d9d9;fill:#d9d9d9}:host .gr-neutral-med-four{color:#e9e9e9;fill:#e9e9e9}:host .gr-neutral-light-one,:host .gr-neutral-light-two{color:#eee;fill:#eee}:host .gr-condition-one{color:#db0020;fill:#db0020}:host .gr-condition-two{color:#038238;fill:#038238}:host .gr-condition-three{color:#da0474;fill:#da0474}:host .gr-theme-one-light{color:#caefee;fill:#caefee}:host .gr-theme-one-med{color:#76d5d4;fill:#76d5d4}:host .gr-theme-one-dark{color:#19a5a3}:host .gr-theme-two-light{color:#f2e5f1;fill:#f2e5f1}:host .gr-theme-two-med{color:#895b9a;fill:#895b9a}:host .gr-theme-two-dark{color:#633673;fill:#633673}:host .gr-theme-three-light{color:#f6f8cc;fill:#f6f8cc}:host .gr-theme-three-med{color:#d2db0e;fill:#d2db0e}:host .gr-theme-three-dark{color:#b0b718;fill:#b0b718}:host .gr-theme-four-light{color:#d9e6f1;fill:#d9e6f1}:host .gr-theme-four-med{color:#356286;fill:#356286}:host .gr-theme-four-dark{color:#1e496c;fill:#356286}:host .gr-theme-five-light{color:#dff5d5;fill:#dff5d5}:host .gr-theme-five-med{color:#66be3e;fill:#66be3e}:host .gr-theme-five-dark{color:#288500;fill:#288500}:host .gr-theme-six-light{color:#d6ecf4;fill:#d6ecf4}:host .gr-theme-six-med{color:#80c5dd;fill:#80c5dd}:host .gr-theme-six-dark{color:#46a9cb;fill:#46a9cb}:host .gr-theme-seven-light{color:#faebc3;fill:#faebc3}:host .gr-theme-seven-med{color:#f5c54c;fill:#f5c54c}:host .gr-theme-seven-dark{color:#dea30d;fill:#dea30d}:host .gr-primary-bg{background-color:#047a9c}:host .gr-secondary-bg{background-color:#ffb81c}:host .gr-white-bg{background-color:#fff}:host .gr-neutral-high-one-bg{background-color:#252525}:host .gr-neutral-high-two-bg{background-color:#6a7070}:host .gr-neutral-med-one-bg{background-color:#a9a9a9}:host .gr-neutral-med-two-bg{background-color:#c7c7c7}:host .gr-neutral-med-three-bg{background-color:#d9d9d9}:host .gr-neutral-med-four-bg{background-color:#e9e9e9}:host .gr-neutral-light-one-bg,:host .gr-neutral-light-two-bg{background-color:#eee}:host .gr-condition-one-bg{background-color:#db0020}:host .gr-condition-two-bg{background-color:#038238}:host .gr-condition-three-bg{background-color:#da0474}:host .gr-theme-one-light-bg{background-color:#caefee}:host .gr-theme-one-med-bg{background-color:#76d5d4}:host .gr-theme-one-dark-bg{background-color:#19a5a3}:host .gr-theme-two-light-bg{background-color:#f2e5f1}:host .gr-theme-two-med-bg{background-color:#895b9a}:host .gr-theme-two-dark-bg{background-color:#633673}:host .gr-theme-three-light-bg{background-color:#f6f8cc}:host .gr-theme-three-med-bg{background-color:#d2db0e}:host .gr-theme-three-dark-bg{background-color:#b0b718}:host .gr-theme-four-light-bg{background-color:#d9e6f1}:host .gr-theme-four-med-bg{background-color:#356286}:host .gr-theme-four-dark-bg{background-color:#1e496c}:host .gr-theme-five-light-bg{background-color:#dff5d5}:host .gr-theme-five-med-bg{background-color:#66be3e}:host .gr-theme-five-dark-bg{background-color:#288500}:host .gr-theme-six-light-bg{background-color:#d6ecf4}:host .gr-theme-six-med-bg{background-color:#80c5dd}:host .gr-theme-six-dark-bg{background-color:#46a9cb}:host .gr-theme-seven-light-bg{background-color:#faebc3}:host .gr-theme-seven-med-bg{background-color:#f5c54c}:host .gr-theme-seven-dark-bg{background-color:#dea30d}:host .gr-h1{font-size:24px;line-height:28px}:host .gr-h1,:host .gr-h2{font-weight:400;margin-top:0}:host .gr-h2{font-size:20px;line-height:26px}:host .gr-h3{font-size:18px;line-height:24px;font-weight:400;margin-top:0}:host .gr-label{font-size:12px;line-height:16px;color:#6a7070;display:block;margin-bottom:4px}:host .gr-meta{font-size:12px;line-height:12px;color:#6a7070}:host .gr-semi-bold{font-weight:600}:host .gr-font-large{font-size:16px;line-height:24px}:host .gr-font-normal{font-size:14px;line-height:20px}:host .gr-btn{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;border:1px solid #6a7070;background:transparent;color:#6a7070;min-width:152px;padding:8px 24px;font-size:16px;cursor:pointer;border-radius:22px;position:relative;margin:12px;line-height:1.15}:host .gr-btn:hover{color:#252525;border:1px solid #252525}:host .gr-btn:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host a.gr-btn{text-decoration:none}:host .gr-btn.primary{background-color:#047a9c;border:1px solid #047a9c;color:#fff}:host .gr-btn.primary:hover{color:#fff;background-color:#035f79}:host .gr-btn.attention{background-color:#ffb81c;border:1px solid #ffb81c;color:#252525}:host .gr-btn.attention:hover{background-color:#f7aa00}:host .gr-btn.small{min-width:128px;padding:7px 20px;font-size:14px}:host .gr-btn.small:focus:after{padding:18px 21px}:host .gr-btn.extra-large{min-width:152px;padding:10px 24px;font-size:16px}:host .gr-btn.extra-large:focus:after{padding:22px 21px;border-radius:24px}:host .gr-btn:disabled{background-color:#e9e9e9!important;color:#6a7070!important;border:0!important;cursor:default!important}:host .gr-btn.icon-btn-24{min-width:auto;padding:5px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-24:focus{border-radius:4px}:host .gr-btn.icon-btn-24:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-24:active svg{fill:#005a70}:host .gr-btn.icon-btn-24 svg{fill:#6a7070}:host .gr-btn.icon-btn-18{min-width:auto;padding:8px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-18:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-18:active svg{fill:#005a70}:host .gr-btn.icon-btn-18:focus{border-radius:4px}:host .gr-btn.icon-btn-18 svg{fill:#6a7070}:host .gr-btn.no-border,:host .gr-btn.no-border:hover{border:0}:host .gr-btn.no-border:focus:after{border-radius:5px;min-width:auto;left:auto;top:auto;padding:16px}:host .gr-input{display:block;margin:4px 0;padding:0 12px;height:36px;color:#252525;border:1px solid #c7c7c7;background:#fff;border-radius:4px;font-size:14px;width:100%;max-width:100%}:host .gr-input:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-input:disabled{color:#c7c7c7;background-color:#e9e9e9}:host .gr-input:-moz-read-only{border:0}:host .gr-input:read-only{border:0}:host .gr-checkbox+.error-state,:host .gr-input+.error-state,:host .gr-radio+.error-state,:host .gr-select-container+.error-state,:host .gr-textarea+.error-state{display:none}:host .gr-checkbox.error,:host .gr-input.error,:host .gr-radio.error,:host .gr-select-container.error,:host .gr-textarea.error{border-color:#db0020}:host .gr-checkbox.error,:host .gr-checkbox.error+.error-state,:host .gr-input.error,:host .gr-input.error+.error-state,:host .gr-radio.error,:host .gr-radio.error+.error-state,:host .gr-select-container.error,:host .gr-select-container.error+.error-state,:host .gr-textarea.error,:host .gr-textarea.error+.error-state{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .gr-checkbox.error+.error-state svg,:host .gr-input.error+.error-state svg,:host .gr-radio.error+.error-state svg,:host .gr-select-container.error+.error-state svg,:host .gr-textarea.error+.error-state svg{fill:#db0020}:host .gr-checkbox.error+.error-state span,:host .gr-input.error+.error-state span,:host .gr-radio.error+.error-state span,:host .gr-select-container.error+.error-state span,:host .gr-textarea.error+.error-state span{margin-left:8px;color:#db0020}:host video{width:100%;position:relative;margin:0}:host .video_title{position:absolute;width:100%;min-width:174px;background:#020024;background:-webkit-linear-gradient(330deg,#191919,rgba(50,50,50,.9) 35%,hsla(0,0%,100%,.1));background:linear-gradient(120deg,#191919,rgba(50,50,50,.9) 35%,hsla(0,0%,100%,.1));height:60px;z-index:5;vertical-align:center;top:0}:host h2{text-shadow:4px 8px 8px rgba(0,0,0,.4);color:#fafafa;font-family:Open Sans,Arial,Helvetica,sans-serif;font-size:18px;line-height:18px;text-align:left;padding:15px;margin-top:5px;font-weight:300!important}:host div#video_player_box{position:absolute;background-color:rgba(0,0,0,.85)}:host #video_controls_bar,:host div#video_player_box{width:100%;margin:0 auto;color:#fff;font-family:Open Sans,Arial,Helvetica,sans-serif;font-size:14px;font-weight:600}:host #video_controls_bar{background:-webkit-gradient(linear,left top,left bottom,color-stop(70%,rgba(37,37,37,.7)),color-stop(81.77%,rgba(37,37,37,.7)));background:-webkit-linear-gradient(top,rgba(37,37,37,.7) 70%,rgba(37,37,37,.7) 81.77%);background:linear-gradient(180deg,rgba(37,37,37,.7) 70%,rgba(37,37,37,.7) 81.77%);padding:10px;min-height:100px;bottom:4px;-webkit-box-orient:vertical;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column}:host #video_controls_bar,:host .row01controls{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-direction:normal}:host .row01controls{width:-webkit-calc(100% - 10px);width:calc(100% - 10px);margin-top:20px;-webkit-box-orient:horizontal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;vertical-align:-20px;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;z-index:2147483647}:host .row01controls:-webkit-full-screen{z-index:2147483647}:host .row01-left{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .row01-left pearson-icon{color:#fff}:host .row01-right{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row}:host .row01-right pearson-icon{color:#fff}:host .volume-container{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .volume-slider{width:90px!important;height:27px}:host .player-control-margin{margin-right:30px}:host .row02controls{width:100%}:host button{background:none;border:none;opacity:1}:host button#fullscreen:hover,:host button#mute:hover,:host button#play:hover,:host button#settingsbtn:hover,:host input[type=range]::-webkit-slider-thumb:hover{opacity:.7}:host .airplay-icon,:host .collapsescreen-icon svg,:host .fullscreen-icon svg,:host .mute-icon svg,:host .pause-icon svg,:host .pip-icon svg,:host .play-icon svg,:host .settings-icon svg,:host .volhigh-icon svg,:host .vollow-icon svg,:host .voloff-icon svg{fill:#fff;background-repeat:no-repeat;border:none;width:24px}:host .active{display:block}:host .hidden{display:none}:host .time{margin-left:25px}:host input#seekbar{width:-webkit-calc(100% - 20px);width:calc(100% - 20px);margin-left:10px}:host input[type=range]{-webkit-appearance:none!important;background:#fff;border:1px solid #666;height:6px;border-radius:4px}:host input[type=range]::-webkit-slider-thumb{-webkit-appearance:none!important;background:#fff;height:15px;width:15px;border-radius:100%;cursor:pointer}:host #volumeslider{width:80px;vertical-align:top;margin-top:10px}:host #volumeslider input[type=range][orient=vertical]{-webkit-writing-mode:bt-lr;-ms-writing-mode:bt-lr;writing-mode:bt-lr;-webkit-appearance:slider-vertical;width:8px;height:175px;padding:0 5px}:host .settings-list{position:absolute;width:255px;height:100%;min-height:103px;right:-10px;bottom:80px;z-index:200}:host .settings-list.dropdown-menu{bottom:55px}:host .settings-list.dropdown-itpro{bottom:260px}:host .menuitemoptions{-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;font-size:14px;font-weight:600;padding:15px!important;background-color:#fff;opacity:1!important}:host .menuitemoptions,:host .right-playbackmenu{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row}:host .right-playbackmenu{margin-right:20px}:host .right-playback{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row}:host .speed-caret{width:16px;height:16px;border:1px solid #252525;fill:#252525;background-color:#252525;margin-left:2px}:host .speed-caret svg{fill:#252525!important;background-repeat:no-repeat;border:none;width:24px}:host .speed-toggle{margin-right:15px}:host .captions-option{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .captions-icon{margin-left:70px}:host .playbackoptions{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:start;-webkit-justify-content:flex-start;-ms-flex-pack:start;justify-content:flex-start;font-size:14px;font-weight:600;padding:15px!important;background-color:#fff;opacity:1!important;border-bottom:1px solid grey}:host .only-playback,:host .playbackoptions span.speed-caret{margin-left:15px}:host .speedlist-select-menu{margin-left:25px}:host .speed-op{font-weight:400!important}:host details{position:relative}:host details>summary::-webkit-details-marker{display:none}:host summary{display:block;cursor:pointer}:host .settings-list-container{position:absolute;right:15px;width:252px;height:100%;min-height:103px;right:0;bottom:50px;z-index:200}:host .settings-list-container ul{background-color:#fff;border-radius:4px!important}:host .settings-list-container li{list-style:none;margin-left:-40px;padding:10px 0}:host .settings-list-container.playlist-menu{width:165px;height:225px;bottom:100px}:host .playbackoptionsF,:host button.menuitemoptionsF{width:235px;margin-left:10px;border-radius:4px}:host .menuitemoptionsF{width:100%;margin:0;font-weight:600;font-size:14px}:host .captions-optionF{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}:host .captions-option-lbl{margin-left:15px}:host .caption-btn-option{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;color:#252525}:host .captions-icon-btn{margin-right:15px}:host .playback-speed-normal{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;-webkit-box-align:center!important;-webkit-align-items:center!important;-ms-flex-align:center!important;align-items:center!important}:host .speed-next-option{vertical-align:4px}:host .playback-rt{padding-left:5px;margin-top:5px;padding-right:5px}:host .playbackoptionsF{margin-left:10px;margin-top:-5px;padding-top:5px;padding-bottom:5px}:host .playback-speed-label{line-height:1;vertical-align:text-top}:host span.all-speed{margin:26px;-webkit-box-pack:start;-webkit-justify-content:flex-start;-ms-flex-pack:start;justify-content:flex-start}:host .settings-list-container.playlist-menu{z-index:100}:host button.playbackoptionsF{width:-webkit-calc(100% - 15px);width:calc(100% - 15px);min-width:145px}:host .playbackoptionsF:hover{background-color:#e9e9e9;padding-top:5px;padding-bottom:5px}:host .activeSO{font-weight:600!important}:host li.settings-list-container.playlist-menu{padding-top:5px;padding-bottom:5px}@media (max-width:768px){:host #video_controls_bar{min-height:80px}:host .row01controls{margin-top:8px}:host .settings-list{bottom:70px}:host .settings-list.dropdown-menu{bottom:45px}:host .settings-list.dropdown-itpro{bottom:255px}}:host .toggle-container.captions-toggle-refactor{display:-webkit-box!important;display:-webkit-flex!important;display:-ms-flexbox!important;display:flex!important;-webkit-box-orient:horizontal!important;-webkit-box-direction:normal!important;-webkit-flex-direction:row!important;-ms-flex-direction:row!important;flex-direction:row!important;-webkit-box-pack:justify!important;-webkit-justify-content:space-between!important;-ms-flex-pack:justify!important;justify-content:space-between!important;width:100%;padding-right:16px;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .toggle-container.captions-toggle-refactor label{margin:0}:host .gr-label.captions-toggle-refactor{line-height:1.8;padding-left:20px}:host .settings-list-container.playlist-select{width:190px}@media (max-width:720px){:host #video_controls_bar{min-height:70px}}@media (max-width:640px){:host .row01controls{margin-top:5px}}@media (max-width:414px){:host .row01controls{margin-top:-5px}:host .video_title{height:40px}:host .video_title h2{font-size:14px;margin-top:-3px}:host .fullscreen-icon svg,:host .mute-icon svg,:host .play-icon svg,:host .settings-icon svg,:host .volhigh-icon svg,:host .vollow-icon svg,:host .voloff-icon svg{width:18px}:host .player-control-margin{margin-right:10px}:host .time{font-size:12px;margin-top:4px;margin-left:10px}:host .settings-list .menu{width:180px;height:110px;border:1px solid #f5f5f5;position:absolute;right:20px;margin-top:-20px}:host .menuitemoptions{-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:start!important;-webkit-justify-content:flex-start!important;-ms-flex-pack:start!important;justify-content:flex-start!important;-webkit-align-content:flex-start!important;-ms-flex-line-pack:start!important;align-content:flex-start!important;font-size:12px!important;margin-top:-10px}:host li{height:50px}:host .gr-dropdown-container li button{-webkit-box-align:start;-webkit-align-items:flex-start;-ms-flex-align:start;align-items:flex-start;height:20px}:host .gr-dropdown-container li button:hover{height:35px}:host .menu{-webkit-box-shadow:11px -7px 17px -22px #000;box-shadow:11px -7px 17px -22px #000}:host label#captions-toggle-label,:host span.left-playback{margin-top:-10px}:host .settings-list.dropdown-menu{bottom:70px!important;right:0}:host .settings-list.dropdown-itpro{bottom:65px;right:0}:host .settings-list-container{width:180px}:host button.menuitemoptionsF.playback-speed-normal{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical!important;-webkit-box-direction:normal!important;-webkit-flex-direction:column!important;-ms-flex-direction:column!important;flex-direction:column!important;border-radius:4px}:host .playback-lft{margin-left:-15px;margin-top:0;padding-left:0}:host .playback-rt{margin-top:5px;padding-right:0}:host button.menuitemoptionsF.playback-speed-normal{width:170px;margin-top:-10px}:host .settings-list-container.playlist-menu{height:190px;overflow-y:scroll;bottom:-10px}:host .settings-list-container.playlist-menu li{height:28px}}@media (max-width:320px){:host input#volumeslider{width:50px;vertical-align:top;margin-top:10px}:host input#volume{width:40px!important}:host .gr-dropdown-container li button:hover{height:45px}:host .settings-list-container.playlist-menu{height:150px;overflow-y:scroll;bottom:-8px}}:host .video_title,:host pearson-icon{pointer-events:none}:host .video_title{left:0;color:#fff;text-align:left}:host .video_title h1{margin:0;padding-left:16px;font-size:18px}:host .circle,:host .course-title,:host .not-started #video_controls_bar{display:none}:host .not-started .course-title{margin-bottom:16px;color:#fff;display:block}:host .not-started .video_title{height:100%!important;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;color:#fff;background:rgba(68,68,68,.38);pointer-events:all}:host .not-started .video_title h1{margin:0;padding-left:16px;font-size:24px}:host .not-started .circle{border-radius:50%;height:75px;width:75px;background:#fff;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;margin-top:50px}:host .not-started .circle pearson-icon{margin-top:2px;color:#252525}:host #submenu{display:none}:host button>*{pointer-events:none}:host ul{-webkit-box-shadow:0 2px 4px 0 rgba(0,0,0,.16);box-shadow:0 2px 4px 0 rgba(0,0,0,.16)}:host .speed-next-option{text-transform:capitalize}:host #video_player_box>*{-webkit-transition:all .2s ease-in-out;transition:all .2s ease-in-out}:host #video_controls_bar,:host div#video_player_box{position:relative}:host #video_controls_bar{position:absolute;background:-webkit-gradient(linear,left top,left bottom,from(rgba(37,37,37,0)),color-stop(81.77%,rgba(37,37,37,.7)));background:-webkit-linear-gradient(top,rgba(37,37,37,0),rgba(37,37,37,.7) 81.77%);background:linear-gradient(180deg,rgba(37,37,37,0),rgba(37,37,37,.7) 81.77%);bottom:0}:host .smallView #video_controls_bar{min-height:auto!important}:host .not-started .video_title{background:rgba(37,37,37,.26)}:host #submenu{display:block}:host .settings-list-container{height:283px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:end;-webkit-justify-content:flex-end;-ms-flex-pack:end;justify-content:flex-end}:host .video_title{background:-webkit-gradient(linear,left bottom,left top,from(rgba(37,37,37,0)),to(rgba(37,37,37,.7)));background:-webkit-linear-gradient(bottom,rgba(37,37,37,0),rgba(37,37,37,.7));background:linear-gradient(0deg,rgba(37,37,37,0),rgba(37,37,37,.7))}:host #videoContainer{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:start;-webkit-align-items:flex-start;-ms-flex-align:start;align-items:flex-start}:host .not-started .course-title{font-family:Work Sans,sans-serif;font-weight:600;font-size:14px;margin-bottom:24px}@media (max-width:300px){:host .not-started .course-title{display:none!important}}:host .not-started .video_title h1.gr-h1{font-family:Work Sans,sans-serif;font-weight:300;font-size:36px;padding:0;text-align:center;max-width:500px;line-height:1.5}@media (max-width:300px){:host .not-started .video_title h1.gr-h1{display:none!important}}@media (max-width:640px){:host .not-started .video_title h1.gr-h1{font-size:24px}}:host .video_title h1.gr-h1{font-weight:600;font-size:16px;line-height:1.5;color:#fafafa;text-align:left;text-shadow:0 0 8px rgba(0,0,0,.7)}:host .not-started .circle pearson-icon{color:#666}@media (max-width:300px){:host .not-started .circle{margin-top:0}}:host .not-started.smallView .course-title,:host .not-started.smallView h1,:host .smallView .row01controls{display:none}:host .not-started.smallView .circle{margin:0;width:50px;height:50px}:host .smallView #video_controls_bar{-webkit-box-pack:end;-webkit-justify-content:flex-end;-ms-flex-pack:end;justify-content:flex-end;background:none}:host .smallView .video_title{min-width:100%}
  :host .not-started .circle {
background:none;
}
:host .not-started .circle svg {
width:75px; height:75px; 
}
:host .not-started.smallView #videoTitle {
overflow: hidden;
}
:host .not-started.smallView .circle {
display:none;
width:35px; 
height:35px; 
}
:host .not-started.smallView.show-play-btn .circle {
    display:flex;
}
:host .smallView.show-play-btn .circle svg {
margin-top:20px;
}
:host .smallView.show-play-btn .circle {
display:flex !important;
}

:host .smallView.show-play-btn .video_title {
opacity: 1 !important;
display: flex !important;
background:transparent;
height:100%;
justify-content: center;
    align-items: center;
}
:host .smallView.show-play-btn h1 {
display:none;
}

:host .smallView #video_controls_bar {
padding-bottom:5px;
}


.row02controls {
  position:relative;
}

:host .smallView .played {
top:0;
}

:host .smallView .row02controls {
  display:flex;
  flex-direction: column;
  bottom:5px;
}

:host .smallView #buffer {
top:0;
}

.played {
    width:100%;
    height:6px;
    background: white;
    top:10px;
    position:absolute;
    pointer-events: none;
    border-radius:4px;
    z-index: 10000;
}

#buffer {
    width:100%;
    height:6px;
    top:10px;
    position:absolute;
    pointer-events: none;
    border-radius:4px;
       z-index: 9000;
}

:host input#seekbar {
width:100%;
margin:0;
height:6px;
background: #99999990 !important;
border:0;
cursor:pointer;
}
:host input#seekbar::-moz-range-thumb {
background-color:transparent;
border:0;
}
:host input#seekbar::-webkit-slider-thumb {
background-color:transparent;
border:0;
}
:host input#seekbar::-ms-thumb {
background-color:transparent;
border:0;
}



    </style>
    <div id="video_player_box" class="not-started hidden animated">
       <div id="videoContainer"></div>
       <button id="videoTitle" class="video_title" aria-labelledby="courseTitle courseSubTitle coursePlayButton">
          <div id="courseTitle" class="course-title">CCNA 200-301 CVC</div>
          <h1 id="courseSubTitle" class="gr-h1">2.1: Foundational Networking Security Technologies</h1>
          <div id="playButton" class="circle">
             <span id="coursePlayButton" class="hidden">play video</span>
             <svg xmlns="http://www.w3.org/2000/svg" width="37" height="36" viewBox="0 0 37 36" fill="none">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M18.4489 35.42C28.1892 35.42 36.0853 27.5239 36.0853 17.7836C36.0853 8.0433 28.1892 0.147217 18.4489 0.147217C8.70858 0.147217 0.8125 8.0433 0.8125 17.7836C0.8125 27.5239 8.70858 35.42 18.4489 35.42ZM15.2743 23.2821L24.798 17.7836L15.2743 12.2851V23.2821Z" fill="white"/>
             </svg>
          </div>
       </button>
       <div id="video_controls_bar">

       </div>
    </div>
`;

  if (win.ShadyCSS) win.ShadyCSS.prepareTemplate(template, 'pearson-video');

  function formatTime(timeInSeconds) {
    const result = new Date(timeInSeconds * 1000).toISOString().substr(11, 8);

    return {
      minutes: result.substr(3, 2),
      seconds: result.substr(6, 2),
    };
  }

  class PearsonVideo extends HTMLElement {
    static get observedAttributes() {
      return ['status', 'muted', 'menu', 'playback', 'url', 'captions', 'poster', 'started', 'title', 'subtitle', 'small', 'showplaybtn'];
    }

    get showPlayBtn () {
      return this.getAttribute('showplaybtn')
    }

    get small () {
      return this.getAttribute('small')
    }

    get subTitle() {
      return this.getAttribute('subtitle')
    }

    get title() {
      return this.getAttribute('title')
    }

    get started() {
      this.getAttribute('started')
    }

    get url() {
      this.getAttribute('url')
    }

    get poster() {
      this.getAttribute('poster')
    }

    get captions() {
      this.getAttribute('captions')
    }

    get source() {
      this.getAttribute('url')
    }

    get playback() {
      this.getAttribute('playback');
    }

    get menu () {
      this.getAttribute('menu');
    }

    get muted() {
      this.hasAttribute('muted');
    }

    get status() {
      this.getAttribute('status');
    }

    set showPlayBtn(bool) {
      this.setAttribute('showplaybtn', bool)
    }
    set small (bool) {
      this.setAttribute('small', bool)
    }

    set subTitle (value) {
      this.setAttribute('subtitle', value)
    }

    set title (value) {
      this.setAttribute('title', value)
    }

    set started(bool) {
      this.setAttribute('started', bool)
    }

    set url(value) {
      this.setAttribute('url', value)
    }

    set captions(url) {
      this.setAttribute('captions', url)
    }

    set source(url) {
      this.setAttribute('src', url)
    }

    set playback(value) {
      this.setAttribute('playback', value);
    }

    set menu(value) {
      this.setAttribute('menu', value)
    }

    set muted(bool) {
      if (bool === true) {
        this.setAttribute('muted', true);
      } else {
        this.removeAttribute('muted')
      }
    }

    set status(value) {
      this.setAttribute('status', value);
    }

    constructor() {
      super();
      this.attachShadow({ mode: 'open' });

      const clone = template.content.cloneNode(true),
        videoClone = videoTemplate.cloneNode(true),
        controlsClone = videoControlsTemplate.content.cloneNode(true),
        scrubberClone = videoScrubberTemplate.content.cloneNode(true);

      this.contentContainer = clone.querySelector('#video_player_box');
      this.videoContainer = clone.querySelector('#videoContainer');
      this.videoControlContainer = clone.querySelector('#video_controls_bar');
      this.startBtn = clone.querySelector('#videoTitle');
      this.courseTitle = clone.querySelector('#courseTitle');
      this.circlePlay = clone.querySelector('#playButton');
      this.courseSubTitle = clone.querySelector('h1')
      this.videoControls = controlsClone.querySelector('.row01controls');
      this.volumeContainer = controlsClone.querySelector('#volumeContainer');
      this.detailsNode = controlsClone.querySelector('details');
      this.volume = controlsClone.querySelector('#volume');
      this.pauseBtn = controlsClone.querySelector('#pau')
      this.muteBtn = controlsClone.querySelector('#mute');
      this.fullscreen = controlsClone.querySelector('#fullscreen');
      this.settingsBtn = controlsClone.querySelector('#settings');
      this.menuContainer = controlsClone.querySelector('.settings-list-container')
      this.actionBtn = controlsClone.querySelector('button');
      this.durationTime = controlsClone.querySelector('#durtime');
      this.timeElapsed = controlsClone.querySelector('#curtime')
      this.videoScrubber = scrubberClone.querySelector('input');
      this.playedProgressBar = scrubberClone.querySelector('#played')
      this.bufferProgressBar = scrubberClone.querySelector('#buffer')

      this.video = videoClone.querySelector('video');
      this.track = videoClone.querySelector('track');

      this.videoControlContainer.appendChild(controlsClone)
      this.videoControlContainer.appendChild(scrubberClone);
      this.videoContainer.appendChild(videoClone);

      this.requestFullscreen = this.video.requestFullscreen;
      this.shadowRoot.appendChild(clone);

      // starting screen.  click to start video
      this.startBtn.addEventListener('click', event => {
        const pauseBtn = this.shadowRoot.querySelector('#pause');
        this.video.currentTime = 0
        this.status = 'play'
        this.startBtn.setAttribute('tabindex', -1)
        this.startBtn.setAttribute('aria-label', this.courseSubTitle.innerHTML)
        this.contentContainer.classList.remove('not-started')
        pauseBtn.focus();

        if (this.small === 'true') {
          this.startBtn.style.display = 'none'
        }

        this.dispatchEvent(new CustomEvent('played', {
          bubbles: true,
          composed: true
        }));

      })

      this.videoControlContainer.style.opacity = 1.0
      this.startBtn.style.opacity = 1.0

      // toggle video controls when mouse is in video
      this.contentContainer.addEventListener('mousemove', event => {
        this.videoControlContainer.style.opacity = 1.0
        this.startBtn.style.opacity = 1.0
      })

      this.contentContainer.addEventListener('mouseleave', event => {
        setTimeout(event => {
          if (this.getAttribute('started') === 'true') {
            this.videoControlContainer.style.opacity = 0.0
            this.startBtn.style.opacity = 0.0
          } else {
            this.videoControlContainer.style.opacity = 1.0
            this.startBtn.style.opacity = 1.0
          }
        }, 1500)

      })

      this.muteBtn.addEventListener('focus', event => {
        this.volume.style.display = 'flex'
        this.volume.classList.add('animateIn')
      })

      this.muteBtn.addEventListener('keydown', event => {
        if (event.key === 'Tab') {
          event.preventDefault();
          setTimeout(event => {
            this.volume.focus();
          },1)
        }
      })

      this.volume.addEventListener('keydown', event => {
        if (event.target.tagName === 'INPUT') {
          event.preventDefault();
          setTimeout(event => {
            this.fullscreen.focus();
            this.volume.style.display = 'none'
            this.volume.classList.remove('animateIn')
          },1)
        }
        if (event.key === 'Tab' && event.shiftKey === true) {

          setTimeout(event => {
            this.shadowRoot.querySelector('.row01-left > button').focus()
            this.volume.style.display = 'none'
            this.volume.classList.remove('animateIn')
          },1)
        }
      })

      // toggle volume control when mouse is in volume
      this.volumeContainer.addEventListener('mouseenter', event => {
        this.volume.style.display = 'flex'
        this.volume.classList.add('animateIn')
      })

      this.volumeContainer.addEventListener('mouseleave', event => {
        this.volume.style.display = 'none'
        this.volume.classList.remove('animateIn')
      })

      // set volume
      this.volume.addEventListener('input', event => {
        this.video.volume = event.target.value / 100;
      })

      // play, pause, mute, settings actions
      this.videoControls.addEventListener('click', event => {
        if (event.target.id === 'play') {
          this.status = 'play'
        } else if (event.target.id === 'pause') {
          this.status = 'pause'
        } else if (event.target.id === 'mute') {
          this.muted = true
        } else if (event.target.id === 'unmute') {
          this.muted = false
        } else if (event.target.id === 'fullscreen') {
          if (this.video.webkitEnterFullScreen) {
            this.video.webkitEnterFullScreen()
          }
          this.video.requestFullscreen()
        } else if (event.target.id === 'settings') {
          if (this.getAttribute('menu') === 'off') {
            this.menu = 'open'
          } else {
            this.menu = 'off'
          }
        } else if (event.target.id === 'trigger-playback'){
          this.menu = 'submenu'
        } else if (event.target.id === 'previous') {
          this.menu = 'open'
        } else if (event.target.hasAttribute('data-speed')) {
          const speed = parseFloat(event.target.getAttribute('data-speed'))
          this.video.playbackRate = speed
          this.playback = event.target.id
          const buttons = event.target.parentNode.parentNode.querySelectorAll('button');

          buttons.forEach(button => {
            button.classList.remove('activeSO')
            event.target.classList.add('activeSO')
          })
        }
      })


      this.detailsNode.addEventListener('mousemove', event => {
        this.detailsNode.style.position = 'relative'
      })

      this.video.addEventListener('click',  event=> {
        if (this.getAttribute('menu') === 'open' || this.getAttribute('menu') === 'submenu') {
          this.menu = 'off'
        } else {
          if (this.getAttribute('status') === 'play') {
            this.status = 'pause'
          } else {
            this.status = 'play'
          }
        }
      })

      doc.addEventListener('click', event => {
        if (event.target !== this) {
          if (this.getAttribute('menu') === 'open' || this.getAttribute('menu') === 'submenu') {
            this.menu = 'off'
          } else {
            return
          }
        }
      })

      // set time and duration
      this.video.addEventListener('loadeddata', event => {
        this.init();
      })


      this.video.addEventListener('timeupdate', event => {
        const time = formatTime(Math.round(this.video.currentTime)),
          pastPlayed = this.shadowRoot.querySelector('.played');

        this.updateTimeElapsed()
        this.drawProgress(this.bufferProgressBar, this.video.buffered, this.video.duration);

        if (this.data !== undefined) {
          this.data.time = {minutes: time.minutes,
            seconds: time.seconds,
            duration: Math.round(this.video.duration),
          }
        }
        this.dispatchEvent(new CustomEvent('timeElapsed', {
          bubbles: true,
          composed: true,
          detail: {
            minutes: time.minutes,
            seconds: time.seconds,
            duration: Math.round(this.video.duration)
          }
        }));
        this.videoScrubber.value = this.video.currentTime
        pastPlayed.style.width = (this.video.currentTime / this.video.duration) * 100 + '%';

      })

      // add scrubber functionality
      this.videoScrubber.addEventListener('input', event => {
        this.skipAhead(event)
      })


      this.video.textTracks[0].mode = 'hidden'

      this.init = this.init.bind(this)
      this.updateTimeElapsed = this.updateTimeElapsed.bind(this)
      this.skipAhead = this.skipAhead.bind(this)
    }

    drawProgress(canvas, buffered, duration) {
      var context = canvas.getContext('2d', { antialias: false });
      context.fillStyle = '#aaa';
      context.shadowBlur = 0;

      var width = canvas.width;
      var height = canvas.height;
      if(!width || !height) throw "Canvas's width or height weren't set!";
      context.clearRect(0, 0, width, height); // clear canvas

      for(var i = 0; i < buffered.length; i++){
        var leadingEdge = Math.round(buffered.start(i) / duration * width);
        var trailingEdge = Math.round(buffered.end(i) / duration * width);
        context.fillRect(leadingEdge, 0, trailingEdge - leadingEdge, height);
      }
    }

    skipAhead(event) {
      const skipTo = event.target.dataset.seek ? event.target.dataset.seek : event.target.value;
      this.video.currentTime = skipTo;
      this.videoScrubber.value = skipTo;
    }

    updateTimeElapsed() {
      const time = formatTime(Math.round(this.video.currentTime));
      this.timeElapsed.innerText = `${time.minutes}:${time.seconds}`;
      this.timeElapsed.setAttribute('datetime', `${time.minutes}m ${time.seconds}s`)
    }

    init() {
      const videoDuration = Math.round(this.video.duration);
      this.videoScrubber.setAttribute('max', videoDuration);
      const time = formatTime(videoDuration);
      this.durationTime.innerText = `${time.minutes}:${time.seconds}`;
      this.durationTime.setAttribute('datetime', `${time.minutes}m ${time.seconds}s`)
    }

    connectedCallback() {
      this.status = 'stop'
      this.menu = 'off'
      this.playback = 'normal'
      if (this.showPlayBtn === null) {
        this.showPlayBtn = true
      }

      if (this.getAttribute('poster') === undefined || this.getAttribute('poster') === null) {
        this.setAttribute('poster', 7)
      }

      if (this.title) {
        this.courseTitle.innerHTML = this.title
      } else {
        this.courseTitle.innerHTML = this.data.title
      }

      if (this.subTitle) {
        this.courseSubTitle.innerHTML = this.subTitle
      } else {
        this.courseSubTitle.innerHTML = this.data.subTitle
      }

      this.videoURL = this.getAttribute('url') + '#t='+ this.getAttribute('poster') + '.0';
      this.video.src = this.videoURL
      this.track.setAttribute('src', this.captions)
      this.video.textTracks[0].mode = 'hidden'



    }

    attributeChangedCallback(name, oldValue, newValue) {
      if (name === 'showplaybtn')  {
        if (newValue === 'true') {

          this.contentContainer.classList.add('show-play-btn')
          this.videoControlContainer.style.display = 'none'
        } else {
          this.contentContainer.classList.remove('show-play-btn')
          this.videoControlContainer.style.display = 'flex'
          this.videoControlContainer.style.paddingTop = '0'
          this.videoControlContainer.style.paddingBottom = '0'
        }

      }
      if (name === 'poster') {
        setTimeout(event => {
          this.contentContainer.classList.remove('hidden')
          this.contentContainer.classList.add('animateIn')
          this.removeAttribute('title')
        }, 300)

      }
      if (name === 'started' && oldValue !== newValue) {
        if (newValue === 'false') {
          this.videoContainer.style.filter = 'grayscale(100%)'
        } else {
          this.videoContainer.removeAttribute('style')
          this.video.textTracks[0].mode = 'hidden'
        }
      }
      if (name === 'status' && oldValue !== newValue) {
        const icon = this.actionBtn.querySelector('pearson-icon');
        if (newValue === 'play') {
          this.video.play();
          this.started = true
          icon.icon = 'pause'
          this.actionBtn.id = 'pause'
          this.showPlayBtn = false
          this.actionBtn.setAttribute('aria-label', 'pause')
        }
        if (newValue === 'pause') {
          this.video.pause();
          icon.icon = 'play'
          this.actionBtn.id = 'play'
          this.showPlayBtn = true
          this.actionBtn.setAttribute('aria-label', 'play')
        }
      }

      if (name === 'muted' && oldValue !== newValue) {
        const icon = this.muteBtn.querySelector('pearson-icon');
        if (newValue === 'true') {
          this.video.muted = true
          this.muteBtn.id = 'unmute'
          icon.icon = 'audio-off'
        } else {
          this.video.muted = false
          this.muteBtn.id = 'mute'
          icon.icon = 'audio-high'
        }
      }

      if (name === 'menu') {
        const menuClone = menuTemplate.content.cloneNode(true),
          subMenuClone = submenuTemplate.content.cloneNode(true),
          buttons = subMenuClone.querySelectorAll('button'),
          buttonText = menuClone.querySelector('.speed-next-option'),
          toggle = menuClone.querySelector('pearson-toggle');

        buttonText.innerHTML = this.getAttribute('playback');
        this.menuContainer.innerHTML = ''
        if (newValue === 'open') {
          this.detailsNode.setAttribute('open', true)
          this.menuContainer.appendChild(menuClone)
          toggle.addEventListener('change', event => {
            if (event.target.on === 1) {
              this.track.setAttribute('src', this.getAttribute('captions'))
              this.video.textTracks[0].mode = 'showing'
            } else {
              this.video.textTracks[0].mode = 'hidden'
            }
          })

        }
        if (newValue === 'submenu') {
          this.menuContainer.appendChild(subMenuClone)
          this.detailsNode.style.display = 'relative'
          buttons.forEach(button => {
            button.classList.remove('activeSO')
            if (button.id === this.getAttribute('playback')) {
              button.classList.add('activeSO')
            }
          })
        }
      }

      if (name === 'small') {
        if (newValue === 'true') {
          this.contentContainer.classList.add('smallView')
        } else {
          this.contentContainer.classList.remove('smallView')
        }
      }
    }
  }

  customElements.define('pearson-video', PearsonVideo);
})(window, document);
