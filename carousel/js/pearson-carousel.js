(function(w, doc) {
  'use strict';

  const template = doc.createElement('template'),
        dotTemplate = doc.createElement('template');


  template.innerHTML = ` 
    <style> 
        @import url("https://fonts.googleapis.com/css?family=Open+Sans:400,600");:host{font-family:Open Sans,sans-serif
  /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */}:host html{line-height:1.15;-webkit-text-size-adjust:100%}:host body{margin:0}:host main{display:block}:host h1{font-size:2em;margin:.67em 0}:host hr{-webkit-box-sizing:content-box;box-sizing:content-box;height:0;overflow:visible}:host pre{font-family:monospace,monospace;font-size:1em}:host a{background-color:transparent}:host abbr[title]{border-bottom:none;text-decoration:underline;-webkit-text-decoration:underline dotted;text-decoration:underline dotted}:host b,:host strong{font-weight:bolder}:host code,:host kbd,:host samp{font-family:monospace,monospace;font-size:1em}:host small{font-size:80%}:host sub,:host sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}:host sub{bottom:-.25em}:host sup{top:-.5em}:host img{border-style:none}:host button,:host input,:host optgroup,:host select,:host textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}:host button,:host input{overflow:visible}:host button,:host select{text-transform:none}:host [type=button],:host [type=reset],:host [type=submit],:host button{-webkit-appearance:button}:host [type=button]::-moz-focus-inner,:host [type=reset]::-moz-focus-inner,:host [type=submit]::-moz-focus-inner,:host button::-moz-focus-inner{border-style:none;padding:0}:host [type=button]:-moz-focusring,:host [type=reset]:-moz-focusring,:host [type=submit]:-moz-focusring,:host button:-moz-focusring{outline:1px dotted ButtonText}:host fieldset{padding:.35em .75em .625em}:host legend{-webkit-box-sizing:border-box;box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}:host progress{vertical-align:baseline}:host textarea{overflow:auto}:host [type=checkbox],:host [type=radio]{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0}:host [type=number]::-webkit-inner-spin-button,:host [type=number]::-webkit-outer-spin-button{height:auto}:host [type=search]{-webkit-appearance:textfield;outline-offset:-2px}:host [type=search]::-webkit-search-decoration{-webkit-appearance:none}:host ::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}:host details{display:block}:host summary{display:list-item}:host [hidden],:host template{display:none}:host html{font-size:14px}:host *,:host html{-webkit-box-sizing:border-box;box-sizing:border-box}:host body{font-family:Open Sans,Arial,Helvetica,sans-serif}:host body,:host p{font-size:14px;line-height:1.5;font-weight:400}:host strong{font-weight:600}:host a{font-size:14px;color:#047a9c}:host a:hover{color:#03536a;text-decoration:none}:host a:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host button{cursor:pointer}:host li,:host ul{font-size:14px}:host svg{fill:#252525}:host svg[focusable=false]:focus{outline:none}:host select{cursor:pointer}:host input,:host textarea{font:inherit;letter-spacing:inherit;word-spacing:inherit}:host svg{pointer-events:none}@-webkit-keyframes shift{to{background-position:9px 9px}}@keyframes shift{to{background-position:9px 9px}}@-webkit-keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@-webkit-keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@-webkit-keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@-webkit-keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@-webkit-keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@-webkit-keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@-webkit-keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}@keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}:host .fadeIn,:host .slideInDown{opacity:1!important;visibility:visible!important}:host .fadeOut,:host .slideOutDown{opacity:0;visibility:hidden}:host .slideInDown{-webkit-animation:slideInDown .3s ease-in-out 0s;animation:slideInDown .3s ease-in-out 0s}:host .slideOutDown{-webkit-animation:slideOutDown .2s ease-in 0s;animation:slideOutDown .2s ease-in 0s}:host .fadeIn{-webkit-animation:fadeIn .3s linear 0s;animation:fadeIn .3s linear 0s}:host .fadeOut{-webkit-animation:fadeOut .2s linear 0s;animation:fadeOut .2s linear 0s}:host .animateIn{-webkit-animation-name:fadeIn;animation-name:fadeIn;-webkit-animation-duration:.5s;animation-duration:.5s}:host .animateOut{-webkit-animation-name:fadeOut;animation-name:fadeOut;-webkit-animation-duration:.2s;animation-duration:.2s}@media (prefers-reduced-motion){:host .animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}}:host html[data-prefers-reduced-motion] .animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}:host .no-border{border:0}:host .icon-18{width:18px;height:18px}:host .icon-24{width:24px;height:24px}:host .hidden{display:none!important}:host pearson-alert{font-size:16px;max-width:580px}:host pearson-alert .alert-title{font-size:14px;margin:0;display:inline;top:0}:host pearson-uploader .alert-title{top:0!important}:host pearson-alert .alert-text{margin:0;display:inline}:host pearson-footer{left:50%;margin-left:-50%;right:50%;margin-right:-50%}:host pearson-header{grid-column:span 12}:host pearson-tabs{font-size:14px}:host pearson-progress-bar{grid-column:1/5}@media (min-width:591px){:host pearson-progress-bar{grid-column:1/9}}@media (min-width:887px){:host pearson-progress-bar{grid-column:1/13}}:host pearson-tabs{grid-column:1/5}@media (min-width:591px){:host pearson-tabs{grid-column:1/9}}@media (min-width:887px){:host pearson-tabs{grid-column:1/13}}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host pearson-card{-ms-grid-column-span:3}:host pearson-card,:host pearson-card[stacked]{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;width:100%}:host pearson-card[stacked]{-ms-grid-column-span:12}}:host pearson-accordion{grid-column:1/5}@media (min-width:591px){:host pearson-accordion{grid-column:1/9}}@media (min-width:887px){:host pearson-accordion{grid-column:1/13}}:host #main{max-width:1280px;margin:0 auto}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host iframe .gr-grid-container{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}}:host .gr-grid-container{display:grid;display:-ms-grid;grid-template-columns:repeat(4,1fr);grid-column-gap:16px;grid-row-gap:16px;margin:0 39.5px}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host .gr-grid-container.ie-flex{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}}@media (min-width:351px){:host .gr-grid-container{margin:0 31.5px}}@media (min-width:399px){:host .gr-grid-container{margin:0 39.5px}}@media (min-width:447px){:host .gr-grid-container{margin:0 79.5px}}@media (min-width:591px){:host .gr-grid-container{grid-template-columns:repeat(8,1fr);margin:0 83.5px}}@media (min-width:727px){:host .gr-grid-container{margin:0 103.5px;grid-column-gap:24px;grid-row-gap:24px}}@media (min-width:887px){:host .gr-grid-container{grid-template-columns:repeat(12,1fr);margin:0 71.5px}}@media (min-width:887px) and (-ms-high-contrast:active),(min-width:887px) and (-ms-high-contrast:none){:host .gr-grid-container>*{margin-right:12px;margin-left:12px;margin-bottom:24px}}@media (min-width:983px){:host .gr-grid-container{margin:0 71.5px}}@media (min-width:1079px){:host .gr-grid-container{margin:0 71.5px}}@media (min-width:1175px){:host .gr-grid-container{margin:0 76px}}:host .gr-grid-container .gr-col-two{grid-column-start:2}:host .gr-grid-container .gr-col-three{grid-column-start:3}:host .gr-grid-container .gr-col-four{grid-column-start:4}:host .gr-grid-container .gr-col-five{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-five{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:5}}:host .gr-grid-container .gr-col-six{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-six{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:6}}:host .gr-grid-container .gr-col-seven{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-seven{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:7}}:host .gr-grid-container .gr-col-eight{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-eight{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:8}}:host .gr-grid-container .gr-col-nine{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-nine{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:9}}:host .gr-grid-container .gr-col-ten{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-ten{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:10}}:host .gr-grid-container .gr-col-eleven{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-eleven{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:11}}:host .gr-grid-container .gr-col-twelve{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-twelve{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:12}}:host .gr-col-span-one{grid-column-end:span 1}:host .gr-col-span-two{grid-column-end:span 2}:host .gr-col-span-three{grid-column-end:span 3}:host .gr-col-span-four{grid-column-end:span 4}@media (min-width:591px){:host .gr-col-span-five{grid-column-end:span 5}}@media (min-width:591px){:host .gr-col-span-six{grid-column-end:span 6}}@media (min-width:591px){:host .gr-col-span-seven{grid-column-end:span 7}}@media (min-width:591px){:host .gr-col-span-eight{grid-column-end:span 8}}@media (min-width:887px){:host .gr-col-span-nine{grid-column-end:span 9}}@media (min-width:887px){:host .gr-col-span-ten{grid-column-end:span 10}}@media (min-width:887px){:host .gr-col-span-eleven{grid-column-end:span 11}}:host .gr-col-span-twelve{grid-column-end:span 4}@media (min-width:591px){:host .gr-col-span-twelve{grid-column-end:span 8}}@media (min-width:887px){:host .gr-col-span-twelve{grid-column-end:span 12}}:host .gr-row-one{grid-row-start:1}:host .gr-row-two{grid-row-start:2}:host .gr-row-three{grid-row-start:3}:host .gr-row-four{grid-row-start:4}:host .gr-row-five{grid-row-start:5}:host .gr-row-six{grid-row-start:6}:host .gr-row-seven{grid-row-start:7}:host .gr-row-eight{grid-row-start:8}:host .gr-row-nine{grid-row-start:9}:host .gr-row-ten{grid-row-start:10}:host .gr-row-eleven{grid-row-start:11}:host .gr-row-twelve{grid-row-start:12}:host .gr-row-thirteen{grid-row-start:13}:host .gr-row-fourteen{grid-row-start:14}:host .gr-row-fifteen{grid-row-start:15}:host .gr-row-span-two{grid-row-end:span 2}:host .gr-row-span-three{grid-row-end:span 3}:host .gr-row-span-four{grid-row-end:span 4}:host .gr-row-span-five{grid-row-end:span 5}:host .gr-row-span-six{grid-row-end:span 6}:host .gr-row-span-seven{grid-row-end:span 7}:host .gr-row-span-eight{grid-row-end:span 8}:host .gr-row-span-nine{grid-row-end:span 9}:host .gr-row-span-ten{grid-row-end:span 10}:host .gr-row-span-eleven{grid-row-end:span 11}:host .gr-row-span-twelve{grid-row-end:span 12}:host .gr-primary{color:#047a9c;fill:#047a9c}:host .gr-secondary{color:#ffb81c;fill:#ffb81c}:host .gr-white{color:#fff;fill:#fff}:host .gr-neutral-high-one{color:#252525;fill:#252525}:host .gr-neutral-high-two{color:#6a7070;fill:#6a7070}:host .gr-neutral-med-one{color:#a9a9a9;fill:#a9a9a9}:host .gr-neutral-med-two{color:#c7c7c7;fill:#c7c7c7}:host .gr-neutral-med-three{color:#d9d9d9;fill:#d9d9d9}:host .gr-neutral-med-four{color:#e9e9e9;fill:#e9e9e9}:host .gr-neutral-light-one,:host .gr-neutral-light-two{color:#eee;fill:#eee}:host .gr-condition-one{color:#db0020;fill:#db0020}:host .gr-condition-two{color:#038238;fill:#038238}:host .gr-condition-three{color:#da0474;fill:#da0474}:host .gr-theme-one-light{color:#caefee;fill:#caefee}:host .gr-theme-one-med{color:#76d5d4;fill:#76d5d4}:host .gr-theme-one-dark{color:#19a5a3}:host .gr-theme-two-light{color:#f2e5f1;fill:#f2e5f1}:host .gr-theme-two-med{color:#895b9a;fill:#895b9a}:host .gr-theme-two-dark{color:#633673;fill:#633673}:host .gr-theme-three-light{color:#f6f8cc;fill:#f6f8cc}:host .gr-theme-three-med{color:#d2db0e;fill:#d2db0e}:host .gr-theme-three-dark{color:#b0b718;fill:#b0b718}:host .gr-theme-four-light{color:#d9e6f1;fill:#d9e6f1}:host .gr-theme-four-med{color:#356286;fill:#356286}:host .gr-theme-four-dark{color:#1e496c;fill:#356286}:host .gr-theme-five-light{color:#dff5d5;fill:#dff5d5}:host .gr-theme-five-med{color:#66be3e;fill:#66be3e}:host .gr-theme-five-dark{color:#288500;fill:#288500}:host .gr-theme-six-light{color:#d6ecf4;fill:#d6ecf4}:host .gr-theme-six-med{color:#80c5dd;fill:#80c5dd}:host .gr-theme-six-dark{color:#46a9cb;fill:#46a9cb}:host .gr-theme-seven-light{color:#faebc3;fill:#faebc3}:host .gr-theme-seven-med{color:#f5c54c;fill:#f5c54c}:host .gr-theme-seven-dark{color:#dea30d;fill:#dea30d}:host .gr-primary-bg{background-color:#047a9c}:host .gr-secondary-bg{background-color:#ffb81c}:host .gr-white-bg{background-color:#fff}:host .gr-neutral-high-one-bg{background-color:#252525}:host .gr-neutral-high-two-bg{background-color:#6a7070}:host .gr-neutral-med-one-bg{background-color:#a9a9a9}:host .gr-neutral-med-two-bg{background-color:#c7c7c7}:host .gr-neutral-med-three-bg{background-color:#d9d9d9}:host .gr-neutral-med-four-bg{background-color:#e9e9e9}:host .gr-neutral-light-one-bg,:host .gr-neutral-light-two-bg{background-color:#eee}:host .gr-condition-one-bg{background-color:#db0020}:host .gr-condition-two-bg{background-color:#038238}:host .gr-condition-three-bg{background-color:#da0474}:host .gr-theme-one-light-bg{background-color:#caefee}:host .gr-theme-one-med-bg{background-color:#76d5d4}:host .gr-theme-one-dark-bg{background-color:#19a5a3}:host .gr-theme-two-light-bg{background-color:#f2e5f1}:host .gr-theme-two-med-bg{background-color:#895b9a}:host .gr-theme-two-dark-bg{background-color:#633673}:host .gr-theme-three-light-bg{background-color:#f6f8cc}:host .gr-theme-three-med-bg{background-color:#d2db0e}:host .gr-theme-three-dark-bg{background-color:#b0b718}:host .gr-theme-four-light-bg{background-color:#d9e6f1}:host .gr-theme-four-med-bg{background-color:#356286}:host .gr-theme-four-dark-bg{background-color:#1e496c}:host .gr-theme-five-light-bg{background-color:#dff5d5}:host .gr-theme-five-med-bg{background-color:#66be3e}:host .gr-theme-five-dark-bg{background-color:#288500}:host .gr-theme-six-light-bg{background-color:#d6ecf4}:host .gr-theme-six-med-bg{background-color:#80c5dd}:host .gr-theme-six-dark-bg{background-color:#46a9cb}:host .gr-theme-seven-light-bg{background-color:#faebc3}:host .gr-theme-seven-med-bg{background-color:#f5c54c}:host .gr-theme-seven-dark-bg{background-color:#dea30d}:host .gr-h1{font-size:24px;line-height:28px}:host .gr-h1,:host .gr-h2{font-weight:400;margin-top:0}:host .gr-h2{font-size:20px;line-height:26px}:host .gr-h3{font-size:18px;line-height:24px;font-weight:400;margin-top:0}:host .gr-label{font-size:12px;line-height:16px;color:#6a7070;display:block;margin-bottom:4px}:host .gr-meta{font-size:12px;line-height:12px;color:#6a7070}:host .gr-semi-bold{font-weight:600}:host .gr-font-large{font-size:16px;line-height:24px}:host .gr-font-normal{font-size:14px;line-height:20px}:host .gr-btn{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;border:1px solid #6a7070;background:transparent;color:#6a7070;min-width:152px;padding:8px 24px;font-size:16px;cursor:pointer;border-radius:22px;position:relative;margin:12px;line-height:1.15}:host .gr-btn:hover{color:#252525;border:1px solid #252525}:host .gr-btn:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host a.gr-btn{text-decoration:none}:host .gr-btn.primary{background-color:#047a9c;border:1px solid #047a9c;color:#fff}:host .gr-btn.primary:hover{color:#fff;background-color:#035f79}:host .gr-btn.attention{background-color:#ffb81c;border:1px solid #ffb81c;color:#252525}:host .gr-btn.attention:hover{background-color:#f7aa00}:host .gr-btn.small{min-width:128px;padding:7px 20px;font-size:14px}:host .gr-btn.small:focus:after{padding:18px 21px}:host .gr-btn.extra-large{min-width:152px;padding:10px 24px;font-size:16px}:host .gr-btn.extra-large:focus:after{padding:22px 21px;border-radius:24px}:host .gr-btn:disabled{background-color:#e9e9e9!important;color:#6a7070!important;border:0!important;cursor:default!important}:host .gr-btn.icon-btn-24{min-width:auto;padding:5px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-24:focus{border-radius:4px}:host .gr-btn.icon-btn-24:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-24:active svg{fill:#005a70}:host .gr-btn.icon-btn-24 svg{fill:#6a7070}:host .gr-btn.icon-btn-18{min-width:auto;padding:8px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-18:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-18:active svg{fill:#005a70}:host .gr-btn.icon-btn-18:focus{border-radius:4px}:host .gr-btn.icon-btn-18 svg{fill:#6a7070}:host .gr-btn.no-border,:host .gr-btn.no-border:hover{border:0}:host .gr-btn.no-border:focus:after{border-radius:5px;min-width:auto;left:auto;top:auto;padding:16px}:host .carousel{position:relative;overflow:hidden;width:100%;height:100%}:host .slides{height:100%;margin:0;padding:0;list-style-type:none}:host slot[name=slides]::slotted(li){width:100%;height:100%;background:no-repeat 50%;background-size:cover;position:absolute;top:0;left:0;z-index:2;-webkit-transition:left .5s ease-in-out;-webkit-transition:all .5s ease-in-out;transition:all .5s ease-in-out;visibility:hidden}:host slot[name=slides]::slotted(li.current){visibility:visible;left:0;z-index:3}:host slot[name=slides]::slotted(li.next){left:100%}:host slot[name=slides]::slotted(li.prev){left:-100%}:host .pause{position:absolute;bottom:14px;left:0;z-index:7;border:none;background-color:transparent;color:#fff;height:24px;width:40px}:host .controls{list-style-type:none;margin:0;padding:0;position:absolute;top:-webkit-calc(50% - 24px);top:calc(50% - 24px);left:0;width:100%;z-index:6}:host .controls .nav-btn{background-image:url(images/semi-circle.svg);background-color:transparent;height:60px;width:30px;border:none;position:absolute}:host .controls .nav-btn.next-slide-btn{right:0}:host .controls .nav-btn.next-slide-btn pearson-icon{top:19px;left:6px}:host .controls .nav-btn.prev-slide-btn{left:0;-webkit-transform:rotate(180deg);transform:rotate(180deg)}:host .controls .nav-btn.prev-slide-btn pearson-icon{right:0;margin-bottom:3px;bottom:15px;-webkit-transform:rotate(180deg);transform:rotate(180deg)}:host .controls .nav-btn pearson-icon{pointer-events:none;fill:#666;position:absolute;height:24px}:host .controls .nav-btn:hover svg{fill:#047a9c}:host .slidenav{position:absolute;bottom:24px;left:50%;margin-left:-50%;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;z-index:6;width:100%}:host .slidenav button{border:0;border-radius:50%;background:#007fa3;height:12px;width:12px;outline:0;line-height:0;font-size:0;color:transparent;padding:0;-webkit-box-shadow:0 1px 2px 0 rgba(0,0,0,.15);box-shadow:0 1px 2px 0 rgba(0,0,0,.15);margin:0 10px 0 0}:host .slidenav button[aria-selected=true]{background-color:#fff}:host .slidenav button:focus,:host button:focus{-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host button:focus{outline:none}
    </style>
    <div id="carousel" class="carousel">
        <ul class="slides" aria-live="off">
            <slot name="slides"><!--Inject slides--></slot>
        </ul>
        <button class="pause">
            <pearson-icon icon="pause" size="24" ></pearson-icon>
        </button>
        <ul class="controls">
            <li>
                <button class="prev-slide-btn nav-btn" aria-label="previous slide">
                    <pearson-icon icon="previous" size="24" ></pearson-icon>
                </button>
            </li>
            <li>
                <button class="next-slide-btn nav-btn" aria-label="next slide">
                    <pearson-icon icon="next" size="24" ></pearson-icon>
                </button>
            </li>
        </ul>
        <div class="slidenav" role="tablist" aria-orientation="horizontal">
             <!--Inject dot template-->
        </div>
    </div>

    `;

  dotTemplate.innerHTML = `
    <button class="dot-btn" role="tab"><span class="visually-hidden"></span></button>
  `;

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-carousel');
  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(dotTemplate, 'pearson-carousel');


  class Carousel extends HTMLElement {

    constructor() {
      super();
      this.currentSlideIndex = 0;
      this.rotationIntervalId = null;
      this.timer = null;
      this.paused = false;

      this.attachShadow({ mode: 'open' });
      const clone = template.content.cloneNode(true);
      this.slideSlot = clone.querySelector('slot')
      this.carousel = clone.querySelector("#carousel");
      this.slideNav = clone.querySelector(".slidenav");
      this.navBtns = clone.querySelectorAll(".nav-btn");
      this.pauseBtn = clone.querySelector('.pause')

      this.shadowRoot.appendChild(clone);

      this.paginateSlides = this.paginateSlides.bind(this);
      this.stopRotation = this.stopRotation.bind(this);
      this.startRotation = this.startRotation.bind(this);
    }

    connectedCallback() {
      //arrange slotted content
      setTimeout(()=>{
        this.slides = this.slideSlot.assignedNodes()
        this.slides.forEach((slide,i)=>{
          let slideImg = slide.querySelector('img');
          slideImg ? slide.style.backgroundImage = `url(${slideImg.src})` : slide.style.backgroundColor = `#6a7070`;
          if(i === 0){slide.classList.add('current')}
          if(i === 1) slide.classList.add('next')
          if(i === this.slides.length - 1) slide.classList.add('prev')
          slide.setAttribute('id', 'slide'+(i+1))

           //generate a dot for the slide
          this.dotClone = dotTemplate.content.cloneNode(true);
          let dotBtn = this.dotClone.querySelector('button'),
              dotSpan = this.dotClone.querySelector('span');
          if(i === 0) dotBtn.setAttribute('aria-selected',true)
          dotBtn.setAttribute('data-slide', i)
          dotBtn.setAttribute('aria-controls', 'slide'+ (i + 1))
          dotSpan.innerText = 'Slide ' + (i + 1)
          dotBtn.addEventListener("click", this.paginateSlides);
          dotBtn.addEventListener('keydown', e => {
            if(e.type === 'click' || e.key === 'ArrowRight' || e.key === 'ArrowLeft'){
              this.paginateSlides(e)
              };
          })
          this.slideNav.appendChild(this.dotClone)
        })
        },1000)

      this.pauseBtn.addEventListener('click',()=>{
        this.paused = !this.paused;
        let icon = this.pauseBtn.querySelector('pearson-icon')
        icon.getAttribute('icon') === 'pause' ? icon.setAttribute('icon','play') : icon.setAttribute('icon','pause');
        if(this.paused === false)this.startRotation()
      })

      this.navBtns.forEach(navBtn => {
          navBtn.addEventListener("click", this.paginateSlides);
      })
      this.carousel.addEventListener("mouseenter", this.stopRotation);
      this.carousel.addEventListener("mouseleave", this.startRotation);

      this.startRotation()
    }

    paginateSlides(e) {
      //event is next or auto or keydown right arrow
      if(e === 'auto' || e.target.classList.contains('next-slide-btn') || e.key === 'ArrowRight'){
          if (this.currentSlideIndex == this.slides.length - 1) {
            this.currentSlideIndex = 0;
          }else{
            this.currentSlideIndex++
          }
      }

      //event is previous or keydown left arrow
      else if(e.target.classList.contains('prev-slide-btn') || e.key === 'ArrowLeft'){
        if (this.currentSlideIndex == 0) {
            this.currentSlideIndex = (this.slides.length - 1)
          }else{
            this.currentSlideIndex--
          }
      }

      //event is dot buttons
      else if(e.target.classList.contains('dot-btn')){
          this.currentSlideIndex = parseInt(e.target.getAttribute('data-slide'))
      }

      //update slides
      let currentSlide = this.slides[this.currentSlideIndex],
          nextSlide = this.currentSlideIndex === (this.slides.length - 1) ? this.slides[0] : this.slides[this.currentSlideIndex + 1],
          prevSlide = this.currentSlideIndex === 0 ? this.slides[this.slides.length - 1] : this.slides[this.currentSlideIndex - 1];

      this.slides.forEach(eachSlide => {
          eachSlide.classList.remove("next");
          eachSlide.classList.remove("prev");
          eachSlide.classList.remove("current");
      });

      currentSlide.classList.add("current");
      nextSlide.classList.add("next");
      prevSlide.classList.add("prev");

      //update slide-nav (if using arrows to navigate update the focus also)
      this.slideNav.querySelectorAll("button").forEach(btn => {
          btn.setAttribute("aria-selected", false);
          if(e.key === 'ArrowLeft' || e.key === 'ArrowRight'){
            btn.blur()
          }
      });
      this.slideNav.querySelectorAll("button")[this.currentSlideIndex].setAttribute("aria-selected", true);
      if(e.key === 'ArrowLeft' || e.key === 'ArrowRight'){
        this.slideNav.querySelectorAll("button")[this.currentSlideIndex].focus()
      }

      //when you paginate in any way the rotation should stop and then restart
      if(e != 'auto'){
        this.stopRotation()
        this.timer = setTimeout(()=>{this.startRotation()},2000)
      }
    };

    stopRotation(){
      clearTimeout(this.timer)
      clearInterval(this.rotationIntervalId);
    }

    startRotation(){
      if(this.paused === false )this.rotationIntervalId = setInterval(()=>{this.paginateSlides('auto')}, 4000);
    }

  }
  customElements.define('pearson-carousel', Carousel);
})(window, document);
