var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

(function (w, doc) {
  'use strict';

  // Create a template element

  var template = doc.createElement('template'),
      numberTemplate = doc.createElement('template'),
      ellipsisTemplate = doc.createElement('template'),
      compactTemplate = doc.createElement('template'),
      previousIcon = '\n      <svg focusable="false" class="icon-18" aria-hidden="true">\n\t      <path d="M5.2383866,9.62193909 C4.90819314,9.25409223 4.92101724,8.69027111 5.27685892,8.33764681 L10.386348,3.27435567 C10.7554932,2.90854811 11.3539959,2.90854811 11.7231411,3.27435567 C12.0922863,3.64016324 12.0922863,4.23325448 11.7231411,4.59906205 L7.28204859,9 L11.7231411,13.400938 C12.0922863,13.7667455 12.0922863,14.3598368 11.7231411,14.7256443 C11.3539959,15.0914519 10.7554932,15.0914519 10.386348,14.7256443 L5.27685892,9.66235319 C5.26334967,9.64896608 5.25054567,9.63548354 5.23843148,9.62191255 L5.2383866,9.62193909 Z"  fill-rule="nonzero"></path>\n      </svg>\n  ',
      nextIcon = '\n      <svg focusable="false" class="icon-18" aria-hidden="true">\n           <path d="M12.7616134,9.62193909 L12.7615685,9.62191255 C12.7494543,9.63548354 12.7366503,9.64896608 12.7231411,9.66235319 L7.61365203,14.7256443 C7.24450681,15.0914519 6.64600414,15.0914519 6.27685892,14.7256443 C5.90771369,14.3598368 5.90771369,13.7667455 6.27685892,13.400938 L10.7179514,9 L6.27685892,4.59906205 C5.90771369,4.23325448 5.90771369,3.64016324 6.27685892,3.27435567 C6.64600414,2.90854811 7.24450681,2.90854811 7.61365203,3.27435567 L12.7231411,8.33764681 C13.0789828,8.69027111 13.0918069,9.25409223 12.7616134,9.62193909 Z"  fill-rule="nonzero"></path>\n      </svg>\n  ';
  template.innerHTML = ' \n      <style>\n/*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */@import url("https://fonts.googleapis.com/css?family=Open+Sans:400,600");html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}main{display:block}h1{font-size:2em;margin:.67em 0}hr{-webkit-box-sizing:content-box;box-sizing:content-box;height:0;overflow:visible}pre{font-family:monospace,monospace;font-size:1em}a{background-color:transparent}abbr[title]{border-bottom:none;text-decoration:underline;-webkit-text-decoration:underline dotted;text-decoration:underline dotted}b,strong{font-weight:bolder}code,kbd,samp{font-family:monospace,monospace;font-size:1em}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}img{border-style:none}button,input,optgroup,select,textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}button,input{overflow:visible}button,select{text-transform:none}[type=button],[type=reset],[type=submit],button{-webkit-appearance:button}[type=button]::-moz-focus-inner,[type=reset]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner{border-style:none;padding:0}[type=button]:-moz-focusring,[type=reset]:-moz-focusring,[type=submit]:-moz-focusring,button:-moz-focusring{outline:1px dotted ButtonText}fieldset{padding:.35em .75em .625em}legend{-webkit-box-sizing:border-box;box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}progress{vertical-align:baseline}textarea{overflow:auto}[type=checkbox],[type=radio]{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0}[type=number]::-webkit-inner-spin-button,[type=number]::-webkit-outer-spin-button{height:auto}[type=search]{-webkit-appearance:textfield;outline-offset:-2px}[type=search]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}details{display:block}summary{display:list-item}template{display:none}[hidden]{display:none}html{-webkit-box-sizing:border-box;box-sizing:border-box;font-size:16px}*,:after,:before{-webkit-box-sizing:inherit;box-sizing:inherit}body{font-size:16px;font-family:Open Sans,Arial,Helvetica,sans-serif}body,p{line-height:1.5;font-weight:400}p{font-size:.875em}strong{font-weight:600}a{color:#047a9c}a:hover{color:#03536a;text-decoration:none}a:focus{outline:2px solid #0b73da;outline-offset:4px}button{cursor:pointer}.no-border{border:0}.icon-18{width:18px;height:18px}.icon-24{width:24px;height:24px}.hidden{display:none}.gr-h1{font-size:1.5em;line-height:1.75em}.gr-h1,.gr-h2{font-weight:400;margin-top:0}.gr-h2{font-size:1.25em;line-height:1.625em}.gr-h3{font-size:1.125em;line-height:1.5em;font-weight:400;margin-top:0}.gr-label{display:block;margin-bottom:4px}.gr-label,.gr-meta{font-size:.75em;line-height:1em;color:#6a7070}.gr-semi-bold{font-weight:600}.gr-font-large{font-size:1em;line-height:1.5em}.gr-font-normal{font-size:.875em;line-height:1.5}.gr-btn{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;border:1px solid #6a7070;background:transparent;color:#6a7070;min-width:152px;padding:8px 24px;font-size:1em;cursor:pointer;border-radius:22px;position:relative;margin:12px}.gr-btn:hover{color:#252525;border:1px solid #252525}.gr-btn:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}.gr-btn.primary{background-color:#047a9c;border:1px solid #047a9c;color:#fff}.gr-btn.primary:hover{color:#fff;background-color:#035f79}.gr-btn.attention{background-color:#ffb81c;border:1px solid #ffb81c;color:#252525}.gr-btn.attention:hover{background-color:#f7aa00}.gr-btn.small{min-width:128px;padding:7px 20px;font-size:.875em}.gr-btn.small:focus:after{padding:18px 21px}.gr-btn.extra-large{min-width:152px;padding:10px 24px;font-size:1em}.gr-btn.extra-large:focus:after{padding:22px 21px;border-radius:24px}.gr-btn:disabled{background-color:#e9e9e9!important;color:#6a7070!important;border:0!important;cursor:default!important}.gr-btn.icon-btn-24{min-width:auto;padding:5px 12px;border:0}.gr-btn.icon-btn-18{min-width:auto;padding:8px 12px;border:0}.gr-btn.no-border{border:0}.gr-btn.no-border:hover{border:0}.gr-btn.no-border:focus:after{border-radius:5px;min-width:auto;left:auto;top:auto;padding:16px}.gr-pagination{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center}.gr-pagination button{margin:0;min-width:44px;border:0!important;padding:0!important}.gr-pagination button span{height:28px;width:28px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;border:1px solid #c7c7c7;border-radius:50%}.gr-pagination button:focus:after{min-height:36px;min-width:36px;padding:0;top:4px;left:8px}.gr-pagination button:hover span{background-color:#e9e9e9}.gr-pagination button:disabled{background:none!important}.gr-pagination button:disabled span{border:1px solid #d9d9d9}.gr-pagination button:disabled span svg{fill:#d9d9d9}.gr-pagination button:disabled:hover span{background:#fff}.gr-pagination a{min-width:44px;color:#6a7070;text-decoration:none;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;font-size:14px}.gr-pagination a:hover{text-decoration:underline;color:#6a7070;font-weight:700}.gr-pagination a:focus{outline-offset:0}.gr-pagination a[aria-current]{text-decoration:underline;color:#252525;font-weight:700}.gr-pagination .compact-text{font-size:.875em;margin:0 1em}.gr-pagination a svg{width:18px;height:18px;display:inline-block;vertical-align:top;fill:currentColor}a.disabled{cursor:default;text-decoration:none}a.disabled:focus{outline:0}a.disabled:hover{text-decoration:none}\n      </style>\n          <nav role="navigation" data-id="pagination" aria-label="Pagination Navigation" class="gr-pagination" data-type="standard" data-pages-total="10" data-max-buttons="5" data-active-page="1" data-label="page" data-label-plural="pages">\n          <button id="prev" class="gr-btn icon-btn-18" aria-label="Previous page">\n            <span>\n              ' + previousIcon + '\n            </span>\n          </button>\n          <div id="pages" style="display:flex;">\n          </div>\n          <button id="next" class="gr-btn icon-btn-18" aria-label="Next page">\n            <span>\n              ' + nextIcon + '\n            </span>\n          </button>\n        </nav>\n  ', compactTemplate.innerHTML = '\n      <style>\n        /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */@import url("https://fonts.googleapis.com/css?family=Open+Sans:400,600");html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}main{display:block}h1{font-size:2em;margin:.67em 0}hr{-webkit-box-sizing:content-box;box-sizing:content-box;height:0;overflow:visible}pre{font-family:monospace,monospace;font-size:1em}a{background-color:transparent}abbr[title]{border-bottom:none;text-decoration:underline;-webkit-text-decoration:underline dotted;text-decoration:underline dotted}b,strong{font-weight:bolder}code,kbd,samp{font-family:monospace,monospace;font-size:1em}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}img{border-style:none}button,input,optgroup,select,textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}button,input{overflow:visible}button,select{text-transform:none}[type=button],[type=reset],[type=submit],button{-webkit-appearance:button}[type=button]::-moz-focus-inner,[type=reset]::-moz-focus-inner,[type=submit]::-moz-focus-inner,button::-moz-focus-inner{border-style:none;padding:0}[type=button]:-moz-focusring,[type=reset]:-moz-focusring,[type=submit]:-moz-focusring,button:-moz-focusring{outline:1px dotted ButtonText}fieldset{padding:.35em .75em .625em}legend{-webkit-box-sizing:border-box;box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}progress{vertical-align:baseline}textarea{overflow:auto}[type=checkbox],[type=radio]{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0}[type=number]::-webkit-inner-spin-button,[type=number]::-webkit-outer-spin-button{height:auto}[type=search]{-webkit-appearance:textfield;outline-offset:-2px}[type=search]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}details{display:block}summary{display:list-item}template{display:none}[hidden]{display:none}html{-webkit-box-sizing:border-box;box-sizing:border-box;font-size:16px}*,:after,:before{-webkit-box-sizing:inherit;box-sizing:inherit}body{font-size:16px;font-family:Open Sans,Arial,Helvetica,sans-serif}body,p{line-height:1.5;font-weight:400}p{font-size:.875em}strong{font-weight:600}a{color:#047a9c}a:hover{color:#03536a;text-decoration:none}a:focus{outline:2px solid #0b73da;outline-offset:4px}button{cursor:pointer}@-webkit-keyframes shift{to{background-position:9px 9px}}@keyframes shift{to{background-position:9px 9px}}@-webkit-keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@-webkit-keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@-webkit-keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@-webkit-keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@-webkit-keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@-webkit-keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@-webkit-keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}@keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}.fadeIn,.slideInDown{opacity:1!important;visibility:visible!important}.fadeOut,.slideOutDown{opacity:0;visibility:hidden}.slideInDown{-webkit-animation:slideInDown .3s ease-in-out 0s;animation:slideInDown .3s ease-in-out 0s}.slideOutDown{-webkit-animation:slideOutDown .2s ease-in 0s;animation:slideOutDown .2s ease-in 0s}.fadeIn{-webkit-animation:fadeIn .3s linear 0s;animation:fadeIn .3s linear 0s}.fadeOut{-webkit-animation:fadeOut .2s linear 0s;animation:fadeOut .2s linear 0s}.animateIn{-webkit-animation-name:fadeIn;animation-name:fadeIn;-webkit-animation-duration:.5s;animation-duration:.5s}.animateOut{-webkit-animation-name:fadeOut;animation-name:fadeOut;-webkit-animation-duration:.2s;animation-duration:.2s}@media (prefers-reduced-motion){.animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}}html[data-prefers-reduced-motion] .animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}.no-border{border:0}.icon-18{width:18px;height:18px}.icon-24{width:24px;height:24px}.hidden{display:none}.gr-h1{font-size:1.5em;line-height:1.75em}.gr-h1,.gr-h2{font-weight:400;margin-top:0}.gr-h2{font-size:1.25em;line-height:1.625em}.gr-h3{font-size:1.125em;line-height:1.5em;font-weight:400;margin-top:0}.gr-label{display:block;margin-bottom:4px}.gr-label,.gr-meta{font-size:.75em;line-height:1em;color:#6a7070}.gr-semi-bold{font-weight:600}.gr-font-large{font-size:1em;line-height:1.5em}.gr-font-normal{font-size:.875em;line-height:1.5}.gr-btn{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;border:1px solid #6a7070;background:transparent;color:#6a7070;min-width:152px;padding:8px 24px;font-size:1em;cursor:pointer;border-radius:22px;position:relative;margin:12px}.gr-btn:hover{color:#252525;border:1px solid #252525}.gr-btn:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}.gr-btn.primary{background-color:#047a9c;border:1px solid #047a9c;color:#fff}.gr-btn.primary:hover{color:#fff;background-color:#035f79}.gr-btn.attention{background-color:#ffb81c;border:1px solid #ffb81c;color:#252525}.gr-btn.attention:hover{background-color:#f7aa00}.gr-btn.small{min-width:128px;padding:7px 20px;font-size:.875em}.gr-btn.small:focus:after{padding:18px 21px}.gr-btn.extra-large{min-width:152px;padding:10px 24px;font-size:1em}.gr-btn.extra-large:focus:after{padding:22px 21px;border-radius:24px}.gr-btn:disabled{background-color:#e9e9e9!important;color:#6a7070!important;border:0!important;cursor:default!important}.gr-btn.icon-btn-24{min-width:auto;padding:5px 12px;border:0}.gr-btn.icon-btn-18{min-width:auto;padding:8px 12px;border:0}.gr-btn.no-border{border:0}.gr-btn.no-border:hover{border:0}.gr-btn.no-border:focus:after{border-radius:5px;min-width:auto;left:auto;top:auto;padding:16px}.gr-pagination{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center}.gr-pagination button{margin:0;min-width:44px;border:0!important;padding:0!important}.gr-pagination button span{height:28px;width:28px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;border:1px solid #c7c7c7;border-radius:50%}.gr-pagination button:focus:after{min-height:36px;min-width:36px;padding:0;top:4px;left:8px}.gr-pagination button:hover span{background-color:#e9e9e9}.gr-pagination button:disabled{background:none!important}.gr-pagination button:disabled span{border:1px solid #d9d9d9}.gr-pagination button:disabled span svg{fill:#d9d9d9}.gr-pagination button:disabled:hover span{background:#fff}.gr-pagination a{min-width:44px;color:#6a7070;text-decoration:none;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;font-size:14px}.gr-pagination a:hover{text-decoration:underline;color:#6a7070;font-weight:700}.gr-pagination a:focus{outline-offset:0}.gr-pagination a[aria-current]{text-decoration:underline;color:#252525;font-weight:700}.gr-pagination .compact-text{font-size:.875em;margin:0 1em}.gr-pagination a svg{width:18px;height:18px;display:inline-block;vertical-align:top;fill:currentColor}a.disabled{cursor:default;text-decoration:none}a.disabled:focus{outline:0}a.disabled:hover{text-decoration:none}\n      </style>\n    \t\t<nav role="navigation" data-id="compactPagination" aria-label="Pagination Navigation" class="gr-pagination" data-pages-total="10" data-type="compact" data-active-page="1" data-label="page" data-label-plural="pages">\n\t\t\t\t<button id="prev" class="gr-btn icon-btn-18" aria-label="Previous page">\n           <span>\n              ' + previousIcon + '\n            </span>\n\t\t\t\t</button>\n\t\t\t\t<span class="compact-text"><span id="pageText">Page</span> <span class="current-page">1</span> of <span class="total-pages">20</span>\n\t\t\t</span>\n\t\t\t\t<button id="next" class="gr-btn icon-btn-18" aria-label="Next page">\n\t\t\t\t\t<span>\n\t             ' + nextIcon + '\n\t\t\t\t\t</span>\n\t\t\t\t</button>\n\t\t\t</nav>\n    ', numberTemplate.innerHTML = '\n      <a href="#"><span></span></a>\n    ', ellipsisTemplate.innerHTML = '\n      <a href="#" class="ellipsis" class="disabled" aria-label="additional pages">...</a>\n    ';

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-pagination');

  /** Any helper functions that do not need to be part of the class
   * can be declared here, before the class is defined.
   */
  function range(start, end) {
    var step = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;

    var len = Math.floor((end - start) / step) + 1;
    return Array(len).fill().map(function (_, idx) {
      return start + idx * step;
    });
  }

  function renderItems(options) {
    var nextEllipsisNumber = parseInt(options.referenceNode.getAttribute('data-page'));

    while (options.start <= options.end && options.end < nextEllipsisNumber) {
      var nextNode = options.reference.nextElementSibling,
          renderTemplate = options.newNode.content.cloneNode(true),
          renderContent = renderTemplate.querySelector('span');

      nextNode.remove();
      renderContent.parentNode.setAttribute('aria-label', 'page ' + options.start);
      renderContent.parentNode.setAttribute('data-page', options.start);
      renderContent.innerHTML = options.start;

      if (options.start === options.newNumber) {
        renderContent.parentNode.setAttribute('aria-current', 'page');
      }

      if (options.end + 1 === nextEllipsisNumber) {
        options.referenceNode.innerHTML = nextEllipsisNumber;
        options.referenceNode.removeAttribute('data-ellipsis');
        options.referenceNode.classList.remove('disabled');
        options.referenceNode.setAttribute('aria-label', 'page ' + nextEllipsisNumber);
      } else {
        options.referenceNode.innerHTML = '...';
        options.referenceNode.classList.add('disabled');
        options.referenceNode.setAttribute('data-ellipsis', true);
        options.referenceNode.setAttribute('aria-label', 'additional pages');
      }

      renderContent.parentNode.addEventListener('click', function (event) {
        options.this.addListener(event, options);
      });

      options.parentNode.insertBefore(renderTemplate, options.referenceNode);
      options.start++;
    }
  }

  var Pagination = function (_HTMLElement) {
    _inherits(Pagination, _HTMLElement);

    _createClass(Pagination, [{
      key: 'addListener',
      value: function addListener(event) {
        event.preventDefault();
        this.currentPage = event.currentTarget.getAttribute('data-page');
        this.dispatchEvent(new Event('newPage', {
          bubbles: true
        }));
      }
    }, {
      key: 'changePage',
      value: function changePage(type) {
        var currentPage = this.shadowRoot.querySelector('#pages a[aria-current]'),
            nextPage = currentPage.nextElementSibling,
            previousPage = currentPage.previousElementSibling;

        if (type === 'next') {
          if (currentPage.getAttribute('data-page') < this.lastPage) {
            this.currentPage = nextPage.getAttribute('data-page');
            currentPage.removeAttribute('aria-current');
          }
        } else {
          if (currentPage.getAttribute('data-page') > this.firstPage) {
            this.currentPage = previousPage.getAttribute('data-page');
            currentPage.removeAttribute('aria-current');
          }
        }
        this.dispatchEvent(new Event(type + 'Page', {
          bubbles: true
        }));
      }
    }, {
      key: 'text',
      get: function get() {
        return this.getAttribute('text');
      },
      set: function set(value) {
        this.setAttribute('text', value);
      }
    }, {
      key: 'compact',
      get: function get() {
        return this.hasAttribute('compact');
      }
    }, {
      key: 'firstPage',
      get: function get() {
        return 1;
      }
    }, {
      key: 'lastPage',
      get: function get() {
        return parseInt(this.getAttribute('lastpage'));
      }
    }, {
      key: 'currentPage',
      get: function get() {
        return parseInt(this.getAttribute('currentpage'));
      },
      set: function set(value) {
        this.setAttribute('currentpage', value);
      }
    }, {
      key: 'ellipsisAt',
      get: function get() {
        return parseInt(this.getAttribute('ellipsisat'));
      },
      set: function set(value) {
        this.setAttribute('ellipsisat', value);
      }
    }], [{
      key: 'observedAttributes',
      get: function get() {
        return ['firstpage', 'lastpage', 'currentpage', 'ellipsisat', 'compact', 'text'];
      }
    }]);

    function Pagination() {
      _classCallCheck(this, Pagination);

      var _this = _possibleConstructorReturn(this, (Pagination.__proto__ || Object.getPrototypeOf(Pagination)).call(this));

      _this.attachShadow({ mode: 'open' });
      return _this;
    }

    _createClass(Pagination, [{
      key: 'connectedCallback',
      value: function connectedCallback() {
        var _this2 = this;

        if (!this.compact) {
          var clone = template.content.cloneNode(true);
          this.pageTarget = clone.querySelector('#pages');
          this.nextPageBtn = clone.querySelector('#next');
          this.prevPageBtn = clone.querySelector('#prev');
          this.changePage = this.changePage.bind(this);
          this.addListener = this.addListener.bind(this);
          this.shadowRoot.appendChild(clone);

          this.pageRange = range(this.firstPage, this.lastPage, 1);
          this.pageRange.forEach(function (number, index) {
            var numberTemplateContainer = numberTemplate.content.cloneNode(true),
                numberTemplateSpan = numberTemplateContainer.querySelector('span'),
                numberTemplateParent = numberTemplateSpan.parentNode;

            var total = index + 1,
                placeLastNumber = _this2.ellipsisAt + 2,
                placeEllipsis = _this2.ellipsisAt + 1;

            _this2.template = numberTemplateContainer;
            _this2.span = numberTemplateSpan;
            _this2.parent = numberTemplateParent;

            _this2.parent.setAttribute('aria-label', 'page ' + number);
            _this2.parent.setAttribute('data-page', number);
            _this2.span.innerHTML = number;

            if (_this2.currentPage === _this2.firstPage) {
              var button = _this2.shadowRoot.querySelector('#prev');
              button.setAttribute('disabled', true);
            }
            if (_this2.currentPage === _this2.lastPage) {
              var _button = _this2.shadowRoot.querySelector('#next');
              _button.setAttribute('disabled', true);
            }

            if (_this2.lastPage > _this2.ellipsisAt) {
              if (total <= placeLastNumber) {
                if (number === _this2.currentPage) {
                  _this2.parent.setAttribute('aria-current', 'page');
                }

                if (total === placeEllipsis) {
                  _this2.parent.setAttribute('data-page', _this2.lastPage - 1);
                  _this2.parent.setAttribute('data-ellipsis', true);
                  _this2.parent.setAttribute('aria-label', 'additional pages');
                  _this2.span.innerHTML = '...';
                  _this2.parent.classList.add('disabled');
                }

                if (total === placeLastNumber) {
                  _this2.parent.setAttribute('data-page', _this2.lastPage);
                  _this2.span.innerHTML = _this2.lastPage;
                }
                _this2.pageTarget.appendChild(_this2.template);
              }
            } else {
              if (number === _this2.currentPage) {
                _this2.parent.setAttribute('aria-current', 'page');
              }
              _this2.pageTarget.appendChild(_this2.template);
            }
          });
          var pageBtns = this.shadowRoot.querySelectorAll('nav button, #pages > a');

          pageBtns.forEach(function (button) {
            button.addEventListener('click', function (event) {
              event.preventDefault();
              button.removeAttribute('disabled');
              if (button.tagName === 'BUTTON') {
                button.setAttribute('aria-label', 'Page ' + _this2.currentPage + ', Next Page');
                if (_this2.currentPage >= _this2.firstPage) {
                  _this2.prevPageBtn.removeAttribute('disabled');
                }
                _this2.changePage(button.id);
              } else if (button.tagName === 'A') {
                _this2.addListener(event);
              }
            });
          });

          this.addEventListener('newPage', function (event) {
            _this2.shadowRoot.querySelector('[data-page="' + _this2.currentPage + '"]').focus();
          });
        } else {
          var _clone = compactTemplate.content.cloneNode(true);
          this.pageText = _clone.querySelector('#pageText');
          this.total = _clone.querySelector('.total-pages');
          this.page = _clone.querySelector('.current-page');
          this.pageTarget = _clone.querySelector('#pages');
          this.nextPageBtn = _clone.querySelector('#next');
          this.prevPageBtn = _clone.querySelector('#prev');
          this.shadowRoot.appendChild(_clone);
          this.currentPage = this.currentPage;
          this.pageText.innerHTML = this.text;
          var _pageBtns = this.shadowRoot.querySelectorAll('nav button');
          _pageBtns.forEach(function (button) {
            button.addEventListener('click', function (event) {
              var target = event.currentTarget.id;
              if (target === 'next') {
                if (_this2.currentPage < _this2.lastPage) {
                  _this2.currentPage = _this2.currentPage + 1;
                  _this2.dispatchEvent(new Event('nextPage', {
                    bubbles: true
                  }));
                }
              }
              if (target === 'prev') {
                if (_this2.currentPage > 1) {
                  _this2.currentPage = _this2.currentPage - 1;
                  _this2.dispatchEvent(new Event('previousPage', {
                    bubbles: true
                  }));
                } else {
                  return false;
                }
              }
            });
          });

          if (this.currentPage < this.lastPage) {
            this.page.innerHTML = this.currentPage;
          } else {
            this.page.innerHTML = this.lastPage;
          }
          if (this.currentPage === this.firstPage) {
            this.prevPageBtn.setAttribute('disabled', true);
          }
          if (this.currentPage === this.lastPage) {
            this.nextPageBtn.setAttribute('disabled', true);
          }
          this.total.innerHTML = this.lastPage;
        }
      }
    }, {
      key: 'attributeChangedCallback',
      value: function attributeChangedCallback(name, oldValue, newValue) {
        var _this3 = this;

        if (!this.compact) {
          if (name === 'ellipsisat') {
            if (this.ellipsisAt >= 1 && this.ellipsisAt <= 3) {
              this.ellipsisAt = 4;
            }
            if (this.ellipsisAt > this.lastPage - 3) {
              this.ellipsisAt = this.lastPage - 3;
            }
          }
          if (oldValue !== null) {
            var newNumber = parseInt(newValue);
            if (oldValue !== newValue) {
              if (name === 'currentpage') {
                var newPage = this.shadowRoot.querySelector('[data-page="' + newValue + '"]'),
                    oldPage = this.shadowRoot.querySelector('[data-page="' + oldValue + '"]'),
                    firstPage = this.shadowRoot.querySelector('[data-page="' + this.firstPage + '"]'),
                    lastPage = this.shadowRoot.querySelector('[data-page="' + this.lastPage + '"]'),
                    allLinks = this.shadowRoot.querySelectorAll('#pages > a');

                if (oldValue !== null && oldPage !== null) {

                  oldPage.removeAttribute('aria-current');
                  if (!newPage.hasAttribute('data-ellipsis')) {
                    newPage.setAttribute('aria-current', 'page');
                    newPage.setAttribute('aria-label', 'page ' + newValue);
                  }
                }

                if (newNumber === this.firstPage) {
                  allLinks.forEach(function (link, index) {
                    var value = index + 1;
                    link.innerHTML = value;
                    link.removeAttribute('data-ellipsis');
                    link.setAttribute('data-page', value);

                    if (value === _this3.ellipsisAt + 1) {
                      link.setAttribute('data-ellipsis', true);
                      link.innerHTML = '...';
                      link.setAttribute('data-page', _this3.lastPage - 1);
                      link.setAttribute('aria-label', 'additional pages');
                      link.classList.add('disabled');
                    }
                    if (value === _this3.ellipsisAt + 2) {
                      link.innerHTML = _this3.lastPage;
                      link.setAttribute('data-page', _this3.lastPage);
                    }
                  });
                }

                if (newNumber === this.lastPage) {

                  var totalPages = allLinks.length;
                  var startNumber = this.lastPage - totalPages + 1;
                  allLinks.forEach(function (link, index) {
                    link.innerHTML = startNumber;
                    link.removeAttribute('data-ellipsis');
                    link.setAttribute('data-page', startNumber);
                    link.classList.remove('disabled');

                    if (index === 0) {
                      link.innerHTML = 1;
                      link.setAttribute('data-page', 1);
                    }
                    if (index === 1 && !isNaN(_this3.ellipsisAt)) {

                      link.setAttribute('data-ellipsis', true);
                      link.setAttribute('aria-label', 'additional pages');
                      link.innerHTML = '...';
                      link.setAttribute('data-page', _this3.firstPage + 1);
                      link.classList.add('disabled');
                    }
                    startNumber++;
                  });
                }
                if (this.currentPage === this.firstPage) {
                  this.prevPageBtn.setAttribute('disabled', true);
                } else {
                  this.prevPageBtn.removeAttribute('disabled');
                }
                if (this.currentPage === this.lastPage) {
                  this.nextPageBtn.setAttribute('disabled', true);
                } else {
                  this.nextPageBtn.removeAttribute('disabled');
                }
                if (newPage !== null && newPage.nextElementSibling !== null && newPage.previousElementSibling !== null) {
                  var nextEllipsis = newPage.nextElementSibling.getAttribute('data-ellipsis'),
                      previousEllipsis = newPage.previousElementSibling.getAttribute('data-ellipsis'),
                      previousEllipsisNode = firstPage.nextElementSibling,
                      nextEllipsisNode = lastPage.previousElementSibling,
                      endRange = nextEllipsisNode.previousElementSibling.getAttribute('data-page'),
                      startRange = previousEllipsisNode.nextElementSibling.getAttribute('data-page'),
                      options = {
                    start: parseInt(startRange),
                    newNumber: newNumber,
                    end: parseInt(endRange),
                    reference: previousEllipsisNode,
                    newNode: numberTemplate,
                    parentNode: this.pageTarget,
                    referenceNode: nextEllipsisNode,
                    this: this,
                    newPage: newPage
                  };

                  if (nextEllipsis) {
                    firstPage.nextElementSibling.innerHTML = '...';
                    firstPage.nextElementSibling.classList.add('disabled');
                    firstPage.nextElementSibling.setAttribute('data-ellipsis', true);
                    firstPage.nextElementSibling.setAttribute('aria-label', 'additional pages');
                    options.end = options.end + 1;
                    renderItems(options);
                    if (newValue > this.firstPage) {
                      var button = this.shadowRoot.querySelector('#next');
                      button.removeAttribute('disabled');
                    }
                  } else if (previousEllipsis) {
                    options.start = options.start - 1;
                    options.end = options.end - 1;
                    renderItems(options);
                    if (options.newNumber - 2 === parseInt(previousEllipsisNode.getAttribute('data-page'))) {
                      firstPage.nextElementSibling.innerHTML = parseInt(previousEllipsisNode.getAttribute('data-page'));
                      firstPage.nextElementSibling.removeAttribute('data-ellipsis');
                      firstPage.nextElementSibling.classList.remove('disabled');
                      firstPage.nextElementSibling.setAttribute('aria-label', 'page ' + parseInt(previousEllipsisNode.getAttribute('data-page')));
                    }
                  }
                }
              }
            }
          }
        } else {
          if (oldValue !== null) {
            if (oldValue !== newValue) {
              this.pageText.innerHTML = this.text;
              this.currentPage = this.currentPage;
              if (newValue > this.firstPage) {
                var _button2 = this.shadowRoot.querySelector('#prev');
                _button2.removeAttribute('disabled');
              } else {
                var _button3 = this.shadowRoot.querySelector('#prev');
                _button3.setAttribute('disabled', true);
              }
              if (newValue < this.lastPage) {
                var _button4 = this.shadowRoot.querySelector('#next');
                _button4.removeAttribute('disabled');
              } else {
                var _button5 = this.shadowRoot.querySelector('#next');
                _button5.setAttribute('disabled', true);
              }
              this.page.innerHTML = this.currentPage;
            }
          }
        }
      }
    }]);

    return Pagination;
  }(HTMLElement);

  customElements.define('pearson-pagination', Pagination);
})(window, document);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

(function (w, doc) {
  'use strict';

  doc.addEventListener("DOMContentLoaded", function () {
    if (!document.getElementById('pe-icons-sprite')) {
      var pe_ajax = new XMLHttpRequest();
      pe_ajax.open("GET", "https://pearsonux.sfo2.cdn.digitaloceanspaces.com/css/p-icons-sprite-1.1.svg", true);
      pe_ajax.responseType = "document";
      pe_ajax.onload = function (e) {
        document.body.insertBefore(pe_ajax.responseXML.documentElement, document.body.childNodes[0]);
        var events = new Event('iconsLoaded');
        document.dispatchEvent(events);
      };
      pe_ajax.send();
    }
  });
  // Create a template element
  var template = doc.createElement('template');

  template.innerHTML = ' \n    <style>\n\n@import url("https://fonts.googleapis.com/css?family=Open+Sans:400,600");:host{\n  /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */}:host html{line-height:1.15;-webkit-text-size-adjust:100%}:host body{margin:0}:host main{display:block}:host h1{font-size:2em;margin:.67em 0}:host hr{-webkit-box-sizing:content-box;box-sizing:content-box;height:0;overflow:visible}:host pre{font-family:monospace,monospace;font-size:1em}:host a{background-color:transparent}:host abbr[title]{border-bottom:none;text-decoration:underline;-webkit-text-decoration:underline dotted;text-decoration:underline dotted}:host b,:host strong{font-weight:bolder}:host code,:host kbd,:host samp{font-family:monospace,monospace;font-size:1em}:host small{font-size:80%}:host sub,:host sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}:host sub{bottom:-.25em}:host sup{top:-.5em}:host img{border-style:none}:host button,:host input,:host optgroup,:host select,:host textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}:host button,:host input{overflow:visible}:host button,:host select{text-transform:none}:host [type=button],:host [type=reset],:host [type=submit],:host button{-webkit-appearance:button}:host [type=button]::-moz-focus-inner,:host [type=reset]::-moz-focus-inner,:host [type=submit]::-moz-focus-inner,:host button::-moz-focus-inner{border-style:none;padding:0}:host [type=button]:-moz-focusring,:host [type=reset]:-moz-focusring,:host [type=submit]:-moz-focusring,:host button:-moz-focusring{outline:1px dotted ButtonText}:host fieldset{padding:.35em .75em .625em}:host legend{-webkit-box-sizing:border-box;box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}:host progress{vertical-align:baseline}:host textarea{overflow:auto}:host [type=checkbox],:host [type=radio]{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0}:host [type=number]::-webkit-inner-spin-button,:host [type=number]::-webkit-outer-spin-button{height:auto}:host [type=search]{-webkit-appearance:textfield;outline-offset:-2px}:host [type=search]::-webkit-search-decoration{-webkit-appearance:none}:host ::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}:host details{display:block}:host summary{display:list-item}:host [hidden],:host template{display:none}:host html{font-size:14px}:host *,:host html{-webkit-box-sizing:border-box;box-sizing:border-box}:host body{font-family:Open Sans,Arial,Helvetica,sans-serif}:host body,:host p{font-size:14px;line-height:1.5;font-weight:400}:host strong{font-weight:600}:host a{font-size:14px;color:#047a9c}:host a:hover{color:#03536a;text-decoration:none}:host a:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host button{cursor:pointer}:host li,:host ul{font-size:14px}:host svg{fill:#252525}:host svg[focusable=false]:focus{outline:none}:host select{cursor:pointer}:host input,:host textarea{font:inherit;letter-spacing:inherit;word-spacing:inherit}:host svg{pointer-events:none}@-webkit-keyframes shift{to{background-position:9px 9px}}@keyframes shift{to{background-position:9px 9px}}@-webkit-keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@-webkit-keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@-webkit-keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@-webkit-keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@-webkit-keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@-webkit-keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@-webkit-keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}@keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}:host .fadeIn,:host .slideInDown{opacity:1!important;visibility:visible!important}:host .fadeOut,:host .slideOutDown{opacity:0;visibility:hidden}:host .slideInDown{-webkit-animation:slideInDown .3s ease-in-out 0s;animation:slideInDown .3s ease-in-out 0s}:host .slideOutDown{-webkit-animation:slideOutDown .2s ease-in 0s;animation:slideOutDown .2s ease-in 0s}:host .fadeIn{-webkit-animation:fadeIn .3s linear 0s;animation:fadeIn .3s linear 0s}:host .fadeOut{-webkit-animation:fadeOut .2s linear 0s;animation:fadeOut .2s linear 0s}:host .animateIn{-webkit-animation-name:fadeIn;animation-name:fadeIn;-webkit-animation-duration:.5s;animation-duration:.5s}:host .animateOut{-webkit-animation-name:fadeOut;animation-name:fadeOut;-webkit-animation-duration:.2s;animation-duration:.2s}@media (prefers-reduced-motion){:host .animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}}:host html[data-prefers-reduced-motion] .animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}:host .no-border{border:0}:host .icon-18{width:18px;height:18px}:host .icon-24{width:24px;height:24px}:host .hidden{display:none!important}@media screen and (-ms-high-contrast:active),screen and (-ms-high-contrast:none){:host button svg{width:auto}}:host svg{fill:currentColor!important}\n\n</style>\n    <svg focusable="false" aria-hidden="true">\n\n    </svg>\n';

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-icon');

  var Icon = function (_HTMLElement) {
    _inherits(Icon, _HTMLElement);

    _createClass(Icon, [{
      key: 'icon',
      get: function get() {
        return this.getAttribute('icon');
      },
      set: function set(value) {
        this.setAttribute('icon', value);
      }
    }, {
      key: 'size',
      get: function get() {
        return this.getAttribute('size');
      },
      set: function set(number) {
        this.setAttribute('size', number);
      }
    }], [{
      key: 'observedAttributes',
      get: function get() {
        return ['icon', 'size'];
      }
    }]);

    function Icon() {
      _classCallCheck(this, Icon);

      var _this = _possibleConstructorReturn(this, (Icon.__proto__ || Object.getPrototypeOf(Icon)).call(this));

      _this.attachShadow({ mode: 'open' });
      var clone = template.content.cloneNode(true);
      _this.svg = clone.querySelector('svg');
      _this.use = clone.querySelector('use');
      _this.shadowRoot.appendChild(clone);
      return _this;
    }

    _createClass(Icon, [{
      key: 'ieBuildIcon',
      value: function ieBuildIcon() {
        var _this2 = this;

        var svgSprite = document.getElementsByTagName('svg')[0].childNodes,
            svgNode = this.shadowRoot.querySelector('svg');

        svgSprite.forEach(function (icon) {
          if (icon.nodeName !== '#text') {
            if (icon.id === _this2.iconName) {
              icon.childNodes.forEach(function (node) {
                if (node.nodeName !== '#text') {
                  _this2.svg.appendChild(node.cloneNode(true));
                }
              });
            }
          }
        });
      }
    }, {
      key: 'attributeChangedCallback',
      value: function attributeChangedCallback(name, prevValue, newValue) {
        var _this3 = this;

        if (name === 'icon' || name === 'size') {
          if (prevValue !== newValue) {
            var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
            this.iconName = this.icon + '-' + this.size;
            if (!document.getElementById('pe-icons-sprite')) {
              document.addEventListener('iconsLoaded', function (event) {
                if (isIE11) {
                  _this3.ieBuildIcon();
                } else {
                  var svgNode = document.querySelector('#' + _this3.iconName);
                  _this3.svg.innerHTML = svgNode.innerHTML;
                  _this3.svg.classList.add('icon-' + _this3.size);
                }
              });
            } else {
              if (isIE11) {
                var svgSprite = document.getElementsByTagName('svg')[0].childNodes;
                if (svgSprite) {
                  while (this.svg.firstChild) {
                    this.svg.removeChild(this.svg.firstChild);
                  }
                  this.ieBuildIcon();
                }
              } else {
                var svgNode = document.querySelector('#' + this.iconName);
                if (svgNode) {
                  this.svg.innerHTML = svgNode.innerHTML;
                  this.svg.classList.add('icon-' + this.size);
                }
              }
            }
          }
        }
      }
    }]);

    return Icon;
  }(HTMLElement);

  customElements.define('pearson-icon', Icon);
})(window, document);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
    }
  }return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
  };
}();

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _possibleConstructorReturn(self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
  }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
}

(function (w, doc) {
  'use strict';

  var template = doc.createElement('template'),
      optionTemplate = doc.createElement('template');

  template.innerHTML = '\n    <style>\n    *{\n    box-sizing: border-box;\n    }\n    \n    .gr-fieldset {\n      border: none;\n      padding: 0;\n    }\n\n    .gr-fieldset legend {\n      margin-bottom: 8px; \n    }\n    \n    .gr-label, .gr-meta {\n        font-size: 12px;\n        color: #6a7070;\n    }\n    \n    .gr-label {\n        line-height: 16px;\n        display: block;\n        margin-bottom: 4px;\n    }\n    \n    .gr-fieldset ul {\n      margin:0;\n      padding-inline-start: 0px;\n    }\n    \n   .gr-radio {\n      margin-bottom: 14px;\n      min-height: 16px;\n      position: relative;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n      -ms-flex-align: center;\n      align-items: center;\n      }\n    \n    .gr-radio input[type=radio] {\n      opacity: 0;\n      position: absolute;\n      margin: 0 0 0 3px;\n     }\n    \n    .gr-radio input[type=radio]:focus ~ span {\n      outline: 0;\n      -webkit-box-shadow: 0 0 0 2px white, 0 0 0 4px #1977D4;\n      box-shadow: 0 0 0 2px white, 0 0 0 4px #1977D4;\n     }\n    \n    .gr-radio input[type=radio] + label {\n      display: inline-block;\n      line-height: 18px;\n      padding-left: 28px;\n      font-size: 14px;\n      cursor: pointer;\n     }\n    \n    .gr-radio input[type=radio] ~ span {\n      -webkit-box-sizing: content-box;\n      border: 2px solid #c7c7c7;\n      background: white;\n      border-radius: 50%;\n      box-sizing: content-box;\n      color: #6a7070;\n      display: block;\n      height: 5px;\n      left: 0;\n      padding: 3px 6px 6px 3px;\n      pointer-events: none;\n      position: absolute;\n      top: 0;\n      width: 5px;\n      z-index:-1;\n      }\n    \n    .gr-radio input[type=radio] ~ span svg {\n      height: 18px;\n      opacity: 0;\n      width: 18px;\n     }\n    \n    .gr-radio input[type=radio]:checked ~ span svg {\n      opacity: 1;\n      top: -5px;\n      position: relative;\n      left: -5px;\n      fill: #047A9C;\n     }\n    \n    .gr-radio input[type=radio]:disabled ~ span svg {\n      opacity: 1;\n      fill: #C7C7C7;\n      top: -5px;\n      left: -5px;\n      position: relative;\n     }\n     \n     .error-state{\n        display: none;\n     }\n     \n     .icon-18 {\n      width: 18px;\n      height: 18px;\n     }\n     \n     fieldset.error~.error-state{\n        display: flex;\n        align-items: center;\n     }\n    \n    fieldset.error + .error-state span{\n        margin-left: 8px;\n        color: #DB0020;\n    }\n     \n     fieldset.error + .error-state svg{\n         fill: #DB0020;\n    }\n     \n    </style>\n    <div class="gr-form-element">\n      <fieldset class="gr-fieldset">\n        <legend class="gr-label">This is a radio group</legend>\n        <ul>\n        <!--radios go here-->\n        </ul>\n      </fieldset>\n      <div class="error-state">\n        <svg focusable="false" class="icon-18" aria-hidden="true">\n          <path d="M10.3203543,1.76322881 L17.7947154,14.7065602 C18.2165963,15.4371302 17.9673988,16.3719805 17.2381172,16.7946067 C17.0059424,16.9291544 16.742459,17 16.4742343,17 L1.52551217,17 C0.682995061,17 0,16.3157983 0,15.4717927 C0,15.2030941 0.0707206291,14.9391453 0.205031086,14.7065602 L7.67939217,1.76322881 C8.10127304,1.03265881 9.03447459,0.78302105 9.76375615,1.20564727 C9.99478499,1.33953088 10.1867068,1.5317918 10.3203543,1.76322881 Z M8.5,13 C8.22385763,13 8,13.2238576 8,13.5 L8,14.5 C8,14.7761424 8.22385763,15 8.5,15 L9.5,15 C9.77614237,15 10,14.7761424 10,14.5 L10,13.5 C10,13.2238576 9.77614237,13 9.5,13 L8.5,13 Z M8.5,7 C8.22385763,7 8,7.22385763 8,7.5 L8,11.5 C8,11.7761424 8.22385763,12 8.5,12 L9.5,12 C9.77614237,12 10,11.7761424 10,11.5 L10,7.5 C10,7.22385763 9.77614237,7 9.5,7 L8.5,7 Z"></path>\n        </svg>\n        <span class="gr-meta warning-text">Warning Text</span>\n      </div>\n    </div>\n';

  optionTemplate.innerHTML = '\n   <li class="gr-radio">\n        <input type="radio" name="scotch" id="radioOne" >\n        <label for="radioOne">Selection One</label>\n        <span>\n        <svg focusable="false" class="icon-18" aria-hidden="true">\n          <path d="M9,18 C4.02943725,18 0,13.9705627 0,9 C0,4.02943725 4.02943725,0 9,0 C13.9705627,0 18,4.02943725 18,9 C18,13.9705627 13.9705627,18 9,18 Z M9,16 C12.8659932,16 16,12.8659932 16,9 C16,5.13400675 12.8659932,2 9,2 C5.13400675,2 2,5.13400675 2,9 C2,12.8659932 5.13400675,16 9,16 Z"></path>\n          <path d="M9,14.3 C11.9271092,14.3 14.3,11.9271092 14.3,9 C14.3,6.07289083 11.9271092,3.7 9,3.7 C6.07289083,3.7 3.7,6.07289083 3.7,9 C3.7,11.9271092 6.07289083,14.3 9,14.3 Z"></path>\n        </svg>\n        </span>\n    </li>\n  ';

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-radio');

  var Radio = function (_HTMLElement) {
    _inherits(Radio, _HTMLElement);

    _createClass(Radio, [{
      key: 'error',
      get: function get() {
        return this.hasAttribute('error');
      }
    }, {
      key: 'errorText',
      get: function get() {
        return this.getAttribute('error');
      }
    }, {
      key: 'disabled',
      get: function get() {
        return this.hasAttribute('disabled');
      }
    }, {
      key: 'labelText',
      get: function get() {
        return this.getAttribute('label');
      }
    }, {
      key: 'groupName',
      get: function get() {
        return this.getAttribute('groupname');
      }
    }, {
      key: 'labeltext',
      set: function set(value) {
        this.setAttribute('label', value);
      }
    }], [{
      key: 'observedAttributes',
      get: function get() {
        return ['label', 'groupName', 'disabled', 'error'];
      }
    }]);

    function Radio() {
      _classCallCheck(this, Radio);

      var _this = _possibleConstructorReturn(this, (Radio.__proto__ || Object.getPrototypeOf(Radio)).call(this));

      _this.attachShadow({ mode: 'open' });
      var clone = template.content.cloneNode(true);
      _this.label = clone.querySelector('legend');
      _this.fieldset = clone.querySelector('fieldset');
      _this.fieldsetList = clone.querySelector('fieldset ul');
      _this.warningText = clone.querySelector('.warning-text');

      _this.renderedInputs = null;
      _this.value = null;

      _this.shadowRoot.appendChild(clone);

      _this.buildOptions = _this.buildOptions.bind(_this);
      _this.disableInputs = _this.disableInputs.bind(_this);

      _this.fieldset.addEventListener('click', function (event) {
        var clickedInput = event.target.closest('input');
        if (!clickedInput) {
          return;
        } else if (clickedInput.checked === true) {
          _this.value = clickedInput.value;
          _this.data = clickedInput.getAttribute('data-index');
        }
      });
      return _this;
    }

    _createClass(Radio, [{
      key: 'connectedCallback',
      value: function connectedCallback() {
        setTimeout(this.buildOptions, 100);
      }
    }, {
      key: 'attributeChangedCallback',
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'label') {
          if (oldValue !== newValue) {
            this.label.innerHTML = this.labelText;
          }
        }

        if (name === 'disabled') {
          if (oldValue !== newValue) {
            setTimeout(this.disableInputs, 100);
          }
        }

        if (name === 'error') {
          if (oldValue !== newValue) {
            if (this.error === true) {
              this.fieldset.classList.add('error');
              this.warningText.innerHTML = this.errorText;
            } else {
              this.fieldset.classList.remove('error');
            }
          }
        }
      }
    }, {
      key: 'buildOptions',
      value: function buildOptions() {
        var _this2 = this;

        this.options = this.querySelectorAll('input');
        this.options.forEach(function (option) {
          var optionClone = optionTemplate.content.cloneNode(true);
          var radioLabel = optionClone.querySelector('label'),
              radio = optionClone.querySelector('input');

          radio.id = option.name;
          radio.setAttribute('data-index', option.getAttribute('data-index'));
          radio.setAttribute('name', _this2.groupName);
          radio.setAttribute('value', option.name);
          radioLabel.innerText = option.name;
          radioLabel.setAttribute('for', option.name);
          _this2.fieldsetList.appendChild(optionClone);
        });
        if (this.disabled === true) {
          this.disableInputs();
        }
      }
    }, {
      key: 'disableInputs',
      value: function disableInputs() {
        var renderedInputs = this.shadowRoot.querySelectorAll('input');
        if (this.disabled === true) {
          renderedInputs.forEach(function (input) {
            input.disabled = true;
          });
        } else {
          renderedInputs.forEach(function (input) {
            input.disabled = false;
          });
        }
      }
    }]);

    return Radio;
  }(HTMLElement);

  customElements.define('pearson-radio', Radio);
})(window, document);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function (w, doc) {
  'use strict';

  var template = doc.createElement('template'),
      dotTemplate = doc.createElement('template');

  template.innerHTML = ' \n    <style> \n        @import url("https://fonts.googleapis.com/css?family=Open+Sans:400,600");:host{font-family:Open Sans,sans-serif\n  /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */}:host html{line-height:1.15;-webkit-text-size-adjust:100%}:host body{margin:0}:host main{display:block}:host h1{font-size:2em;margin:.67em 0}:host hr{-webkit-box-sizing:content-box;box-sizing:content-box;height:0;overflow:visible}:host pre{font-family:monospace,monospace;font-size:1em}:host a{background-color:transparent}:host abbr[title]{border-bottom:none;text-decoration:underline;-webkit-text-decoration:underline dotted;text-decoration:underline dotted}:host b,:host strong{font-weight:bolder}:host code,:host kbd,:host samp{font-family:monospace,monospace;font-size:1em}:host small{font-size:80%}:host sub,:host sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}:host sub{bottom:-.25em}:host sup{top:-.5em}:host img{border-style:none}:host button,:host input,:host optgroup,:host select,:host textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}:host button,:host input{overflow:visible}:host button,:host select{text-transform:none}:host [type=button],:host [type=reset],:host [type=submit],:host button{-webkit-appearance:button}:host [type=button]::-moz-focus-inner,:host [type=reset]::-moz-focus-inner,:host [type=submit]::-moz-focus-inner,:host button::-moz-focus-inner{border-style:none;padding:0}:host [type=button]:-moz-focusring,:host [type=reset]:-moz-focusring,:host [type=submit]:-moz-focusring,:host button:-moz-focusring{outline:1px dotted ButtonText}:host fieldset{padding:.35em .75em .625em}:host legend{-webkit-box-sizing:border-box;box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}:host progress{vertical-align:baseline}:host textarea{overflow:auto}:host [type=checkbox],:host [type=radio]{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0}:host [type=number]::-webkit-inner-spin-button,:host [type=number]::-webkit-outer-spin-button{height:auto}:host [type=search]{-webkit-appearance:textfield;outline-offset:-2px}:host [type=search]::-webkit-search-decoration{-webkit-appearance:none}:host ::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}:host details{display:block}:host summary{display:list-item}:host [hidden],:host template{display:none}:host html{font-size:14px}:host *,:host html{-webkit-box-sizing:border-box;box-sizing:border-box}:host body{font-family:Open Sans,Arial,Helvetica,sans-serif}:host body,:host p{font-size:14px;line-height:1.5;font-weight:400}:host strong{font-weight:600}:host a{font-size:14px;color:#047a9c}:host a:hover{color:#03536a;text-decoration:none}:host a:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host button{cursor:pointer}:host li,:host ul{font-size:14px}:host svg{fill:#252525}:host svg[focusable=false]:focus{outline:none}:host select{cursor:pointer}:host input,:host textarea{font:inherit;letter-spacing:inherit;word-spacing:inherit}:host svg{pointer-events:none}@-webkit-keyframes shift{to{background-position:9px 9px}}@keyframes shift{to{background-position:9px 9px}}@-webkit-keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@-webkit-keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@-webkit-keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@-webkit-keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@-webkit-keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@-webkit-keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@-webkit-keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}@keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}:host .fadeIn,:host .slideInDown{opacity:1!important;visibility:visible!important}:host .fadeOut,:host .slideOutDown{opacity:0;visibility:hidden}:host .slideInDown{-webkit-animation:slideInDown .3s ease-in-out 0s;animation:slideInDown .3s ease-in-out 0s}:host .slideOutDown{-webkit-animation:slideOutDown .2s ease-in 0s;animation:slideOutDown .2s ease-in 0s}:host .fadeIn{-webkit-animation:fadeIn .3s linear 0s;animation:fadeIn .3s linear 0s}:host .fadeOut{-webkit-animation:fadeOut .2s linear 0s;animation:fadeOut .2s linear 0s}:host .animateIn{-webkit-animation-name:fadeIn;animation-name:fadeIn;-webkit-animation-duration:.5s;animation-duration:.5s}:host .animateOut{-webkit-animation-name:fadeOut;animation-name:fadeOut;-webkit-animation-duration:.2s;animation-duration:.2s}@media (prefers-reduced-motion){:host .animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}}:host html[data-prefers-reduced-motion] .animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}:host .no-border{border:0}:host .icon-18{width:18px;height:18px}:host .icon-24{width:24px;height:24px}:host .hidden{display:none!important}:host pearson-alert{font-size:16px;max-width:580px}:host pearson-alert .alert-title{font-size:14px;margin:0;display:inline;top:0}:host pearson-uploader .alert-title{top:0!important}:host pearson-alert .alert-text{margin:0;display:inline}:host pearson-footer{left:50%;margin-left:-50%;right:50%;margin-right:-50%}:host pearson-header{grid-column:span 12}:host pearson-tabs{font-size:14px}:host pearson-progress-bar{grid-column:1/5}@media (min-width:591px){:host pearson-progress-bar{grid-column:1/9}}@media (min-width:887px){:host pearson-progress-bar{grid-column:1/13}}:host pearson-tabs{grid-column:1/5}@media (min-width:591px){:host pearson-tabs{grid-column:1/9}}@media (min-width:887px){:host pearson-tabs{grid-column:1/13}}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host pearson-card{-ms-grid-column-span:3}:host pearson-card,:host pearson-card[stacked]{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;width:100%}:host pearson-card[stacked]{-ms-grid-column-span:12}}:host pearson-accordion{grid-column:1/5}@media (min-width:591px){:host pearson-accordion{grid-column:1/9}}@media (min-width:887px){:host pearson-accordion{grid-column:1/13}}:host #main{max-width:1280px;margin:0 auto}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host iframe .gr-grid-container{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}}:host .gr-grid-container{display:grid;display:-ms-grid;grid-template-columns:repeat(4,1fr);grid-column-gap:16px;grid-row-gap:16px;margin:0 39.5px}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host .gr-grid-container.ie-flex{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}}@media (min-width:351px){:host .gr-grid-container{margin:0 31.5px}}@media (min-width:399px){:host .gr-grid-container{margin:0 39.5px}}@media (min-width:447px){:host .gr-grid-container{margin:0 79.5px}}@media (min-width:591px){:host .gr-grid-container{grid-template-columns:repeat(8,1fr);margin:0 83.5px}}@media (min-width:727px){:host .gr-grid-container{margin:0 103.5px;grid-column-gap:24px;grid-row-gap:24px}}@media (min-width:887px){:host .gr-grid-container{grid-template-columns:repeat(12,1fr);margin:0 71.5px}}@media (min-width:887px) and (-ms-high-contrast:active),(min-width:887px) and (-ms-high-contrast:none){:host .gr-grid-container>*{margin-right:12px;margin-left:12px;margin-bottom:24px}}@media (min-width:983px){:host .gr-grid-container{margin:0 71.5px}}@media (min-width:1079px){:host .gr-grid-container{margin:0 71.5px}}@media (min-width:1175px){:host .gr-grid-container{margin:0 76px}}:host .gr-grid-container .gr-col-two{grid-column-start:2}:host .gr-grid-container .gr-col-three{grid-column-start:3}:host .gr-grid-container .gr-col-four{grid-column-start:4}:host .gr-grid-container .gr-col-five{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-five{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:5}}:host .gr-grid-container .gr-col-six{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-six{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:6}}:host .gr-grid-container .gr-col-seven{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-seven{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:7}}:host .gr-grid-container .gr-col-eight{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-eight{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:8}}:host .gr-grid-container .gr-col-nine{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-nine{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:9}}:host .gr-grid-container .gr-col-ten{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-ten{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:10}}:host .gr-grid-container .gr-col-eleven{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-eleven{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:11}}:host .gr-grid-container .gr-col-twelve{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-twelve{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:12}}:host .gr-col-span-one{grid-column-end:span 1}:host .gr-col-span-two{grid-column-end:span 2}:host .gr-col-span-three{grid-column-end:span 3}:host .gr-col-span-four{grid-column-end:span 4}@media (min-width:591px){:host .gr-col-span-five{grid-column-end:span 5}}@media (min-width:591px){:host .gr-col-span-six{grid-column-end:span 6}}@media (min-width:591px){:host .gr-col-span-seven{grid-column-end:span 7}}@media (min-width:591px){:host .gr-col-span-eight{grid-column-end:span 8}}@media (min-width:887px){:host .gr-col-span-nine{grid-column-end:span 9}}@media (min-width:887px){:host .gr-col-span-ten{grid-column-end:span 10}}@media (min-width:887px){:host .gr-col-span-eleven{grid-column-end:span 11}}:host .gr-col-span-twelve{grid-column-end:span 4}@media (min-width:591px){:host .gr-col-span-twelve{grid-column-end:span 8}}@media (min-width:887px){:host .gr-col-span-twelve{grid-column-end:span 12}}:host .gr-row-one{grid-row-start:1}:host .gr-row-two{grid-row-start:2}:host .gr-row-three{grid-row-start:3}:host .gr-row-four{grid-row-start:4}:host .gr-row-five{grid-row-start:5}:host .gr-row-six{grid-row-start:6}:host .gr-row-seven{grid-row-start:7}:host .gr-row-eight{grid-row-start:8}:host .gr-row-nine{grid-row-start:9}:host .gr-row-ten{grid-row-start:10}:host .gr-row-eleven{grid-row-start:11}:host .gr-row-twelve{grid-row-start:12}:host .gr-row-thirteen{grid-row-start:13}:host .gr-row-fourteen{grid-row-start:14}:host .gr-row-fifteen{grid-row-start:15}:host .gr-row-span-two{grid-row-end:span 2}:host .gr-row-span-three{grid-row-end:span 3}:host .gr-row-span-four{grid-row-end:span 4}:host .gr-row-span-five{grid-row-end:span 5}:host .gr-row-span-six{grid-row-end:span 6}:host .gr-row-span-seven{grid-row-end:span 7}:host .gr-row-span-eight{grid-row-end:span 8}:host .gr-row-span-nine{grid-row-end:span 9}:host .gr-row-span-ten{grid-row-end:span 10}:host .gr-row-span-eleven{grid-row-end:span 11}:host .gr-row-span-twelve{grid-row-end:span 12}:host .gr-primary{color:#047a9c;fill:#047a9c}:host .gr-secondary{color:#ffb81c;fill:#ffb81c}:host .gr-white{color:#fff;fill:#fff}:host .gr-neutral-high-one{color:#252525;fill:#252525}:host .gr-neutral-high-two{color:#6a7070;fill:#6a7070}:host .gr-neutral-med-one{color:#a9a9a9;fill:#a9a9a9}:host .gr-neutral-med-two{color:#c7c7c7;fill:#c7c7c7}:host .gr-neutral-med-three{color:#d9d9d9;fill:#d9d9d9}:host .gr-neutral-med-four{color:#e9e9e9;fill:#e9e9e9}:host .gr-neutral-light-one,:host .gr-neutral-light-two{color:#eee;fill:#eee}:host .gr-condition-one{color:#db0020;fill:#db0020}:host .gr-condition-two{color:#038238;fill:#038238}:host .gr-condition-three{color:#da0474;fill:#da0474}:host .gr-theme-one-light{color:#caefee;fill:#caefee}:host .gr-theme-one-med{color:#76d5d4;fill:#76d5d4}:host .gr-theme-one-dark{color:#19a5a3}:host .gr-theme-two-light{color:#f2e5f1;fill:#f2e5f1}:host .gr-theme-two-med{color:#895b9a;fill:#895b9a}:host .gr-theme-two-dark{color:#633673;fill:#633673}:host .gr-theme-three-light{color:#f6f8cc;fill:#f6f8cc}:host .gr-theme-three-med{color:#d2db0e;fill:#d2db0e}:host .gr-theme-three-dark{color:#b0b718;fill:#b0b718}:host .gr-theme-four-light{color:#d9e6f1;fill:#d9e6f1}:host .gr-theme-four-med{color:#356286;fill:#356286}:host .gr-theme-four-dark{color:#1e496c;fill:#356286}:host .gr-theme-five-light{color:#dff5d5;fill:#dff5d5}:host .gr-theme-five-med{color:#66be3e;fill:#66be3e}:host .gr-theme-five-dark{color:#288500;fill:#288500}:host .gr-theme-six-light{color:#d6ecf4;fill:#d6ecf4}:host .gr-theme-six-med{color:#80c5dd;fill:#80c5dd}:host .gr-theme-six-dark{color:#46a9cb;fill:#46a9cb}:host .gr-theme-seven-light{color:#faebc3;fill:#faebc3}:host .gr-theme-seven-med{color:#f5c54c;fill:#f5c54c}:host .gr-theme-seven-dark{color:#dea30d;fill:#dea30d}:host .gr-primary-bg{background-color:#047a9c}:host .gr-secondary-bg{background-color:#ffb81c}:host .gr-white-bg{background-color:#fff}:host .gr-neutral-high-one-bg{background-color:#252525}:host .gr-neutral-high-two-bg{background-color:#6a7070}:host .gr-neutral-med-one-bg{background-color:#a9a9a9}:host .gr-neutral-med-two-bg{background-color:#c7c7c7}:host .gr-neutral-med-three-bg{background-color:#d9d9d9}:host .gr-neutral-med-four-bg{background-color:#e9e9e9}:host .gr-neutral-light-one-bg,:host .gr-neutral-light-two-bg{background-color:#eee}:host .gr-condition-one-bg{background-color:#db0020}:host .gr-condition-two-bg{background-color:#038238}:host .gr-condition-three-bg{background-color:#da0474}:host .gr-theme-one-light-bg{background-color:#caefee}:host .gr-theme-one-med-bg{background-color:#76d5d4}:host .gr-theme-one-dark-bg{background-color:#19a5a3}:host .gr-theme-two-light-bg{background-color:#f2e5f1}:host .gr-theme-two-med-bg{background-color:#895b9a}:host .gr-theme-two-dark-bg{background-color:#633673}:host .gr-theme-three-light-bg{background-color:#f6f8cc}:host .gr-theme-three-med-bg{background-color:#d2db0e}:host .gr-theme-three-dark-bg{background-color:#b0b718}:host .gr-theme-four-light-bg{background-color:#d9e6f1}:host .gr-theme-four-med-bg{background-color:#356286}:host .gr-theme-four-dark-bg{background-color:#1e496c}:host .gr-theme-five-light-bg{background-color:#dff5d5}:host .gr-theme-five-med-bg{background-color:#66be3e}:host .gr-theme-five-dark-bg{background-color:#288500}:host .gr-theme-six-light-bg{background-color:#d6ecf4}:host .gr-theme-six-med-bg{background-color:#80c5dd}:host .gr-theme-six-dark-bg{background-color:#46a9cb}:host .gr-theme-seven-light-bg{background-color:#faebc3}:host .gr-theme-seven-med-bg{background-color:#f5c54c}:host .gr-theme-seven-dark-bg{background-color:#dea30d}:host .gr-h1{font-size:24px;line-height:28px}:host .gr-h1,:host .gr-h2{font-weight:400;margin-top:0}:host .gr-h2{font-size:20px;line-height:26px}:host .gr-h3{font-size:18px;line-height:24px;font-weight:400;margin-top:0}:host .gr-label{font-size:12px;line-height:16px;color:#6a7070;display:block;margin-bottom:4px}:host .gr-meta{font-size:12px;line-height:12px;color:#6a7070}:host .gr-semi-bold{font-weight:600}:host .gr-font-large{font-size:16px;line-height:24px}:host .gr-font-normal{font-size:14px;line-height:20px}:host .gr-btn{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;border:1px solid #6a7070;background:transparent;color:#6a7070;min-width:152px;padding:8px 24px;font-size:16px;cursor:pointer;border-radius:22px;position:relative;margin:12px;line-height:1.15}:host .gr-btn:hover{color:#252525;border:1px solid #252525}:host .gr-btn:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host a.gr-btn{text-decoration:none}:host .gr-btn.primary{background-color:#047a9c;border:1px solid #047a9c;color:#fff}:host .gr-btn.primary:hover{color:#fff;background-color:#035f79}:host .gr-btn.attention{background-color:#ffb81c;border:1px solid #ffb81c;color:#252525}:host .gr-btn.attention:hover{background-color:#f7aa00}:host .gr-btn.small{min-width:128px;padding:7px 20px;font-size:14px}:host .gr-btn.small:focus:after{padding:18px 21px}:host .gr-btn.extra-large{min-width:152px;padding:10px 24px;font-size:16px}:host .gr-btn.extra-large:focus:after{padding:22px 21px;border-radius:24px}:host .gr-btn:disabled{background-color:#e9e9e9!important;color:#6a7070!important;border:0!important;cursor:default!important}:host .gr-btn.icon-btn-24{min-width:auto;padding:5px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-24:focus{border-radius:4px}:host .gr-btn.icon-btn-24:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-24:active svg{fill:#005a70}:host .gr-btn.icon-btn-24 svg{fill:#6a7070}:host .gr-btn.icon-btn-18{min-width:auto;padding:8px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-18:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-18:active svg{fill:#005a70}:host .gr-btn.icon-btn-18:focus{border-radius:4px}:host .gr-btn.icon-btn-18 svg{fill:#6a7070}:host .gr-btn.no-border,:host .gr-btn.no-border:hover{border:0}:host .gr-btn.no-border:focus:after{border-radius:5px;min-width:auto;left:auto;top:auto;padding:16px}:host .carousel{position:relative;overflow:hidden;width:100%;height:100%}:host .slides{height:100%;margin:0;padding:0;list-style-type:none}:host slot[name=slides]::slotted(li){width:100%;height:100%;background:no-repeat 50%;background-size:cover;position:absolute;top:0;left:0;z-index:2;-webkit-transition:left .5s ease-in-out;-webkit-transition:all .5s ease-in-out;transition:all .5s ease-in-out;visibility:hidden}:host slot[name=slides]::slotted(li.current){visibility:visible;left:0;z-index:3}:host slot[name=slides]::slotted(li.next){left:100%}:host slot[name=slides]::slotted(li.prev){left:-100%}:host .pause{position:absolute;bottom:14px;left:0;z-index:7;border:none;background-color:transparent;color:#fff;height:24px;width:40px}:host .controls{list-style-type:none;margin:0;padding:0;position:absolute;top:-webkit-calc(50% - 24px);top:calc(50% - 24px);left:0;width:100%;z-index:6}:host .controls .nav-btn{background-image:url(images/semi-circle.svg);background-color:transparent;height:60px;width:30px;border:none;position:absolute}:host .controls .nav-btn.next-slide-btn{right:0}:host .controls .nav-btn.next-slide-btn pearson-icon{top:19px;left:6px}:host .controls .nav-btn.prev-slide-btn{left:0;-webkit-transform:rotate(180deg);transform:rotate(180deg)}:host .controls .nav-btn.prev-slide-btn pearson-icon{right:0;margin-bottom:3px;bottom:15px;-webkit-transform:rotate(180deg);transform:rotate(180deg)}:host .controls .nav-btn pearson-icon{pointer-events:none;fill:#666;position:absolute;height:24px}:host .controls .nav-btn:hover svg{fill:#047a9c}:host .slidenav{position:absolute;bottom:24px;left:50%;margin-left:-50%;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;z-index:6;width:100%}:host .slidenav button{border:0;border-radius:50%;background:#007fa3;height:12px;width:12px;outline:0;line-height:0;font-size:0;color:transparent;padding:0;-webkit-box-shadow:0 1px 2px 0 rgba(0,0,0,.15);box-shadow:0 1px 2px 0 rgba(0,0,0,.15);margin:0 10px 0 0}:host .slidenav button[aria-selected=true]{background-color:#fff}:host .slidenav button:focus,:host button:focus{-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host button:focus{outline:none}\n    </style>\n    <div id="carousel" class="carousel">\n        <ul class="slides" aria-live="off">\n            <slot name="slides"><!--Inject slides--></slot>\n        </ul>\n        <button class="pause">\n            <pearson-icon icon="pause" size="24" ></pearson-icon>\n        </button>\n        <ul class="controls">\n            <li>\n                <button class="prev-slide-btn nav-btn" aria-label="previous slide">\n                    <pearson-icon icon="previous" size="24" ></pearson-icon>\n                </button>\n            </li>\n            <li>\n                <button class="next-slide-btn nav-btn" aria-label="next slide">\n                    <pearson-icon icon="next" size="24" ></pearson-icon>\n                </button>\n            </li>\n        </ul>\n        <div class="slidenav" role="tablist" aria-orientation="horizontal">\n             <!--Inject dot template-->\n        </div>\n    </div>\n\n    ';

  dotTemplate.innerHTML = '\n    <button class="dot-btn" role="tab"><span class="visually-hidden"></span></button>\n  ';

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-carousel');
  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(dotTemplate, 'pearson-carousel');

  var Carousel = function (_HTMLElement) {
    _inherits(Carousel, _HTMLElement);

    function Carousel() {
      _classCallCheck(this, Carousel);

      var _this = _possibleConstructorReturn(this, (Carousel.__proto__ || Object.getPrototypeOf(Carousel)).call(this));

      _this.currentSlideIndex = 0;
      _this.rotationIntervalId = null;
      _this.timer = null;
      _this.paused = false;

      _this.attachShadow({ mode: 'open' });
      var clone = template.content.cloneNode(true);
      _this.slideSlot = clone.querySelector('slot');
      _this.carousel = clone.querySelector("#carousel");
      _this.slideNav = clone.querySelector(".slidenav");
      _this.navBtns = clone.querySelectorAll(".nav-btn");
      _this.pauseBtn = clone.querySelector('.pause');

      _this.shadowRoot.appendChild(clone);

      _this.paginateSlides = _this.paginateSlides.bind(_this);
      _this.stopRotation = _this.stopRotation.bind(_this);
      _this.startRotation = _this.startRotation.bind(_this);
      return _this;
    }

    _createClass(Carousel, [{
      key: 'connectedCallback',
      value: function connectedCallback() {
        var _this2 = this;

        //arrange slotted content
        setTimeout(function () {
          _this2.slides = _this2.slideSlot.assignedNodes();
          _this2.slides.forEach(function (slide, i) {
            var slideImg = slide.querySelector('img');
            slideImg ? slide.style.backgroundImage = 'url(' + slideImg.src + ')' : slide.style.backgroundColor = '#6a7070';
            if (i === 0) {
              slide.classList.add('current');
            }
            if (i === 1) slide.classList.add('next');
            if (i === _this2.slides.length - 1) slide.classList.add('prev');
            slide.setAttribute('id', 'slide' + (i + 1));

            //generate a dot for the slide
            _this2.dotClone = dotTemplate.content.cloneNode(true);
            var dotBtn = _this2.dotClone.querySelector('button'),
                dotSpan = _this2.dotClone.querySelector('span');
            if (i === 0) dotBtn.setAttribute('aria-selected', true);
            dotBtn.setAttribute('data-slide', i);
            dotBtn.setAttribute('aria-controls', 'slide' + (i + 1));
            dotSpan.innerText = 'Slide ' + (i + 1);
            dotBtn.addEventListener("click", _this2.paginateSlides);
            dotBtn.addEventListener('keydown', function (e) {
              if (e.type === 'click' || e.key === 'ArrowRight' || e.key === 'ArrowLeft') {
                _this2.paginateSlides(e);
              };
            });
            _this2.slideNav.appendChild(_this2.dotClone);
          });
        }, 1000);

        this.pauseBtn.addEventListener('click', function () {
          _this2.paused = !_this2.paused;
          var icon = _this2.pauseBtn.querySelector('pearson-icon');
          icon.getAttribute('icon') === 'pause' ? icon.setAttribute('icon', 'play') : icon.setAttribute('icon', 'pause');
          if (_this2.paused === false) _this2.startRotation();
        });

        this.navBtns.forEach(function (navBtn) {
          navBtn.addEventListener("click", _this2.paginateSlides);
        });
        this.carousel.addEventListener("mouseenter", this.stopRotation);
        this.carousel.addEventListener("mouseleave", this.startRotation);

        this.startRotation();
      }
    }, {
      key: 'paginateSlides',
      value: function paginateSlides(e) {
        var _this3 = this;

        //event is next or auto or keydown right arrow
        if (e === 'auto' || e.target.classList.contains('next-slide-btn') || e.key === 'ArrowRight') {
          if (this.currentSlideIndex == this.slides.length - 1) {
            this.currentSlideIndex = 0;
          } else {
            this.currentSlideIndex++;
          }
        }

        //event is previous or keydown left arrow
        else if (e.target.classList.contains('prev-slide-btn') || e.key === 'ArrowLeft') {
            if (this.currentSlideIndex == 0) {
              this.currentSlideIndex = this.slides.length - 1;
            } else {
              this.currentSlideIndex--;
            }
          }

          //event is dot buttons
          else if (e.target.classList.contains('dot-btn')) {
              this.currentSlideIndex = parseInt(e.target.getAttribute('data-slide'));
            }

        //update slides
        var currentSlide = this.slides[this.currentSlideIndex],
            nextSlide = this.currentSlideIndex === this.slides.length - 1 ? this.slides[0] : this.slides[this.currentSlideIndex + 1],
            prevSlide = this.currentSlideIndex === 0 ? this.slides[this.slides.length - 1] : this.slides[this.currentSlideIndex - 1];

        this.slides.forEach(function (eachSlide) {
          eachSlide.classList.remove("next");
          eachSlide.classList.remove("prev");
          eachSlide.classList.remove("current");
        });

        currentSlide.classList.add("current");
        nextSlide.classList.add("next");
        prevSlide.classList.add("prev");

        //update slide-nav (if using arrows to navigate update the focus also)
        this.slideNav.querySelectorAll("button").forEach(function (btn) {
          btn.setAttribute("aria-selected", false);
          if (e.key === 'ArrowLeft' || e.key === 'ArrowRight') {
            btn.blur();
          }
        });
        this.slideNav.querySelectorAll("button")[this.currentSlideIndex].setAttribute("aria-selected", true);
        if (e.key === 'ArrowLeft' || e.key === 'ArrowRight') {
          this.slideNav.querySelectorAll("button")[this.currentSlideIndex].focus();
        }

        //when you paginate in any way the rotation should stop and then restart
        if (e != 'auto') {
          this.stopRotation();
          this.timer = setTimeout(function () {
            _this3.startRotation();
          }, 2000);
        }
      }
    }, {
      key: 'stopRotation',
      value: function stopRotation() {
        clearTimeout(this.timer);
        clearInterval(this.rotationIntervalId);
      }
    }, {
      key: 'startRotation',
      value: function startRotation() {
        var _this4 = this;

        if (this.paused === false) this.rotationIntervalId = setInterval(function () {
          _this4.paginateSlides('auto');
        }, 4000);
      }
    }]);

    return Carousel;
  }(HTMLElement);

  customElements.define('pearson-carousel', Carousel);
})(window, document);