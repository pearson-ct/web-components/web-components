(function(win, doc) {
  'use strict';
  const tabs = document.querySelector('pearson-tabs'),
    span = tabs.shadowRoot.querySelectorAll('span');

  function numberGenerate() {
    span.forEach(node => {
      node.innerHTML = Math.floor(Math.random() * 100) + 1;
    })
  }
  numberGenerate();


  tabs.addEventListener('tabChange', event => {
    console.log('change')
    numberGenerate();
  })
})(window, document)
