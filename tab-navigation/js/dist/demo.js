(function (win, doc) {
  'use strict';

  var tabs = document.querySelector('pearson-tabs'),
      span = tabs.shadowRoot.querySelectorAll('span');

  function numberGenerate() {
    span.forEach(function (node) {
      node.innerHTML = Math.floor(Math.random() * 100) + 1;
    });
  }
  numberGenerate();

  tabs.addEventListener('tabChange', function (event) {
    console.log('change');
    numberGenerate();
  });
})(window, document);