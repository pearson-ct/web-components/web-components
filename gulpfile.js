const gulp = require('gulp');
const sass = require('sass');
const concat = require('gulp-concat');
const minify = require('gulp-minify');
const awspublish = require('gulp-awspublish');
const path = require('path');
const rename = require('gulp-rename');
const through = require('through2'); // Needed to handle sass compilation in a gulp pipeline
require('dotenv').config();

const NEW_S3_DIRECTORY = 'components';
const paths = {
    wcscript: [
        './alert/js/dist/pearson-alert.js'
    ],
    dist: './build/',
};

function scripts() {
    return gulp
        .src([
            './accordion/js/dist/pearson-accordion.js',
            './alert/js/dist/pearson-alert.js',
            './button-bar/js/dist/pearson-button-bar.js',
            './card/js/dist/pearson-card.js',
            './checkbox/js/dist/pearson-checkbox.js',
            './coachmark/js/dist/pearson-coachmark.js',
            './datepicker/js/dist/pearson-datepicker.js',
            './drawer/js/dist/pearson-drawer.js',
            './dropdown/js/dist/pearson-dropdown.js',
            './file-uploader/js/dist/pearson-file-upload.js',
            './footer/js/dist/pearson-footer.js',
            './header/js/dist/pearson-header.js',
            './icon/js/dist/pearson-icon.js',
            './input/js/dist/pearson-input.js',
            './loader/js/dist/pearson-loader.js',
            './modal/js/dist/pearson-modal.js',
            './pagination/js/dist/pearson-pagination.js',
            './progress-bar/js/dist/pearson-progress-bar.js',
            './radio/js/dist/pearson-radio.js',
            './range-slider/js/dist/pearson-range-slider.js',
            './select/js/dist/pearson-select.js',
            './tab-navigation/js/dist/pearson-tabs.js',
            './textarea/js/dist/pearson-textarea.js',
            './timepicker/js/dist/pearson-timepicker.js',
            './toggle/js/dist/pearson-toggle.js',
            './video/js/dist/pearson-video.js',
            './login/js/dist/pearson-login.js',
        ])
        .pipe(concat('pearson-web-components.js'))
        .pipe(minify())
        .pipe(gulp.dest(paths.dist));
}

function compileSass() {
    return through.obj(function (file, enc, cb) {
        if (file.isBuffer()) {
            const result = sass.renderSync({ file: file.path });
            file.contents = result.css;
            cb(null, file);
        } else {
            cb(null, file);
        }
    });
}

function styles() {
    return gulp
        .src('./styles/**/*.scss')  // Adjust the path to your SCSS files
        .pipe(compileSass()) // Compile Sass
        .pipe(gulp.dest(paths.dist));
}

const build = gulp.series(scripts, styles);
exports.build = build;
exports.default = gulp.series(build);

gulp.task('publish', function() {
    var publisher = awspublish.create({
        region: 'sfo2',
        endpoint: 'sfo2.digitaloceanspaces.com',
        params: {
            Bucket: 'pearsonux'
        },
        accessKeyId: process.env.S3_KEY,
        secretAccessKey: process.env.S3_SECRET
    });

    const headers = {
        'Cache-Control': 'max-age=315360000, no-transform, public'
    };

    return gulp
        .src('./build/pearson-web-components-min.js')
        .pipe(rename(function(filePath) {
            filePath.dirname = path.join(NEW_S3_DIRECTORY, filePath.dirname);
        }))
        .pipe(awspublish.gzip({ ext: '.gz' }))
        .pipe(publisher.publish(headers))
        .pipe(publisher.cache())
        .pipe(awspublish.reporter());
});
