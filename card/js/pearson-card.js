(function(w, doc) {
  'use strict';

  // Create a template element
  const template = doc.createElement('template'),
    modular = doc.createElement('template'),
    stacked = doc.createElement('template');

  template.innerHTML = ` 

		<div class="card-container" part="container">
			<div class="card" part="card">
			    <slot></slot>
			</div>
		</div>  
`;
  modular.innerHTML = `
    <style>
        :host{grid-column:span 4}@media (min-width:887px){:host{grid-column:span 3}}
        @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
        :host {
        display:flex;
        }
}
    </style>
  `;

  stacked.innerHTML = `
    <style>
        :host{-ms-grid-column:1;grid-column-start:1;-ms-grid-column-span:12;grid-column-end:13}
    </style>
  `;

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-card');
  class Card extends HTMLElement {
    static get observedAttributes() {
      return ['stacked'];
    }

    get isStacked() {
      return (
        this.hasAttribute('stacked') && this.getAttribute('stacked') !== 'false'
      );
    }

    constructor() {
      super();
      this.attachShadow({ mode: 'open' });
      const clone = template.content.cloneNode(true);
      this.shadowRoot.appendChild(clone);
    }

    connectedCallback() {
      if (!this.isStacked) {
        const modularLayout = modular.content.cloneNode(true);
        this.shadowRoot.appendChild(modularLayout);
      }
    }

    attributeChangedCallback() {
      if (this.isStacked) {
        const stackedLayout = stacked.content.cloneNode(true);
        this.shadowRoot.appendChild(stackedLayout);
      } else {
        const modularLayout = modular.content.cloneNode(true);
        this.shadowRoot.appendChild(modularLayout);
      }
    }
  }
  customElements.define('pearson-card', Card);
})(window, document);
