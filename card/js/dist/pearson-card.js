var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function (w, doc) {
  'use strict';

  // Create a template element

  var template = doc.createElement('template'),
      modular = doc.createElement('template'),
      stacked = doc.createElement('template');

  template.innerHTML = ' \n\n\t\t<div class="card-container" part="container">\n\t\t\t<div class="card" part="card">\n\t\t\t    <slot></slot>\n\t\t\t</div>\n\t\t</div>  \n';
  modular.innerHTML = '\n    <style>\n        :host{grid-column:span 4}@media (min-width:887px){:host{grid-column:span 3}}\n        @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {\n        :host {\n        display:flex;\n        }\n}\n    </style>\n  ';

  stacked.innerHTML = '\n    <style>\n        :host{-ms-grid-column:1;grid-column-start:1;-ms-grid-column-span:12;grid-column-end:13}\n    </style>\n  ';

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-card');

  var Card = function (_HTMLElement) {
    _inherits(Card, _HTMLElement);

    _createClass(Card, [{
      key: 'isStacked',
      get: function get() {
        return this.hasAttribute('stacked') && this.getAttribute('stacked') !== 'false';
      }
    }], [{
      key: 'observedAttributes',
      get: function get() {
        return ['stacked'];
      }
    }]);

    function Card() {
      _classCallCheck(this, Card);

      var _this = _possibleConstructorReturn(this, (Card.__proto__ || Object.getPrototypeOf(Card)).call(this));

      _this.attachShadow({ mode: 'open' });
      var clone = template.content.cloneNode(true);
      _this.shadowRoot.appendChild(clone);
      return _this;
    }

    _createClass(Card, [{
      key: 'connectedCallback',
      value: function connectedCallback() {
        if (!this.isStacked) {
          var modularLayout = modular.content.cloneNode(true);
          this.shadowRoot.appendChild(modularLayout);
        }
      }
    }, {
      key: 'attributeChangedCallback',
      value: function attributeChangedCallback() {
        if (this.isStacked) {
          var stackedLayout = stacked.content.cloneNode(true);
          this.shadowRoot.appendChild(stackedLayout);
        } else {
          var modularLayout = modular.content.cloneNode(true);
          this.shadowRoot.appendChild(modularLayout);
        }
      }
    }]);

    return Card;
  }(HTMLElement);

  customElements.define('pearson-card', Card);
})(window, document);