(function(w, doc) {
  'use strict';

  const template = doc.createElement('template'),
        optionTemplate = doc.createElement('template');

  template.innerHTML = `
    <style>
    *{
    box-sizing: border-box;
    }
    
    .gr-fieldset {
      border: none;
      padding: 0;
    }

    .gr-fieldset legend {
      margin-bottom: 8px; 
    }
    
    .gr-label, .gr-meta {
        font-size: 12px;
        color: #6a7070;
    }
    
    .gr-label {
        line-height: 16px;
        display: block;
        margin-bottom: 4px;
    }
    
    .gr-fieldset ul {
      margin:0;
      padding-inline-start: 0px;
    }
    
   .gr-radio {
      margin-bottom: 14px;
      min-height: 16px;
      position: relative;
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      }
    
    .gr-radio input[type=radio] {
      opacity: 0;
      position: absolute;
      margin: 0 0 0 3px;
     }
    
    .gr-radio input[type=radio]:focus ~ span {
      outline: 0;
      -webkit-box-shadow: 0 0 0 2px white, 0 0 0 4px #1977D4;
      box-shadow: 0 0 0 2px white, 0 0 0 4px #1977D4;
     }
    
    .gr-radio input[type=radio] + label {
      display: inline-block;
      line-height: 18px;
      padding-left: 28px;
      font-size: 14px;
      cursor: pointer;
     }
    
    .gr-radio input[type=radio] ~ span {
      -webkit-box-sizing: content-box;
      border: 2px solid #c7c7c7;
      background: white;
      border-radius: 50%;
      box-sizing: content-box;
      color: #6a7070;
      display: block;
      height: 5px;
      left: 0;
      padding: 3px 6px 6px 3px;
      pointer-events: none;
      position: absolute;
      top: 0;
      width: 5px;
      z-index:-1;
      }
    
    .gr-radio input[type=radio] ~ span svg {
      height: 18px;
      opacity: 0;
      width: 18px;
     }
    
    .gr-radio input[type=radio]:checked ~ span svg {
      opacity: 1;
      top: -5px;
      position: relative;
      left: -5px;
      fill: #047A9C;
     }
    
    .gr-radio input[type=radio]:disabled ~ span svg {
      opacity: 1;
      fill: #C7C7C7;
      top: -5px;
      left: -5px;
      position: relative;
     }
     
     .error-state{
        display: none;
     }
     
     .icon-18 {
      width: 18px;
      height: 18px;
     }
     
     fieldset.error~.error-state{
        display: flex;
        align-items: center;
     }
    
    fieldset.error + .error-state span{
        margin-left: 8px;
        color: #DB0020;
    }
     
     fieldset.error + .error-state svg{
         fill: #DB0020;
    }
     
    </style>
    <div class="gr-form-element">
      <fieldset class="gr-fieldset">
        <legend class="gr-label">This is a radio group</legend>
        <ul>
        <!--radios go here-->
        </ul>
      </fieldset>
      <div class="error-state">
        <svg focusable="false" class="icon-18" aria-hidden="true">
          <path d="M10.3203543,1.76322881 L17.7947154,14.7065602 C18.2165963,15.4371302 17.9673988,16.3719805 17.2381172,16.7946067 C17.0059424,16.9291544 16.742459,17 16.4742343,17 L1.52551217,17 C0.682995061,17 0,16.3157983 0,15.4717927 C0,15.2030941 0.0707206291,14.9391453 0.205031086,14.7065602 L7.67939217,1.76322881 C8.10127304,1.03265881 9.03447459,0.78302105 9.76375615,1.20564727 C9.99478499,1.33953088 10.1867068,1.5317918 10.3203543,1.76322881 Z M8.5,13 C8.22385763,13 8,13.2238576 8,13.5 L8,14.5 C8,14.7761424 8.22385763,15 8.5,15 L9.5,15 C9.77614237,15 10,14.7761424 10,14.5 L10,13.5 C10,13.2238576 9.77614237,13 9.5,13 L8.5,13 Z M8.5,7 C8.22385763,7 8,7.22385763 8,7.5 L8,11.5 C8,11.7761424 8.22385763,12 8.5,12 L9.5,12 C9.77614237,12 10,11.7761424 10,11.5 L10,7.5 C10,7.22385763 9.77614237,7 9.5,7 L8.5,7 Z"></path>
        </svg>
        <span class="gr-meta warning-text">Warning Text</span>
      </div>
    </div>
`;

  optionTemplate.innerHTML = `
   <li class="gr-radio">
        <input type="radio" name="scotch" id="radioOne" >
        <label for="radioOne">Selection One</label>
        <span>
        <svg focusable="false" class="icon-18" aria-hidden="true">
          <path d="M9,18 C4.02943725,18 0,13.9705627 0,9 C0,4.02943725 4.02943725,0 9,0 C13.9705627,0 18,4.02943725 18,9 C18,13.9705627 13.9705627,18 9,18 Z M9,16 C12.8659932,16 16,12.8659932 16,9 C16,5.13400675 12.8659932,2 9,2 C5.13400675,2 2,5.13400675 2,9 C2,12.8659932 5.13400675,16 9,16 Z"></path>
          <path d="M9,14.3 C11.9271092,14.3 14.3,11.9271092 14.3,9 C14.3,6.07289083 11.9271092,3.7 9,3.7 C6.07289083,3.7 3.7,6.07289083 3.7,9 C3.7,11.9271092 6.07289083,14.3 9,14.3 Z"></path>
        </svg>
        </span>
    </li>
  `;

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-radio');


  class Radio extends HTMLElement {
    static get observedAttributes() {
      return ['label', 'groupName', 'disabled', 'error', 'selected'];
    }

    get selected() {
      return this.getAttribute('selected')
    }
    get error() {
      return this.hasAttribute('error')
    }

    get errorText() {
      return this.getAttribute('error')
    }

    get disabled() {
      return this.hasAttribute('disabled');
    }

    get labelText() {
      return this.getAttribute('label');
    }

    get groupName() {
      return this.getAttribute('groupname');
    }

    set selected(value) {
      this.setAttribute('selected', value)
    }

    set labeltext(value) {
      this.setAttribute('label', value)
    }

    constructor() {
      super();
      this.attachShadow({mode: 'open'});
      const clone = template.content.cloneNode(true);
      this.label = clone.querySelector('legend');
      this.fieldset = clone.querySelector('fieldset');
      this.fieldsetList = clone.querySelector('fieldset ul');
      this.warningText = clone.querySelector('.warning-text');

      this.renderedInputs = null;
      this.value = null;

      this.shadowRoot.appendChild(clone);

      this.buildOptions = this.buildOptions.bind(this);
      this.disableInputs = this.disableInputs.bind(this);


      this.fieldset.addEventListener('click', event => {
        let clickedInput = event.target.closest('input');
        if(!clickedInput){return}
        else if (clickedInput.checked===true){
          this.selected = clickedInput.getAttribute('data-index')
          this.value = clickedInput.value;
          this.data = clickedInput.getAttribute('data-index');
        }
      })
    }

    connectedCallback() {
      setTimeout(this.buildOptions, 100);
    }

    attributeChangedCallback(name, oldValue, newValue) {
      if (name === 'label') {
        if (oldValue !== newValue) {
          this.label.innerHTML = this.labelText;
        }
      }

      if (name === 'disabled') {
        if (oldValue !== newValue) {
          setTimeout(this.disableInputs, 100);
        }
      }

      if (name === 'error') {
        if (oldValue !== newValue) {
          if (this.error === true) {
            this.fieldset.classList.add('error');
            this.warningText.innerHTML = this.errorText
          } else {
            this.fieldset.classList.remove('error')
          }
        }
      }

      if (name === 'selected') {
          const inputs = this.shadowRoot.querySelectorAll('input')
          inputs.forEach((input, index) => {
            if (parseInt(newValue) === index) {
              input.checked = true
            }
          })
      }

    }

    buildOptions(){
      this.options = this.querySelectorAll('input');
      this.options.forEach((option, index)=>{

        let optionClone = optionTemplate.content.cloneNode(true);
        let radioLabel = optionClone.querySelector('label'),
            radio = optionClone.querySelector('input');

        radio.id = option.name;
        radio.setAttribute('data-index',index)
        radio.setAttribute('name',this.groupName);
        radio.setAttribute('value', option.name);
        radioLabel.innerText = option.name;
        radioLabel.setAttribute('for',option.name);
        this.fieldsetList.appendChild(optionClone);
      })
      if (this.disabled === true) {
        this.disableInputs()
      }
    }

    disableInputs(){
      let renderedInputs = this.shadowRoot.querySelectorAll('input');
      if (this.disabled === true) {
        renderedInputs.forEach((input)=>{
          input.disabled = true;
        })
      } else {
        renderedInputs.forEach((input)=>{
          input.disabled = false;
        })
      }
    }

  }

  customElements.define('pearson-radio', Radio);
})(window, document);
