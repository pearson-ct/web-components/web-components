var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function (w, doc) {
  'use strict';

  var template = doc.createElement('template'),
      optionTemplate = doc.createElement('template');

  template.innerHTML = '\n    <style>\n    *{\n    box-sizing: border-box;\n    }\n    \n    .gr-fieldset {\n      border: none;\n      padding: 0;\n    }\n\n    .gr-fieldset legend {\n      margin-bottom: 8px; \n    }\n    \n    .gr-label, .gr-meta {\n        font-size: 12px;\n        color: #6a7070;\n    }\n    \n    .gr-label {\n        line-height: 16px;\n        display: block;\n        margin-bottom: 4px;\n    }\n    \n    .gr-fieldset ul {\n      margin:0;\n      padding-inline-start: 0px;\n    }\n    \n   .gr-radio {\n      margin-bottom: 14px;\n      min-height: 16px;\n      position: relative;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n      -ms-flex-align: center;\n      align-items: center;\n      }\n    \n    .gr-radio input[type=radio] {\n      opacity: 0;\n      position: absolute;\n      margin: 0 0 0 3px;\n     }\n    \n    .gr-radio input[type=radio]:focus ~ span {\n      outline: 0;\n      -webkit-box-shadow: 0 0 0 2px white, 0 0 0 4px #1977D4;\n      box-shadow: 0 0 0 2px white, 0 0 0 4px #1977D4;\n     }\n    \n    .gr-radio input[type=radio] + label {\n      display: inline-block;\n      line-height: 18px;\n      padding-left: 28px;\n      font-size: 14px;\n      cursor: pointer;\n     }\n    \n    .gr-radio input[type=radio] ~ span {\n      -webkit-box-sizing: content-box;\n      border: 2px solid #c7c7c7;\n      background: white;\n      border-radius: 50%;\n      box-sizing: content-box;\n      color: #6a7070;\n      display: block;\n      height: 5px;\n      left: 0;\n      padding: 3px 6px 6px 3px;\n      pointer-events: none;\n      position: absolute;\n      top: 0;\n      width: 5px;\n      z-index:-1;\n      }\n    \n    .gr-radio input[type=radio] ~ span svg {\n      height: 18px;\n      opacity: 0;\n      width: 18px;\n     }\n    \n    .gr-radio input[type=radio]:checked ~ span svg {\n      opacity: 1;\n      top: -5px;\n      position: relative;\n      left: -5px;\n      fill: #047A9C;\n     }\n    \n    .gr-radio input[type=radio]:disabled ~ span svg {\n      opacity: 1;\n      fill: #C7C7C7;\n      top: -5px;\n      left: -5px;\n      position: relative;\n     }\n     \n     .error-state{\n        display: none;\n     }\n     \n     .icon-18 {\n      width: 18px;\n      height: 18px;\n     }\n     \n     fieldset.error~.error-state{\n        display: flex;\n        align-items: center;\n     }\n    \n    fieldset.error + .error-state span{\n        margin-left: 8px;\n        color: #DB0020;\n    }\n     \n     fieldset.error + .error-state svg{\n         fill: #DB0020;\n    }\n     \n    </style>\n    <div class="gr-form-element">\n      <fieldset class="gr-fieldset">\n        <legend class="gr-label">This is a radio group</legend>\n        <ul>\n        <!--radios go here-->\n        </ul>\n      </fieldset>\n      <div class="error-state">\n        <svg focusable="false" class="icon-18" aria-hidden="true">\n          <path d="M10.3203543,1.76322881 L17.7947154,14.7065602 C18.2165963,15.4371302 17.9673988,16.3719805 17.2381172,16.7946067 C17.0059424,16.9291544 16.742459,17 16.4742343,17 L1.52551217,17 C0.682995061,17 0,16.3157983 0,15.4717927 C0,15.2030941 0.0707206291,14.9391453 0.205031086,14.7065602 L7.67939217,1.76322881 C8.10127304,1.03265881 9.03447459,0.78302105 9.76375615,1.20564727 C9.99478499,1.33953088 10.1867068,1.5317918 10.3203543,1.76322881 Z M8.5,13 C8.22385763,13 8,13.2238576 8,13.5 L8,14.5 C8,14.7761424 8.22385763,15 8.5,15 L9.5,15 C9.77614237,15 10,14.7761424 10,14.5 L10,13.5 C10,13.2238576 9.77614237,13 9.5,13 L8.5,13 Z M8.5,7 C8.22385763,7 8,7.22385763 8,7.5 L8,11.5 C8,11.7761424 8.22385763,12 8.5,12 L9.5,12 C9.77614237,12 10,11.7761424 10,11.5 L10,7.5 C10,7.22385763 9.77614237,7 9.5,7 L8.5,7 Z"></path>\n        </svg>\n        <span class="gr-meta warning-text">Warning Text</span>\n      </div>\n    </div>\n';

  optionTemplate.innerHTML = '\n   <li class="gr-radio">\n        <input type="radio" name="scotch" id="radioOne" >\n        <label for="radioOne">Selection One</label>\n        <span>\n        <svg focusable="false" class="icon-18" aria-hidden="true">\n          <path d="M9,18 C4.02943725,18 0,13.9705627 0,9 C0,4.02943725 4.02943725,0 9,0 C13.9705627,0 18,4.02943725 18,9 C18,13.9705627 13.9705627,18 9,18 Z M9,16 C12.8659932,16 16,12.8659932 16,9 C16,5.13400675 12.8659932,2 9,2 C5.13400675,2 2,5.13400675 2,9 C2,12.8659932 5.13400675,16 9,16 Z"></path>\n          <path d="M9,14.3 C11.9271092,14.3 14.3,11.9271092 14.3,9 C14.3,6.07289083 11.9271092,3.7 9,3.7 C6.07289083,3.7 3.7,6.07289083 3.7,9 C3.7,11.9271092 6.07289083,14.3 9,14.3 Z"></path>\n        </svg>\n        </span>\n    </li>\n  ';

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-radio');

  var Radio = function (_HTMLElement) {
    _inherits(Radio, _HTMLElement);

    _createClass(Radio, [{
      key: 'selected',
      get: function get() {
        return this.getAttribute('selected');
      },
      set: function set(value) {
        this.setAttribute('selected', value);
      }
    }, {
      key: 'error',
      get: function get() {
        return this.hasAttribute('error');
      }
    }, {
      key: 'errorText',
      get: function get() {
        return this.getAttribute('error');
      }
    }, {
      key: 'disabled',
      get: function get() {
        return this.hasAttribute('disabled');
      }
    }, {
      key: 'labelText',
      get: function get() {
        return this.getAttribute('label');
      }
    }, {
      key: 'groupName',
      get: function get() {
        return this.getAttribute('groupname');
      }
    }, {
      key: 'labeltext',
      set: function set(value) {
        this.setAttribute('label', value);
      }
    }], [{
      key: 'observedAttributes',
      get: function get() {
        return ['label', 'groupName', 'disabled', 'error', 'selected'];
      }
    }]);

    function Radio() {
      _classCallCheck(this, Radio);

      var _this = _possibleConstructorReturn(this, (Radio.__proto__ || Object.getPrototypeOf(Radio)).call(this));

      _this.attachShadow({ mode: 'open' });
      var clone = template.content.cloneNode(true);
      _this.label = clone.querySelector('legend');
      _this.fieldset = clone.querySelector('fieldset');
      _this.fieldsetList = clone.querySelector('fieldset ul');
      _this.warningText = clone.querySelector('.warning-text');

      _this.renderedInputs = null;
      _this.value = null;

      _this.shadowRoot.appendChild(clone);

      _this.buildOptions = _this.buildOptions.bind(_this);
      _this.disableInputs = _this.disableInputs.bind(_this);

      _this.fieldset.addEventListener('click', function (event) {
        var clickedInput = event.target.closest('input');
        if (!clickedInput) {
          return;
        } else if (clickedInput.checked === true) {
          _this.selected = clickedInput.getAttribute('data-index');
          _this.value = clickedInput.value;
          _this.data = clickedInput.getAttribute('data-index');
        }
      });
      return _this;
    }

    _createClass(Radio, [{
      key: 'connectedCallback',
      value: function connectedCallback() {
        setTimeout(this.buildOptions, 100);
      }
    }, {
      key: 'attributeChangedCallback',
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'label') {
          if (oldValue !== newValue) {
            this.label.innerHTML = this.labelText;
          }
        }

        if (name === 'disabled') {
          if (oldValue !== newValue) {
            setTimeout(this.disableInputs, 100);
          }
        }

        if (name === 'error') {
          if (oldValue !== newValue) {
            if (this.error === true) {
              this.fieldset.classList.add('error');
              this.warningText.innerHTML = this.errorText;
            } else {
              this.fieldset.classList.remove('error');
            }
          }
        }

        if (name === 'selected') {
          var inputs = this.shadowRoot.querySelectorAll('input');
          inputs.forEach(function (input, index) {
            if (parseInt(newValue) === index) {
              input.checked = true;
            }
          });
        }
      }
    }, {
      key: 'buildOptions',
      value: function buildOptions() {
        var _this2 = this;

        this.options = this.querySelectorAll('input');
        this.options.forEach(function (option, index) {

          var optionClone = optionTemplate.content.cloneNode(true);
          var radioLabel = optionClone.querySelector('label'),
              radio = optionClone.querySelector('input');

          radio.id = option.name;
          radio.setAttribute('data-index', index);
          radio.setAttribute('name', _this2.groupName);
          radio.setAttribute('value', option.name);
          radioLabel.innerText = option.name;
          radioLabel.setAttribute('for', option.name);
          _this2.fieldsetList.appendChild(optionClone);
        });
        if (this.disabled === true) {
          this.disableInputs();
        }
      }
    }, {
      key: 'disableInputs',
      value: function disableInputs() {
        var renderedInputs = this.shadowRoot.querySelectorAll('input');
        if (this.disabled === true) {
          renderedInputs.forEach(function (input) {
            input.disabled = true;
          });
        } else {
          renderedInputs.forEach(function (input) {
            input.disabled = false;
          });
        }
      }
    }]);

    return Radio;
  }(HTMLElement);

  customElements.define('pearson-radio', Radio);
})(window, document);