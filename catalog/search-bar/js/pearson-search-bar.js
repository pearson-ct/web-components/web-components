(function(w, doc) {
  'use strict';
  // Create a template element
  const template = doc.createElement('template'),
        searchOptsBtnTemplate = doc.createElement('template'),
        filterBtnTemplate = doc.createElement('template'),
        menuItemTemplate = doc.createElement('template');

  template.innerHTML = `
  <style>
    :host{display:none;position:relative}:host *{font-family:Open Sans,Calibri,Tahoma,sans-serif;color:#252525;-webkit-box-sizing:border-box;box-sizing:border-box}@-webkit-keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@-webkit-keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@-webkit-keyframes fadeIn{0%{opacity:0}to{opacity:1}}@keyframes fadeIn{0%{opacity:0}to{opacity:1}}@-webkit-keyframes fadeOut{0%{opacity:1}to{opacity:0}}@keyframes fadeOut{0%{opacity:1}to{opacity:0}}:host .animated{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-fill-mode:backwards;animation-fill-mode:backwards}@media screen and (prefers-reduced-motion:reduce){:host .animated{-webkit-animation:unset!important;animation:unset!important}}:host .animateIn{-webkit-animation-name:fadeIn;animation-name:fadeIn;-webkit-animation-duration:.5s;animation-duration:.5s}:host .animateOut{-webkit-animation-name:fadeOut;animation-name:fadeOut;-webkit-animation-duration:.2s;animation-duration:.2s}:host .catalog.search-bar-container{display:-ms-grid;display:grid;grid-gap:4px;-ms-grid-rows:auto;-ms-grid-columns:1fr 4px auto;grid-template:"one   two"/1fr auto;background-color:#fff;border:1px solid #c7c7c7;border-radius:4px}:host .catalog.search-bar-container .search-options-container{position:relative;border-right:1px solid #c7c7c7;background-color:#fff;border-radius:4px 0 0 4px}:host .catalog.search-bar-container .search-options-container .search-options{-webkit-appearance:none;-moz-appearance:none;border-radius:4px 0 0 4px;display:block;margin:0;padding:0 0 0 12px;height:36px;color:#252525;border:0;font-size:14px;width:100%;max-width:100%;background-color:transparent;cursor:pointer;text-align:left}:host .catalog.search-bar-container .search-options-container .search-options::-ms-expand{display:none}:host .catalog.search-bar-container .search-options-container .search-options:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .catalog.search-bar-container .search-options-container pearson-icon{position:absolute;right:12px;top:-webkit-calc(50% - 9px);top:calc(50% - 9px);fill:#6a7070;pointer-events:none}:host .catalog.search-bar-container .search-input{-ms-grid-row:1;-ms-grid-column:1;border-radius:4px 0 0 4px;grid-area:one;display:block;margin:0;padding:0 12px;height:36px;color:#252525;border:0;background:transparent;font-size:14px;width:100%;max-width:100%;z-index:10}:host .catalog.search-bar-container .search-input:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .catalog.search-bar-container button{background:transparent;color:#6a7070;padding:0;cursor:pointer;position:relative;margin:1px 0;border:0;height:34px;width:44px}:host .catalog.search-bar-container button:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .catalog.search-bar-container .filterBtn{border-radius:0}:host .catalog.search-bar-container .searchBtn{-ms-grid-row:1;-ms-grid-column:3;grid-area:two;background-color:#ffb81c;border-radius:0 4px 4px 0;margin-right:1px}:host .catalog.search-bar-container.onlySearchOpts{grid-gap:4px;-ms-grid-rows:auto;-ms-grid-columns:140px 4px 1fr 4px auto;grid-template:"one   two   three"/140px 1fr auto}:host .catalog.search-bar-container.onlySearchOpts .search-options-container{-ms-grid-row:1;-ms-grid-column:1;grid-area:one}:host .catalog.search-bar-container.onlySearchOpts .search-input{-ms-grid-row:1;-ms-grid-column:3;border-radius:0;grid-area:two}:host .catalog.search-bar-container.onlySearchOpts .searchBtn{-ms-grid-row:1;-ms-grid-column:5;grid-area:three}:host .catalog.search-bar-container.collapsed.onlySearchOpts{grid-gap:4px;-ms-grid-rows:36px 4px 36px;-ms-grid-columns:1fr 4px auto 4px auto;grid-template:"one   one   one" 36px "two   two   three" 36px/1fr auto auto}:host .catalog.search-bar-container.collapsed.onlySearchOpts .search-options-container{-ms-grid-row:1;-ms-grid-column:1;-ms-grid-column-span:5;grid-area:one;border-right:0;border-bottom:1px solid #c7c7c7;border-radius:4px 4px 0 0;height:40px}:host .catalog.search-bar-container.collapsed.onlySearchOpts .search-options{border-radius:4px 4px 0 0}:host .catalog.search-bar-container.collapsed.onlySearchOpts .search-input{-ms-grid-row:3;-ms-grid-column:1;-ms-grid-column-span:3;border-radius:0 0 0 4px;grid-area:two}:host .catalog.search-bar-container.collapsed.onlySearchOpts .searchBtn{-ms-grid-row:3;-ms-grid-column:5;border-radius:0 0 4px 0;grid-area:three}:host .catalog.search-bar-container.onlyFilter{grid-gap:4px;-ms-grid-rows:auto;-ms-grid-columns:1fr 4px auto 4px auto;grid-template:"one   two   three"/1fr auto auto}:host .catalog.search-bar-container.onlyFilter .search-input{-ms-grid-row:1;-ms-grid-column:1;-ms-grid-column-span:1;border-radius:4px 0 0 4px;grid-area:one}:host .catalog.search-bar-container.onlyFilter .filterBtn{-ms-grid-row:1;-ms-grid-column:3;-ms-grid-column-span:1;grid-area:two}:host .catalog.search-bar-container.onlyFilter .searchBtn{-ms-grid-row:1;-ms-grid-column:5;grid-area:three}:host .catalog.search-bar-container.both{grid-gap:4px;-ms-grid-rows:auto;-ms-grid-columns:140px 4px 1fr 4px auto 4px auto;grid-template:"one   two   three   four"/140px 1fr auto auto}:host .catalog.search-bar-container.both .search-options-container{-ms-grid-row:1;-ms-grid-column:1;-ms-grid-column-span:1;grid-area:one}:host .catalog.search-bar-container.both .search-input{-ms-grid-row:1;-ms-grid-column:3;-ms-grid-column-span:1;border-radius:0;grid-area:two}:host .catalog.search-bar-container.both .filterBtn{-ms-grid-row:1;-ms-grid-column:5;grid-area:three}:host .catalog.search-bar-container.both .searchBtn{-ms-grid-row:1;-ms-grid-column:7;grid-area:four}:host .catalog.search-bar-container.collapsed.both{grid-gap:4px;-ms-grid-rows:36px 4px 36px;-ms-grid-columns:1fr 4px auto 4px auto;grid-template:"one   one   one" 36px "two   three four" 36px/1fr auto auto}:host .catalog.search-bar-container.collapsed.both .search-options-container{-ms-grid-row:1;-ms-grid-column:1;-ms-grid-column-span:5;grid-area:one;border-right:0;border-bottom:1px solid #c7c7c7;border-radius:4px 4px 0 0;height:40px}:host .catalog.search-bar-container.collapsed.both .search-options{border-radius:4px 4px 0 0}:host .catalog.search-bar-container.collapsed.both .search-input{-ms-grid-row:3;-ms-grid-column:1;-ms-grid-column-span:1;border-radius:0 0 0 4px;grid-area:two}:host .catalog.search-bar-container.collapsed.both .filterBtn{-ms-grid-row:3;-ms-grid-column:3;grid-area:three}:host .catalog.search-bar-container.collapsed.both .searchBtn{-ms-grid-row:3;-ms-grid-column:5;border-radius:0 0 4px 0;grid-area:four}:host .catalog.search-bar-container.disabled{background-color:#e9e9e9}:host .catalog.search-bar-container.disabled .filterBtn{cursor:auto}:host .catalog.search-bar-container.disabled .searchBtn{background-color:transparent;cursor:auto}:host .catalog.search-bar-container.disabled .searchBtn pearson-icon{fill:#6a7070}:host .catalog.search-bar-container.error~.error-state{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}:host .catalog.search-bar-container.error~.error-state span{margin-left:8px;color:#db0020}:host .catalog.search-bar-container.error~.error-state pearson-icon{fill:#db0020}:host .menu{background-color:#fff;position:absolute;list-style-type:none;padding:1em 4px 1em 0;border:1px solid #d9d9d9;border-radius:4px;width:220px;margin-top:0;z-index:10}:host .menu#search-options{top:40px;left:0}:host .menu#filter-options{top:40px;right:0}:host .menu button{background:none;border:0;font-size:14px;position:relative;padding:6px;width:100%;text-align:left;cursor:pointer}:host .menu button>*{pointer-events:none}:host .menu button:focus{outline:0;-webkit-box-shadow:none;box-shadow:none}:host .menu button:focus:after{border:2px solid #1977d4;content:"";position:absolute;border-radius:4px;width:-webkit-calc(100% + -10px);width:calc(100% + -10px);height:100%;top:0;left:5px;z-index:1}:host .menu button[aria-checked=false] pearson-icon{display:none}:host .menu button[aria-checked=true] pearson-icon{display:inline-block}:host .menu button pearson-icon{position:absolute;left:12px}:host .menu button span{margin-left:32px}:host .collapsed~#search-options{width:100%}:host .collapsed~#filter-options{width:100%}:host .both.collapsed~#filter-options{top:80px}:host .error-state{margin-top:4px;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;display:none}
  </style>
    <form class="catalog search-bar-container">
      <input aria-label="search site" class="search-input" type="text"/>
      <button class="searchBtn" aria-label="search">
          <pearson-icon icon="search" size="18"></pearson-icon>
      </button>
    </form>
    <!--error-->
    <div class="error-state">
        <pearson-icon icon="warning" size="18"></pearson-icon>
        <span class="gr-meta warning-text"></span>
    </div>
`;

  searchOptsBtnTemplate.innerHTML = `
    <div class="search-options-container">
       <button class="search-options">Search in</button>
       <pearson-icon icon="drop-down" size="18"></pearson-icon>
    </div>
  `;

  filterBtnTemplate.innerHTML = `
    <button class="filterBtn">
        <pearson-icon icon="filter" size="18"></pearson-icon>
    </button>
  `;

  menuItemTemplate.innerHTML = `
   <li role="none" >
    <button role="menuitemradio" aria-checked="false">
        <pearson-icon icon="correct" size="18"></pearson-icon>
       <span class="option-text">Option Two</span>
    </button>
   </li>
  `;


  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-search-bar');
  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(searchOptsBtnTemplate, 'pearson-search-bar');
  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(filterBtnTemplate, 'pearson-search-bar');
  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(menuItemTemplate, 'pearson-search-bar');


  class SearchBar extends HTMLElement {
    static get observedAttributes() {
      return ['placeholder', 'error', 'collapsed', 'startdisabled'];
    }

    get placeholderText() {
      return this.getAttribute('placeholder');
    }

    get errorText() {
      return this.getAttribute('error');
    }

    get collapsed() {
      return this.hasAttribute('collapsed');
    }

    get startdisabled() {
      return this.hasAttribute('startdisabled');
    }


    constructor() {
      super();
      this.attachShadow({mode: 'open'});
      const templateClone = template.content.cloneNode(true);
      this.shadowRoot.appendChild(templateClone);

      this.searchBar = this.shadowRoot.querySelector('.search-bar-container');
      this.input = this.shadowRoot.querySelector('.search-input');
      this.searchBtn = this.shadowRoot.querySelector('.searchBtn');
      this.errorState = this.shadowRoot.querySelector('.error-state');
      this.warningText = this.shadowRoot.querySelector('.warning-text');

      //keeps track of if and what the children are
      this.searchOptionsState = false;
      this.filterState = false;

      //value state
      this.value = {};

      //set up component based on if and what children are present
      this.childrenCheck = this.childrenCheck.bind(this);
      this.createSearchOptions = this.createSearchOptions.bind(this);
      this.createFilter = this.createFilter.bind(this);
      this.getFocusableElements = this.getFocusableElements.bind(this);
      this.setParentGrid = this.setParentGrid.bind(this);

      //handle events
      this.handleSearchOptions = this.handleSearchOptions.bind(this);
      this.handleFilter = this.handleFilter.bind(this);
      this.handleSearch = this.handleSearch.bind(this);
      this.closeBothMenus = this.closeBothMenus.bind(this);
    }

    connectedCallback(){
      setTimeout(this.childrenCheck, 100)
      //prevent fouc
      setTimeout(()=>{this.style.display = 'block'}, 600)

      if (w.ShadyCSS) {
        //this is a work around for a gravity.css override
        let shadowSearchIcon = this.shadowRoot.querySelector('pearson-icon[icon="search"]');
        this.shadowSearchSvg = shadowSearchIcon.shadowRoot.querySelector('svg');
        if (this.startdisabled === true && this.searchOptionsState === true) {
        this.shadowSearchSvg.style.fill = "#6a7070";
        }else {
        this.shadowSearchSvg.style.fill = '#252525';
        }
      }

      this.searchBtn.addEventListener('click', this.handleSearch);
      this.input.addEventListener('click', this.closeBothMenus);

      doc.addEventListener('keydown', event => {
        if(event.key === 'Escape'){
          this.closeBothMenus()
        }
      });
    }

    attributeChangedCallback(name, oldValue, newValue) {
      if (name === 'placeholder') {
        if (oldValue !== newValue) {
          this.input.setAttribute('placeholder', this.placeholderText);
        }
      }

      if (name === 'error') {
        if (oldValue !== newValue) {
          if (this.errorText) {
            this.searchBar.classList.add('error');
            this.warningText.innerHTML = this.errorText
              if (w.ShadyCSS) {
                //this is a work around for a gravity.css override
                let shadowErrorIcon = this.shadowRoot.querySelector('pearson-icon[icon="warning"]');
                let shadowErrorSvg = shadowErrorIcon.shadowRoot.querySelector('svg');
                shadowErrorSvg.style.fill = '#DB0020';
              }
          } else {
            this.searchBar.classList.remove('error');
          }
        }
      }

      if (name === 'collapsed') {
        if (oldValue !== newValue) {
          this.collapsed === true ? this.searchBar.classList.add('collapsed') : this.searchBar.classList.remove('collapsed');
        }
      }

    }

    //check for user added children
    childrenCheck(){
      if(this.children){
      for(let i = 0; i < this.children.length; i++){
        if(this.children[i].classList.contains('search-options')){
         this.createSearchOptions(this.children[i]);
         this.searchOptionsState = true;
        }
        if(this.children[i].classList.contains('filter-options')){
          this.createFilter(this.children[i]);
          this.filterState = true;
        }
      }
      this.setParentGrid()
      }
    }

    //create the search options select menu
    createSearchOptions(list){
      //create button and menu
      const searchOptsBtnClone = searchOptsBtnTemplate.content.cloneNode(true);
      this.searchOptionsBtn = searchOptsBtnClone.querySelector('button');
      this.optsMenu = document.createElement('ul');
      this.optsMenu.setAttribute('role', 'menu');
      this.optsMenu.classList.add('menu', 'animated', 'animateIn');
      this.optsMenu.id = list.className;
      this.optsMenu.setAttribute('hidden', '')
      this.shadowRoot.insertBefore(this.optsMenu, this.errorState);
      this.searchBar.prepend(searchOptsBtnClone);

      //create list
      let listArray = Array.from(list.children)
      listArray.forEach((child, index)=>{
        let optionClone = menuItemTemplate.content.cloneNode(true);
        let optionText = optionClone.querySelector('span');
        optionText.innerText = child.innerText;
        let optionButton = optionClone.querySelector('button');
        optionButton.setAttribute('data-index', index);


        optionButton.addEventListener('click', event => {
          const focusItems = this.getFocusableElements(event.target.parentNode.parentNode);
          focusItems.forEach(item => {
            item.setAttribute('aria-checked', false)
          });

          event.target.setAttribute('aria-checked', true);
          this.optsMenu.setAttribute('hidden','')
          this.searchOptionsBtn.innerText = event.target.querySelector('.option-text').innerText;

          //save value
          this.value.searchOption = event.target.querySelector('.option-text').innerText;

          //enable searchBar if startdisabled is set
          if(this.startdisabled === true){
            this.searchBar.classList.remove('disabled');
            this.input.removeAttribute('disabled');
            this.searchBtn.removeAttribute('disabled');
            if(w.ShadyCSS) this.shadowSearchSvg.style.fill = '#252525';
            if(this.filterBtn) {
              this.filterBtn.removeAttribute('disabled');
              this.shadowFilterSvg.style.fill = "#252525";
            }
           }
        });

        optionButton.addEventListener('keydown',(e)=>{
          this.handleMenuKeyboard(e, this.optsMenu)
        });

        this.optsMenu.append(optionClone)
      })

      //set startdisabled
        if(this.startdisabled === true){
          this.searchBar.classList.add('disabled');
          this.input.setAttribute('disabled','');
          this.searchBtn.setAttribute('disabled','');
          setTimeout(()=>{this.filterBtn.setAttribute('disabled','');}, 300)
       }


      if (w.ShadyCSS) {
        //this is a work around for a gravity.css override
        let shadowDropDownIcon = this.shadowRoot.querySelector('pearson-icon[icon="drop-down"]');
        let shadowDropDownSvg = shadowDropDownIcon.shadowRoot.querySelector('svg');
        shadowDropDownSvg.style.fill = '#6a7070';
      }

      //add event listener
      this.searchOptionsBtn.addEventListener('click', this.handleSearchOptions);
    }

    //create the filter drop down
    createFilter(list){
      //create filter button and menu
      const filterBtnClone = filterBtnTemplate.content.cloneNode(true);
      this.filterMenu = document.createElement('ul');
      this.filterMenu.setAttribute('role', 'menu');
      this.filterMenu.classList.add('menu', 'animated', 'animateIn');
      this.filterMenu.id = list.className;
      this.filterMenu.setAttribute('hidden', '')
      this.shadowRoot.insertBefore(this.filterMenu, this.errorState);

      //create list
      let listArray = Array.from(list.children)
      listArray.forEach((child, index)=>{
        let optionClone = menuItemTemplate.content.cloneNode(true);
        let optionText = optionClone.querySelector('span');
        optionText.innerText = child.innerText;
        let optionButton = optionClone.querySelector('button');
        optionButton.setAttribute('data-index', index);

        optionButton.addEventListener('click', event => {
            const ariaChecked = event.target.getAttribute('aria-checked');
            if (ariaChecked === 'false') {
              event.target.setAttribute('aria-checked', true)
              this.value.filters.push(event.target.innerText)
            } else {
              event.target.setAttribute('aria-checked', false)
              let index = this.value.filters.indexOf(event.target.innerText)
              this.value.filters.splice(index, 1)
            }
        });

        optionButton.addEventListener('keydown',(e)=>{
          this.handleMenuKeyboard(e, this.filterMenu)
        });

        this.filterMenu.append(optionClone)
      })

      this.searchBar.insertBefore(filterBtnClone, this.searchBtn);


      //this is a work around for the faulty filter icon coming through the pearson-icon component and gravity css override
      let shadowFilterIcon = this.shadowRoot.querySelector('pearson-icon[icon="filter"]');
      this.shadowFilterSvg = shadowFilterIcon.shadowRoot.querySelector('svg');
      this.shadowFilterSvg.setAttribute('viewBox', '3 3 18 18');
      if (this.startdisabled === true && this.searchOptionsState === true) {
        this.shadowFilterSvg.style.fill = "#6a7070"
      }else {
        this.shadowFilterSvg.style.fill = "#252525";
      }

      //set filter button event listener
      this.filterBtn = this.shadowRoot.querySelector('.filterBtn');
      this.filterBtn.addEventListener('click', this.handleFilter);

      //set value
      this.value.filters = [];
    }


    //set the grid of the search-bar-container depending on what children are present
    setParentGrid(){
      if(this.searchOptionsState === true && this.filterState === true){
        this.searchBar.classList.add('both')
      }else if(this.searchOptionsState === true){
        this.searchBar.classList.add('onlySearchOpts')
      }else if(this.filterState === true){
        this.searchBar.classList.add('onlyFilter')
      }
    }

    getFocusableElements(node) {
      return node.querySelectorAll('[role^="menuitemradio"]');
    }

    handleMenuKeyboard(e, menu){
        var nextButton = parseInt(e.target.getAttribute('data-index')) + 1,
            prevButton = parseInt(e.target.getAttribute('data-index')) - 1;
        if (e.key === 'ArrowUp' || e.key === 'Up') {
          e.preventDefault();
          if (this.shadowRoot.activeElement === this.getFocusableElements(menu)[0]) {
            this.getFocusableElements(menu)[this.getFocusableElements(menu).length - 1].focus();
          } else {
            this.getFocusableElements(menu)[prevButton].focus();
          }
        }
        if (e.key === 'ArrowDown' || e.key === 'Down') {
          e.preventDefault();
          if (this.shadowRoot.activeElement === this.getFocusableElements(menu)[this.getFocusableElements(menu).length - 1]) {
            this.getFocusableElements(menu)[0].focus();
          } else {
            this.getFocusableElements(menu)[nextButton].focus();
          }
        }
        if (e.key === 'Home') {
          this.getFocusableElements(menu)[0].focus();
        }

        if (e.key === 'End') {
          this.getFocusableElements(menu)[this.getFocusableElements(menu).length - 1].focus();
        }
    }

    handleSearchOptions(e){
      e.preventDefault();
      const hidden = this.optsMenu.getAttribute('hidden');
        if (hidden === '') {
          this.optsMenu.removeAttribute('hidden')
          this.shadowRoot.addEventListener('animationend', event => {
            this.getFocusableElements(this.optsMenu)[0].focus();
          })
        } else {
          this.optsMenu.setAttribute('hidden', '')
      }

      if(this.filterMenu){
        this.filterMenu.getAttribute('hidden') != '' ? this.filterMenu.setAttribute('hidden','') : null;
      }
    }

    handleFilter(e){
      e.preventDefault();
      const hidden = this.filterMenu.getAttribute('hidden');
        if (hidden === '') {
          this.filterMenu.removeAttribute('hidden')
          this.shadowRoot.addEventListener('animationend', event => {
            this.getFocusableElements(this.filterMenu)[0].focus();
          })
        } else {
          this.filterMenu.setAttribute('hidden', '')
      }
      if(this.optsMenu){
        this.optsMenu.getAttribute('hidden') != '' ? this.optsMenu.setAttribute('hidden','') : null;
      }
    }

    handleSearch(e){
      e.preventDefault();
      this.searchOptionsState === false && this.filterState === false ? this.value = this.input.value : this.value.searchTerm = this.input.value;
      let search = new Event('search');
      this.dispatchEvent(search);
      this.closeBothMenus()
    }

    closeBothMenus(){
    if(this.filterMenu) {
      if (this.filterMenu.getAttribute('hidden') != '') {
        this.filterMenu.setAttribute('hidden', '')
      }
    }
    if(this.optsMenu) {
      if (this.optsMenu.getAttribute('hidden') != '') {
        this.optsMenu.setAttribute('hidden', '')
      }
    }
    }


  }
  customElements.define('pearson-search-bar', SearchBar)
})(window, document);
