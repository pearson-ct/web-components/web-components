var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function (w, doc) {
  'use strict';
  // Create a template element

  var template = doc.createElement('template'),
      searchOptsBtnTemplate = doc.createElement('template'),
      filterBtnTemplate = doc.createElement('template'),
      menuItemTemplate = doc.createElement('template');

  template.innerHTML = '\n  <style>\n    :host{display:none;position:relative}:host *{font-family:Open Sans,Calibri,Tahoma,sans-serif;color:#252525;-webkit-box-sizing:border-box;box-sizing:border-box}@-webkit-keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@-webkit-keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@-webkit-keyframes fadeIn{0%{opacity:0}to{opacity:1}}@keyframes fadeIn{0%{opacity:0}to{opacity:1}}@-webkit-keyframes fadeOut{0%{opacity:1}to{opacity:0}}@keyframes fadeOut{0%{opacity:1}to{opacity:0}}:host .animated{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-fill-mode:backwards;animation-fill-mode:backwards}@media screen and (prefers-reduced-motion:reduce){:host .animated{-webkit-animation:unset!important;animation:unset!important}}:host .animateIn{-webkit-animation-name:fadeIn;animation-name:fadeIn;-webkit-animation-duration:.5s;animation-duration:.5s}:host .animateOut{-webkit-animation-name:fadeOut;animation-name:fadeOut;-webkit-animation-duration:.2s;animation-duration:.2s}:host .catalog.search-bar-container{display:-ms-grid;display:grid;grid-gap:4px;-ms-grid-rows:auto;-ms-grid-columns:1fr 4px auto;grid-template:"one   two"/1fr auto;background-color:#fff;border:1px solid #c7c7c7;border-radius:4px}:host .catalog.search-bar-container .search-options-container{position:relative;border-right:1px solid #c7c7c7;background-color:#fff;border-radius:4px 0 0 4px}:host .catalog.search-bar-container .search-options-container .search-options{-webkit-appearance:none;-moz-appearance:none;border-radius:4px 0 0 4px;display:block;margin:0;padding:0 0 0 12px;height:36px;color:#252525;border:0;font-size:14px;width:100%;max-width:100%;background-color:transparent;cursor:pointer;text-align:left}:host .catalog.search-bar-container .search-options-container .search-options::-ms-expand{display:none}:host .catalog.search-bar-container .search-options-container .search-options:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .catalog.search-bar-container .search-options-container pearson-icon{position:absolute;right:12px;top:-webkit-calc(50% - 9px);top:calc(50% - 9px);fill:#6a7070;pointer-events:none}:host .catalog.search-bar-container .search-input{-ms-grid-row:1;-ms-grid-column:1;border-radius:4px 0 0 4px;grid-area:one;display:block;margin:0;padding:0 12px;height:36px;color:#252525;border:0;background:transparent;font-size:14px;width:100%;max-width:100%;z-index:10}:host .catalog.search-bar-container .search-input:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .catalog.search-bar-container button{background:transparent;color:#6a7070;padding:0;cursor:pointer;position:relative;margin:1px 0;border:0;height:34px;width:44px}:host .catalog.search-bar-container button:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .catalog.search-bar-container .filterBtn{border-radius:0}:host .catalog.search-bar-container .searchBtn{-ms-grid-row:1;-ms-grid-column:3;grid-area:two;background-color:#ffb81c;border-radius:0 4px 4px 0;margin-right:1px}:host .catalog.search-bar-container.onlySearchOpts{grid-gap:4px;-ms-grid-rows:auto;-ms-grid-columns:140px 4px 1fr 4px auto;grid-template:"one   two   three"/140px 1fr auto}:host .catalog.search-bar-container.onlySearchOpts .search-options-container{-ms-grid-row:1;-ms-grid-column:1;grid-area:one}:host .catalog.search-bar-container.onlySearchOpts .search-input{-ms-grid-row:1;-ms-grid-column:3;border-radius:0;grid-area:two}:host .catalog.search-bar-container.onlySearchOpts .searchBtn{-ms-grid-row:1;-ms-grid-column:5;grid-area:three}:host .catalog.search-bar-container.collapsed.onlySearchOpts{grid-gap:4px;-ms-grid-rows:36px 4px 36px;-ms-grid-columns:1fr 4px auto 4px auto;grid-template:"one   one   one" 36px "two   two   three" 36px/1fr auto auto}:host .catalog.search-bar-container.collapsed.onlySearchOpts .search-options-container{-ms-grid-row:1;-ms-grid-column:1;-ms-grid-column-span:5;grid-area:one;border-right:0;border-bottom:1px solid #c7c7c7;border-radius:4px 4px 0 0;height:40px}:host .catalog.search-bar-container.collapsed.onlySearchOpts .search-options{border-radius:4px 4px 0 0}:host .catalog.search-bar-container.collapsed.onlySearchOpts .search-input{-ms-grid-row:3;-ms-grid-column:1;-ms-grid-column-span:3;border-radius:0 0 0 4px;grid-area:two}:host .catalog.search-bar-container.collapsed.onlySearchOpts .searchBtn{-ms-grid-row:3;-ms-grid-column:5;border-radius:0 0 4px 0;grid-area:three}:host .catalog.search-bar-container.onlyFilter{grid-gap:4px;-ms-grid-rows:auto;-ms-grid-columns:1fr 4px auto 4px auto;grid-template:"one   two   three"/1fr auto auto}:host .catalog.search-bar-container.onlyFilter .search-input{-ms-grid-row:1;-ms-grid-column:1;-ms-grid-column-span:1;border-radius:4px 0 0 4px;grid-area:one}:host .catalog.search-bar-container.onlyFilter .filterBtn{-ms-grid-row:1;-ms-grid-column:3;-ms-grid-column-span:1;grid-area:two}:host .catalog.search-bar-container.onlyFilter .searchBtn{-ms-grid-row:1;-ms-grid-column:5;grid-area:three}:host .catalog.search-bar-container.both{grid-gap:4px;-ms-grid-rows:auto;-ms-grid-columns:140px 4px 1fr 4px auto 4px auto;grid-template:"one   two   three   four"/140px 1fr auto auto}:host .catalog.search-bar-container.both .search-options-container{-ms-grid-row:1;-ms-grid-column:1;-ms-grid-column-span:1;grid-area:one}:host .catalog.search-bar-container.both .search-input{-ms-grid-row:1;-ms-grid-column:3;-ms-grid-column-span:1;border-radius:0;grid-area:two}:host .catalog.search-bar-container.both .filterBtn{-ms-grid-row:1;-ms-grid-column:5;grid-area:three}:host .catalog.search-bar-container.both .searchBtn{-ms-grid-row:1;-ms-grid-column:7;grid-area:four}:host .catalog.search-bar-container.collapsed.both{grid-gap:4px;-ms-grid-rows:36px 4px 36px;-ms-grid-columns:1fr 4px auto 4px auto;grid-template:"one   one   one" 36px "two   three four" 36px/1fr auto auto}:host .catalog.search-bar-container.collapsed.both .search-options-container{-ms-grid-row:1;-ms-grid-column:1;-ms-grid-column-span:5;grid-area:one;border-right:0;border-bottom:1px solid #c7c7c7;border-radius:4px 4px 0 0;height:40px}:host .catalog.search-bar-container.collapsed.both .search-options{border-radius:4px 4px 0 0}:host .catalog.search-bar-container.collapsed.both .search-input{-ms-grid-row:3;-ms-grid-column:1;-ms-grid-column-span:1;border-radius:0 0 0 4px;grid-area:two}:host .catalog.search-bar-container.collapsed.both .filterBtn{-ms-grid-row:3;-ms-grid-column:3;grid-area:three}:host .catalog.search-bar-container.collapsed.both .searchBtn{-ms-grid-row:3;-ms-grid-column:5;border-radius:0 0 4px 0;grid-area:four}:host .catalog.search-bar-container.disabled{background-color:#e9e9e9}:host .catalog.search-bar-container.disabled .filterBtn{cursor:auto}:host .catalog.search-bar-container.disabled .searchBtn{background-color:transparent;cursor:auto}:host .catalog.search-bar-container.disabled .searchBtn pearson-icon{fill:#6a7070}:host .catalog.search-bar-container.error~.error-state{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}:host .catalog.search-bar-container.error~.error-state span{margin-left:8px;color:#db0020}:host .catalog.search-bar-container.error~.error-state pearson-icon{fill:#db0020}:host .menu{background-color:#fff;position:absolute;list-style-type:none;padding:1em 4px 1em 0;border:1px solid #d9d9d9;border-radius:4px;width:220px;margin-top:0;z-index:10}:host .menu#search-options{top:40px;left:0}:host .menu#filter-options{top:40px;right:0}:host .menu button{background:none;border:0;font-size:14px;position:relative;padding:6px;width:100%;text-align:left;cursor:pointer}:host .menu button>*{pointer-events:none}:host .menu button:focus{outline:0;-webkit-box-shadow:none;box-shadow:none}:host .menu button:focus:after{border:2px solid #1977d4;content:"";position:absolute;border-radius:4px;width:-webkit-calc(100% + -10px);width:calc(100% + -10px);height:100%;top:0;left:5px;z-index:1}:host .menu button[aria-checked=false] pearson-icon{display:none}:host .menu button[aria-checked=true] pearson-icon{display:inline-block}:host .menu button pearson-icon{position:absolute;left:12px}:host .menu button span{margin-left:32px}:host .collapsed~#search-options{width:100%}:host .collapsed~#filter-options{width:100%}:host .both.collapsed~#filter-options{top:80px}:host .error-state{margin-top:4px;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;display:none}\n  </style>\n    <form class="catalog search-bar-container">\n      <input aria-label="search site" class="search-input" type="text"/>\n      <button class="searchBtn" aria-label="search">\n          <pearson-icon icon="search" size="18"></pearson-icon>\n      </button>\n    </form>\n    <!--error-->\n    <div class="error-state">\n        <pearson-icon icon="warning" size="18"></pearson-icon>\n        <span class="gr-meta warning-text"></span>\n    </div>\n';

  searchOptsBtnTemplate.innerHTML = '\n    <div class="search-options-container">\n       <button class="search-options">Search in</button>\n       <pearson-icon icon="drop-down" size="18"></pearson-icon>\n    </div>\n  ';

  filterBtnTemplate.innerHTML = '\n    <button class="filterBtn">\n        <pearson-icon icon="filter" size="18"></pearson-icon>\n    </button>\n  ';

  menuItemTemplate.innerHTML = '\n   <li role="none" >\n    <button role="menuitemradio" aria-checked="false">\n        <pearson-icon icon="correct" size="18"></pearson-icon>\n       <span class="option-text">Option Two</span>\n    </button>\n   </li>\n  ';

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-search-bar');
  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(searchOptsBtnTemplate, 'pearson-search-bar');
  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(filterBtnTemplate, 'pearson-search-bar');
  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(menuItemTemplate, 'pearson-search-bar');

  var SearchBar = function (_HTMLElement) {
    _inherits(SearchBar, _HTMLElement);

    _createClass(SearchBar, [{
      key: 'placeholderText',
      get: function get() {
        return this.getAttribute('placeholder');
      }
    }, {
      key: 'errorText',
      get: function get() {
        return this.getAttribute('error');
      }
    }, {
      key: 'collapsed',
      get: function get() {
        return this.hasAttribute('collapsed');
      }
    }, {
      key: 'startdisabled',
      get: function get() {
        return this.hasAttribute('startdisabled');
      }
    }], [{
      key: 'observedAttributes',
      get: function get() {
        return ['placeholder', 'error', 'collapsed', 'startdisabled'];
      }
    }]);

    function SearchBar() {
      _classCallCheck(this, SearchBar);

      var _this = _possibleConstructorReturn(this, (SearchBar.__proto__ || Object.getPrototypeOf(SearchBar)).call(this));

      _this.attachShadow({ mode: 'open' });
      var templateClone = template.content.cloneNode(true);
      _this.shadowRoot.appendChild(templateClone);

      _this.searchBar = _this.shadowRoot.querySelector('.search-bar-container');
      _this.input = _this.shadowRoot.querySelector('.search-input');
      _this.searchBtn = _this.shadowRoot.querySelector('.searchBtn');
      _this.errorState = _this.shadowRoot.querySelector('.error-state');
      _this.warningText = _this.shadowRoot.querySelector('.warning-text');

      //keeps track of if and what the children are
      _this.searchOptionsState = false;
      _this.filterState = false;

      //value state
      _this.value = {};

      //set up component based on if and what children are present
      _this.childrenCheck = _this.childrenCheck.bind(_this);
      _this.createSearchOptions = _this.createSearchOptions.bind(_this);
      _this.createFilter = _this.createFilter.bind(_this);
      _this.getFocusableElements = _this.getFocusableElements.bind(_this);
      _this.setParentGrid = _this.setParentGrid.bind(_this);

      //handle events
      _this.handleSearchOptions = _this.handleSearchOptions.bind(_this);
      _this.handleFilter = _this.handleFilter.bind(_this);
      _this.handleSearch = _this.handleSearch.bind(_this);
      _this.closeBothMenus = _this.closeBothMenus.bind(_this);
      return _this;
    }

    _createClass(SearchBar, [{
      key: 'connectedCallback',
      value: function connectedCallback() {
        var _this2 = this;

        setTimeout(this.childrenCheck, 100);
        //prevent fouc
        setTimeout(function () {
          _this2.style.display = 'block';
        }, 600);

        if (w.ShadyCSS) {
          //this is a work around for a gravity.css override
          var shadowSearchIcon = this.shadowRoot.querySelector('pearson-icon[icon="search"]');
          this.shadowSearchSvg = shadowSearchIcon.shadowRoot.querySelector('svg');
          if (this.startdisabled === true && this.searchOptionsState === true) {
            this.shadowSearchSvg.style.fill = "#6a7070";
          } else {
            this.shadowSearchSvg.style.fill = '#252525';
          }
        }

        this.searchBtn.addEventListener('click', this.handleSearch);
        this.input.addEventListener('click', this.closeBothMenus);

        doc.addEventListener('keydown', function (event) {
          if (event.key === 'Escape') {
            _this2.closeBothMenus();
          }
        });
      }
    }, {
      key: 'attributeChangedCallback',
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'placeholder') {
          if (oldValue !== newValue) {
            this.input.setAttribute('placeholder', this.placeholderText);
          }
        }

        if (name === 'error') {
          if (oldValue !== newValue) {
            if (this.errorText) {
              this.searchBar.classList.add('error');
              this.warningText.innerHTML = this.errorText;
              if (w.ShadyCSS) {
                //this is a work around for a gravity.css override
                var shadowErrorIcon = this.shadowRoot.querySelector('pearson-icon[icon="warning"]');
                var shadowErrorSvg = shadowErrorIcon.shadowRoot.querySelector('svg');
                shadowErrorSvg.style.fill = '#DB0020';
              }
            } else {
              this.searchBar.classList.remove('error');
            }
          }
        }

        if (name === 'collapsed') {
          if (oldValue !== newValue) {
            this.collapsed === true ? this.searchBar.classList.add('collapsed') : this.searchBar.classList.remove('collapsed');
          }
        }
      }

      //check for user added children

    }, {
      key: 'childrenCheck',
      value: function childrenCheck() {
        if (this.children) {
          for (var i = 0; i < this.children.length; i++) {
            if (this.children[i].classList.contains('search-options')) {
              this.createSearchOptions(this.children[i]);
              this.searchOptionsState = true;
            }
            if (this.children[i].classList.contains('filter-options')) {
              this.createFilter(this.children[i]);
              this.filterState = true;
            }
          }
          this.setParentGrid();
        }
      }

      //create the search options select menu

    }, {
      key: 'createSearchOptions',
      value: function createSearchOptions(list) {
        var _this3 = this;

        //create button and menu
        var searchOptsBtnClone = searchOptsBtnTemplate.content.cloneNode(true);
        this.searchOptionsBtn = searchOptsBtnClone.querySelector('button');
        this.optsMenu = document.createElement('ul');
        this.optsMenu.setAttribute('role', 'menu');
        this.optsMenu.classList.add('menu', 'animated', 'animateIn');
        this.optsMenu.id = list.className;
        this.optsMenu.setAttribute('hidden', '');
        this.shadowRoot.insertBefore(this.optsMenu, this.errorState);
        this.searchBar.prepend(searchOptsBtnClone);

        //create list
        var listArray = Array.from(list.children);
        listArray.forEach(function (child, index) {
          var optionClone = menuItemTemplate.content.cloneNode(true);
          var optionText = optionClone.querySelector('span');
          optionText.innerText = child.innerText;
          var optionButton = optionClone.querySelector('button');
          optionButton.setAttribute('data-index', index);

          optionButton.addEventListener('click', function (event) {
            var focusItems = _this3.getFocusableElements(event.target.parentNode.parentNode);
            focusItems.forEach(function (item) {
              item.setAttribute('aria-checked', false);
            });

            event.target.setAttribute('aria-checked', true);
            _this3.optsMenu.setAttribute('hidden', '');
            _this3.searchOptionsBtn.innerText = event.target.querySelector('.option-text').innerText;

            //save value
            _this3.value.searchOption = event.target.querySelector('.option-text').innerText;

            //enable searchBar if startdisabled is set
            if (_this3.startdisabled === true) {
              _this3.searchBar.classList.remove('disabled');
              _this3.input.removeAttribute('disabled');
              _this3.searchBtn.removeAttribute('disabled');
              if (w.ShadyCSS) _this3.shadowSearchSvg.style.fill = '#252525';
              if (_this3.filterBtn) {
                _this3.filterBtn.removeAttribute('disabled');
                _this3.shadowFilterSvg.style.fill = "#252525";
              }
            }
          });

          optionButton.addEventListener('keydown', function (e) {
            _this3.handleMenuKeyboard(e, _this3.optsMenu);
          });

          _this3.optsMenu.append(optionClone);
        });

        //set startdisabled
        if (this.startdisabled === true) {
          this.searchBar.classList.add('disabled');
          this.input.setAttribute('disabled', '');
          this.searchBtn.setAttribute('disabled', '');
          setTimeout(function () {
            _this3.filterBtn.setAttribute('disabled', '');
          }, 300);
        }

        if (w.ShadyCSS) {
          //this is a work around for a gravity.css override
          var shadowDropDownIcon = this.shadowRoot.querySelector('pearson-icon[icon="drop-down"]');
          var shadowDropDownSvg = shadowDropDownIcon.shadowRoot.querySelector('svg');
          shadowDropDownSvg.style.fill = '#6a7070';
        }

        //add event listener
        this.searchOptionsBtn.addEventListener('click', this.handleSearchOptions);
      }

      //create the filter drop down

    }, {
      key: 'createFilter',
      value: function createFilter(list) {
        var _this4 = this;

        //create filter button and menu
        var filterBtnClone = filterBtnTemplate.content.cloneNode(true);
        this.filterMenu = document.createElement('ul');
        this.filterMenu.setAttribute('role', 'menu');
        this.filterMenu.classList.add('menu', 'animated', 'animateIn');
        this.filterMenu.id = list.className;
        this.filterMenu.setAttribute('hidden', '');
        this.shadowRoot.insertBefore(this.filterMenu, this.errorState);

        //create list
        var listArray = Array.from(list.children);
        listArray.forEach(function (child, index) {
          var optionClone = menuItemTemplate.content.cloneNode(true);
          var optionText = optionClone.querySelector('span');
          optionText.innerText = child.innerText;
          var optionButton = optionClone.querySelector('button');
          optionButton.setAttribute('data-index', index);

          optionButton.addEventListener('click', function (event) {
            var ariaChecked = event.target.getAttribute('aria-checked');
            if (ariaChecked === 'false') {
              event.target.setAttribute('aria-checked', true);
              _this4.value.filters.push(event.target.innerText);
            } else {
              event.target.setAttribute('aria-checked', false);
              var _index = _this4.value.filters.indexOf(event.target.innerText);
              _this4.value.filters.splice(_index, 1);
            }
          });

          optionButton.addEventListener('keydown', function (e) {
            _this4.handleMenuKeyboard(e, _this4.filterMenu);
          });

          _this4.filterMenu.append(optionClone);
        });

        this.searchBar.insertBefore(filterBtnClone, this.searchBtn);

        //this is a work around for the faulty filter icon coming through the pearson-icon component and gravity css override
        var shadowFilterIcon = this.shadowRoot.querySelector('pearson-icon[icon="filter"]');
        this.shadowFilterSvg = shadowFilterIcon.shadowRoot.querySelector('svg');
        this.shadowFilterSvg.setAttribute('viewBox', '3 3 18 18');
        if (this.startdisabled === true && this.searchOptionsState === true) {
          this.shadowFilterSvg.style.fill = "#6a7070";
        } else {
          this.shadowFilterSvg.style.fill = "#252525";
        }

        //set filter button event listener
        this.filterBtn = this.shadowRoot.querySelector('.filterBtn');
        this.filterBtn.addEventListener('click', this.handleFilter);

        //set value
        this.value.filters = [];
      }

      //set the grid of the search-bar-container depending on what children are present

    }, {
      key: 'setParentGrid',
      value: function setParentGrid() {
        if (this.searchOptionsState === true && this.filterState === true) {
          this.searchBar.classList.add('both');
        } else if (this.searchOptionsState === true) {
          this.searchBar.classList.add('onlySearchOpts');
        } else if (this.filterState === true) {
          this.searchBar.classList.add('onlyFilter');
        }
      }
    }, {
      key: 'getFocusableElements',
      value: function getFocusableElements(node) {
        return node.querySelectorAll('[role^="menuitemradio"]');
      }
    }, {
      key: 'handleMenuKeyboard',
      value: function handleMenuKeyboard(e, menu) {
        var nextButton = parseInt(e.target.getAttribute('data-index')) + 1,
            prevButton = parseInt(e.target.getAttribute('data-index')) - 1;
        if (e.key === 'ArrowUp' || e.key === 'Up') {
          e.preventDefault();
          if (this.shadowRoot.activeElement === this.getFocusableElements(menu)[0]) {
            this.getFocusableElements(menu)[this.getFocusableElements(menu).length - 1].focus();
          } else {
            this.getFocusableElements(menu)[prevButton].focus();
          }
        }
        if (e.key === 'ArrowDown' || e.key === 'Down') {
          e.preventDefault();
          if (this.shadowRoot.activeElement === this.getFocusableElements(menu)[this.getFocusableElements(menu).length - 1]) {
            this.getFocusableElements(menu)[0].focus();
          } else {
            this.getFocusableElements(menu)[nextButton].focus();
          }
        }
        if (e.key === 'Home') {
          this.getFocusableElements(menu)[0].focus();
        }

        if (e.key === 'End') {
          this.getFocusableElements(menu)[this.getFocusableElements(menu).length - 1].focus();
        }
      }
    }, {
      key: 'handleSearchOptions',
      value: function handleSearchOptions(e) {
        var _this5 = this;

        e.preventDefault();
        var hidden = this.optsMenu.getAttribute('hidden');
        if (hidden === '') {
          this.optsMenu.removeAttribute('hidden');
          this.shadowRoot.addEventListener('animationend', function (event) {
            _this5.getFocusableElements(_this5.optsMenu)[0].focus();
          });
        } else {
          this.optsMenu.setAttribute('hidden', '');
        }

        if (this.filterMenu) {
          this.filterMenu.getAttribute('hidden') != '' ? this.filterMenu.setAttribute('hidden', '') : null;
        }
      }
    }, {
      key: 'handleFilter',
      value: function handleFilter(e) {
        var _this6 = this;

        e.preventDefault();
        var hidden = this.filterMenu.getAttribute('hidden');
        if (hidden === '') {
          this.filterMenu.removeAttribute('hidden');
          this.shadowRoot.addEventListener('animationend', function (event) {
            _this6.getFocusableElements(_this6.filterMenu)[0].focus();
          });
        } else {
          this.filterMenu.setAttribute('hidden', '');
        }
        if (this.optsMenu) {
          this.optsMenu.getAttribute('hidden') != '' ? this.optsMenu.setAttribute('hidden', '') : null;
        }
      }
    }, {
      key: 'handleSearch',
      value: function handleSearch(e) {
        e.preventDefault();
        this.searchOptionsState === false && this.filterState === false ? this.value = this.input.value : this.value.searchTerm = this.input.value;
        var search = new Event('search');
        this.dispatchEvent(search);
        this.closeBothMenus();
      }
    }, {
      key: 'closeBothMenus',
      value: function closeBothMenus() {
        if (this.filterMenu) {
          if (this.filterMenu.getAttribute('hidden') != '') {
            this.filterMenu.setAttribute('hidden', '');
          }
        }
        if (this.optsMenu) {
          if (this.optsMenu.getAttribute('hidden') != '') {
            this.optsMenu.setAttribute('hidden', '');
          }
        }
      }
    }]);

    return SearchBar;
  }(HTMLElement);

  customElements.define('pearson-search-bar', SearchBar);
})(window, document);