(function(w, doc) {
  'use strict';

  // build the different views
  const template = doc.createElement('template'),
    actionTemplate = doc.createElement('template'),
    hintBoxTemplate = doc.createElement('template'),
    radioTemplate = doc.createElement('template'),
    radioInputTemplate = doc.createElement('template'),
    wrongActionTemplate = doc.createElement('template'),
    correctActionTemplate = doc.createElement('template');

  correctActionTemplate.innerHTML = `
  <div class="response-container" role="alert">
    <div class="response correct">
      <h3>Correct!</h3>
        <p id="explanation"></p>
      <div class="button-bar">
        <button class="gr-btn primary next">Next Question</button>
      </div>
    </div>
  </div>
  `

  wrongActionTemplate.innerHTML = `
  <div class="response-container" role="alert"">
    <div class="response incorrect">
      <h3>In-Correct</h3>
      <p id="explanation"></p>
        <div class="button-bar try-again" id="buttonBar">
         <button class="link-btn hint-btn">Need a hint?</button>
         <button class="gr-btn primary submit">Try Again</button>
      </div>
    </div>
  </div>
  `

  radioInputTemplate.innerHTML = `
    <input name="doh" />
  `

  radioTemplate.innerHTML = `
    <pearson-radio label="Label" groupName="multipleChoice">
   
    </pearson-radio>
  `

  hintBoxTemplate.innerHTML = `
    <div class="hint-box">
        <button class="gr-btn icon-btn-18 close-hint" aria-label="Close hint">
          <pearson-icon icon="close" size="18" />
        </button>
        <div class="hint-container" role="alert">
        <h4>Hint</h4>
        <p id="prompt"> </p>
   </div>
  `

  actionTemplate.innerHTML = `
   <div class="button-bar submit-bar" id="buttonBar">
       <button class="link-btn hint-btn">Need a hint?</button>
       <button class="gr-btn primary submit" disabled>Submit</button>
    </div>
  `

  template.innerHTML = ` 
    <style>
        @import url("https://fonts.googleapis.com/css?family=Open+Sans:400,600");:host{
        /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */}:host html{line-height:1.15;-webkit-text-size-adjust:100%}:host body{margin:0}:host main{display:block}:host h1{font-size:2em;margin:.67em 0}:host hr{-webkit-box-sizing:content-box;box-sizing:content-box;height:0;overflow:visible}:host pre{font-family:monospace,monospace;font-size:1em}:host a{background-color:transparent}:host abbr[title]{border-bottom:none;text-decoration:underline;-webkit-text-decoration:underline dotted;text-decoration:underline dotted}:host b,:host strong{font-weight:bolder}:host code,:host kbd,:host samp{font-family:monospace,monospace;font-size:1em}:host small{font-size:80%}:host sub,:host sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}:host sub{bottom:-.25em}:host sup{top:-.5em}:host img{border-style:none}:host button,:host input,:host optgroup,:host select,:host textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}:host button,:host input{overflow:visible}:host button,:host select{text-transform:none}:host [type=button],:host [type=reset],:host [type=submit],:host button{-webkit-appearance:button}:host [type=button]::-moz-focus-inner,:host [type=reset]::-moz-focus-inner,:host [type=submit]::-moz-focus-inner,:host button::-moz-focus-inner{border-style:none;padding:0}:host [type=button]:-moz-focusring,:host [type=reset]:-moz-focusring,:host [type=submit]:-moz-focusring,:host button:-moz-focusring{outline:1px dotted ButtonText}:host fieldset{padding:.35em .75em .625em}:host legend{-webkit-box-sizing:border-box;box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}:host progress{vertical-align:baseline}:host textarea{overflow:auto}:host [type=checkbox],:host [type=radio]{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0}:host [type=number]::-webkit-inner-spin-button,:host [type=number]::-webkit-outer-spin-button{height:auto}:host [type=search]{-webkit-appearance:textfield;outline-offset:-2px}:host [type=search]::-webkit-search-decoration{-webkit-appearance:none}:host ::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}:host details{display:block}:host summary{display:list-item}:host template{display:none}:host html{font-size:14px}:host *,:host html{-webkit-box-sizing:border-box;box-sizing:border-box}:host body{font-family:Open Sans,Arial,Helvetica,sans-serif}:host body,:host p{font-size:14px;line-height:1.5;font-weight:400}:host strong{font-weight:600}:host a{font-size:14px;color:#047a9c}:host a:hover{color:#03536a;text-decoration:none}:host a:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host button{cursor:pointer}:host li,:host ul{font-size:14px}:host svg{fill:#252525}:host svg[focusable=false]:focus{outline:none}:host select{cursor:pointer}:host input,:host textarea{font:inherit;letter-spacing:inherit;word-spacing:inherit}:host svg{pointer-events:none}@-webkit-keyframes shift{to{background-position:9px 9px}}@keyframes shift{to{background-position:9px 9px}}@-webkit-keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@-webkit-keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@-webkit-keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@-webkit-keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@-webkit-keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@-webkit-keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@-webkit-keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}@keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}:host .fadeIn,:host .slideInDown{opacity:1!important;visibility:visible!important}:host .fadeOut,:host .slideOutDown{opacity:0;visibility:hidden}:host .slideInDown{-webkit-animation:slideInDown .3s ease-in-out 0s;animation:slideInDown .3s ease-in-out 0s}:host .slideOutDown{-webkit-animation:slideOutDown .2s ease-in 0s;animation:slideOutDown .2s ease-in 0s}:host .fadeIn{-webkit-animation:fadeIn .3s linear 0s;animation:fadeIn .3s linear 0s}:host .fadeOut{-webkit-animation:fadeOut .2s linear 0s;animation:fadeOut .2s linear 0s}:host .animateIn{-webkit-animation-name:fadeIn;animation-name:fadeIn;-webkit-animation-duration:.5s;animation-duration:.5s}:host .animateOut{-webkit-animation-name:fadeOut;animation-name:fadeOut;-webkit-animation-duration:.2s;animation-duration:.2s}@media (prefers-reduced-motion){:host .animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}}:host html[data-prefers-reduced-motion] .animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}:host .no-border{border:0}:host .icon-18{width:18px;height:18px}:host .icon-24{width:24px;height:24px}:host .hidden{display:none!important}:host pearson-alert{font-size:16px;max-width:580px}:host pearson-alert .alert-title{font-size:14px;margin:0;display:inline;top:0}:host pearson-uploader .alert-title{top:0!important}:host pearson-alert .alert-text{margin:0;display:inline}:host pearson-footer{left:50%;margin-left:-50%;right:50%;margin-right:-50%}:host pearson-header{grid-column:span 12}:host pearson-tabs{font-size:14px}:host pearson-progress-bar{grid-column:1/5}@media (min-width:591px){:host pearson-progress-bar{grid-column:1/9}}@media (min-width:887px){:host pearson-progress-bar{grid-column:1/13}}:host pearson-tabs{grid-column:1/5}@media (min-width:591px){:host pearson-tabs{grid-column:1/9}}@media (min-width:887px){:host pearson-tabs{grid-column:1/13}}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host pearson-card{-ms-grid-column-span:3}:host pearson-card,:host pearson-card[stacked]{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;width:100%}:host pearson-card[stacked]{-ms-grid-column-span:12}}:host pearson-accordion{grid-column:1/5}@media (min-width:591px){:host pearson-accordion{grid-column:1/9}}@media (min-width:887px){:host pearson-accordion{grid-column:1/13}}:host #main{max-width:1280px;margin:0 auto}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host iframe .gr-grid-container{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}}:host .gr-grid-container{display:grid;display:-ms-grid;grid-template-columns:repeat(4,1fr);grid-column-gap:16px;grid-row-gap:16px;margin:0 39.5px}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host .gr-grid-container.ie-flex{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}}@media (min-width:351px){:host .gr-grid-container{margin:0 31.5px}}@media (min-width:399px){:host .gr-grid-container{margin:0 39.5px}}@media (min-width:447px){:host .gr-grid-container{margin:0 79.5px}}@media (min-width:591px){:host .gr-grid-container{grid-template-columns:repeat(8,1fr);margin:0 83.5px}}@media (min-width:727px){:host .gr-grid-container{margin:0 103.5px;grid-column-gap:24px;grid-row-gap:24px}}@media (min-width:887px){:host .gr-grid-container{grid-template-columns:repeat(12,1fr);margin:0 71.5px}}@media (min-width:887px) and (-ms-high-contrast:active),(min-width:887px) and (-ms-high-contrast:none){:host .gr-grid-container>*{margin-right:12px;margin-left:12px;margin-bottom:24px}}@media (min-width:983px){:host .gr-grid-container{margin:0 71.5px}}@media (min-width:1079px){:host .gr-grid-container{margin:0 71.5px}}@media (min-width:1175px){:host .gr-grid-container{margin:0 76px}}:host .gr-grid-container .gr-col-two{grid-column-start:2}:host .gr-grid-container .gr-col-three{grid-column-start:3}:host .gr-grid-container .gr-col-four{grid-column-start:4}:host .gr-grid-container .gr-col-five{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-five{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:5}}:host .gr-grid-container .gr-col-six{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-six{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:6}}:host .gr-grid-container .gr-col-seven{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-seven{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:7}}:host .gr-grid-container .gr-col-eight{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-eight{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:8}}:host .gr-grid-container .gr-col-nine{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-nine{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:9}}:host .gr-grid-container .gr-col-ten{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-ten{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:10}}:host .gr-grid-container .gr-col-eleven{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-eleven{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:11}}:host .gr-grid-container .gr-col-twelve{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-twelve{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:12}}:host .gr-col-span-one{grid-column-end:span 1}:host .gr-col-span-two{grid-column-end:span 2}:host .gr-col-span-three{grid-column-end:span 3}:host .gr-col-span-four{grid-column-end:span 4}@media (min-width:591px){:host .gr-col-span-five{grid-column-end:span 5}}@media (min-width:591px){:host .gr-col-span-six{grid-column-end:span 6}}@media (min-width:591px){:host .gr-col-span-seven{grid-column-end:span 7}}@media (min-width:591px){:host .gr-col-span-eight{grid-column-end:span 8}}@media (min-width:887px){:host .gr-col-span-nine{grid-column-end:span 9}}@media (min-width:887px){:host .gr-col-span-ten{grid-column-end:span 10}}@media (min-width:887px){:host .gr-col-span-eleven{grid-column-end:span 11}}:host .gr-col-span-twelve{grid-column-end:span 4}@media (min-width:591px){:host .gr-col-span-twelve{grid-column-end:span 8}}@media (min-width:887px){:host .gr-col-span-twelve{grid-column-end:span 12}}:host .gr-row-one{grid-row-start:1}:host .gr-row-two{grid-row-start:2}:host .gr-row-three{grid-row-start:3}:host .gr-row-four{grid-row-start:4}:host .gr-row-five{grid-row-start:5}:host .gr-row-six{grid-row-start:6}:host .gr-row-seven{grid-row-start:7}:host .gr-row-eight{grid-row-start:8}:host .gr-row-nine{grid-row-start:9}:host .gr-row-ten{grid-row-start:10}:host .gr-row-eleven{grid-row-start:11}:host .gr-row-twelve{grid-row-start:12}:host .gr-row-thirteen{grid-row-start:13}:host .gr-row-fourteen{grid-row-start:14}:host .gr-row-fifteen{grid-row-start:15}:host .gr-row-span-two{grid-row-end:span 2}:host .gr-row-span-three{grid-row-end:span 3}:host .gr-row-span-four{grid-row-end:span 4}:host .gr-row-span-five{grid-row-end:span 5}:host .gr-row-span-six{grid-row-end:span 6}:host .gr-row-span-seven{grid-row-end:span 7}:host .gr-row-span-eight{grid-row-end:span 8}:host .gr-row-span-nine{grid-row-end:span 9}:host .gr-row-span-ten{grid-row-end:span 10}:host .gr-row-span-eleven{grid-row-end:span 11}:host .gr-row-span-twelve{grid-row-end:span 12}:host .gr-primary{color:#047a9c;fill:#047a9c}:host .gr-secondary{color:#ffb81c;fill:#ffb81c}:host .gr-white{color:#fff;fill:#fff}:host .gr-neutral-high-one{color:#252525;fill:#252525}:host .gr-neutral-high-two{color:#6a7070;fill:#6a7070}:host .gr-neutral-med-one{color:#a9a9a9;fill:#a9a9a9}:host .gr-neutral-med-two{color:#c7c7c7;fill:#c7c7c7}:host .gr-neutral-med-three{color:#d9d9d9;fill:#d9d9d9}:host .gr-neutral-med-four{color:#e9e9e9;fill:#e9e9e9}:host .gr-neutral-light-one,:host .gr-neutral-light-two{color:#eee;fill:#eee}:host .gr-condition-one{color:#db0020;fill:#db0020}:host .gr-condition-two{color:#038238;fill:#038238}:host .gr-condition-three{color:#da0474;fill:#da0474}:host .gr-theme-one-light{color:#caefee;fill:#caefee}:host .gr-theme-one-med{color:#76d5d4;fill:#76d5d4}:host .gr-theme-one-dark{color:#19a5a3}:host .gr-theme-two-light{color:#f2e5f1;fill:#f2e5f1}:host .gr-theme-two-med{color:#895b9a;fill:#895b9a}:host .gr-theme-two-dark{color:#633673;fill:#633673}:host .gr-theme-three-light{color:#f6f8cc;fill:#f6f8cc}:host .gr-theme-three-med{color:#d2db0e;fill:#d2db0e}:host .gr-theme-three-dark{color:#b0b718;fill:#b0b718}:host .gr-theme-four-light{color:#d9e6f1;fill:#d9e6f1}:host .gr-theme-four-med{color:#356286;fill:#356286}:host .gr-theme-four-dark{color:#1e496c;fill:#356286}:host .gr-theme-five-light{color:#dff5d5;fill:#dff5d5}:host .gr-theme-five-med{color:#66be3e;fill:#66be3e}:host .gr-theme-five-dark{color:#288500;fill:#288500}:host .gr-theme-six-light{color:#d6ecf4;fill:#d6ecf4}:host .gr-theme-six-med{color:#80c5dd;fill:#80c5dd}:host .gr-theme-six-dark{color:#46a9cb;fill:#46a9cb}:host .gr-theme-seven-light{color:#faebc3;fill:#faebc3}:host .gr-theme-seven-med{color:#f5c54c;fill:#f5c54c}:host .gr-theme-seven-dark{color:#dea30d;fill:#dea30d}:host .gr-primary-bg{background-color:#047a9c}:host .gr-secondary-bg{background-color:#ffb81c}:host .gr-white-bg{background-color:#fff}:host .gr-neutral-high-one-bg{background-color:#252525}:host .gr-neutral-high-two-bg{background-color:#6a7070}:host .gr-neutral-med-one-bg{background-color:#a9a9a9}:host .gr-neutral-med-two-bg{background-color:#c7c7c7}:host .gr-neutral-med-three-bg{background-color:#d9d9d9}:host .gr-neutral-med-four-bg{background-color:#e9e9e9}:host .gr-neutral-light-one-bg,:host .gr-neutral-light-two-bg{background-color:#eee}:host .gr-condition-one-bg{background-color:#db0020}:host .gr-condition-two-bg{background-color:#038238}:host .gr-condition-three-bg{background-color:#da0474}:host .gr-theme-one-light-bg{background-color:#caefee}:host .gr-theme-one-med-bg{background-color:#76d5d4}:host .gr-theme-one-dark-bg{background-color:#19a5a3}:host .gr-theme-two-light-bg{background-color:#f2e5f1}:host .gr-theme-two-med-bg{background-color:#895b9a}:host .gr-theme-two-dark-bg{background-color:#633673}:host .gr-theme-three-light-bg{background-color:#f6f8cc}:host .gr-theme-three-med-bg{background-color:#d2db0e}:host .gr-theme-three-dark-bg{background-color:#b0b718}:host .gr-theme-four-light-bg{background-color:#d9e6f1}:host .gr-theme-four-med-bg{background-color:#356286}:host .gr-theme-four-dark-bg{background-color:#1e496c}:host .gr-theme-five-light-bg{background-color:#dff5d5}:host .gr-theme-five-med-bg{background-color:#66be3e}:host .gr-theme-five-dark-bg{background-color:#288500}:host .gr-theme-six-light-bg{background-color:#d6ecf4}:host .gr-theme-six-med-bg{background-color:#80c5dd}:host .gr-theme-six-dark-bg{background-color:#46a9cb}:host .gr-theme-seven-light-bg{background-color:#faebc3}:host .gr-theme-seven-med-bg{background-color:#f5c54c}:host .gr-theme-seven-dark-bg{background-color:#dea30d}:host .gr-h1{font-size:24px;line-height:28px}:host .gr-h1,:host .gr-h2{font-weight:400;margin-top:0}:host .gr-h2{font-size:20px;line-height:26px}:host .gr-h3{font-size:18px;line-height:24px;font-weight:400;margin-top:0}:host .gr-label{font-size:12px;line-height:16px;color:#6a7070;display:block;margin-bottom:4px}:host .gr-meta{font-size:12px;line-height:12px;color:#6a7070}:host .gr-semi-bold{font-weight:600}:host .gr-font-large{font-size:16px;line-height:24px}:host .gr-font-normal{font-size:14px;line-height:20px}:host .gr-btn{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;border:1px solid #6a7070;background:transparent;color:#6a7070;min-width:152px;padding:8px 24px;font-size:16px;cursor:pointer;border-radius:22px;position:relative;margin:12px;line-height:1.15}:host .gr-btn:hover{color:#252525;border:1px solid #252525}:host .gr-btn:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host a.gr-btn{text-decoration:none}:host .gr-btn.primary{background-color:#047a9c;border:1px solid #047a9c;color:#fff}:host .gr-btn.primary:hover{color:#fff;background-color:#035f79}:host .gr-btn.attention{background-color:#ffb81c;border:1px solid #ffb81c;color:#252525}:host .gr-btn.attention:hover{background-color:#f7aa00}:host .gr-btn.small{min-width:128px;padding:7px 20px;font-size:14px}:host .gr-btn.small:focus:after{padding:18px 21px}:host .gr-btn.extra-large{min-width:152px;padding:10px 24px;font-size:16px}:host .gr-btn.extra-large:focus:after{padding:22px 21px;border-radius:24px}:host .gr-btn:disabled{background-color:#e9e9e9!important;color:#6a7070!important;border:0!important;cursor:default!important}:host .gr-btn.icon-btn-24{min-width:auto;padding:5px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-24:focus{border-radius:4px}:host .gr-btn.icon-btn-24:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-24:active svg{fill:#005a70}:host .gr-btn.icon-btn-24 svg{fill:#6a7070}:host .gr-btn.icon-btn-18{min-width:auto;padding:8px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-18:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-18:active svg{fill:#005a70}:host .gr-btn.icon-btn-18:focus{border-radius:4px}:host .gr-btn.icon-btn-18 svg{fill:#6a7070}:host .gr-btn.no-border,:host .gr-btn.no-border:hover{border:0}:host .gr-btn.no-border:focus:after{border-radius:5px;min-width:auto;left:auto;top:auto;padding:16px}:host .gr-input{display:block;margin:4px 0;padding:0 12px;height:36px;color:#252525;border:1px solid #c7c7c7;background:#fff;border-radius:4px;font-size:14px;width:100%;max-width:100%}:host .gr-input:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-input:disabled{color:#c7c7c7;background-color:#e9e9e9}:host .gr-input:-moz-read-only{border:0}:host .gr-input:read-only{border:0}:host .gr-checkbox+.error-state,:host .gr-input+.error-state,:host .gr-radio+.error-state,:host .gr-select-container+.error-state,:host .gr-textarea+.error-state{display:none}:host .gr-checkbox.error,:host .gr-input.error,:host .gr-radio.error,:host .gr-select-container.error,:host .gr-textarea.error{border-color:#db0020}:host .gr-checkbox.error,:host .gr-checkbox.error+.error-state,:host .gr-input.error,:host .gr-input.error+.error-state,:host .gr-radio.error,:host .gr-radio.error+.error-state,:host .gr-select-container.error,:host .gr-select-container.error+.error-state,:host .gr-textarea.error,:host .gr-textarea.error+.error-state{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .gr-checkbox.error+.error-state svg,:host .gr-input.error+.error-state svg,:host .gr-radio.error+.error-state svg,:host .gr-select-container.error+.error-state svg,:host .gr-textarea.error+.error-state svg{fill:#db0020}:host .gr-checkbox.error+.error-state span,:host .gr-input.error+.error-state span,:host .gr-radio.error+.error-state span,:host .gr-select-container.error+.error-state span,:host .gr-textarea.error+.error-state span{margin-left:8px;color:#db0020}:host .gr-textarea{display:block;margin:4px 0;padding:12px;color:#252525;border:1px solid #c7c7c7;background:#fff;border-radius:4px;font-size:14px;width:100%;max-width:100%;line-height:1.5}:host .gr-textarea:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-textarea:disabled{color:#c7c7c7;background-color:#e9e9e9}:host .gr-textarea:-moz-read-only{border:0}:host .gr-textarea:read-only{border:0}:host .gr-checkbox{margin-bottom:14px;min-height:16px;position:relative;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .gr-checkbox input[type=checkbox]{opacity:0;position:absolute;cursor:pointer}:host .gr-checkbox input[type=checkbox]~label{display:inline-block;line-height:1.5;min-height:24px;padding-left:2.5em;position:relative;z-index:2;cursor:pointer;font-size:14px}:host .gr-checkbox input[type=checkbox]~span{background:#fff}:host .gr-checkbox input[type=checkbox]:disabled~span,:host .gr-checkbox input[type=checkbox]~span{height:22px;line-height:1.5;text-align:center;width:22px;border:2px solid #c7c7c7;border-radius:2px;left:0;position:absolute;top:0;z-index:0}:host .gr-checkbox input[type=checkbox]:disabled~span{background-color:#e9e9e9}:host .gr-checkbox input[type=checkbox]:focus~span{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-checkbox input[type=checkbox]~span svg{height:24px;opacity:0;width:24px;top:-3px;position:relative;left:-3px;fill:#047a9c}:host .gr-checkbox input[type=checkbox]:disabled~span svg{fill:#c7c7c7}:host .gr-fieldset{border:none;padding:0}:host .gr-fieldset legend{margin-bottom:8px}:host .gr-radio{margin-bottom:14px;min-height:16px;position:relative;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .gr-radio input[type=radio]{opacity:0;position:absolute}:host .gr-radio input[type=radio]:focus~span{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-radio input[type=radio]+label{display:inline-block;line-height:18px;padding-left:28px;cursor:pointer}:host .gr-radio input[type=radio]~span{-webkit-box-sizing:content-box;border:2px solid #c7c7c7;background:#fff;border-radius:50%;box-sizing:content-box;color:#6a7070;display:block;height:5px;left:0;padding:3px 6px 6px 3px;pointer-events:none;position:absolute;top:0;width:5px}:host .gr-radio input[type=radio]~span svg{height:18px;opacity:0;width:18px}:host .gr-radio input[type=radio]:checked~span svg{top:-5px;position:relative;left:-5px;fill:#047a9c}:host .gr-radio input[type=radio]:disabled~span svg{opacity:1;fill:#c7c7c7;top:-5px;left:-5px;position:relative}:host .gr-select-container{position:relative}:host .gr-select-container svg{position:absolute;right:12px;top:-webkit-calc(50% - 9px);top:calc(50% - 9px);fill:#6a7070}:host .gr-select{-webkit-appearance:none;-moz-appearance:none;display:block;margin:4px 0;padding:0 42px 0 12px;height:36px;color:#252525;border:1px solid #c7c7c7;background:#fff;border-radius:4px;font-size:14px;width:100%;max-width:100%}:host .gr-select:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-select:disabled{color:#c7c7c7;background-color:#e9e9e9}:host .gr-select:disabled+svg{fill:#c7c7c7}:host .gr-select[multiple]{height:auto}:host .gr-select[multiple] option{cursor:pointer}:host .margin-container{max-width:1280px;margin:0 auto}:host .header-container.light{width:100%;background-color:#f5f5f5}:host .page-frame{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;grid-column:span 4}@media (min-width:591px){:host .page-frame{grid-column:span 8}}@media (min-width:887px){:host .page-frame{grid-column:span 12}}:host .page-frame h1{margin:0;padding:32px 0 0}:host pearson-tabs .back-bar{width:100%;grid-column:span 12;border-top:1px solid #c7c7c7;border-bottom:1px solid #c7c7c7;top:-22px;position:relative}:host .page-frame-simple{grid-column:span 4;height:70px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:baseline;-webkit-align-items:baseline;-ms-flex-align:baseline;align-items:baseline;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between}@media (min-width:591px){:host .page-frame-simple{grid-column:span 8}}@media (min-width:887px){:host .page-frame-simple{grid-column:span 12}}:host .page-frame-simple .actions{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .page-frame-simple .actions button:not(#more){display:none}@media (min-width:887px){:host .page-frame-simple .actions button:not(#more){display:inline-block}}:host .page-frame-simple .actions #showMore{display:inline-block}@media (min-width:887px){:host .page-frame-simple .actions #showMore{display:none}}:host .page-frame-simple .actions #showMore button{margin:0;min-width:auto;padding:6px;border:0}:host .page-frame-simple .actions #showMore button svg{margin:0}:host .page-frame-simple .actions .gr-dropdown-container .menu{right:0;max-height:auto;overflow:hidden;width:auto;top:16px;border:1px solid #c7c7c7;height:auto}:host .page-frame-simple .actions .dropdown-menu{width:auto;right:0}:host .page-frame-simple .actions .dropdown-menu button{display:-webkit-inline-box!important;display:-webkit-inline-flex!important;display:-ms-inline-flexbox!important;display:inline-flex!important;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .page-frame-simple h2{margin:0;padding-left:0}@media (min-width:887px){:host .page-frame-simple h2{padding-left:145px}}:host .gr-table-container .gr-table{border-collapse:collapse;width:100%;border-bottom:1px solid #c7c7c7}:host .gr-table-container .gr-table th{font-weight:400;padding:16px 20px;text-align:left}:host .gr-table-container .gr-table td{padding:16px 20px;text-align:left}:host .gr-table-container .gr-table thead{border-top:1px solid #c7c7c7;border-bottom:1px solid #c7c7c7}:host .gr-table-container .gr-table tbody th{font-weight:400}:host .gr-table-container.hoverable tbody tr,:host .gr-table-container.selectable tbody tr{cursor:pointer}:host .gr-table-container.hoverable tbody tr:hover,:host .gr-table-container.selectable tbody tr:hover{background-color:#e9e9e9;border-top:1px solid #047a9c;border-bottom:1px solid #047a9c}:host .gr-table-container.hoverable tbody tr.selected,:host .gr-table-container.selectable tbody tr.selected{border-top:1px solid #19a5a3;border-bottom:1px solid #19a5a3;background-color:#caefee}:host html>*{font-family:Open Sans,Arial,Helvetica,sans-serif}:host [hidden]{display:none}:host .finished-quiz-buttons{display:grid;grid-template-columns:repeat(2,1fr)}:host .finished-quiz-buttons p{grid-column-start:1;grid-column-end:3;margin:20px;padding:10px 0;border-top:1px solid #ccc}:host .finished-quiz-buttons button{margin-left:20px}:host .finished-quiz-buttons button:last-child{margin-left:12px;margin-right:20px}:host .gr-btn.selfCheckBtn{position:absolute;bottom:-88px;left:-webkit-calc(50% - 85.5px);left:calc(50% - 85.5px);-webkit-filter:drop-shadow(rgba(0,0,0,.7) 1px 2px 2px);filter:drop-shadow(rgba(0,0,0,.7) 1px 2px 2px);-webkit-transition:bottom .3s ease-in;transition:bottom .3s ease-in;z-index:2}:host .gr-btn.selfCheckBtn.show{bottom:88px}:host .gr-btn.selfCheckBtn:focus{background-color:#f7aa00}:host .selfCheck-container{position:absolute;max-width:500px;width:100%;min-width:320px;height:-webkit-calc(100% - 80px);height:calc(100% - 80px);margin-top:80px;top:0;left:0;z-index:1101;background:#fff;-webkit-box-shadow:0 0 5px 0 #c7c7c7;box-shadow:0 0 5px 0 #c7c7c7;-webkit-animation-fill-mode:forwards;animation-fill-mode:forwards;-webkit-animation-duration:.6s;animation-duration:.6s}:host .bio-interactive section.assessment{overflow:auto}:host .assessment{font-size:14px;position:relative;height:100%}:host .question-container button{font-size:14px}:host .question-header{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;padding-left:24px;padding-top:4px;padding-right:4px;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap}:host .question-header h2{margin-bottom:8px}:host .question-header .questionNav{width:100%;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;margin:0 0 24px -5px}:host .question-header .questionNav .number{display:inline;font-size:12px;font-weight:400;margin:0 8px}:host .question-header .questionNav button{padding:0;height:18px;width:18px}:host .gr-radio input[type=radio]+label{font-size:14px;line-height:1.5}:host .gr-radio input[type=radio]~span{margin-top:3px}:host .gr-radio input[type=radio]:disabled~span svg{opacity:0}:host .gr-radio input[type=radio]:checked~span svg{opacity:1}:host .gr-radio input[type=radio]:checked~span svg.icon-correct,:host .gr-radio input[type=radio]:checked~span svg.icon-incorrect{opacity:0}:host .gr-radio input[type=radio]:checked:disabled~span svg{display:none}:host .gr-radio input[type=radio]:checked:disabled.correct~span svg.icon-correct{display:block;fill:#038238;opacity:1}:host .gr-radio input[type=radio]:checked:disabled.incorrect~span svg.icon-incorrect{fill:#db0020;opacity:1;display:block}:host .gr-checkbox input[type=checkbox]+label{font-size:14px}:host .gr-checkbox input[type=checkbox]:disabled~span svg{opacity:0}:host .gr-checkbox input[type=checkbox]:checked~span svg{opacity:1}:host .gr-checkbox input[type=checkbox]:checked~span svg.icon-correct,:host .gr-checkbox input[type=checkbox]:checked~span svg.icon-incorrect{opacity:0}:host .gr-checkbox input[type=checkbox]:checked:disabled~span svg{display:none}:host .gr-checkbox input[type=checkbox]:checked:disabled.correct~span svg.icon-correct{display:block;fill:#038238;opacity:1}:host .gr-checkbox input[type=checkbox]:checked:disabled.incorrect~span svg.icon-incorrect{fill:#db0020;opacity:1;display:block}:host xpso-selfcheck[standalone] .assessment{height:auto;margin-top:20px;padding:4px}:host xpso-selfcheck[standalone] .assessment .button-bar,:host xpso-selfcheck[standalone] .assessment .question-container{padding:0}:host .hint-box{position:relative;margin:0 20px}:host .hint-box .close-hint{position:absolute;top:0;right:0}:host .hint-container{border:1px solid #c7c7c7;padding:12px;background:rgba(0,0,0,.05);text-align:left!important;margin-bottom:40px}:host .hint-container h3,:host .hint-container h4{margin-top:0;margin-bottom:8px;font-size:16px}:host .hint-container p{margin-top:0}:host fieldset{border:0;padding:0;margin:0}:host .question-container{padding:8px 24px}:host .assessment fieldset legend{margin-bottom:24px}:host .assessment .drag-drop-containers{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:start;-webkit-align-items:flex-start;-ms-flex-align:start;align-items:flex-start;-webkit-box-orient:vertical;-webkit-box-direction:normal;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;width:100%}:host .assessment .drag-drop-containers h2{margin:0;font-size:16px}:host .assessment .drag-drop-containers legend{font-size:16px;margin-bottom:12px}:host .drag-drop-containers .drag-container{width:100%;margin-bottom:24px}:host .drag-drop-containers .drag-container legend{margin-bottom:12px}:host .drag-drop-containers .drag-buttons{border:1px solid #e9e9e9;border-radius:4px;background:#f5f5f5;min-height:100px;padding:12px}:host .drag-drop-containers .drag-buttons button{margin:8px 0;background:#fff;padding:8px 12px;border-radius:8px}:host .drag-drop-containers .drag-buttons button:disabled{border-width:1px!important;border-style:solid!important;background:#fff!important}:host .drag-drop-containers .drop-buttons{width:100%}:host .drag-drop-containers .drop-buttons ol{list-style:none;margin:0;padding:0}:host .drag-drop-containers .drop-buttons li{display:inline-block;position:relative;font-size:14px;margin:12px 0;padding:8px 16px;border:1px dashed #6a7070;background:#f5f5f5;border-radius:2px;width:100%;line-height:1.15}:host .drag-drop-containers .drop-buttons li:first-of-type{margin-top:0}:host .drag-drop-containers .drop-buttons li .answer{margin-top:4px;padding:8px 12px;border-radius:8px}:host .drag-drop-containers .drop-buttons li button:not(.answer){margin-bottom:8px}:host .drag-drop-containers .drop-buttons li span{display:inline-block;padding:8px 0;color:#6a7070;margin:0}:host .drag-drop-containers .drop-buttons li button{left:0;top:0;margin:0;background:#fff;width:100%;text-align:center;border-radius:22px}:host .drag-drop-containers .drop-buttons li button.incorrect,:host .drag-drop-containers .drop-buttons li button.incorrect:disabled{color:#db0020!important;border:1px solid #db0020!important;background:#fff!important}:host .drag-drop-containers .drop-buttons li button.incorrect:before,:host .drag-drop-containers .drop-buttons li button.incorrect:disabled:before{content:url("data:image/svg+xml; utf8, <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' width='24px' height='24px'><path fill='%23DB0020' d='M12,24 C5.372583,24 0,18.627417 0,12 C0,5.372583 5.372583,0 12,0 C18.627417,0 24,5.372583 24,12 C24,18.627417 18.627417,24 12,24 Z M12,10.5857864 L8.70710678,7.29289322 C8.31658249,6.90236893 7.68341751,6.90236893 7.29289322,7.29289322 C6.90236893,7.68341751 6.90236893,8.31658249 7.29289322,8.70710678 L10.5857864,12 L7.29248537,15.2933011 C6.90196108,15.6838254 6.90196108,16.3169903 7.29248537,16.7075146 C7.68300967,17.0980389 8.31617464,17.0980389 8.70669894,16.7075146 L12,13.4142136 L15.2928932,16.7071068 C15.6834175,17.0976311 16.3165825,17.0976311 16.7071068,16.7071068 C17.0976311,16.3165825 17.0976311,15.6834175 16.7071068,15.2928932 L13.4142136,12 L16.7071068,8.70710678 C17.0976311,8.31658249 17.0976311,7.68341751 16.7071068,7.29289322 C16.3165825,6.90236893 15.6834175,6.90236893 15.2928932,7.29289322 L12,10.5857864 Z'></path></svg>");display:block;color:#db0020;width:24px;height:24px;position:absolute;left:-12px;top:-6px;background-color:#fff;border-radius:50%}:host .drag-drop-containers .drop-buttons li button.correct,:host .drag-drop-containers .drop-buttons li button.correct:disabled{background-color:#fff!important;color:#038238!important;border:1px solid #038238!important;background:#fff!important}:host .drag-drop-containers .drop-buttons li button.correct:before,:host .drag-drop-containers .drop-buttons li button.correct:disabled:before{content:url("data:image/svg+xml; utf8, <svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' width='24px' height='24px'><path fill='%23038238' d='M12,24 C5.372583,24 0,18.627417 0,12 C0,5.372583 5.372583,0 12,0 C18.627417,0 24,5.372583 24,12 C24,18.627417 18.627417,24 12,24 Z M11.0412736,17.2562587 L17.5667013,7.93421914 C17.8834158,7.48176985 17.773381,6.85823939 17.3209317,6.54152488 C16.8684824,6.22481037 16.2449519,6.33484516 15.9282374,6.78729446 L10.095704,15.1194851 L8.01169103,12.9052221 C7.6331745,12.5030484 7.00030003,12.4838704 6.59812633,12.862387 C6.19595264,13.2409035 6.17677472,13.873778 6.55529125,14.2759517 L9.48179897,17.3853652 C9.78426112,17.7067311 10.2491351,17.7835478 10.6289337,17.6083156 C10.7601737,17.5774562 10.8824713,17.4774636 11.0139388,17.2949987 C11.0233905,17.2822589 11.0325022,17.2693413 11.0412736,17.2562587 Z'></path></svg>");color:#038238;display:block;width:24px;height:24px;position:absolute;left:-12px;top:-6px;background-color:#fff;border-radius:50%}:host xpso-selfcheck{display:block;height:100%;overflow-y:auto}:host .assessment{display:block}:host .assessment .response{padding:12px 24px 0;margin:0;width:100%}:host .assessment .response h3{margin:0 0 8px}:host .assessment .response p{margin:4px 0}:host .assessment .response .button-bar{-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap}:host .assessment .response .button-bar button{margin:6px}:host .assessment .response.correct .explanation,:host .assessment .response.correct .try-again,:host .assessment .response.incorrect .next-question{display:none}@media (max-width:414px) and (orientation:portrait){:host .selfCheck-container{margin-bottom:0;height:-webkit-calc(100% - 80px);height:calc(100% - 80px)}:host xpso-selfcheck{display:block}:host .drag-drop-containers .drag-buttons button{padding:8px 12px;margin:6px 0}:host .drag-drop-containers .drop-buttons li{padding:8px 16px;margin:8px 0}:host .drag-drop-containers .drop-buttons li .answer{margin-top:4px;padding:8px 12px;border-radius:8px}}@media (min-width:521px){:host .selfCheck-container{height:-webkit-calc(100vh - 80px);height:calc(100vh - 80px);top:80px;bottom:0;left:0;margin:0;z-index:1101}:host .assessment .drag-drop-containers{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:start;-webkit-align-items:flex-start;-ms-flex-align:start;align-items:flex-start;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row}:host .assessment .drag-drop-containers h2{margin:0;font-size:16px}:host .drag-drop-containers .drag-container{width:50%;padding-right:24px;margin-bottom:0}:host .drag-drop-containers .drag-container h2{margin-bottom:12px}:host .drag-drop-containers .drop-buttons{width:50%}}:host h3.incorrect{color:#db0020}:host h3.correct{color:#038238}:host .assessment .button-bar{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-wrap:wrap!important;-ms-flex-wrap:wrap!important;flex-wrap:wrap!important;-webkit-box-orient:horizontal;-webkit-box-direction:normal;-webkit-flex-flow:row;-ms-flex-flow:row;flex-flow:row;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;width:100%;margin-top:16px;padding:0 24px 24px;text-align:center}:host .assessment .button-bar div{text-align:center}:host .link-btn{border:none;background:transparent;font-size:14px;color:#047a9c;text-decoration:underline}:host .link-btn:disabled{color:#505759;cursor:default}:host .link-btn:disabled:focus,:host .link-btn:disabled:hover{text-decoration:underline;color:#505759}:host .link-btn:hover{color:#03536a;text-decoration:none}:host .link-btn:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}@-webkit-keyframes slideInLeft{0%{left:-520px}to{left:0;visibility:visible}}@keyframes slideInLeft{0%{left:-520px}to{left:0;visibility:visible}}@-webkit-keyframes slideOutLeft{0%{left:0}to{left:-520px}}@keyframes slideOutLeft{0%{left:0}to{left:-520px}}:host .slideInLeft{-webkit-animation-name:slideInLeft;animation-name:slideInLeft}:host .slideOutLeft{-webkit-animation-name:slideOutLeft;animation-name:slideOutLeft}@media (prefers-reduced-motion){:host .selfCheck-container{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}}   
     </style>
    <section class="assessment">
      <div class="question-header">
         <h2>Self-check</h2>
         <div id="question-nav-06160489173258643" class="questionNav">
              <pearson-pagination text="Question" compact> </pearson-pagination>
         </div>
      </div>
      <div class="question-container">
        <!-- Inject Question Template -->
      </div>
        <!-- Inject Button Bar Template -->
        <div id="actionContainer"></div>
        <!-- Inject Response Template -->
        <div id="responseContainer"></div>
        <!-- Inject HintBox Template -->
        <div id="hintBoxContainer"></div>
   </section>

`;

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate([template, actionTemplate, hintBoxTemplate, radioTemplate], 'pearson-quiz');

  /** Any helper functions that do not need to be part of the class
   * can be declared here, before the class is defined.
   */


  class Quiz extends HTMLElement {
    static get observedAttributes() {
      return ['showhint', 'question', 'id', 'status', 'selected', 'match'];
    }

    get match() {
      return this.getAttribute('match');
    }

    get selected() {
      return this.getAttribute('selected')
    }

    get status() {
      return this.getAttribute('status');
    }

    get id () {
      return this.getAttribute('id');
    }

    get question () {
      return parseInt(this.getAttribute('question'))
    }

    get showHint() {
      return this.hasAttribute('showhint')
    }

    set match (number) {
      this.setAttribute('match', number);
    }

    set selected(number) {
      this.setAttribute('selected', number)
    }

    set status(value) {
      this.setAttribute('status', value)
    }

    set id (value) {
        this.setAttribute('id', value)
    }

    set question (number) {
      this.setAttribute('question', number)
    }

    set showHint(bool) {
      if (bool === true) {
        this.setAttribute('showhint', true)
      } else {
        this.removeAttribute('showhint')
      }
    }

    constructor() {
      super();
      this.attachShadow({ mode: 'open' });

      const clone = template.content.cloneNode(true);
      this.pagination = clone.querySelector('pearson-pagination');
      this.actionContainer = clone.querySelector('#actionContainer');
      this.hintBoxContainer = clone.querySelector('#hintBoxContainer');
      this.questionContainer = clone.querySelector('.question-container');

      this.shadowRoot.appendChild(clone);

      this.renderMultipleChoice = this.renderMultipleChoice.bind(this)
      this.toggleHint = this.toggleHint.bind(this)
      this.renderCorrectActionBar = this.renderCorrectActionBar.bind(this)
      this.selfCheck = this.selfCheck.bind(this)
      this.tryAgain = this.tryAgain.bind(this)
      this.renderQuestions = this.renderQuestions.bind(this)
    }

    // render question based on type
    renderQuestions () {
      if (this.type === 'MC') {
        this.answers.MultipleChoice.map((answer, index) => {
          if (answer.IsCorrect) {
            this.match = index;
          }
        })
        this.renderMultipleChoice(this.answers.MultipleChoice);
      }
    }

    // reset the UI
    tryAgain () {
      this.status = 'new'
      this.showHint = false
    }

    // checks the answer to see if its correct
    selfCheck () {
      if (this.type === 'MC') {
        if (this.match !== this.selected) {
          this.status = 'incorrect'
        } else {
          this.status = 'correct'
        }
      }
      this.showHint = false
    }

    // handles the rendering and styling of multiple choice questions
    renderMultipleChoice (answers) {
      // resets the UI
      if (this.status === 'new') {
        const radios = document.createElement('pearson-radio');
        radios.labeltext = this.data.question.questionText;
        radios.setAttribute('groupname', 'multiple');

        answers.map((answer,index) => {
          const input = document.createElement('input');
          input.name = answer.Text;
          input.setAttribute('data-index', index)
          radios.appendChild(input)
        })

        radios.addEventListener('input', event => {
          this.submitBtn.disabled = false
          this.selected = event.target.data
        })

        if (this.shadowRoot.querySelector('pearson-radio')) {
          this.shadowRoot.querySelector('pearson-radio').remove();
        }

        this.shadowRoot.querySelector('.question-container').appendChild(radios)

      }
      // displays an incorrect UI
      if (this.status === 'incorrect') {
        const existingRadios = this.shadowRoot.querySelector('pearson-radio'),
          existingRadio = existingRadios.shadowRoot.querySelectorAll('input'),
          explanation = this.shadowRoot.querySelector('#explanation');

        explanation.innerHTML = this.answers.MultipleChoice[parseInt(this.selected)].ExplanationText;
        existingRadio.forEach((radio,index) => {
          if (parseInt(this.selected) !== index) {
            radio.disabled = true
          } else {
            const svg = radio.nextElementSibling.nextElementSibling.querySelector('svg')
            console.log(svg)
            svg.style.fill = 'red'
          }
        })
      }
      // displays a correct ui
      if (this.status === 'correct') {
        const existingRadios = this.shadowRoot.querySelector('pearson-radio'),
          existingRadio = existingRadios.shadowRoot.querySelectorAll('input'),
          explanation = this.shadowRoot.querySelector('#explanation');

        explanation.innerHTML = this.data.question.explanation
        existingRadio.forEach((radio,index) => {
          if (parseInt(this.selected) === index) {
            const svg = radio.nextElementSibling.nextElementSibling.querySelector('svg')
            svg.style.fill = 'green'
            radio.disabled = true
          } else {
            const svg = radio.nextElementSibling.nextElementSibling.querySelector('svg')
            svg.style.fill = 'red'
            radio.disabled = true
          }
        })
      }
    }

    // toggle hint button
    toggleHint (event) {
      event.stopImmediatePropagation();
      this.showHint = !this.showHint
    }

    // renders the different action bars
    renderCorrectActionBar () {
      this.correctClone = correctActionTemplate.content.cloneNode(true);
      this.explanation = this.correctClone.querySelector('#explanation');
      this.buttonBar =  this.correctClone.querySelector('.response-container');
      this.submitBtn =  this.correctClone.querySelector('.submit');

      this.explanation.innerHTML = 'get explanation data'
      this.actionContainer.appendChild( this.correctClone);
    }

    renderWrongActionBar () {
      this.wrongClone = wrongActionTemplate.content.cloneNode(true);
      this.buttonBar = this.wrongClone.querySelector('.response-container');
      this.hintBtn = this.wrongClone.querySelector('.hint-btn');
      this.submitBtn = this.wrongClone.querySelector('.try-again');
      this.actionContainer.appendChild(this.wrongClone);

      this.hintBtn.addEventListener('click', this.toggleHint)
      this.submitBtn.addEventListener('click', this.tryAgain)
    }

    renderNewActionBar () {
      this.actionClone = actionTemplate.content.cloneNode(true);
      this.buttonBar = this.actionClone.querySelector('.button-bar');
      this.hintBtn = this.actionClone.querySelector('.hint-btn');
      this.submitBtn = this.actionClone.querySelector('.submit');
      this.actionContainer.appendChild(this.actionClone);

      this.hintBtn.addEventListener('click', this.toggleHint)
      this.submitBtn.addEventListener('click', this.selfCheck)
    }

    connectedCallback() {
      this.pagination.lastPage = this.data.numberOfQuestions
      this.answers = JSON.parse(this.data.question.answer);
      this.type = this.data.question.answerType;
      this.status = 'new';
    }

    attributeChangedCallback(name, oldValue, newValue) {
      const hintClone = hintBoxTemplate.content.cloneNode(true),
        hintBox = hintClone.querySelector('.hint-box'),
        hintText = hintBox.querySelector('#prompt'),
        closeBtn = hintClone.querySelector('.close-hint');

      this.hint = JSON.parse(this.data.question.prompt);
      closeBtn.addEventListener('click', event => {
        this.showHint = false
      })

      if (name === 'showhint') {
        if (newValue === 'true') {
          this.hintBoxContainer.appendChild(hintClone)
          hintText.innerHTML = this.hint.Reference.ReferenceText;

        } else {
          this.hintBoxContainer.querySelector('.hint-box').remove();
        }
      }

      if (name === 'question') {
        if (newValue !== oldValue) {
          this.id = this.data.questionIds[this.question]
          this.pagination.setAttribute('currentpage', parseInt(this.question))
        }
      }

      if (name === 'status') {
        if (newValue === 'new') {
          if (this.buttonBar) {
            this.buttonBar.remove();
          }
          this.renderNewActionBar();
          this.renderQuestions();
        }
        if (newValue === 'incorrect') {
          this.buttonBar.remove();
          this.renderWrongActionBar();
          this.renderQuestions();
        }
        if (newValue === 'correct') {
          this.buttonBar.remove();
          this.renderCorrectActionBar();
          this.renderQuestions();
        }
      }
    }


  }
  customElements.define('pearson-quiz', Quiz);
})(window, document);
