var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function (w, doc) {
  'use strict';

  // Create a template element

  var template = doc.createElement('template'),
      button = doc.createElement('template'),
      menu = doc.createElement('template'),
      item = doc.createElement('template'),
      check = '\n            <svg id="correct-18" viewBox="0 0 18 18" style="width:18px; height:18px;">\n                <path d="M7.63703177,13.7249483 C7.2712246,14.0916839 6.67813399,14.0916839 6.31232681,13.7249483 C6.31087256,13.7235264 5.29821541,12.7082993 3.27435538,10.679267 C2.90854821,10.3125314 2.90854821,9.71793566 3.27435538,9.35120012 C3.64016255,8.98446458 4.23325316,8.98446458 4.59906034,9.35120012 L6.9290522,11.6871052 L13.3583348,4.3207061 C13.6989986,3.93038793 14.2907756,3.89083601 14.6801057,4.2323644 C15.0694358,4.57389279 15.1088876,5.16717165 14.7682238,5.55748981 L7.68563681,13.6724173 C7.67022431,13.6903888 7.65402263,13.7079144 7.63703177,13.7249483 Z"></path>\n            </svg>\n        ',
      close = '\n            <svg id="close-24" viewBox="0 0 24 24" style="width:24px; height:24px;">\n                <path d="M12,10.5857864 L17.2928932,5.29289322 C17.6834175,4.90236893 18.3165825,4.90236893 18.7071068,5.29289322 C19.0976311,5.68341751 19.0976311,6.31658249 18.7071068,6.70710678 L13.4142136,12 L18.7071068,17.2928932 C19.0976311,17.6834175 19.0976311,18.3165825 18.7071068,18.7071068 C18.3165825,19.0976311 17.6834175,19.0976311 17.2928932,18.7071068 L12,13.4142136 L6.70710678,18.7071068 C6.31658249,19.0976311 5.68341751,19.0976311 5.29289322,18.7071068 C4.90236893,18.3165825 4.90236893,17.6834175 5.29289322,17.2928932 L10.5857864,12 L5.29289322,6.70710678 C4.90236893,6.31658249 4.90236893,5.68341751 5.29289322,5.29289322 C5.68341751,4.90236893 6.31658249,4.90236893 6.70710678,5.29289322 L12,10.5857864 Z"></path>\n            </svg>\n        ';

  template.innerHTML = ' \n  <style> \n\n@import url("https://fonts.googleapis.com/css?family=Open+Sans:400,600");:host{\n  /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */}:host html{line-height:1.15;-webkit-text-size-adjust:100%}:host body{margin:0}:host main{display:block}:host h1{font-size:2em;margin:.67em 0}:host hr{-webkit-box-sizing:content-box;box-sizing:content-box;height:0;overflow:visible}:host pre{font-family:monospace,monospace;font-size:1em}:host a{background-color:transparent}:host abbr[title]{border-bottom:none;text-decoration:underline;-webkit-text-decoration:underline dotted;text-decoration:underline dotted}:host b,:host strong{font-weight:bolder}:host code,:host kbd,:host samp{font-family:monospace,monospace;font-size:1em}:host small{font-size:80%}:host sub,:host sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}:host sub{bottom:-.25em}:host sup{top:-.5em}:host img{border-style:none}:host button,:host input,:host optgroup,:host select,:host textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}:host button,:host input{overflow:visible}:host button,:host select{text-transform:none}:host [type=button],:host [type=reset],:host [type=submit],:host button{-webkit-appearance:button}:host [type=button]::-moz-focus-inner,:host [type=reset]::-moz-focus-inner,:host [type=submit]::-moz-focus-inner,:host button::-moz-focus-inner{border-style:none;padding:0}:host [type=button]:-moz-focusring,:host [type=reset]:-moz-focusring,:host [type=submit]:-moz-focusring,:host button:-moz-focusring{outline:1px dotted ButtonText}:host fieldset{padding:.35em .75em .625em}:host legend{-webkit-box-sizing:border-box;box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}:host progress{vertical-align:baseline}:host textarea{overflow:auto}:host [type=checkbox],:host [type=radio]{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0}:host [type=number]::-webkit-inner-spin-button,:host [type=number]::-webkit-outer-spin-button{height:auto}:host [type=search]{-webkit-appearance:textfield;outline-offset:-2px}:host [type=search]::-webkit-search-decoration{-webkit-appearance:none}:host ::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}:host details{display:block}:host summary{display:list-item}:host template{display:none}:host [hidden]{display:none}:host html{font-size:14px}:host *,:host html{-webkit-box-sizing:border-box;box-sizing:border-box}:host body{font-family:Open Sans,Arial,Helvetica,sans-serif}:host body,:host p{font-size:14px;line-height:1.5;font-weight:400}:host strong{font-weight:600}:host a{font-size:14px;color:#047a9c}:host a:hover{color:#03536a;text-decoration:none}:host a:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host button{cursor:pointer}:host li,:host ul{font-size:14px}:host svg{fill:#252525}:host svg[focusable=false]:focus{outline:none}:host input,:host textarea{font:inherit;letter-spacing:inherit;word-spacing:inherit}:host .no-border{border:0}:host .icon-18{width:18px;height:18px}:host .icon-24{width:24px;height:24px}:host .hidden{display:none!important}:host .gr-btn{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;border:1px solid #6a7070;background:transparent;color:#6a7070;min-width:152px;padding:8px 24px;font-size:16px;cursor:pointer;border-radius:22px;position:relative;margin:12px;line-height:1.15}:host .gr-btn:hover{color:#252525;border:1px solid #252525}:host .gr-btn:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host a.gr-btn{text-decoration:none}:host .gr-btn.primary{background-color:#047a9c;border:1px solid #047a9c;color:#fff}:host .gr-btn.primary:hover{color:#fff;background-color:#035f79}:host .gr-btn.attention{background-color:#ffb81c;border:1px solid #ffb81c;color:#252525}:host .gr-btn.attention:hover{background-color:#f7aa00}:host .gr-btn.small{min-width:128px;padding:7px 20px;font-size:14px}:host .gr-btn.small:focus:after{padding:18px 21px}:host .gr-btn.extra-large{min-width:152px;padding:10px 24px;font-size:16px}:host .gr-btn.extra-large:focus:after{padding:22px 21px;border-radius:24px}:host .gr-btn:disabled{background-color:#e9e9e9!important;color:#6a7070!important;border:0!important;cursor:default!important}:host .gr-btn.icon-btn-24{min-width:auto;padding:5px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-24:focus{border-radius:4px}:host .gr-btn.icon-btn-24:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-24:active svg{fill:#005a70}:host .gr-btn.icon-btn-24 svg{fill:#6a7070}:host .gr-btn.icon-btn-18{min-width:auto;padding:8px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-18:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-18:active svg{fill:#005a70}:host .gr-btn.icon-btn-18:focus{border-radius:4px}:host .gr-btn.icon-btn-18 svg{fill:#6a7070}:host .gr-btn.no-border{border:0}:host .gr-btn.no-border:hover{border:0}:host .gr-btn.no-border:focus:after{border-radius:5px;min-width:auto;left:auto;top:auto;padding:16px}:host .gr-dropdown-container .gr-btn{margin:0;font-size:14px;font-weight:700}:host .gr-dropdown-container .gr-btn svg{margin-left:1em}:host .gr-dropdown-container .dropdown-menu{position:absolute;z-index:10;margin-top:-9px;-ms-overflow-style:none}:host .gr-dropdown-container .mobile-group{background:#f5f5f5;border-bottom:1px solid #e9e9e9;display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;width:100%;padding:24px;position:relative;top:-27px;z-index:10;overflow:hidden}:host .gr-dropdown-container .mobile-group span{width:100%;text-align:center}:host .gr-dropdown-container .mobile-group button{margin:0 0 0 12px}:host .gr-dropdown-container .menu{background-color:#fff;position:absolute}:host .gr-dropdown-container .menu button{background:none;border:0;font-size:14px}:host .gr-dropdown-container .menu button>*{pointer-events:none}:host .gr-dropdown-container .menu button:focus{outline:none}:host .gr-dropdown-container .menu button:focus:after{border:2px solid #1977d4;content:"";position:absolute;border-radius:4px;width:-webkit-calc(100% + -10px);width:calc(100% + -10px);height:100%;top:0;left:5px;z-index:1}:host .gr-dropdown-container .menu{list-style-type:none;padding:1em 0;border:1px solid #d9d9d9;border-radius:2px;margin-top:0;top:16px;position:relative}:host .gr-dropdown-container [aria-checked=false] svg{display:none}:host .gr-dropdown-container [aria-checked=true] svg{display:inline-block}:host .gr-dropdown-container .seperator{padding-bottom:6px;margin-bottom:6px;border-bottom:1px solid #d9d9d9}:host .gr-dropdown-container li button{position:relative;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;text-decoration:none;width:100%;color:#252525;text-align:left;padding:0}:host .gr-dropdown-container li button:hover{background-color:#e9e9e9}:host .gr-dropdown-container li button svg{position:absolute;left:12px}:host .gr-dropdown-container .option-text{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;margin:5px 24px 5px 34px;width:100%}:host .gr-dropdown-container .divider{border-bottom:1px solid #c7c7c7;padding-bottom:8px;margin:0 25px}:host .gr-dropdown-container .divider+li{margin-top:8px}:host .gr-dropdown-container .truncate{max-width:100%;width:250px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}@media (max-width:480px){:host .gr-dropdown-container li{padding:8px 4px}}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host .gr-dropdown-container .menu li button[aria-checked=true] svg{top:4px}}@-webkit-keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@-webkit-keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@-webkit-keyframes fadeIn{0%{opacity:0}to{opacity:1}}@keyframes fadeIn{0%{opacity:0}to{opacity:1}}@-webkit-keyframes fadeOut{0%{opacity:1}to{opacity:0}}@keyframes fadeOut{0%{opacity:1}to{opacity:0}}:host .animated{-webkit-animation-duration:1s;animation-duration:1s;-webkit-animation-fill-mode:backwards;animation-fill-mode:backwards}@media screen and (prefers-reduced-motion:reduce){:host .animated{-webkit-animation:unset!important;animation:unset!important}}:host .animateIn{-webkit-animation-name:fadeIn;animation-name:fadeIn;-webkit-animation-duration:.5s;animation-duration:.5s}:host .animateOut{-webkit-animation-name:fadeOut;animation-name:fadeOut;-webkit-animation-duration:.2s;animation-duration:.2s}:host .gr-dropdown-container .menu button:focus:after{top:-2px}:host .gr-btn,:host button{font-family:Open Sans,Arial,Helvetica,sans-serif;font-weight:400;font-size:14px;font-weight:400!important}:host .gr-btn.text{border:0}:host pearson-dropdown:slotted{display:none}:host #collapse-18,:host #expand-18{position:relative;top:-2px;left:-2px}:host .collapse-icon{display:none}:host .primary svg{fill:#fff}:host .gr-dropdown-container .menu{border-radius:4px;width:220px;max-height:300px;-webkit-box-shadow:0 2px 4px 0 rgba(0,0,0,.16);box-shadow:0 2px 4px 0 rgba(0,0,0,.16);overflow:scroll}:host .gr-dropdown-container .gr-btn svg{margin-left:8px}:host .gr-dropdown-container .icon-btn-18{margin:0;padding:0}:host .gr-dropdown-container .icon-btn-18 span,:host .gr-dropdown-container .icon-btn-18 svg{margin:0}\n   </style>\n   \n  <div class="gr-dropdown-container">\n  \n  </div>\n';

  button.innerHTML = '\n  <button data-action="trigger" aria-haspopup="true" aria-expanded="false" class="gr-btn">\n     <span class="dropdown-text"></span>\n     <div class="expand-icon">\n        <pearson-icon icon="more" size="18"></pearson-icon>\n    </div>\n  </button>\n  ';

  menu.innerHTML = '\n  <div class="dropdown-menu animateIn" data-action="menu">\n\t\t\t<ul role="menu" class="menu" style="right:177px">\n\t\t\t  <slot></slot>\n\t\t\t</ul>\n\t</div>\n  ';

  item.innerHTML = '\n  <li role="none" >\n    <button role="menuitemradio" aria-checked="false">\n         ' + check + '\n      <span class="option-text">Option One</span>\n    </button>\n  </li>\n  ';

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'search-card-options');

  /** Any helper functions that do not need to be part of the class
   * can be declared here, before the class is defined.
   */

  function getFocusableElements(node) {
    return node.querySelectorAll('[role^="menuitemradio"]');
  }

  function buildListItems(content, component, menu, index, checked) {
    var li = item.content.cloneNode(true),
        text = li.querySelector('.option-text'),
        button = li.querySelector('button');

    text.innerHTML = content.text;
    if (content.value) {
      if (content.id === content.value) {
        button.setAttribute('aria-checked', true);
      }
    }
    if (content.divider === true) {
      button.parentNode.classList.add('seperator');
    }

    if (checked !== undefined && checked.length > 0) {
      checked.forEach(function (item) {
        if (parseInt(item.getAttribute('data-index')) === index) {
          button.setAttribute('aria-checked', true);
        }
      });
    }

    button.setAttribute('data-id', content.id);
    button.setAttribute('data-index', index);
    button.addEventListener('click', function (event) {
      // unless multi select, only select one item at a time.

      if (content.multiSelect) {
        var ariaChecked = event.target.getAttribute('aria-checked');
        if (ariaChecked === 'false') {
          event.target.setAttribute('aria-checked', true);
        } else {
          event.target.setAttribute('aria-checked', false);
        }
      } else {
        var focusItems = getFocusableElements(event.target.parentNode.parentNode);
        focusItems.forEach(function (item) {
          item.setAttribute('aria-checked', false);
        });
        event.target.setAttribute('aria-checked', true);
        component.setAttribute('value', content.id);
        component.closeDropdown();
      }
    });

    button.addEventListener('keydown', function (event) {

      var nextButton = parseInt(event.target.getAttribute('data-index')) + 1,
          prevButton = parseInt(event.target.getAttribute('data-index')) - 1;
      console.log(event);
      if (event.key === 'ArrowUp' || event.key === 'Up') {
        event.preventDefault();
        if (component.shadowRoot.activeElement === getFocusableElements(menu)[0]) {
          getFocusableElements(menu)[getFocusableElements(menu).length - 1].focus();
        } else {
          getFocusableElements(menu)[prevButton].focus();
        }
      }
      if (event.key === 'ArrowDown' || event.key === 'Down') {
        event.preventDefault();
        if (component.shadowRoot.activeElement === getFocusableElements(menu)[getFocusableElements(menu).length - 1]) {
          getFocusableElements(menu)[0].focus();
        } else {
          getFocusableElements(menu)[nextButton].focus();
        }
      }
      if (event.key === 'Home') {
        getFocusableElements(menu)[0].focus();
      }

      if (event.key === 'End') {
        getFocusableElements(menu)[getFocusableElements(menu).length - 1].focus();
      }
    });
    return li;
  }

  var Dropdown = function (_HTMLElement) {
    _inherits(Dropdown, _HTMLElement);

    _createClass(Dropdown, [{
      key: 'type',
      get: function get() {
        return this.getAttribute('type');
      }
    }, {
      key: 'buttonText',
      get: function get() {
        return this.getAttribute('buttonText');
      }
    }, {
      key: 'textOnly',
      get: function get() {
        return this.hasAttribute('textOnly');
      }
    }, {
      key: 'multiSelect',
      get: function get() {
        return this.hasAttribute('multiSelect');
      }
    }, {
      key: 'value',
      get: function get() {
        return this.getAttribute('value');
      }
    }, {
      key: 'open',
      get: function get() {
        return this.getAttribute('open');
      },
      set: function set(value) {
        this.setAttribute('open', value);
      }
    }], [{
      key: 'observedAttributes',
      get: function get() {
        return ['buttonText', 'textOnly', 'open', 'value', 'multiSelect', 'type'];
      }
    }]);

    function Dropdown() {
      _classCallCheck(this, Dropdown);

      var _this = _possibleConstructorReturn(this, (Dropdown.__proto__ || Object.getPrototypeOf(Dropdown)).call(this));

      _this.attachShadow({ mode: 'open' });
      var clone = template.content.cloneNode(true);
      _this.shadowRoot.appendChild(clone);

      _this.openDropdown = _this.openDropdown.bind(_this);
      _this.closeDropdown = _this.closeDropdown.bind(_this);
      _this.returnChecked = _this.returnChecked.bind(_this);

      return _this;
    }

    _createClass(Dropdown, [{
      key: 'openDropdown',
      value: function openDropdown() {
        var _this2 = this;

        if (this.open === null) {
          this.open = true;
          this.button.setAttribute('aria-expanded', true);
          this.shadowRoot.addEventListener('animationend', function (event) {
            getFocusableElements(_this2.shadowRoot)[0].focus();
          });
        } else if (this.open) {
          this.closeDropdown();
        }
      }
    }, {
      key: 'closeDropdown',
      value: function closeDropdown() {
        this.checked = this.returnChecked();
        this.removeAttribute('open');
        this.button.setAttribute('aria-expanded', false);

        if (this.multiSelect) {
          var arr = [];
          this.checked.forEach(function (item) {
            arr.push(item.getAttribute('data-id'));
          });
          this.dispatchEvent(new CustomEvent('change', {
            bubbles: true,
            detail: {
              selected: arr
            }
          }));
        } else {
          this.dispatchEvent(new CustomEvent('change', {
            bubbles: true,
            detail: {
              selected: this.checked !== false ? this.checked.getAttribute('data-id') : 'no selection'
            }
          }));
        }

        this.button.focus();
      }
    }, {
      key: 'returnChecked',
      value: function returnChecked() {
        var checked = [];
        if (this.multiSelect) {
          checked = this.shadowRoot.querySelectorAll('[aria-checked="true"]');
        } else {
          checked = this.shadowRoot.querySelector('[aria-checked="true"]');
        }
        if (checked !== null) {
          return checked;
        } else {
          return false;
        }
      }
    }, {
      key: 'connectedCallback',
      value: function connectedCallback() {
        var _this3 = this;

        /** Any changes to what the component renders should be done here. */
        var dropdownTrigger = button.content.cloneNode(true),
            dropdownTemplate = this.shadowRoot.querySelector('.gr-dropdown-container');
        dropdownTemplate.appendChild(dropdownTrigger);

        this.button = dropdownTemplate.querySelector('button');
        this.button.classList.add('icon-btn-18');

        var buttonText = dropdownTemplate.querySelector('button .dropdown-text');
        buttonText.innerHTML = this.buttonText;

        // Get the attributes set by the consumer
        if (this.textOnly) {
          this.button.classList.add('text');
        }

        this.button.addEventListener('click', this.openDropdown);
        /** Event listeners should also be bound here. */
        doc.addEventListener('click', function (event) {
          if (_this3.open === 'true') {
            var target = event.target;
            var dropdownMenu = _this3.shadowRoot.querySelector('.menu');
            do {
              if (target === dropdownMenu || target === _this3) {
                return;
              }
              target = target.parentNode;
            } while (target);
            _this3.closeDropdown();
          }
        });

        doc.addEventListener('keydown', function (event) {
          if (_this3.open === 'true' && event.key === 'Escape') {
            _this3.closeDropdown();
          }
        });

        if (this.type === 'primary') {
          this.button.classList.add('primary');
        }

        if (this.type === 'attention') {
          this.button.classList.add('attention');
        }

        if (this.type === 'icon') {
          var _buttonText = dropdownTemplate.querySelector('button .dropdown-text');
          _buttonText.innerHTML = '';
          this.button.classList.add('icon-btn-18');
        }
      }
    }, {
      key: 'attributeChangedCallback',
      value: function attributeChangedCallback(name, oldValue, newValue) {
        var _this4 = this;

        if (name === 'open' && newValue === 'true') {
          var dropdownMenu = menu.content.cloneNode(true),
              dropdownTemplate = this.shadowRoot.querySelector('.gr-dropdown-container');

          dropdownTemplate.appendChild(dropdownMenu);

          var items = this.querySelectorAll('li'),
              dropdownMenuTemplate = this.shadowRoot.querySelector('.menu'),
              itemContent = [];

          // get items from slot to render new list for dropdown
          items.forEach(function (item) {
            item.style.display = 'none';
            itemContent.push({
              divider: item.classList.contains('divider'),
              text: item.innerHTML,
              id: item.id,
              multiSelect: _this4.multiSelect,
              value: _this4.value });
          });

          // render list items based on items in slot
          itemContent.forEach(function (content, index) {
            dropdownMenuTemplate.appendChild(buildListItems(content, _this4, dropdownMenuTemplate, index, _this4.checked));
          });
        } else if (name === 'open' && newValue === null) {
          var _dropdownMenu = this.shadowRoot.querySelector('.dropdown-menu');
          _dropdownMenu.remove();
          this.button.setAttribute('aria-expanded', false);
        }
      }
    }]);

    return Dropdown;
  }(HTMLElement);

  customElements.define('search-card-options', Dropdown);
})(window, document);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function (w, doc) {
  'use strict';

  var template = doc.createElement('template');

  template.innerHTML = ' \n\n<style>\n\n@import url("https://fonts.googleapis.com/css?family=Open+Sans:400,600");:host{\n  /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */}:host html{line-height:1.15;-webkit-text-size-adjust:100%}:host body{margin:0}:host main{display:block}:host h1{font-size:2em;margin:.67em 0}:host hr{-webkit-box-sizing:content-box;box-sizing:content-box;height:0;overflow:visible}:host pre{font-family:monospace,monospace;font-size:1em}:host a{background-color:transparent}:host abbr[title]{border-bottom:none;text-decoration:underline;-webkit-text-decoration:underline dotted;text-decoration:underline dotted}:host b,:host strong{font-weight:bolder}:host code,:host kbd,:host samp{font-family:monospace,monospace;font-size:1em}:host small{font-size:80%}:host sub,:host sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}:host sub{bottom:-.25em}:host sup{top:-.5em}:host img{border-style:none}:host button,:host input,:host optgroup,:host select,:host textarea{font-family:inherit;font-size:100%;line-height:1.15;margin:0}:host button,:host input{overflow:visible}:host button,:host select{text-transform:none}:host [type=button],:host [type=reset],:host [type=submit],:host button{-webkit-appearance:button}:host [type=button]::-moz-focus-inner,:host [type=reset]::-moz-focus-inner,:host [type=submit]::-moz-focus-inner,:host button::-moz-focus-inner{border-style:none;padding:0}:host [type=button]:-moz-focusring,:host [type=reset]:-moz-focusring,:host [type=submit]:-moz-focusring,:host button:-moz-focusring{outline:1px dotted ButtonText}:host fieldset{padding:.35em .75em .625em}:host legend{-webkit-box-sizing:border-box;box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}:host progress{vertical-align:baseline}:host textarea{overflow:auto}:host [type=checkbox],:host [type=radio]{-webkit-box-sizing:border-box;box-sizing:border-box;padding:0}:host [type=number]::-webkit-inner-spin-button,:host [type=number]::-webkit-outer-spin-button{height:auto}:host [type=search]{-webkit-appearance:textfield;outline-offset:-2px}:host [type=search]::-webkit-search-decoration{-webkit-appearance:none}:host ::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}:host details{display:block}:host summary{display:list-item}:host [hidden],:host template{display:none}:host html{font-size:14px}:host *,:host html{-webkit-box-sizing:border-box;box-sizing:border-box}:host body{font-family:Open Sans,Arial,Helvetica,sans-serif}:host body,:host p{font-size:14px;line-height:1.5;font-weight:400}:host strong{font-weight:600}:host a{font-size:14px;color:#047a9c}:host a:hover{color:#03536a;text-decoration:none}:host a:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host button{cursor:pointer}:host li,:host ul{font-size:14px}:host svg{fill:#252525}:host svg[focusable=false]:focus{outline:none}:host select{cursor:pointer}:host input,:host textarea{font:inherit;letter-spacing:inherit;word-spacing:inherit}:host svg{pointer-events:none}@-webkit-keyframes shift{to{background-position:9px 9px}}@keyframes shift{to{background-position:9px 9px}}@-webkit-keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@keyframes bouncedelay{0%,25%,to{transform:scale(1);-webkit-transform:scale(1)}12.5%{transform:scale(1.5);-webkit-transform:scale(1.5)}}@-webkit-keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@keyframes slideInUp{0%{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);visibility:visible}to{-webkit-transform:translateZ(0);transform:translateZ(0)}}@-webkit-keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@keyframes fadeOutUp{0%{opacity:1}to{opacity:0;-webkit-transform:translate3d(0,-100%,0);transform:translate3d(0,-100%,0)}}@-webkit-keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@keyframes slideInDown{0%{-webkit-transform:translate3d(0,-150%,0);transform:translate3d(0,-150%,0);opacity:0;visibility:hidden}to{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}}@-webkit-keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@keyframes slideOutDown{0%{-webkit-transform:translateZ(0);transform:translateZ(0);opacity:1;visibility:visible}to{-webkit-transform:translate3d(0,100%,0);transform:translate3d(0,100%,0);opacity:0;visibility:hidden}}@-webkit-keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@keyframes fadeIn{0%{opacity:0;visibility:hidden}to{opacity:1;visibility:visible}}@-webkit-keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}@keyframes fadeOut{0%{opacity:1;visibility:visible}to{opacity:0;visibility:hidden}}:host .fadeIn,:host .slideInDown{opacity:1!important;visibility:visible!important}:host .fadeOut,:host .slideOutDown{opacity:0;visibility:hidden}:host .slideInDown{-webkit-animation:slideInDown .3s ease-in-out 0s;animation:slideInDown .3s ease-in-out 0s}:host .slideOutDown{-webkit-animation:slideOutDown .2s ease-in 0s;animation:slideOutDown .2s ease-in 0s}:host .fadeIn{-webkit-animation:fadeIn .3s linear 0s;animation:fadeIn .3s linear 0s}:host .fadeOut{-webkit-animation:fadeOut .2s linear 0s;animation:fadeOut .2s linear 0s}:host .animateIn{-webkit-animation-name:fadeIn;animation-name:fadeIn;-webkit-animation-duration:.5s;animation-duration:.5s}:host .animateOut{-webkit-animation-name:fadeOut;animation-name:fadeOut;-webkit-animation-duration:.2s;animation-duration:.2s}@media (prefers-reduced-motion){:host .animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}}:host html[data-prefers-reduced-motion] .animated{-webkit-animation:unset!important;animation:unset!important;-webkit-transition:none!important;transition:none!important}:host .no-border{border:0}:host .icon-18{width:18px;height:18px}:host .icon-24{width:24px;height:24px}:host .hidden{display:none!important}:host pearson-alert{font-size:16px;max-width:580px}:host pearson-alert .alert-title{font-size:14px;margin:0;display:inline;top:0}:host pearson-uploader .alert-title{top:0!important}:host pearson-alert .alert-text{margin:0;display:inline}:host pearson-footer{left:50%;margin-left:-50%;right:50%;margin-right:-50%}:host pearson-header{grid-column:span 12}:host pearson-tabs{font-size:14px}:host pearson-progress-bar{grid-column:1/5}@media (min-width:591px){:host pearson-progress-bar{grid-column:1/9}}@media (min-width:887px){:host pearson-progress-bar{grid-column:1/13}}:host pearson-tabs{grid-column:1/5}@media (min-width:591px){:host pearson-tabs{grid-column:1/9}}@media (min-width:887px){:host pearson-tabs{grid-column:1/13}}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host pearson-card{-ms-grid-column-span:3}:host pearson-card,:host pearson-card[stacked]{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;width:100%}:host pearson-card[stacked]{-ms-grid-column-span:12}}:host pearson-accordion{grid-column:1/5}@media (min-width:591px){:host pearson-accordion{grid-column:1/9}}@media (min-width:887px){:host pearson-accordion{grid-column:1/13}}:host #main{max-width:1280px;margin:0 auto}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host iframe .gr-grid-container{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}}:host .gr-grid-container{display:grid;display:-ms-grid;grid-template-columns:repeat(4,1fr);grid-column-gap:16px;grid-row-gap:16px;margin:0 39.5px}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host .gr-grid-container.ie-flex{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}}@media (min-width:351px){:host .gr-grid-container{margin:0 31.5px}}@media (min-width:399px){:host .gr-grid-container{margin:0 39.5px}}@media (min-width:447px){:host .gr-grid-container{margin:0 79.5px}}@media (min-width:591px){:host .gr-grid-container{grid-template-columns:repeat(8,1fr);margin:0 83.5px}}@media (min-width:727px){:host .gr-grid-container{margin:0 103.5px;grid-column-gap:24px;grid-row-gap:24px}}@media (min-width:887px){:host .gr-grid-container{grid-template-columns:repeat(12,1fr);margin:0 71.5px}}@media (min-width:887px) and (-ms-high-contrast:active),(min-width:887px) and (-ms-high-contrast:none){:host .gr-grid-container>*{margin-right:12px;margin-left:12px;margin-bottom:24px}}@media (min-width:983px){:host .gr-grid-container{margin:0 71.5px}}@media (min-width:1079px){:host .gr-grid-container{margin:0 71.5px}}@media (min-width:1175px){:host .gr-grid-container{margin:0 76px}}:host .gr-grid-container .gr-col-two{grid-column-start:2}:host .gr-grid-container .gr-col-three{grid-column-start:3}:host .gr-grid-container .gr-col-four{grid-column-start:4}:host .gr-grid-container .gr-col-five{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-five{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:5}}:host .gr-grid-container .gr-col-six{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-six{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:6}}:host .gr-grid-container .gr-col-seven{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-seven{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:7}}:host .gr-grid-container .gr-col-eight{display:none}@media (min-width:591px){:host .gr-grid-container .gr-col-eight{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:8}}:host .gr-grid-container .gr-col-nine{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-nine{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:9}}:host .gr-grid-container .gr-col-ten{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-ten{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:10}}:host .gr-grid-container .gr-col-eleven{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-eleven{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:11}}:host .gr-grid-container .gr-col-twelve{display:none}@media (min-width:887px){:host .gr-grid-container .gr-col-twelve{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;grid-column-start:12}}:host .gr-col-span-one{grid-column-end:span 1}:host .gr-col-span-two{grid-column-end:span 2}:host .gr-col-span-three{grid-column-end:span 3}:host .gr-col-span-four{grid-column-end:span 4}@media (min-width:591px){:host .gr-col-span-five{grid-column-end:span 5}}@media (min-width:591px){:host .gr-col-span-six{grid-column-end:span 6}}@media (min-width:591px){:host .gr-col-span-seven{grid-column-end:span 7}}@media (min-width:591px){:host .gr-col-span-eight{grid-column-end:span 8}}@media (min-width:887px){:host .gr-col-span-nine{grid-column-end:span 9}}@media (min-width:887px){:host .gr-col-span-ten{grid-column-end:span 10}}@media (min-width:887px){:host .gr-col-span-eleven{grid-column-end:span 11}}:host .gr-col-span-twelve{grid-column-end:span 4}@media (min-width:591px){:host .gr-col-span-twelve{grid-column-end:span 8}}@media (min-width:887px){:host .gr-col-span-twelve{grid-column-end:span 12}}:host .gr-row-one{grid-row-start:1}:host .gr-row-two{grid-row-start:2}:host .gr-row-three{grid-row-start:3}:host .gr-row-four{grid-row-start:4}:host .gr-row-five{grid-row-start:5}:host .gr-row-six{grid-row-start:6}:host .gr-row-seven{grid-row-start:7}:host .gr-row-eight{grid-row-start:8}:host .gr-row-nine{grid-row-start:9}:host .gr-row-ten{grid-row-start:10}:host .gr-row-eleven{grid-row-start:11}:host .gr-row-twelve{grid-row-start:12}:host .gr-row-thirteen{grid-row-start:13}:host .gr-row-fourteen{grid-row-start:14}:host .gr-row-fifteen{grid-row-start:15}:host .gr-row-span-two{grid-row-end:span 2}:host .gr-row-span-three{grid-row-end:span 3}:host .gr-row-span-four{grid-row-end:span 4}:host .gr-row-span-five{grid-row-end:span 5}:host .gr-row-span-six{grid-row-end:span 6}:host .gr-row-span-seven{grid-row-end:span 7}:host .gr-row-span-eight{grid-row-end:span 8}:host .gr-row-span-nine{grid-row-end:span 9}:host .gr-row-span-ten{grid-row-end:span 10}:host .gr-row-span-eleven{grid-row-end:span 11}:host .gr-row-span-twelve{grid-row-end:span 12}:host .gr-primary{color:#047a9c;fill:#047a9c}:host .gr-secondary{color:#ffb81c;fill:#ffb81c}:host .gr-white{color:#fff;fill:#fff}:host .gr-neutral-high-one{color:#252525;fill:#252525}:host .gr-neutral-high-two{color:#6a7070;fill:#6a7070}:host .gr-neutral-med-one{color:#a9a9a9;fill:#a9a9a9}:host .gr-neutral-med-two{color:#c7c7c7;fill:#c7c7c7}:host .gr-neutral-med-three{color:#d9d9d9;fill:#d9d9d9}:host .gr-neutral-med-four{color:#e9e9e9;fill:#e9e9e9}:host .gr-neutral-light-one,:host .gr-neutral-light-two{color:#eee;fill:#eee}:host .gr-condition-one{color:#db0020;fill:#db0020}:host .gr-condition-two{color:#038238;fill:#038238}:host .gr-condition-three{color:#da0474;fill:#da0474}:host .gr-theme-one-light{color:#caefee;fill:#caefee}:host .gr-theme-one-med{color:#76d5d4;fill:#76d5d4}:host .gr-theme-one-dark{color:#19a5a3}:host .gr-theme-two-light{color:#f2e5f1;fill:#f2e5f1}:host .gr-theme-two-med{color:#895b9a;fill:#895b9a}:host .gr-theme-two-dark{color:#633673;fill:#633673}:host .gr-theme-three-light{color:#f6f8cc;fill:#f6f8cc}:host .gr-theme-three-med{color:#d2db0e;fill:#d2db0e}:host .gr-theme-three-dark{color:#b0b718;fill:#b0b718}:host .gr-theme-four-light{color:#d9e6f1;fill:#d9e6f1}:host .gr-theme-four-med{color:#356286;fill:#356286}:host .gr-theme-four-dark{color:#1e496c;fill:#356286}:host .gr-theme-five-light{color:#dff5d5;fill:#dff5d5}:host .gr-theme-five-med{color:#66be3e;fill:#66be3e}:host .gr-theme-five-dark{color:#288500;fill:#288500}:host .gr-theme-six-light{color:#d6ecf4;fill:#d6ecf4}:host .gr-theme-six-med{color:#80c5dd;fill:#80c5dd}:host .gr-theme-six-dark{color:#46a9cb;fill:#46a9cb}:host .gr-theme-seven-light{color:#faebc3;fill:#faebc3}:host .gr-theme-seven-med{color:#f5c54c;fill:#f5c54c}:host .gr-theme-seven-dark{color:#dea30d;fill:#dea30d}:host .gr-primary-bg{background-color:#047a9c}:host .gr-secondary-bg{background-color:#ffb81c}:host .gr-white-bg{background-color:#fff}:host .gr-neutral-high-one-bg{background-color:#252525}:host .gr-neutral-high-two-bg{background-color:#6a7070}:host .gr-neutral-med-one-bg{background-color:#a9a9a9}:host .gr-neutral-med-two-bg{background-color:#c7c7c7}:host .gr-neutral-med-three-bg{background-color:#d9d9d9}:host .gr-neutral-med-four-bg{background-color:#e9e9e9}:host .gr-neutral-light-one-bg,:host .gr-neutral-light-two-bg{background-color:#eee}:host .gr-condition-one-bg{background-color:#db0020}:host .gr-condition-two-bg{background-color:#038238}:host .gr-condition-three-bg{background-color:#da0474}:host .gr-theme-one-light-bg{background-color:#caefee}:host .gr-theme-one-med-bg{background-color:#76d5d4}:host .gr-theme-one-dark-bg{background-color:#19a5a3}:host .gr-theme-two-light-bg{background-color:#f2e5f1}:host .gr-theme-two-med-bg{background-color:#895b9a}:host .gr-theme-two-dark-bg{background-color:#633673}:host .gr-theme-three-light-bg{background-color:#f6f8cc}:host .gr-theme-three-med-bg{background-color:#d2db0e}:host .gr-theme-three-dark-bg{background-color:#b0b718}:host .gr-theme-four-light-bg{background-color:#d9e6f1}:host .gr-theme-four-med-bg{background-color:#356286}:host .gr-theme-four-dark-bg{background-color:#1e496c}:host .gr-theme-five-light-bg{background-color:#dff5d5}:host .gr-theme-five-med-bg{background-color:#66be3e}:host .gr-theme-five-dark-bg{background-color:#288500}:host .gr-theme-six-light-bg{background-color:#d6ecf4}:host .gr-theme-six-med-bg{background-color:#80c5dd}:host .gr-theme-six-dark-bg{background-color:#46a9cb}:host .gr-theme-seven-light-bg{background-color:#faebc3}:host .gr-theme-seven-med-bg{background-color:#f5c54c}:host .gr-theme-seven-dark-bg{background-color:#dea30d}:host .gr-h1{font-size:24px;line-height:28px}:host .gr-h1,:host .gr-h2{font-weight:400;margin-top:0}:host .gr-h2{font-size:20px;line-height:26px}:host .gr-h3{font-size:18px;line-height:24px;font-weight:400;margin-top:0}:host .gr-label{font-size:12px;line-height:16px;color:#6a7070;display:block;margin-bottom:4px}:host .gr-meta{font-size:12px;line-height:12px;color:#6a7070}:host .gr-semi-bold{font-weight:600}:host .gr-font-large{font-size:16px;line-height:24px}:host .gr-font-normal{font-size:14px;line-height:20px}:host .gr-btn{display:-webkit-inline-box;display:-webkit-inline-flex;display:-ms-inline-flexbox;display:inline-flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;border:1px solid #6a7070;background:transparent;color:#6a7070;min-width:152px;padding:8px 24px;font-size:16px;cursor:pointer;border-radius:22px;position:relative;margin:12px;line-height:1.15}:host .gr-btn:hover{color:#252525;border:1px solid #252525}:host .gr-btn:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host a.gr-btn{text-decoration:none}:host .gr-btn.primary{background-color:#047a9c;border:1px solid #047a9c;color:#fff}:host .gr-btn.primary:hover{color:#fff;background-color:#035f79}:host .gr-btn.attention{background-color:#ffb81c;border:1px solid #ffb81c;color:#252525}:host .gr-btn.attention:hover{background-color:#f7aa00}:host .gr-btn.small{min-width:128px;padding:7px 20px;font-size:14px}:host .gr-btn.small:focus:after{padding:18px 21px}:host .gr-btn.extra-large{min-width:152px;padding:10px 24px;font-size:16px}:host .gr-btn.extra-large:focus:after{padding:22px 21px;border-radius:24px}:host .gr-btn:disabled{background-color:#e9e9e9!important;color:#6a7070!important;border:0!important;cursor:default!important}:host .gr-btn.icon-btn-24{min-width:auto;padding:5px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-24:focus{border-radius:4px}:host .gr-btn.icon-btn-24:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-24:active svg{fill:#005a70}:host .gr-btn.icon-btn-24 svg{fill:#6a7070}:host .gr-btn.icon-btn-18{min-width:auto;padding:8px 12px;border:0;margin:0;height:48px;width:48px}:host .gr-btn.icon-btn-18:hover svg{fill:#047a9c}:host .gr-btn.icon-btn-18:active svg{fill:#005a70}:host .gr-btn.icon-btn-18:focus{border-radius:4px}:host .gr-btn.icon-btn-18 svg{fill:#6a7070}:host .gr-btn.no-border,:host .gr-btn.no-border:hover{border:0}:host .gr-btn.no-border:focus:after{border-radius:5px;min-width:auto;left:auto;top:auto;padding:16px}:host .gr-input{display:block;margin:4px 0;padding:0 12px;height:36px;color:#252525;border:1px solid #c7c7c7;background:#fff;border-radius:4px;font-size:14px;width:100%;max-width:100%}:host .gr-input:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-input:disabled{color:#c7c7c7;background-color:#e9e9e9}:host .gr-input:-moz-read-only{border:0}:host .gr-input:read-only{border:0}:host .gr-checkbox+.error-state,:host .gr-input+.error-state,:host .gr-radio+.error-state,:host .gr-select-container+.error-state,:host .gr-textarea+.error-state{display:none}:host .gr-checkbox.error,:host .gr-input.error,:host .gr-radio.error,:host .gr-select-container.error,:host .gr-textarea.error{border-color:#db0020}:host .gr-checkbox.error,:host .gr-checkbox.error+.error-state,:host .gr-input.error,:host .gr-input.error+.error-state,:host .gr-radio.error,:host .gr-radio.error+.error-state,:host .gr-select-container.error,:host .gr-select-container.error+.error-state,:host .gr-textarea.error,:host .gr-textarea.error+.error-state{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .gr-checkbox.error+.error-state svg,:host .gr-input.error+.error-state svg,:host .gr-radio.error+.error-state svg,:host .gr-select-container.error+.error-state svg,:host .gr-textarea.error+.error-state svg{fill:#db0020}:host .gr-checkbox.error+.error-state span,:host .gr-input.error+.error-state span,:host .gr-radio.error+.error-state span,:host .gr-select-container.error+.error-state span,:host .gr-textarea.error+.error-state span{margin-left:8px;color:#db0020}:host .gr-textarea{display:block;margin:4px 0;padding:12px;color:#252525;border:1px solid #c7c7c7;background:#fff;border-radius:4px;font-size:14px;width:100%;max-width:100%;line-height:1.5}:host .gr-textarea:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-textarea:disabled{color:#c7c7c7;background-color:#e9e9e9}:host .gr-textarea:-moz-read-only{border:0}:host .gr-textarea:read-only{border:0}:host .gr-checkbox{margin-bottom:14px;min-height:16px;position:relative;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .gr-checkbox input[type=checkbox]{opacity:0;position:absolute;cursor:pointer}:host .gr-checkbox input[type=checkbox]~label{display:inline-block;line-height:1.5;min-height:24px;padding-left:2.5em;position:relative;z-index:2;cursor:pointer;font-size:14px}:host .gr-checkbox input[type=checkbox]~span{background:#fff}:host .gr-checkbox input[type=checkbox]:disabled~span,:host .gr-checkbox input[type=checkbox]~span{height:22px;line-height:1.5;text-align:center;width:22px;border:2px solid #c7c7c7;border-radius:2px;left:0;position:absolute;top:0;z-index:0}:host .gr-checkbox input[type=checkbox]:disabled~span{background-color:#e9e9e9}:host .gr-checkbox input[type=checkbox]:focus~span{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-checkbox input[type=checkbox]~span svg{height:24px;opacity:0;width:24px;top:-3px;position:relative;left:-3px;fill:#047a9c}:host .gr-checkbox input[type=checkbox]:disabled~span svg{fill:#c7c7c7}:host .gr-checkbox input[type=checkbox]:checked~span svg{opacity:1}:host .gr-fieldset{border:none;padding:0}:host .gr-fieldset legend{margin-bottom:8px}:host .gr-radio{margin-bottom:14px;min-height:16px;position:relative;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .gr-radio input[type=radio]{opacity:0;position:absolute}:host .gr-radio input[type=radio]:focus~span{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-radio input[type=radio]+label{display:inline-block;line-height:18px;padding-left:28px;font-size:14px;cursor:pointer}:host .gr-radio input[type=radio]~span{-webkit-box-sizing:content-box;border:2px solid #c7c7c7;background:#fff;border-radius:50%;box-sizing:content-box;color:#6a7070;display:block;height:5px;left:0;padding:3px 6px 6px 3px;pointer-events:none;position:absolute;top:0;width:5px}:host .gr-radio input[type=radio]~span svg{height:18px;opacity:0;width:18px}:host .gr-radio input[type=radio]:checked~span svg{opacity:1;top:-5px;position:relative;left:-5px;fill:#047a9c}:host .gr-radio input[type=radio]:disabled~span svg{opacity:1;fill:#c7c7c7;top:-5px;left:-5px;position:relative}:host .gr-select-container{position:relative}:host .gr-select-container svg{position:absolute;right:12px;top:-webkit-calc(50% - 9px);top:calc(50% - 9px);fill:#6a7070}:host .gr-select{-webkit-appearance:none;-moz-appearance:none;display:block;margin:4px 0;padding:0 42px 0 12px;height:36px;color:#252525;border:1px solid #c7c7c7;background:#fff;border-radius:4px;font-size:14px;width:100%;max-width:100%}:host .gr-select:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-select:disabled{color:#c7c7c7;background-color:#e9e9e9}:host .gr-select:disabled+svg{fill:#c7c7c7}:host .gr-select[multiple]{height:auto}:host .gr-select[multiple] option{cursor:pointer}:host .margin-container{max-width:1280px;margin:0 auto}:host .header-container.light{width:100%;background-color:#f5f5f5}:host .page-frame{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;grid-column:span 4}@media (min-width:591px){:host .page-frame{grid-column:span 8}}@media (min-width:887px){:host .page-frame{grid-column:span 12}}:host .page-frame h1{margin:0;padding:32px 0 0}:host pearson-tabs .back-bar{width:100%;grid-column:span 12;border-top:1px solid #c7c7c7;border-bottom:1px solid #c7c7c7;top:-22px;position:relative}:host .page-frame-simple{grid-column:span 4;height:70px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:baseline;-webkit-align-items:baseline;-ms-flex-align:baseline;align-items:baseline;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between}@media (min-width:591px){:host .page-frame-simple{grid-column:span 8}}@media (min-width:887px){:host .page-frame-simple{grid-column:span 12}}:host .page-frame-simple .actions{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .page-frame-simple .actions button:not(#more){display:none}@media (min-width:887px){:host .page-frame-simple .actions button:not(#more){display:inline-block}}:host .page-frame-simple .actions #showMore{display:inline-block}@media (min-width:887px){:host .page-frame-simple .actions #showMore{display:none}}:host .page-frame-simple .actions #showMore button{margin:0;min-width:auto;padding:6px;border:0}:host .page-frame-simple .actions #showMore button svg{margin:0}:host .page-frame-simple .actions .gr-dropdown-container .menu{right:0;max-height:auto;overflow:hidden;width:auto;top:16px;border:1px solid #c7c7c7;height:auto}:host .page-frame-simple .actions .dropdown-menu{width:auto;right:0}:host .page-frame-simple .actions .dropdown-menu button{display:-webkit-inline-box!important;display:-webkit-inline-flex!important;display:-ms-inline-flexbox!important;display:inline-flex!important;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center}:host .page-frame-simple h2{margin:0;padding-left:0}@media (min-width:887px){:host .page-frame-simple h2{padding-left:145px}}:host .gr-table-container .gr-table{border-collapse:collapse;width:100%;border-bottom:1px solid #c7c7c7}:host .gr-table-container .gr-table th{font-weight:400;padding:16px 20px;text-align:left}:host .gr-table-container .gr-table td{padding:16px 20px;text-align:left}:host .gr-table-container .gr-table thead{border-top:1px solid #c7c7c7;border-bottom:1px solid #c7c7c7}:host .gr-table-container .gr-table tbody th{font-weight:400}:host .gr-table-container.hoverable tbody tr,:host .gr-table-container.selectable tbody tr{cursor:pointer}:host .gr-table-container.hoverable tbody tr:hover,:host .gr-table-container.selectable tbody tr:hover{background-color:#e9e9e9;border-top:1px solid #047a9c;border-bottom:1px solid #047a9c}:host .gr-table-container.hoverable tbody tr.selected,:host .gr-table-container.selectable tbody tr.selected{border-top:1px solid #19a5a3;border-bottom:1px solid #19a5a3;background-color:#caefee}:host .card-container{width:100%;-webkit-box-shadow:0 1px 1px 0 rgba(0,0,0,.1);box-shadow:0 1px 1px 0 rgba(0,0,0,.1);grid-column:span 4}@media (min-width:887px){:host .card-container{grid-column:span 3}}:host .card-container.stacked{grid-column-start:1;grid-column-end:13}:host .card{border-radius:4px;-webkit-box-shadow:0 2px 4px 0 rgba(0,0,0,.16);box-shadow:0 2px 4px 0 rgba(0,0,0,.16);background:#fff;padding:16px}:host .catalog.card-container{display:grid;border-top:5px solid transparent;-webkit-transition:all .1s ease-in;transition:all .1s ease-in}:host .catalog.card-container:hover{border-top:5px solid #047a9c}:host .catalog.card-container .gr-checkbox{margin:0}:host .catalog.card-container .info{display:grid;-webkit-box-align:center;-webkit-align-items:center;-ms-flex-align:center;align-items:center;grid-template-rows:repeat(4,1fr);grid-gap:16px}@media (min-width:887px){:host .catalog.card-container .info{grid-template-rows:1fr}}@media (-ms-high-contrast:active),(-ms-high-contrast:none){:host .catalog.card-container .info{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;-webkit-flex-wrap:wrap;-ms-flex-wrap:wrap;flex-wrap:wrap}}:host .catalog.card-container .actions{grid-row:1;justify-self:flex-end;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex}:host .catalog.card-container .actions .gr-dropdown-container button{border:0;min-width:auto}:host .catalog.card-container .actions .gr-dropdown-container button svg{padding:0;margin:0}@media (min-width:887px){:host .catalog.card-container .gr-form-element{grid-row:1;justify-self:flex-start}}@media (min-width:887px){:host .catalog.card-container .attribute-container{grid-row:1;justify-self:center}}:host .catalog .card{position:relative;border-radius:5px 5px 0 0}:host .catalog .card.results{border-top:1px solid #e7e7e7;border-radius:0 0 5px 5px;position:relative;-ms-grid-row:2}:host .gr-grid-container{margin:0}:host .gr-grid-container>*{grid-column:span 4;-ms-grid-column-span:4}@media (min-width:591px){:host .gr-grid-container>*{grid-column:span 8;-ms-grid-column-span:8}}@media (min-width:887px){:host .gr-grid-container>*{grid-column:span 12;-ms-grid-column-span:12}}:host *{font-family:Open Sans,Arial,Helvetica,sans-serif}\n\n</style>\n<div class="gr-grid-container">\n    <div class="catalog card-container">\n        <div class="card">\n            <div class="info">\n                <div class="gr-form-element">\n                  <div class="gr-checkbox">\n                      <input type="checkbox" id="idOne">\n                      <label for="idOne">Add a checkbox label</label>\n                      <span>\n                        <svg focusable="false" class="icon-24" aria-hidden="true">\n                            <path d="M4.5,1 L19.5,1 C21.4329966,1 23,2.56700338 23,4.5 L23,19.5 C23,21.4329966 21.4329966,23 19.5,23 L4.5,23 C2.56700338,23 1,21.4329966 1,19.5 L1,4.5 C1,2.56700338 2.56700338,1 4.5,1 Z M9.48055302,17.9017747 C9.913256,18.1093819 10.4463555,17.981452 10.7309467,17.5789431 L17.8197099,7.55301635 C18.135437,7.10647067 18.0257453,6.49107611 17.5747066,6.17849414 C17.1236679,5.86591216 16.5020814,5.9745112 16.1863543,6.42105688 L9.80033032,15.4530709 L7.72281473,13.2676998 C7.3454783,12.8707737 6.71457695,12.851846 6.31365712,13.2254236 C5.91273728,13.5990012 5.89361915,14.2236179 6.27095558,14.620544 L9.17399136,17.6742927 C9.18395884,17.6852092 9.19420598,17.6959379 9.20473089,17.7064685 C9.32298853,17.8285251 9.40711664,17.8887858 9.48055302,17.9017747 Z"></path>\n                        </svg>\n                     </span>\n                  </div>\n                </div>\n                <div class="attribute-container">\n                    <span class="gr-meta" id="titleOne">Label</span>\n                    <span id="contentOne">Content</span>\n                </div>\n                <div class="attribute-container">\n                    <span class="gr-meta" id="titleTwo">Label</span>\n                    <span id="contentTwo">Content</span>\n                </div>\n                <div class="actions">\n                    <slot name="dropdown"></slot>\n                    <button id="expandResults" class="gr-btn icon-btn-24">\n                        <pearson-icon icon="expand" size="18"/>\n                    </button>\n                </div>\n            </div>\n            <div class="content">\n                <slot name="cardcontent"></slot>\n            </div>\n        </div>\n        <div id="resultCard" class="card results animate hidden">\n                <slot name="results"></slot>\n        </div>\n    </div>\n</div>\n\n';

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-search-card');

  var SearchCard = function (_HTMLElement) {
    _inherits(SearchCard, _HTMLElement);

    _createClass(SearchCard, [{
      key: 'contentTwo',
      get: function get() {
        return this.getAttribute('contentTwo');
      },
      set: function set(value) {
        this.setAttribute('contentTwo', value);
      }
    }, {
      key: 'titleTwo',
      get: function get() {
        return this.getAttribute('titleTwo');
      },
      set: function set(value) {
        this.setAttribute('titleTwo', value);
      }
    }, {
      key: 'contentOne',
      get: function get() {
        return this.getAttribute('contentone');
      },
      set: function set(value) {
        this.setAttribute('contentone', value);
      }
    }, {
      key: 'titleOne',
      get: function get() {
        return this.getAttribute('titleone');
      },
      set: function set(value) {
        this.setAttribute('titleone', value);
      }
    }, {
      key: 'checked',
      get: function get() {
        return this.getAttribute('checked');
      },
      set: function set(bool) {
        this.setAttribute('checked', bool);
      }
    }, {
      key: 'checkBoxLabel',
      get: function get() {
        return this.getAttribute('checkboxlabel');
      },
      set: function set(value) {
        this.setAttribute('checkboxlabel', value);
      }
    }, {
      key: 'checkBox',
      get: function get() {
        return this.getAttribute('checkbox');
      },
      set: function set(bool) {
        this.setAttribute('checkbox', bool);
      }
    }], [{
      key: 'observedAttributes',
      get: function get() {
        return ['checkbox', 'checkboxlabel', 'checked', 'titleone', 'contentone', 'titletwo', 'contenttwo'];
      }
    }]);

    function SearchCard() {
      _classCallCheck(this, SearchCard);

      var _this = _possibleConstructorReturn(this, (SearchCard.__proto__ || Object.getPrototypeOf(SearchCard)).call(this));

      _this.attachShadow({ mode: 'open' });
      var clone = template.content.cloneNode(true);
      _this.grFormElement = clone.querySelector('.gr-form-element');
      _this.checkBoxLabelNode = clone.querySelector('.gr-form-element label');
      _this.checkBoxNode = clone.querySelector('input');
      _this.titleOneNode = clone.querySelector('#titleOne');
      _this.contentOneNode = clone.querySelector('#contentOne');
      _this.titleTwoNode = clone.querySelector('#titleTwo');
      _this.contentTwoNode = clone.querySelector('#contentTwo');
      _this.expandAccordionButton = clone.querySelector('#expandResults');
      _this.resultsCard = clone.querySelector('#resultCard');
      _this.shadowRoot.appendChild(clone);
      return _this;
    }

    _createClass(SearchCard, [{
      key: 'updateCheckedAttributes',
      value: function updateCheckedAttributes(node) {
        if (node === null) {
          node = false;
        } else {
          node = true;
        }
      }
    }, {
      key: 'connectedCallback',
      value: function connectedCallback() {
        var _this2 = this;

        var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
        if (this.checkBoxLabel !== null) {
          this.checkBoxLabelNode.innerHTML = this.checkBoxLabel;
        }

        if (this.contentTwo !== null) {
          this.contentTwoNode.innerHTML = this.contentTwo;
        }

        if (this.titleTwo !== null) {
          this.titleTwoNode.innerHTML = this.titleTwo;
          this.titleTwoNode.parentNode.classList.remove('hidden');
        } else {
          this.titleTwoNode.parentNode.classList.add('hidden');
        }

        if (this.contentOne !== null) {
          this.contentOneNode.innerHTML = this.contentOne;
        }

        if (this.titleOne !== null) {
          this.titleOneNode.innerHTML = this.titleOne;
          this.titleOneNode.parentNode.classList.remove('hidden');
        } else {
          this.titleOneNode.parentNode.classList.add('hidden');
        }

        this.updateCheckedAttributes(this.checkBox);
        this.updateCheckedAttributes(this.checked);

        this.checkBoxNode.addEventListener('change', function (event) {
          if (_this2.checkBoxNode.checked === true) {
            _this2.checked = true;
          } else if (_this2.checkBoxNode.checked === false) {
            _this2.checked = false;
          }
        });
        this.expandAccordionButton.addEventListener('click', function (event) {
          var iconEl = _this2.expandAccordionButton.querySelector('pearson-icon');
          if (_this2.resultsCard.classList.contains('hidden')) {
            if (!isIE11) {
              iconEl.icon = 'collapse';
            }
            _this2.resultsCard.classList.remove('hidden');
            _this2.resultsCard.classList.add('fadeIn');
          } else {

            if (!isIE11) {
              iconEl.icon = 'expand';
            }
            _this2.resultsCard.classList.add('hidden');
            _this2.resultsCard.classList.remove('fadeIn');
          }
        });
      }
    }, {
      key: 'attributeChangedCallback',
      value: function attributeChangedCallback(name, prevValue, newValue) {
        if (name === 'checkbox') {
          if (prevValue !== newValue) {
            if (this.checkBox === 'false') {
              this.grFormElement.classList.add('hidden');
            } else if (this.checkBox === 'true') {
              this.grFormElement.classList.remove('hidden');
            }
          }
        }
        if (name === 'checkboxlabel') {
          if (prevValue !== newValue) {
            this.checkBoxLabelNode.innerHTML = this.checkBoxLabel;
          }
        }
        if (name === 'checked') {
          if (prevValue !== newValue) {
            if (this.checked === 'true') {
              this.checkBoxNode.checked = true;
            } else if (this.checked === 'false') {
              this.checkBoxNode.checked = false;
            }
          }
        }
      }
    }]);

    return SearchCard;
  }(HTMLElement);

  customElements.define('pearson-search-card', SearchCard);
})(window, document);