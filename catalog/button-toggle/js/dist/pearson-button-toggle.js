var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

(function (w, doc) {
  'use strict';
  // Create a template element

  var template = doc.createElement('template'),
      customStyleTemplate = doc.createElement('template'),
      svgTemplate = doc.createElement('template');

  template.innerHTML = '\n  <style>\n       :host{display:block;font-family:14px,18px,Open Sans,Calibri,Tahoma,sans-serif;background-color:#fff;color:#6a7070;-webkit-box-sizing:border-box;box-sizing:border-box}:host label{display:block;width:100%;margin:0 0 8px}:host .container{display:inline-block;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;width:100%}:host .container button{height:38px;min-width:132px;border:none;background-color:#e9e9e9;color:#6a7070;font-size:16px;line-height:20px;margin:0;-webkit-box-flex:2;-webkit-flex-grow:2;-ms-flex-positive:2;flex-grow:2;z-index:1;cursor:pointer}:host .container button:not(:first-child):not(:last-child){border-right:1px solid #6a7070}:host .container button:first-of-type{border-radius:36px 0 0 36px;border-right:1px solid #6a7070}:host .container button:last-of-type{border-radius:0 36px 36px 0}:host .container button:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-select-container{position:relative}:host .gr-select-container select{display:block;margin:4px 0;padding:0 42px 0 12px;height:36px;color:#252525;border:1px solid #c7c7c7;background:#fff;border-radius:4px;font-size:14px;width:100%;max-width:100%;-webkit-appearance:none;-moz-appearance:none;appearance:none}:host .gr-select-container select:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-select-container select::-ms-expand{display:none}:host .gr-select-container svg{position:absolute;right:12px;top:-webkit-calc(50% - 9px);top:calc(50% - 9px);fill:#6a7070;width:18px;height:18px}:host(.theme--dark) .tab-button:hover{color:#fff;border-bottom-color:#d9d9d9}:host(.theme--dark) .tab-button.active{color:#fff}:host(.theme--dark){color:#d9d9d9}:host(.theme--dark) .tabs-wrapper{background-color:#005a70}\n  </style>\n  <div>\n    <label class="gr-meta"></label>\n    <div class="container"></div>\n  </div>\n';

  customStyleTemplate.innerHTML = '<style>h1{color:red;}</style>';

  svgTemplate.innerHTML = '\n    <svg focusable="false" aria-hidden="true">\n        <path d="M5.99434194,7 L12.0056581,7 C12.554818,7 13,7.44826851 13,8.00123574 C13,8.26844875 12.8939222,8.52457525 12.7053749,8.7126126 L9.69971675,11.7101411 C9.31219009,12.0966196 8.68780998,12.0966196 8.30028332,11.7101411 L5.29462522,8.7126126 C4.90444777,8.32349048 4.90142029,7.68955041 5.28786316,7.29666781 C5.47460581,7.10681327 5.72896878,7 5.99434194,7 Z"></path>\n    </svg>\n  ';

  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-button-toggle');
  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(customStyleTemplate, 'pearson-button-toggle');

  var ButtonToggle = function (_HTMLElement) {
    _inherits(ButtonToggle, _HTMLElement);

    _createClass(ButtonToggle, [{
      key: 'labelText',
      get: function get() {
        return this.getAttribute('label');
      }
    }, {
      key: 'stage',
      get: function get() {
        return this.getAttribute('stage');
      },
      set: function set(number) {
        return this.setAttribute('stage', number);
      }
    }, {
      key: 'collapsed',
      get: function get() {
        return this.hasAttribute('collapsed');
      }
    }, {
      key: 'selectedcolor',
      get: function get() {
        return this.getAttribute('selectedcolor');
      }
    }, {
      key: 'selectedtextcolor',
      get: function get() {
        return this.getAttribute('selectedtextcolor');
      }
    }, {
      key: 'backgroundcolor',
      get: function get() {
        return this.getAttribute('backgroundcolor');
      }
    }, {
      key: 'textcolor',
      get: function get() {
        return this.getAttribute('textcolor');
      }
    }], [{
      key: 'observedAttributes',
      get: function get() {
        return ['label', 'stage', 'collapsed', 'selectedcolor', 'selectedtextcolor', 'backgroundcolor', 'textcolor'];
      }
    }]);

    function ButtonToggle() {
      _classCallCheck(this, ButtonToggle);

      var _this = _possibleConstructorReturn(this, (ButtonToggle.__proto__ || Object.getPrototypeOf(ButtonToggle)).call(this));

      _this.attachShadow({ mode: 'open' });
      var clone = template.content.cloneNode(true);
      _this.label = clone.querySelector('label');
      _this.container = clone.querySelector('.container');

      var customStyleClone = customStyleTemplate.content.cloneNode(true);
      if (w.ShadyCSS) {
        _this.customStyle = document.querySelectorAll('style')[0];
      } else {
        _this.customStyle = customStyleClone.querySelector('style');
      }

      _this.shadowRoot.appendChild(clone);
      _this.shadowRoot.appendChild(customStyleClone);

      _this.buildToggles = _this.buildToggles.bind(_this);
      _this.setSelected = _this.setSelected.bind(_this);
      return _this;
    }

    _createClass(ButtonToggle, [{
      key: 'connectedCallback',
      value: function connectedCallback() {
        setTimeout(this.buildToggles, 500);
      }
    }, {
      key: 'attributeChangedCallback',
      value: function attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'label') {
          if (oldValue !== newValue) {
            this.label.innerHTML = this.labelText;
          }
        }

        if (name === 'stage') {
          if (oldValue !== newValue) {
            if (this.buttonsArray) {
              this.setSelected(this.buttonsArray[this.stage - 1]);
            }
          }
        }

        if (name === 'collapsed') {
          if (oldValue !== newValue) {
            setTimeout(this.buildToggles, 100);
          }
        }

        if (name === 'selectedcolor') {
          if (oldValue !== newValue) {
            this.setCustomStyle();
          }
        }

        if (name === 'selectedtextcolor') {
          if (oldValue !== newValue) {
            this.setCustomStyle();
          }
        }

        if (name === 'backgroundcolor') {
          if (oldValue !== newValue) {
            this.setCustomStyle();
          }
        }

        if (name === 'textcolor') {
          if (oldValue !== newValue) {
            this.setCustomStyle();
          }
        }
      }
    }, {
      key: 'buildToggles',
      value: function buildToggles() {
        var _this2 = this;

        //reset
        this.container.innerHTML = '';
        this.buttonsArray = [];
        //build source
        this.toggles = this.querySelectorAll('button');
        //build select
        if (this.collapsed === true) {
          var select = document.createElement('select');
          this.toggles.forEach(function (toggle) {
            var toggleEl = document.createElement('option');
            toggleEl.classList.add('btn-toggle');
            toggleEl.innerText = toggle.innerText;
            toggleEl.setAttribute('value', toggle.innerText);
            select.appendChild(toggleEl);
            _this2.buttonsArray.push(toggleEl);
          });
          select.addEventListener('change', function (event) {
            var selectedOption = _this2.shadowRoot.querySelector('select option[value="' + event.target.value + '"]');
            _this2.stage = _this2.buttonsArray.indexOf(selectedOption) + 1;
            _this2.value = selectedOption.value;
            _this2.setSelected(selectedOption);
            var change = new CustomEvent('change');
            _this2.dispatchEvent(change);
          }, false);
          //set stage
          if (this.stage != null) {
            this.value = this.buttonsArray[this.stage - 1].innerText;
            this.setSelected(this.buttonsArray[this.stage - 1]);
          }
          this.container.setAttribute('class', 'gr-select-container');
          this.container.append(select, svgTemplate.content.cloneNode(true));
          //build button toggle
        } else {
          this.toggles.forEach(function (toggle) {
            var toggleEl = document.createElement('button');
            toggleEl.classList.add('btn-toggle');
            toggleEl.innerText = toggle.innerText;
            toggleEl.classList.add('custom-style');
            toggleEl.addEventListener('click', function (event) {
              _this2.stage = _this2.buttonsArray.indexOf(event.target) + 1;
              _this2.value = event.target.innerText;
              _this2.setSelected(event.target);
            }, false);
            _this2.container.setAttribute('class', 'container');
            _this2.container.appendChild(toggleEl);
            _this2.buttonsArray.push(toggleEl);
          });
          //set stage
          if (this.stage != null) {
            this.value = this.buttonsArray[this.stage - 1].innerText;
            this.setSelected(this.buttonsArray[this.stage - 1]);
          }
        }
        //set styles
        // var style = document.createElement('style');
        // style.type='text/css';
        // this.shadowRoot.appendChild(style);
        this.setCustomStyle();
      }
    }, {
      key: 'setSelected',
      value: function setSelected(selectedBtn) {
        this.container.querySelectorAll('.btn-toggle').forEach(function (child) {
          child.classList.remove('selected');
          child.removeAttribute('selected');
        });
        selectedBtn.classList.add('selected');
        selectedBtn.setAttribute('selected', '');
      }
    }, {
      key: 'setCustomStyle',
      value: function setCustomStyle() {
        this.customStyle.innerText = '';
        if (w.ShadyCSS) {
          this.customStyle.innerText = 'pearson-button-toggle .container.pearson-button-toggle button.custom-style.pearson-button-toggle{color:' + this.textcolor + ';background-color:' + this.backgroundcolor + '}pearson-button-toggle .container.pearson-button-toggle button.selected.pearson-button-toggle{z-index:2;color:' + (this.selectedtextcolor || 'white') + ';background-color:' + (this.selectedcolor || '#047A9C') + ';}';
        } else {
          this.customStyle.innerText = ':host .container button.custom-style{color:' + this.textcolor + ';background-color:' + this.backgroundcolor + '}:host .container button.selected{z-index:2;color:' + (this.selectedtextcolor || 'white') + ';background-color:' + (this.selectedcolor || '#047A9C') + ';}';
        }
      }
    }]);

    return ButtonToggle;
  }(HTMLElement);

  customElements.define('pearson-button-toggle', ButtonToggle);
})(window, document);