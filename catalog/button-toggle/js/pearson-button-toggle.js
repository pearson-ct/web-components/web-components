(function(w, doc) {
  'use strict';
  // Create a template element
  const template = doc.createElement('template'),
        customStyleTemplate = doc.createElement('template'),
        svgTemplate = doc.createElement('template');

  template.innerHTML = `
  <style>
       :host{display:block;font-family:14px,18px,Open Sans,Calibri,Tahoma,sans-serif;background-color:#fff;color:#6a7070;-webkit-box-sizing:border-box;box-sizing:border-box}:host label{display:block;width:100%;margin:0 0 8px}:host .container{display:inline-block;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;width:100%}:host .container button{height:38px;min-width:132px;border:none;background-color:#e9e9e9;color:#6a7070;font-size:16px;line-height:20px;margin:0;-webkit-box-flex:2;-webkit-flex-grow:2;-ms-flex-positive:2;flex-grow:2;z-index:1;cursor:pointer}:host .container button:not(:first-child):not(:last-child){border-right:1px solid #6a7070}:host .container button:first-of-type{border-radius:36px 0 0 36px;border-right:1px solid #6a7070}:host .container button:last-of-type{border-radius:0 36px 36px 0}:host .container button:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-select-container{position:relative}:host .gr-select-container select{display:block;margin:4px 0;padding:0 42px 0 12px;height:36px;color:#252525;border:1px solid #c7c7c7;background:#fff;border-radius:4px;font-size:14px;width:100%;max-width:100%;-webkit-appearance:none;-moz-appearance:none;appearance:none}:host .gr-select-container select:focus{outline:0;-webkit-box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4;box-shadow:0 0 0 2px #fff,0 0 0 4px #1977d4}:host .gr-select-container select::-ms-expand{display:none}:host .gr-select-container svg{position:absolute;right:12px;top:-webkit-calc(50% - 9px);top:calc(50% - 9px);fill:#6a7070;width:18px;height:18px}:host(.theme--dark) .tab-button:hover{color:#fff;border-bottom-color:#d9d9d9}:host(.theme--dark) .tab-button.active{color:#fff}:host(.theme--dark){color:#d9d9d9}:host(.theme--dark) .tabs-wrapper{background-color:#005a70}
  </style>
  <div>
    <label class="gr-meta"></label>
    <div class="container"></div>
  </div>
`;

  customStyleTemplate.innerHTML=`<style>h1{color:red;}</style>`;

  svgTemplate.innerHTML=`
    <svg focusable="false" aria-hidden="true">
        <path d="M5.99434194,7 L12.0056581,7 C12.554818,7 13,7.44826851 13,8.00123574 C13,8.26844875 12.8939222,8.52457525 12.7053749,8.7126126 L9.69971675,11.7101411 C9.31219009,12.0966196 8.68780998,12.0966196 8.30028332,11.7101411 L5.29462522,8.7126126 C4.90444777,8.32349048 4.90142029,7.68955041 5.28786316,7.29666781 C5.47460581,7.10681327 5.72896878,7 5.99434194,7 Z"></path>
    </svg>
  `;


  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(template, 'pearson-button-toggle');
  if (w.ShadyCSS) w.ShadyCSS.prepareTemplate(customStyleTemplate, 'pearson-button-toggle');


  class ButtonToggle extends HTMLElement {
    static get observedAttributes() {
      return ['label', 'stage', 'collapsed', 'selectedcolor', 'selectedtextcolor', 'backgroundcolor', 'textcolor'];
    }

    get labelText() {
      return this.getAttribute('label');
    }

    get stage() {
      return this.getAttribute('stage');
    }

    get collapsed() {
      return this.hasAttribute('collapsed');
    }

    set stage(number) {
      return this.setAttribute('stage',number);
    }

    get selectedcolor() {
      return this.getAttribute('selectedcolor');
    }

     get selectedtextcolor() {
      return this.getAttribute('selectedtextcolor');
    }

    get backgroundcolor() {
      return this.getAttribute('backgroundcolor');
    }

    get textcolor() {
      return this.getAttribute('textcolor');
    }

    constructor() {
      super();
      this.attachShadow({mode: 'open'});
      const clone = template.content.cloneNode(true);
      this.label = clone.querySelector('label');
      this.container = clone.querySelector('.container');

      const customStyleClone = customStyleTemplate.content.cloneNode(true);
      if (w.ShadyCSS){
        this.customStyle = document.querySelectorAll('style')[0];
      }else{
        this.customStyle = customStyleClone.querySelector('style');
      }

      this.shadowRoot.appendChild(clone);
      this.shadowRoot.appendChild(customStyleClone);

      this.buildToggles = this.buildToggles.bind(this);
      this.setSelected = this.setSelected.bind(this);
    }

    connectedCallback(){
      setTimeout(this.buildToggles, 500);
    }

    attributeChangedCallback(name, oldValue, newValue) {
      if (name === 'label') {
        if (oldValue !== newValue) {
          this.label.innerHTML = this.labelText
        }
      }

      if (name === 'stage') {
        if (oldValue !== newValue) {
          if(this.buttonsArray){this.setSelected(this.buttonsArray[this.stage-1])}
        }
      }

      if (name === 'collapsed') {
        if (oldValue !== newValue) {
         setTimeout(this.buildToggles, 100);
        }
      }

      if (name === 'selectedcolor') {
        if (oldValue !== newValue) {
          this.setCustomStyle();
        }
      }

      if (name === 'selectedtextcolor') {
        if (oldValue !== newValue) {
          this.setCustomStyle();
        }
      }

      if (name === 'backgroundcolor') {
        if (oldValue !== newValue) {
          this.setCustomStyle();
        }
      }

      if (name === 'textcolor') {
        if (oldValue !== newValue) {
          this.setCustomStyle();
        }
      }

    }

    buildToggles(){
    //reset
    this.container.innerHTML = '';
    this.buttonsArray = [];
    //build source
    this.toggles = this.querySelectorAll('button');
    //build select
    if(this.collapsed===true) {
      let select = document.createElement('select')
      this.toggles.forEach((toggle) => {
          let toggleEl = document.createElement('option');
          toggleEl.classList.add('btn-toggle');
          toggleEl.innerText = toggle.innerText;
          toggleEl.setAttribute('value',toggle.innerText)
          select.appendChild(toggleEl)
          this.buttonsArray.push(toggleEl)
      });
      select.addEventListener('change', event => {
        let selectedOption = this.shadowRoot.querySelector(`select option[value="${event.target.value}"]`)
        this.stage = this.buttonsArray.indexOf(selectedOption)+1;
        this.value = selectedOption.value;
        this.setSelected(selectedOption)
        let change = new CustomEvent('change');
        this.dispatchEvent(change);
      }, false)
      //set stage
      if(this.stage != null){
        this.value = this.buttonsArray[this.stage-1].innerText
        this.setSelected(this.buttonsArray[this.stage-1])
      }
      this.container.setAttribute('class','gr-select-container');
      this.container.append(select, svgTemplate.content.cloneNode(true));
      //build button toggle
    }else {
      this.toggles.forEach((toggle) => {
          let toggleEl = document.createElement('button');
          toggleEl.classList.add('btn-toggle');
          toggleEl.innerText = toggle.innerText;
          toggleEl.classList.add('custom-style');
          toggleEl.addEventListener('click', event => {
            this.stage = this.buttonsArray.indexOf(event.target)+1;
            this.value = event.target.innerText
            this.setSelected(event.target);
          }, false)
          this.container.setAttribute('class','container')
          this.container.appendChild(toggleEl);
          this.buttonsArray.push(toggleEl)
      });
      //set stage
      if(this.stage != null){
      this.value = this.buttonsArray[this.stage-1].innerText
      this.setSelected(this.buttonsArray[this.stage-1])
      }
    }
      //set styles
      // var style = document.createElement('style');
      // style.type='text/css';
      // this.shadowRoot.appendChild(style);
      this.setCustomStyle();
    }

    setSelected(selectedBtn){
      this.container.querySelectorAll('.btn-toggle').forEach(child=>{
        child.classList.remove('selected');
        child.removeAttribute('selected')
      })
        selectedBtn.classList.add('selected')
        selectedBtn.setAttribute('selected', '')
    }

    setCustomStyle(){
        this.customStyle.innerText = '';
        if (w.ShadyCSS){
          this.customStyle.innerText = `pearson-button-toggle .container.pearson-button-toggle button.custom-style.pearson-button-toggle{color:${this.textcolor};background-color:${this.backgroundcolor}}pearson-button-toggle .container.pearson-button-toggle button.selected.pearson-button-toggle{z-index:2;color:${this.selectedtextcolor || 'white'};background-color:${this.selectedcolor || '#047A9C'};}`
        }else {
          this.customStyle.innerText = `:host .container button.custom-style{color:${this.textcolor};background-color:${this.backgroundcolor}}:host .container button.selected{z-index:2;color:${this.selectedtextcolor || 'white'};background-color:${this.selectedcolor || '#047A9C'};}`
        }
    }

  }
  customElements.define('pearson-button-toggle', ButtonToggle)
})(window, document);
